var data = [

    @foreach($data as  $key=>$value)
    ['{{$key}}','{{ $value}}'],
    @endforeach
];
var data2 = [...data];

// chart create
$(document).ready(function () {
  var generate_tooltips = function () {
     //var major_minerals = this.point.NoneFuelMinerals.split(', ');
     //console.log(major_minerals);
     //var text ="";
     //for(i = 0; i < major_minerals.length; i++ ){
      //text += "<br/>" + major_minerals[i] ;
     //}
    var dep = this.point.value;
    var eiti = this.point.eiti_2016;
    var nyc = this.point.nyc;
    var icsid = this.point.icsid;
    var ohada = this.point.ohada;
    var name = this.point.name;
   // alert(this.point.trans);
      var mintype=" ";
      // switch (eiti) {
      //   // case 6:
      //   // case 5:
      //   // case 4:
      //   //   color = '<tr><td>EITI: </td>' +
      //   //   '<td><b>Suspended</b></td></tr>';
      //   //   break;
      //   // case 3:
      //   //   color = '<tr><td>EITI: </td>' +
      //   //   '<td><b>Meaningful progress</b></td></tr>';
      //   //   break;
      //   // case 2:
      //   //   color = '<tr><td>EITI: </td>' +
      //   //   '<td><b>Satisfactory progress</b></td></tr>';
      //   //   break;
      //   // case 1:
      //   //   color = '<tr><td>EITI: </td>' +
      //   //   '<td><b>Yet to be assessed against 2016 Standard</b></td></tr>';
      //   //   break;
      //   // default:
      //   //   color = '<tr><td>EITI: </td>' +
      //   //   '<td><b>No Status</b></td></tr>';
      //   //   break;
      
      // }
      console.log(data);

    if (dep == 2) {
      depend = '<tr><td >Arbitration Institution: </td>' +
      '<td> <b>Not Present</b></td></tr>';
    }else{
      depend = '<tr><td >Arbitration Institution: </td>' +
      '<td> <b>Present</b> </td></tr>';
    };    

    return '<div>' + '<span>' + name  +'</span>' +'<br/>' +
      depend + '<br/> ' + ' ' +'</div>';
     
     
     
     
  };

    var maps = Highcharts.mapChart('map', {
        chart: {
            map: 'custom/africa',
             //width: 1000,
             //height: 700,
            backgroundColor: "rgba(0,0,0,0)",
            events: {
                load: function() {
                    console.log('loaded')
                }
            }
        },
        // responsive:{
        //   rules: 
        //     maxWidth:500
          
        // },

        title: {
            text: ''
        },

        subtitle: {
            text: ''
        },

        mapNavigation: {
            enabled: false,
            buttonOptions: {
                verticalAlign: 'bottom'
            }
        },

        colorAxis: {
            min: 0
        },
        tooltip: {
          formatter: generate_tooltips,
          backgroundColor: '#333333',
          borderColor:'#333333',
          style: {
            color: '#ffffff',
          }
        },


        series: [{
            data: data.map(elem => {
                elem.push('white');
                //console.log(elem);
                return elem;
            }),
            keys: ['hc-key', 'value', 'color'],
            borderWidth: '1px',
            borderColor: '#000000',
            name: '',
            states: {
                hover: {
                    color: '#CEA01A'
                }
            },
            dataLabels: {
                enabled: false,
                format: '{point.name}'
            }
        }],

        plotOptions: {
            series: {
                point: {
                    events: {
                        click: function () {
                            location.href = '{{ url("/country") }}'+'/' + this.name;
                        }
                    }
                }
            }
        },
        
    });

     $('#nyc').on('mouseenter',function (e) {

         $.ajax({
                 url: '{{url("/compliance/0") }}',
                 type: 'GET',
                 success: function(res)
                 {
                    var list = res;

                     var localdata = JSON.parse( JSON.stringify( data ) );
                      $.each(localdata,function (key,value) {
                          if (list.includes(value[0]))
                          {
                              value[2] = '#0EA616';
                          }
                      });
                      //alert(data);
                      maps.series[0].setData(localdata);
                 }
             });

    });


     $('#icsid').on('mouseenter',function (e) {

         $.ajax({
                 url: '{{url("/compliance/1") }}',
                 type: 'GET',
                 success: function(res)
                 {
                    var list = res;

                     var localdata = JSON.parse( JSON.stringify( data ) );
                      $.each(localdata,function (key,value) {
                          if (list.includes(value[0]))
                          {
                              value[2] = '#DD0302';
                          }
                      });
                      //alert(data);
                      maps.series[0].setData(localdata);
                 }
             });

    });

     $('.cardpill').on('mouseout',function (e) {
        $.each(data2,function (key,value) {
            value[2] = 'white';
        });
        maps.series[0].setData(data2);
    });

     $('#ohada').on('mouseenter',function (e) {

         $.ajax({
                 url: '{{url("/compliance/2") }}',
                 type: 'GET',
                 success: function(res)
                 {
                    var list = res;

                     var localdata = JSON.parse( JSON.stringify( data ) );
                      $.each(localdata,function (key,value) {
                          if (list.includes(value[0]))
                          {
                              value[2] = '#333333';
                          }
                      });
                      //alert(data);
                      maps.series[0].setData(localdata);
                 }
             });

    });

     $('#present').on('mouseenter',function (e) {

         $.ajax({
                 url: '{{url("/present/3") }}',
                 type: 'GET',
                 success: function(res)
                 {
                    var list = res;

                     var localdata = JSON.parse( JSON.stringify( data ) );

                      $.each(localdata,function (key,value) {

                          if (list.includes(value[0]))
                          {
                              value[2] = '#28D4CA';
                          }
                      });

                      maps.series[0].setData(localdata);
                 }
             });

    });

       $('#notpresent').on('mouseenter',function (e) {

         $.ajax({
                 url: '{{url("/present/2") }}',
                 type: 'GET',
                 success: function(res)
                 {
                    var list = res;

                     var localdata = JSON.parse( JSON.stringify( data ) );
                      $.each(localdata,function (key,value) {
                          if (list.includes(value[0]))
                          {
                              value[2] = '#D7DD3C';
                          }
                      });
                      // alert(data);
                      maps.series[0].setData(localdata);
                 }
             });

    });

$('#yes').on('mouseenter',function (e) {

         $.ajax({
                 url: '{{url("/uncitral/2") }}',
                 type: 'GET',
                 success: function(res)
                 {
                    var list = res;

                     var localdata = JSON.parse( JSON.stringify( data ) );
                      $.each(localdata,function (key,value) {
                          if (list.includes(value[0]))
                          {
                              value[2] = '#6933E9';
                          }
                      });
                      //alert(data);
                      maps.series[0].setData(localdata);
                 }
             });

    });
$('#no').on('mouseenter',function (e) {

         $.ajax({
                 url: '{{url("/uncitral/3") }}',
                 type: 'GET',
                 success: function(res)
                 {
                    var list = res;

                     var localdata = JSON.parse( JSON.stringify( data ) );
                      $.each(localdata,function (key,value) {
                          if (list.includes(value[0]))
                          {
                              value[2] = '#32ABDF';
                          }
                      });
                      //alert(data);
                      maps.series[0].setData(localdata);
                 }
             });

    });
});

function requestData() {
$.ajax({
url: 'api/getdata',
type: "GET",
dataType: "json",
success: function(data) {
    console.log(data);
},
cache: false
});
}