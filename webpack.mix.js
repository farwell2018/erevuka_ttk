const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');


mix.js(['resources/js/admin/admin.js'], 'public/js')
    .sass('resources/sass/admin/admin.scss', 'public/css');

mix.js(['public/emojione/emojionearea.js'], 'public/js')
    .postCss('public/emojione/emojionearea.css', 'public/css');

mix.sass('resources/sass/reports/reports.scss', 'public/css');

mix.copyDirectory('node_modules/emojionearea/dist', 'public/node_modules/emojionearea/dist');
// mix.copyDirectory('node_modules/emojionearea/img', 'public/node_modules/emojionearea/img');
mix.copyDirectory('node_modules/emojionearea/scss', 'public/node_modules/emojionearea/scss');
mix.copyDirectory('node_modules/emojionearea/src', 'public/node_modules/emojionearea/src');
mix.copyDirectory('node_modules/emojionearea/tasks', 'public/node_modules/emojionearea/tasks');
