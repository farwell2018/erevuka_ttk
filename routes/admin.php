<?php

use App\Http\Controllers\Admin\CouponController;
use Illuminate\Support\Facades\Route;

// Register Twill routes here eg.
// Route::module('posts');

if (config('twill.enabled.users-management')) {
    Route::module('users', ['except' => ['sort', 'feature']]);
    Route::get('/upload', 'UploadUserController@usersUpload')->name('usersUpload');
    Route::post('/upload','UploadUserController@uploadStore')->name('upload.store');

}

Route::group(['prefix' => 'content'], function(){
    Route::module('pages');
    Route::module('events');
    Route::module('testimonials');
    Route::module('clients');
    Route::group(['prefix' => 'menuses'], function () {
		Route::module('menuses');
        Route::module('menuTypes');
    });
    Route::module('menuses');
    Route::module('socialmedia');
    Route::module('projecttopics');
    Route::module('footerTexts');

});

Route::module('contentBuckets');
Route::module('featureWeeks');
Route::module('roles');
//  Route::module('coupons');
Route::module('rates');


Route::group(['prefix' => 'groupPurchase'], function () {
    Route::name('group.purchase')->any('groupPurchase','UserController@groupPurchase');
    Route::name('user.group.purchase')->any('usergroupPurchase/{id}','UserController@userGroupPurchase');

});

Route::group(['prefix' => 'promoCodes'], function () {
    Route::name('promo.codes')->any('promoCodes','CouponController@promoCodes');

    Route::name('user.promo.codes')->any('userPromoCodes/{id}','CouponController@userPromoCodes');

    Route::name('daycare.index')->any('daycares','CouponController@daycares');
    Route::name('daycare.create')->any('daycareCreate','CouponController@daycareCreate');
    Route::name('daycare.store')->any('daycareStore','CouponController@daycareStore');
    Route::name('daycare.show')->any('daycareShow/{id}','CouponController@daycareShow');
    Route::name('daycare.update')->any('daycareUpdate/{id}','CouponController@daycareUpdate');
    Route::name('daycare.destroy')->any('daycareDestroy/{id}','CouponController@daycareDesroy');
    Route::name('daycare.upload')->any('daycareUpload','CouponController@daycareUpload');

   
  
    Route::name('daycare.codes')->any('daycareCodes','CouponController@daycareCodes');
  
    
    Route::name('daycare.promo.codes')->any('daycarePromoCodes/{id}','CouponController@daycarePromoCodes');
    Route::name('daycare.users')->any('daycareUsers/{id}','CouponController@daycareUsers');
    Route::name('daycare.promocode.show')->any('promocodeEdit/{id}','CouponController@promocodeEdit');
    Route::name('daycare.promocode.update')->any('promocodeUpdate/{id}','CouponController@promocodeUpdate');
});


Route::group(['prefix' => 'resources'], function(){

    Route::module('resources');
    Route::module('resourceTypes');
    Route::module('resourceCategories');

});

Route::group(['prefix' => 'sessions'], function(){
    Route::module('sessions');
    Route::module('sessionResources');

});

Route::group(['prefix' => 'groups'], function(){
    Route::module('groups');
    Route::module('groupTypes');
    Route::module('groupFormats');
    Route::module('groupThemes');

});

Route::group(['prefix' => 'reports'], function(){
	Route::group(['prefix' => 'graph'], function () {
        Route::name('graph.dashboard')->get('dashboard','CourseCompletionController@dashboard');

        
        Route::name('graph.yearlyEResults')->get('yearlyEResults','CourseCompletionController@yearlyEResults');
        Route::name('graph.yearlyCRresults')->get('yearlyCRresults','CourseCompletionController@yearlyCRresults');


        Route::name('graph.yearlyQuickTipEResults')->get('yearlyQuickTipEResults','CourseCompletionController@yearlyQuickTipEResults');
        Route::name('graph.yearlyQuickTipCRresults')->get('yearlyQuickTipCRresults','CourseCompletionController@yearlyQuickTipCRresults');
    });

Route::module('courseCompletions');
});


Route::group(['prefix' => 'evaluation'], function(){
    Route::module('userEvaluations');
    Route::module('evaluationOptions');
    Route::module('evaluations');
});

Route::get('/coupons',[CouponController::class,'getIndex'])->name('coupons.index');
Route::get('/daycare/coupons',[CouponController::class,'getDaycareIndex'])->name('coupons.daycare.index');
Route::get('/coupons/store',[CouponController::class,'storeCoupon'])->name('coupons.store');
Route::any('/daycare/coupons/store',[CouponController::class,'storeDaycareCoupon'])->name('daycare.coupons.store');
 Route::get('/courses', 'CoursesController@index')->name('courses.index');
 Route::get('/courses/create', 'CoursesController@create')->name('courses.create');
 Route::post('/courses','CoursesController@store')->name('courses.store');
 Route::get('/courses/{course}/edit', 'CoursesController@edit')->name('courses.edit');
 Route::post('/courses/bulk-destroy','CoursesController@bulkDestroy')->name('courses.bulk-destroy');
 Route::post('/courses/{course}','CoursesController@update')->name('courses.update');
 Route::delete('/courses/{course}', 'CoursesController@destroy')->name('courses.destroy');


 Route::get('/user-licenses','UserLicensesController@index')->name('user-licenses.index');
 Route::get('/user-licenses/create','UserLicensesController@create')->name('user-licenses.create');
 Route::post('/user-licenses','UserLicensesController@store')->name('user-licenses.store');
 Route::get('/user-licenses/{userLicense}/edit','UserLicensesController@edit')->name('user-licenses.edit');
 Route::post('/user-licenses/bulk-destroy','UserLicensesController@bulkDestroy')->name('user-licenses.bulk-destroy');
 Route::post('/user-licenses/{userLicense}','UserLicensesController@update')->name('user-licenses.update');
 Route::delete('/user-licenses/{userLicense}','UserLicensesController@destroy')->name('user-licenses.destroy');
// //  Route::name('about.overview')->get('overview', 'PageController@overview');
//  Route::get('/{course}/edit', 'CourseController@edit')->where(['course' => '[a-z]+'])->name('courses.edit');

Route::get('/sessionmeetings','SessionMeetingsController@index')->name('sessionmeetings.index');
Route::get('/sessionmeetings/create','SessionMeetingsController@create')->name('sessionmeetings.create');
Route::post('/sessionmeetings','SessionMeetingsController@store')->name('sessionmeetings.store');
Route::get('/sessionmeetings/{sessionMeeting}/edit','SessionMeetingsController@edit')->name('sessionmeetings.edit');
Route::post('/sessionmeetings/bulk-destroy','SessionMeetingsController@bulkDestroy')->name('sessionmeetings.bulk-destroy');
Route::post('/sessionmeetings/{sessionMeeting}','SessionMeetingsController@update')->name('sessionmeetings.update');
Route::delete('/sessionmeetings/{sessionMeeting}','SessionMeetingsController@destroy')->name('sessionmeetings.destroy');


Route::get('/upload/download', 'UploadUserController@downloadExcel')->name('downloadExcel');
Route::post('courseCompletion/export', 'CourseCompletionController@export')->name('courseCompletion.export');