<?php

return [
    
    'enabled' => [
        'settings' => true,
        'user-description' => false,
        'users-image' => true,
        'permission' => true,
		'dashboard' => true,
        'search' => true,
        'activitylog' => true,
        'users-management'=>true,
        
    ],
    'block_editor'=>[
        'block_single_layout' => 'layouts.block',
        'repeaters' => [
            'domain' => [
                'title'=>'Domain',
                'trigger' => 'Add Domain',
                 'component' => 'a17-block-domain'
            ],
            'cta_link' => [
				'title' => 'CTA Link',
				'trigger' => 'Add a CTA Link',
				'component' => 'a17-block-cta_link',
				'max' => 2,
			],

            'resource_downloable' => [
				'title' => 'Downloable Resources',
				'trigger' => 'Add a Downloable Resource',
				'component' => 'a17-block-resource_downloable',
				'max' => 3,
			],
            'panel' => [
				'title' => 'Panel',
				'trigger' => 'Add a Panel',
				'component' => 'a17-block-panel',
				'max' => 2,
			],
              
            'evaluation_repeater' => [
				'title' => 'Evaluation',
				'trigger' => 'Add a Evaluation',
				'component' => 'a17-block-evaluation_repeater',
				'max' => 10,
			],
        ],

        'blocks' => [
            'quote' => [
                'title' => 'Quote',
                'icon' => 'text',
                'component' => 'a17-block-quote',
            ],
            'gallery' => [
                'title' => 'Gallery',
                'icon' => 'image',
                'component' => 'a17-block-gallery',
            ],
            'image_with_text' => [
                'title' => 'Image with text',
                'icon' => 'image-text',
                'component' => 'a17-block-image_with_text',
            ],
            'image_top_text' => [
                'title' => 'Image top of text',
                'icon' => 'image-text',
                'component' => 'a17-block-image_top_text',
            ],
            'paragraph' => [
                'title' => 'Paragraph',
                'icon' => 'text',
                'component' => 'a17-block-paragraph',
            ],
            'track_record' => [
                'title' => 'Track Record',
                'icon' => 'text',
                'component' => 'a17-block-track_record',
            ],
        
            'testimonial' => [
                'title' => 'Testimonials',
                'icon' => 'text',
                'component' => 'a17-block-testimonial',
            ],
            'client' => [
                'title' => 'Clients',
                'icon' => 'text',
                'component' => 'a17-block-client',
            ],
            'text_btn' => [
                'title' => 'Text With CTA',
                'icon' => 'image',
                'component' => 'a17-block-text_btn',
			],
            'text_with_title' => [
                'title' => 'Text With Title',
                'icon' => 'image',
                'component' => 'a17-block-text_with_title',
			],
            'block_text_center' => [
                'title' => 'Text With Title',
                'icon' => 'image',
                'component' => 'a17-block-text_center',
			],
            'block_text_left_image_right' => [
                'title' => 'Text With Title',
                'icon' => 'image',
                'component' => 'a17-block-text_left_image_right',
			],
            'block_text_right_image_left' => [
                'title' => 'Text With Title',
                'icon' => 'image',
                'component' => 'a17-block-text_right_image_left',
			],
            'featured_courses' => [
                'title' => 'Text With Title',
                'icon' => 'image',
                'component' => 'a17-block-featured_courses',
			],
            'session_in_panel' => [
                'title' => 'Session in Panel',
                'icon' => 'image',
                'component' => 'a17-block-session_in_panel',
			],
            'child_resources' => [
                'title' => 'Caring for your child Bucket',
                'icon' => 'image',
                'component' => 'a17-block-child_resources',
			],
            'business_resources' => [
                'title' => 'Managing my business',
                'icon' => 'image',
                'component' => 'a17-block-business_resources',
			],
            'network_resources' => [
                'title' => 'Managing my network',
                'icon' => 'image',
                'component' => 'a17-block-network_resources',
			],

            'feature_week' => [
                'title' => 'Feature of the week',
                'icon' => 'image',
                'component' => 'a17-block-feature_week',
			],
            'feature_week' => [
                'title' => 'Feature of the week',
                'icon' => 'image',
                'component' => 'a17-block-feature_week',
			],
            // 'all_courses' => [
            //     'title' => 'Feature of the week',
            //     'icon' => 'image',
            //     'component' => 'a17-block-all_courses',
			// ],
            'block_text_center' => [
                'title' => 'Feature of the week',
                'icon' => 'image',
                'component' => 'a17-block-block_text_center',
			],
            'block_text_left_image_right' => [
                'title' => 'Feature of the week',
                'icon' => 'image',
                'component' => 'a17-block-block_text_left_image_right',
			],
            'block_text_right_image_left' => [
                'title' => 'Feature of the week',
                'icon' => 'image',
                'component' => 'a17-block-block_text_right_image_left',
			],
            'featured_courses' => [
                'title' => 'Feature of the week',
                'icon' => 'image',
                'component' => 'a17-block-featured_courses',
			],

            'blog' => [
                'title' => 'blog',
                'icon' => 'image',
                'component' => 'a17-block-blog',
			],
            'modal'=> [
                'title' => 'modal',
                 'component' => 'a17-modal',
            ],
            'video_text_grid' => [
                'title' => 'Video Text Grid',
                'icon' => 'text',
                'component' => 'a17-block-video_text_grid',
			],
            'evaluation' => [
                'title' => 'Evaluation Block',
                'icon' => 'text',
                'component' => 'a17-block-evaluation',
			],
            
        ],
        'files' => ['video'],
        'crops' => [
            'cover' => [
                'default' => [
                    [
                        'name' => 'default',
                        'ratio' => 1 / 1,
                        'minValues' => [
                            'width' => 100,
                            'height' => 100,
                        ],
                    ],
                ],
            ],
            'gallery' => [
                'default' => [
                    [
                        'name' => 'default',
                        'ratio' => 16 / 9,
                        'minValues' => [
                            'width' => 1024,
                            'height' => 768,
                        ],
                    ],
                ],
            ],

            'image_block' => [
				'default' => [
					[
						'name' => 'landscape',
						'ratio' => 16 / 9,
					],
					[
						'name' => 'square',
						'ratio' => 1,
					],
					[
						'name' => 'portrait',
						'ratio' => 3 / 4,
					],
				],
			],

            'cta_text_image' => [
                'default' => [
                    [
                        'name' => 'default',
                        'ratio' => 1.5,
                    ],
                ],
			],
        ],

    ],
    'admin_middleware_group' => 'admin',
];
