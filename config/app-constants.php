<?php

return [
  /*
  Let's create the nav structure of the admin section.
  */
  'nav' => [
    ['title' => 'Home', 'route' => 'home', 'parent'=>0, 'id'=>1],
    ['title' => 'About', 'route' => 'about', 'parent'=>0, 'id'=>2],
  ],
  'auth_menu'=>[
    ['title' => 'Sessions', 'route' => 'sessions','parent'=>0, 'id'=>3],
    ['title' => 'Resources', 'route' => 'resources','parent'=>0, 'id'=>4],
    ['title'=> 'Connect', 'route'=>'connect.activity','parent'=>0,'id'=>5],
  ],

  'resources'=>[
    ['title' => 'Resources', 'route' => 'resources','parent'=>0,],
    ['title' => 'Events', 'route' => 'events','parent'=>4 , ],
    ['title' => 'Blogs', 'route' => 'blogs', 'parent'=>4, ],
  ],

  'connect'=>[
    ['title'=> 'Connect', 'route'=>'connect.activity','parent'=>5],
    ['title'=> 'Our Reach', 'route'=>'community','parent'=>5],
    // ['title'=> 'Messages', 'route'=>'message.index','parent'=>5],

  ],

  'mobile_auth_menu'=>[
    ['title' => 'Sessions', 'route' => 'sessions','parent'=>0, 'id'=>3],
    ['title' => 'Resources', 'route' => 'resources','parent'=>0, 'id'=>4],
    ['title' => 'Events', 'route' => 'events','parent'=>4 , ],
    ['title' => 'Blogs', 'route' => 'blogs', 'parent'=>4, ],
    ['title'=> 'Connect', 'route'=>'connect.activity','parent'=>0,'id'=>5],
    ['title'=> 'Our Reach', 'route'=>'community','parent'=>5],
    ['title'=> 'Messages', 'route'=>'message.index','parent'=>5],
  ],

  'connect-nav' => [
     ['title' => 'Home', 'route' => 'connect.activity','icon'=>'activity-feed-icon', 'icon-active'=>'home-community-active.svg'],
    ['title' => 'Our Reach', 'route' => 'community', 'icon'=>'our-reach-icon', 'icon-active'=>'our-reach-icon-active.svg'], 
    ['title' => 'Issue Based Collaborative Learning Networks', 'route' => 'connect.forum', 'icon'=>'discussion-icon', 'icon-active'=>'discussion-forum-active.svg'], 
    ['title' => 'Communities of Practice', 'route' => 'connect.groups', 'icon'=>'group-community-icon', 'icon-active'=>'group-community-active.svg'], 
     ['title' => 'Profile', 'route' => 'connect.my_profile','icon'=>'profile-icon', 'icon-active'=>'profile-community-active.svg'], 
  ],

  'auth' => [
    ['title' => 'Register', 'route' => 'register'],
  ],
  'profile' => [
     ['title' => 'My Enrolled Courses', 'route' => 'profile.courses'],
     ['title' => 'My Achievements', 'route' => 'profile.achievements'],
    //  ['title' => 'Training Feed', 'route' => 'profile.trainingFeed'],

    // ['title' => 'Training Groups', 'route' => 'profile.trainingGroups'],
  ],

  'support_email' => 'support@erevuka.farwell-consultants.com',
];
