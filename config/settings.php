<?php

return [
    'app' => [
        'APP_DOMAIN' => 'tinytotos.com',
        'VERIFY_SSL' => true,
    ],

    'lms' => [
        'status' => 'test',
        'live' => [
            'EDX_KEY' => 'd2af15cf46d10f6be4ce',
            'EDX_SECRET' => '6e59d9b83569bd5904c4124238aaf9fe4e430b84',
            'EDX_CONNECT' => true,
            'LMS_BASE' => 'https://courses.tinytotos.com',
            'CMS_BASE' => 'https://studio.tinytotos.com',
            'LMS_REGISTRATION_URL' => 'https://courses.tinytotos.com/user_api/v1/account/registration/',
            'LMS_LOGIN_URL' => 'https://courses.tinytotos.com/user_api/v1/account/login_session/',
            'LMS_RESET_PASSWORD_PAGE' => 'https://courses.tinytotos.com/user_api/v1/account/password_reset/',
            'LMS_RESET_PASSWORD_API_URL' => 'https://courses.tinytotos.com/user_api/v1/account/password_reset/',
            'edxWebServiceApiToken' => 'c90685ce8bc75a8e03ad35deb28a5fade4a4cc87',
            'default_token' => '133ad7f0ecd269b63637a522dbb529c50475a493'
        ],
        
    ],

    'fix_page_css' => [
        'case-study-index'
    ],

    'AfricasTalking' => [

        'test'=> [
            'AFRICASTALKING_USERNAME' => 'ttk_app',
            'AFRICASTALKING_APIKEY' =>  'c1be22951d9137e5ca275536ca9de030e94af49aaa693f95ae90d60b6907608b',
        ],

        'live' =>[
            'AFRICASTALKING_USERNAME' => 'ttk_elearning_api',
            'AFRICASTALKING_APIKEY' =>  '2717de51e0f461d314b85416c53269140d1fcb485b65e6a13de2c8bb050b352a',
            'AFRICASTALKING_FROM' => 'TINY-TOTOS',

        ]
        

    ]


];
