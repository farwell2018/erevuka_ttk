<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupForumReplyRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_forum_reply_replies', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('group_forum_id');
            $table->integer('group_forum_reply_id');
            $table->text('comment')->nullable();
            $table->json('images')->nullable();
            $table->json('gifs')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_forum_reply_replies');
    }
}
