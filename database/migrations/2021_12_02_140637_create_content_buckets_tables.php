<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContentBucketsTables extends Migration
{
    public function up()
    {
        Schema::create('content_buckets', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('content_bucket_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'content_bucket');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('content_bucket_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'content_bucket');
        });

        Schema::create('content_bucket_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'content_bucket');
        });
    }

    public function down()
    {
        Schema::dropIfExists('content_bucket_revisions');
        Schema::dropIfExists('content_bucket_translations');
        Schema::dropIfExists('content_bucket_slugs');
        Schema::dropIfExists('content_buckets');
    }
}
