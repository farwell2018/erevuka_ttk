<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSuperpowerTwillusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $twillUsersTable = config('twill.users_table', 'twill_users');

        Schema::table($twillUsersTable, function (Blueprint $table) {
            $table->text('super_power')->nullable();
            
            
		
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        $twillUsersTable = config('twill.users_table', 'twill_users');
        Schema::table($twillUsersTable, function (Blueprint $table) {
            $table->dropColumn('super_power');
           
        });
    }
}
