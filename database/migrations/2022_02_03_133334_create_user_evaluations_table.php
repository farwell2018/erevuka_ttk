<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_evaluations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->string('course_id');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('course_id')->references('course_id')->on('courses')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->string('question1');
            $table->string('answer1');
            $table->string('question2');
            $table->string('answer2');
            $table->string('question3');
            $table->string('answer3');
            $table->string('question4');
            $table->string('answer4');
            $table->string('question5');
            $table->string('answer5');
            $table->string('question6');
            $table->string('answer6');
            $table->string('question7');
            $table->string('answer7');
            $table->string('question8')->nullable();
            $table->string('answer8')->nullable();
            $table->string('question9')->nullable();
            $table->string('answer9')->nullable();
            $table->string('question10')->nullable();
            $table->string('answer10')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_evaluations');
    }
}
