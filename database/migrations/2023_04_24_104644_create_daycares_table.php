<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDaycaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daycares', function (Blueprint $table) {
            $table->id();
            $table->string('daycare_id');
            $table->string('name');
            $table->string('contact_name')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('location')->nullable();
            $table->unsignedInteger('cohort')->nullable();
            $table->string('joining_date');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daycares');
    }
}
