<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSponsorsTables extends Migration
{
    public function up()
    {
        Schema::create('sponsors', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('sponsor_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'sponsor');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('sponsor_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'sponsor');
        });

        Schema::create('sponsor_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'sponsor');
        });
    }

    public function down()
    {
        Schema::dropIfExists('sponsor_revisions');
        Schema::dropIfExists('sponsor_translations');
        Schema::dropIfExists('sponsor_slugs');
        Schema::dropIfExists('sponsors');
    }
}
