<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesBucketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_bucket_role', function (Blueprint $table) {
            createDefaultRelationshipTableFields($table, 'roles', 'content_buckets');
            $table->integer('position')->unsigned()->index();
        });

        // Schema::create('roles_buckets', function (Blueprint $table) {
        //     $table->id();
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_bucket_role');
    }
}
