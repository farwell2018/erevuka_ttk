<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDaycarePromocodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daycare_promocodes', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->unsignedBigInteger('daycare_id')->nullable();
            $table->foreign('daycare_id')->references('id')->on('daycares')->onDelete('cascade');
            $table->string('code')->unique();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('twill_users')->onDelete('cascade');
            $table->dateTime('expiration_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daycare_promocodes');
    }
}
