<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTables extends Migration
{
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            $table->string('code');
            $table->unsignedBigInteger('user_id');
            $table->date('expiration_date');
   

          

            
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('coupon_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'coupon');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('coupon_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'coupon');
        });

        Schema::create('coupon_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'coupon');
        });
    }

    public function down()
    {
        Schema::dropIfExists('coupon_revisions');
        Schema::dropIfExists('coupon_translations');
        Schema::dropIfExists('coupon_slugs');
        Schema::dropIfExists('coupons');
    }
}
