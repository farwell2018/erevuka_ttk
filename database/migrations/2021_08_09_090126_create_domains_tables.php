<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDomainsTables extends Migration
{
    public function up()
    {
        Schema::create('domains', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            $table->string('name', 200)->nullable();
            $table->integer('position')->unsigned()->nullable();
            $table->integer('company_id')->unsigned()->nullable();
            
        });    
    }

    public function down()
    {
        
        Schema::dropIfExists('domains');
    }
}
