<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEvaluationOptionsTables extends Migration
{
    public function up()
    {
        Schema::table('evaluation_options', function (Blueprint $table) {
            $table->string('position')->nullable();
         
        });
        
    }

    public function down()
    {
        
        Schema::table('evaluation_options', function (Blueprint $table) {
            $table->dropColumn('position');
         
        });
    }
}
