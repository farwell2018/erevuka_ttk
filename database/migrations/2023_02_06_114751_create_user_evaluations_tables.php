<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserEvaluationsTables extends Migration
{
    public function up()
    {
        Schema::table('user_evaluations', function (Blueprint $table) {
            $table->string('position')->nullable();
            $table->softDeletes();
            $table->string('username');
        });
        

        

        
    }

    public function down()
    {
        
        Schema::table('user_evaluations', function (Blueprint $table) {
            $table->dropColumn('position');
            $table->dropSoftDeletes();
            $table->string('username');
        });
    }
}
