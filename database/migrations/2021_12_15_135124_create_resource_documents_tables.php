<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResourceDocumentsTables extends Migration
{
    public function up()
    {
        Schema::create('resource_documents', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            $table->string('document_title');
            $table->foreignId('resource_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
        
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('resource_document_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'resource_document');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('resource_document_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'resource_document');
        });

        Schema::create('resource_document_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'resource_document');
        });
    }

    public function down()
    {
        Schema::dropIfExists('resource_document_revisions');
        Schema::dropIfExists('resource_document_translations');
        Schema::dropIfExists('resource_document_slugs');
        Schema::dropIfExists('resource_documents');
    }
}
