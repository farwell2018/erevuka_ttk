<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsGroupPurchases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('group_purchases', function (Blueprint $table) {
            $table->string('status')->nullable();
            $table->timestamp('date_used')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::table('resources', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('date_used');
        });
    }
}
