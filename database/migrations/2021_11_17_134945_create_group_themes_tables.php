<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGroupThemesTables extends Migration
{
    public function up()
    {
        Schema::create('group_themes', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('group_theme_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'group_theme');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('group_theme_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'group_theme');
        });

        Schema::create('group_theme_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'group_theme');
        });
    }

    public function down()
    {
        Schema::dropIfExists('group_theme_revisions');
        Schema::dropIfExists('group_theme_translations');
        Schema::dropIfExists('group_theme_slugs');
        Schema::dropIfExists('group_themes');
    }
}
