<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCountriesTables extends Migration
{
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            $table->string('title', 200)->nullable();
            $table->string('code', 200)->nullable();
            $table->text('description')->nullable();
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('country_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'country');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('country_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'country');
        });

        Schema::create('country_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'country');
        });
    }

    public function down()
    {
        Schema::dropIfExists('country_revisions');
        Schema::dropIfExists('country_translations');
        Schema::dropIfExists('country_slugs');
        Schema::dropIfExists('countries');
    }
}
