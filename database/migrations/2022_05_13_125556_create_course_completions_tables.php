<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCourseCompletionsTables extends Migration
{
    public function up()
    {
        Schema::create('course_completions', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            
            $table->bigInteger('user_id')->unsigned();
            $table->string('course_id');
            $table->foreign('user_id')->references('id')->on('twill_users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->string('username');
            $table->string('email');
            $table->string('branch');
            $table->string('score');
            $table->string('max_score');
            $table->string('completion_date');
            $table->string('enrollment_date');
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        

        

        
    }

    public function down()
    {
        
        Schema::dropIfExists('course_completions');
    }
}
