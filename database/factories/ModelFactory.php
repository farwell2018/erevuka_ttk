<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\UserLicense::class, static function (Faker\Generator $faker) {
    return [
        'course_id' => $faker->sentence,
        'user_id' => $faker->sentence,
        'company_license_id' => $faker->sentence,
        'enrolled_at' => $faker->dateTime,
        'expired_at' => $faker->dateTime,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Course::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'short_description' => $faker->text(),
        'overview' => $faker->text(),
        'more_info' => $faker->text(),
        'effort' => $faker->sentence,
        'start' => $faker->dateTime,
        'end' => $faker->dateTime,
        'enrol_start' => $faker->dateTime,
        'enrol_end' => $faker->dateTime,
        'price' => $faker->randomFloat,
        'course_image_uri' => $faker->sentence,
        'course_video_uri' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'job_group_id' => $faker->sentence,
        'status' => $faker->boolean(),
        'course_category_id' => $faker->sentence,
        'slug' => $faker->unique()->slug,
        'order_id' => $faker->randomNumber(5),
        'course_video' => $faker->sentence,
        'sessions_id' => $faker->sentence,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\SessionMeeting::class, static function (Faker\Generator $faker) {
    return [
        'meeting_id' => $faker->sentence,
        'host_email' => $faker->sentence,
        'topic' => $faker->text(),
        'status' => $faker->sentence,
        'start_time' => $faker->sentence,
        'agenda' => $faker->text(),
        'join_url' => $faker->text(),
        'meeting_password' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'session_image' => $faker->sentence,
        'session_video' => $faker->sentence,
        
        
    ];
});
