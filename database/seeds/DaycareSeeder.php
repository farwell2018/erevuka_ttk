<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;

class DaycareSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = storage_path('app/ttk.xls');
        $rows = IOFactory::load($file)->getActiveSheet()->toArray(null, true, true, true);
        
        // Remove header row
        unset($rows[1]);

        foreach ($rows as $row) {
            $joining_date = date('Y-m-d H:i:s', strtotime($row['G']));
            DB::table('daycares')->insert([
                'daycare_id' => $row['A'],
                'name' => $row['B'],
                'contact_name' => $row['C'],
                'contact_number' => $row['D'],
                'location' => $row['E'],
                'cohort' => $row['F'],
                
                'joining_date' =>$joining_date,
            ]);
        }
    }
}
