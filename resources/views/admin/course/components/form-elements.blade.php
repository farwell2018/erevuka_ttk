<div class="form-group row align-items-center"
    :class="{'has-danger': errors.has('name'), 'has-success': fields.name && fields.name.valid }">
    <label for="name" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.name') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.name" v-validate="''" @input="validate($event)" class="form-control"
            :class="{'form-control-danger': errors.has('name'), 'form-control-success': fields.name && fields.name.valid}"
            id="name" name="name" placeholder="{{ trans('admin.course.columns.name') }}">
        <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>



<div class="form-group row align-items-center"
    :class="{'has-danger': errors.has('more_info'), 'has-success': fields.more_info && fields.more_info.valid }">
    <label for="more_info" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.more_info') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <quill-editor v-model="form.more_info" :options="wysiwygConfig" name="more_info" id="more_info" />
            {{-- <textarea class="form-control" v-model="form.more_info" v-validate="''" id="more_info" name="more_info" :options="wysiwygConfig"></textarea> --}}
        </div>
        <div v-if="errors.has('more_info')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('more_info') }}</div>
    </div>
</div>

<div class="form-group row align-items-center"
    :class="{'has-danger': errors.has('effort'), 'has-success': fields.effort && fields.effort.valid }">
    <label for="effort" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.effort') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.effort" v-validate="''" @input="validate($event)" class="form-control"
            :class="{'form-control-danger': errors.has('effort'), 'form-control-success': fields.effort && fields.effort.valid}"
            id="effort" name="effort" placeholder="{{ trans('admin.course.columns.effort') }}">
        <div v-if="errors.has('effort')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('effort') }}</div>
    </div>
</div>


<div class="form-check row"
    :class="{'has-danger': errors.has('status'), 'has-success': fields.status && fields.status.valid }">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="status" type="checkbox" v-model="form.status" v-validate="''"
            data-vv-name="status" name="status_fake_element">
        <label class="form-check-label" for="status">
            {{ trans('admin.course.columns.status') }}
        </label>
        <input type="hidden" name="status" :value="form.status">
        <div v-if="errors.has('status')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('status') }}</div>
    </div>
</div>

<div class="form-check row mt-3"
    :class="{'has-danger': errors.has('mobile_available'), 'has-success': fields.mobile_available && fields.mobile_available.valid }">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="mobile_available" type="checkbox" v-model="form.mobile_available" v-validate="''"
            data-vv-name="mobile_available" name="status_fake_element">
        <label class="form-check-label" for="mobile_available">
            {{ trans('admin.course.columns.mobile_available') }}
        </label>
        <input type="hidden" name="mobile_available" :value="form.mobile_available">
        <div v-if="errors.has('mobile_available')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('mobile_available') }}</div>
    </div>
</div>
