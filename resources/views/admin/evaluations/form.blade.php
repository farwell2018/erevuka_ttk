@extends('twill::layouts.form')

@section('contentFields')
@formField('select', [
	'name' => 'course_id',
	'label' => 'Related course',
	'placeholder' => 'Select related course',
	'options' => collect($courseList ?? ''),
])

@formField('block_editor', [

    'blocks' => ['evaluation']

    ])
@stop
