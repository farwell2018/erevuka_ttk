@extends('twill::layouts.form')

@section('contentFields')

@formField('select', [
	'name' => 'parent_id',
	'label' => 'Parent Menu',
	'placeholder' => 'Select Parent',
	'options' => collect($menuList ?? ''),
])
    @formField('input', [
        'name' => 'description',
        'label' => 'Description',
        'translated' => true,
        'maxlength' => 100
    ])
@stop
