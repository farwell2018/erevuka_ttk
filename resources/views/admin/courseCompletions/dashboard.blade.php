@php
    use Carbon\Carbon;
    $month = [];
    for ($m = 1; $m <= 12; $m++) {
        $month[] = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
    }
    
    $year = date('Y');
    $previousyear = $year - 1;
    
    $years = range($previousyear, $previousyear + 1);
    //$years= range(now()->year, now()->year + 5);
@endphp

@extends('layouts.admin.reports')
@push('extra_css')
    <link href="{{ asset('/css/reports.css') }}" rel="stylesheet">
    <style>
        .report_visual {
            margin-bottom: 30px;
        }

        .container {
            width: 100% !important;
        }

        .table {
            margin-top: 2rem;
        }

        .table th {
            border-top: none;
            font-weight: bold;
            font-size: 16px
        }

        .table td {
            text-align: center;
        }

        .btn.btn-primary {
            background: #520e33;
            border: 1px solid #520e33;
        }

        .btn.btn-primary:hover {
            background: #520e33;
            border: 1px solid #520e33;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('customPageContent')
    <div class="listing">

        <div id="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="report_visual" id="registrationDistribution" style="height: 500px">

                    </div>
                </div>

                <!-- Top 5 quick tips -->

                <div class="col-6">
                    <div class="buttons col-12 " style="background-color:#ffff;display:grid">
                        <div class="mt-4 mb-2 ">

                            <select name="year" id="quicktip-completion-year" class="form-control  minimal col-3"
                                style="float: right">
                                <option value="0">To date</option>
                                @foreach ($years as $key => $y)
                                    @if ($y === Carbon::now()->format('Y'))
                                        <option value="{{ $y }}" selected> {{ $y }}</option>
                                    @else
                                        <option value="{{ $y }}"> {{ $y }}</option>
                                    @endif
                                @endforeach
                            </select>
                            <label style="float: right;margin-right:1rem; margin-top:8px; color:#000">Filter by year</label>
                        </div>

                        <div class="report_visual" id="quicktip_completion_yearly_container" style="height: 500px">

                        </div>

                    </div>

                </div>


                <div class="col-6">
                    <div class="buttons col-12 " style="background-color:#ffff;display:grid">
                        <div class="mt-4 mb-2 ">

                            <select name="year" id="quicktip-dashboard-year" class="form-control  minimal col-3"
                                style="float: right">
                                <option value="0">To date</option>
                                @foreach ($years as $key => $y)
                                    @if ($y === Carbon::now()->format('Y'))
                                        <option value="{{ $y }}" selected> {{ $y }}</option>
                                    @else
                                        <option value="{{ $y }}"> {{ $y }}</option>
                                    @endif
                                @endforeach
                            </select>
                            <label style="float: right;margin-right:1rem; margin-top:8px; color:#000">Filter by year</label>
                        </div>

                        <div class="report_visual" id="quicktip_yearly_container" style="height: 500px">

                        </div>

                    </div>

                </div>
            </div>
        </div>


                <!-- End Top 5 quick tips -->
                <!-- Top 5 courses -->
                <div class="container-fluid mt-5">

                    <div class="row">
                <div class="col-6">
                    <div class="buttons col-12 " style="background-color:#ffff;display:grid">
                        <div class="mt-4 mb-2 ">

                            <select name="year" id="completion-year" class="form-control  minimal col-3"
                                style="float: right">
                                <option value="0">To date</option>
                                @foreach ($years as $key => $y)
                                    @if ($y === Carbon::now()->format('Y'))
                                        <option value="{{ $y }}" selected> {{ $y }}</option>
                                    @else
                                        <option value="{{ $y }}"> {{ $y }}</option>
                                    @endif
                                @endforeach
                            </select>
                            <label style="float: right;margin-right:1rem; margin-top:8px; color:#000">Filter by year</label>
                        </div>

                        <div class="report_visual" id="completion_yearly_container" style="height: 500px">

                        </div>

                    </div>

                </div>


                <div class="col-6">
                    <div class="buttons col-12 " style="background-color:#ffff;display:grid">
                        <div class="mt-4 mb-2 ">

                            <select name="year" id="dashboard-year" class="form-control  minimal col-3"
                                style="float: right">
                                <option value="0">To date</option>
                                @foreach ($years as $key => $y)
                                    @if ($y === Carbon::now()->format('Y'))
                                        <option value="{{ $y }}" selected> {{ $y }}</option>
                                    @else
                                        <option value="{{ $y }}"> {{ $y }}</option>
                                    @endif
                                @endforeach
                            </select>
                            <label style="float: right;margin-right:1rem; margin-top:8px; color:#000">Filter by year</label>
                        </div>

                        <div class="report_visual" id="yearly_container" style="height: 500px">

                        </div>

                    </div>

                </div>

            </div>


        </div>

        <div class="container-fluid">

            <div class="row">
                <div class="col-6">
                    <div style="background: #ffff; padding:20px" class="mt-5 ">
                        <div class="row">
                            <div class="col-10">
                                <h3 style="font-size: 18px;text-align:center;padding-top:20px"> Top 10 users with most
                                    completed courses</h3>
                            </div>
                            <div class="col-2">
                                <a href="{{ route('admin.reports.courseCompletions.index') }}" class="btn btn-primary">
                                    View More</a>
                            </div>
                        </div>

                        <table class="table table--sized">
                            <tr>
                                <th class="tablehead__cell f--small">
                                    Name
                                </th>

                                <th class="tablehead__cell f--small">
                                    Number of completed courses

                                </th>
                            </tr>

                            @foreach ($userCompletions as $value)
                                <tr class="tablerow">
                                    <td class="tablecell">{{ $value['name'] }}</td>
                                    <td class="tablecell">{{ $value['total'] }}</td>

                                </tr>
                            @endforeach


                        </table>



                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
@push('extra_js')
    <script src="{{ twillAsset('main-free.js') }}" crossorigin></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>


    <script>
        Highcharts.chart('registrationDistribution', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'spline'
            },
            title: {
                text: 'Registration Trends {{ now()->year }}'
            },
            subtitle: {
                text: 'Active vs Inactive Accounts'
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                ],
                accessibility: {
                    description: 'Months of the year'
                }
            },
            yAxis: {
                title: {
                    text: 'Number of Registrations'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: [

                {
                    name: 'Active Accounts',
                    marker: {
                        symbol: 'square'
                    },
                    data: [
                        @foreach ($completedRegistrationoption as $key => $value)
                            {{ $value . ',' }}
                        @endforeach

                    ]
                },
                {
                    name: 'Inactive Accounts',
                    marker: {
                        symbol: 'diamond'
                    },
                    data: [
                        @foreach ($pendingRegistrationoption as $key => $value)
                            {{ $value . ',' }}
                        @endforeach

                    ]



                }

            ]
        });
    </script>

    <script>
        const completion_yearly = Highcharts.chart('completion_yearly_container', {
            chart: {
                type: 'column'
            },
            title: {
                text: '{{ $yearlyCompletionGraph['chart_title'] }}',

            },

            plotOptions: {
                series: {
                    grouping: false,
                    borderWidth: 0
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                shared: true,
                headerFormat: '<span style="font-size: 15px">{point.point.name}</span><br/>',
                pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y} </b><br/>'
            },
            xAxis: {
                type: 'category',
                accessibility: {
                    description: 'Courses'
                },

            },
            yAxis: [{
                title: {
                    text: 'Numbers'
                },
                showFirstLabel: false
            }],
            series: [{
                color: 'rgb(82, 14, 51)',
                pointPlacement: -0.2,
                linkedTo: 'main',
                dataSorting: {
                    enabled: true,
                    matchByName: true
                },
                data: [
                    @foreach ($yearlyCompletionGraph['completion_data'] as $key => $value)
                        {
                            name: '{{ $value['name'] }}',
                            y: {{ $value['total'] }}
                        },
                    @endforeach
                ],
                name: 'Enrollments'
            }],
            exporting: {
                allowHTML: true
            }
        });


        var select = document.getElementById('completion-year');

        select.addEventListener('change', (e) => {
            var year = e.target.value;
            var term;

            if(year == 0) {

                term = "to date"
                
            }else{

                term = year
            }

            completion_yearly.update({
                title: {
                    text: `Top 5 Course Completions of ${term}`
                },
                series: [{
                    dataSorting: {
                        enabled: true,
                        matchByName: true
                    },
                    data: [
                        $.ajax({
                            url: '{{ url('/admin/reports/graph/yearlyCRresults') }}?cyear=' +
                                year,
                            type: 'GET',
                            dataType: 'json',
                            success: function(res) {
                                $.each(res, function(key, value) {
                                    completion_yearly.series[0].addPoint([value
                                        .name, parseInt(
                                            value.total)
                                    ]);
                                })
                            }
                        })
                    ],
                    name: 'Completions',
                }]
            }, true, false, {
                duration: 800
            });
        });
    </script>


    <script>
        const yearly = Highcharts.chart('yearly_container', {
            chart: {
                type: 'column'
            },
            title: {
                text: '{{ $yearlyEnrollmentGraph['chart_title'] }}',

            },

            plotOptions: {
                series: {
                    grouping: false,
                    borderWidth: 0
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                shared: true,
                headerFormat: '<span style="font-size: 15px">{point.point.name}</span><br/>',
                pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y} </b><br/>'
            },
            xAxis: {
                type: 'category',
                accessibility: {
                    description: 'Courses'
                },

            },
            yAxis: [{
                title: {
                    text: 'Numbers'
                },
                showFirstLabel: false
            }],
            series: [{
                color: 'rgb(82, 14, 51)',
                pointPlacement: -0.2,
                linkedTo: 'main',
                dataSorting: {
                    enabled: true,
                    matchByName: true
                },
                data: [
                    @foreach ($yearlyEnrollmentGraph['enrollment_data'] as $key => $value)
                        {
                            name: '{{ $value['name'] }}',
                            y: {{ $value['total'] }}
                        },
                    @endforeach
                ],
                name: 'Enrollments'
            }],
            exporting: {
                allowHTML: true
            }
        });


        var select = document.getElementById('dashboard-year');

        select.addEventListener('change', (e) => {
            var year = e.target.value;
            var term;
            if(year == 0) {

                term = "to date"

                }else{

                term = year
                }

            yearly.update({
                title: {
                    text: `Top 5 Course Enrollments of ${term}`
                },
                series: [{
                    dataSorting: {
                        enabled: true,
                        matchByName: true
                    },
                    data: [
                        $.ajax({
                            url: '{{ url('/admin/reports/graph/yearlyEResults') }}?eyear=' +
                                year,
                            type: 'GET',
                            dataType: 'json',
                            success: function(res) {
                                $.each(res, function(key, value) {
                                    yearly.series[0].addPoint([value
                                        .name, parseInt(
                                            value.total)
                                    ]);
                                })
                            }
                        })
                    ],
                    name: 'Enrollments',
                }]
            }, true, false, {
                duration: 800
            });
        });
    </script>

/** End Courses Scripts **/


/** Quick Tip Scripts **/

<script>
        const quicktip_completion_yearly = Highcharts.chart('quicktip_completion_yearly_container', {
            chart: {
                type: 'column'
            },
            title: {
                text: '{{ $yearlyQuickTipCompletionGraph['chart_title'] }}',

            },

            plotOptions: {
                series: {
                    grouping: false,
                    borderWidth: 0
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                shared: true,
                headerFormat: '<span style="font-size: 15px">{point.point.name}</span><br/>',
                pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y} </b><br/>'
            },
            xAxis: {
                type: 'category',
                accessibility: {
                    description: 'Courses'
                },

            },
            yAxis: [{
                title: {
                    text: 'Numbers'
                },
                showFirstLabel: false
            }],
            series: [{
                color: 'rgb(82, 14, 51)',
                pointPlacement: -0.2,
                linkedTo: 'main',
                dataSorting: {
                    enabled: true,
                    matchByName: true
                },
                data: [
                    @foreach ($yearlyQuickTipCompletionGraph['completion_data'] as $key => $value)
                        {
                            name: '{{ $value['name'] }}',
                            y: {{ $value['total'] }}
                        },
                    @endforeach
                ],
                name: 'Enrollments'
            }],
            exporting: {
                allowHTML: true
            }
        });


        var select = document.getElementById('quicktip-completion-year');

        select.addEventListener('change', (e) => {
            var year = e.target.value;

            var term;
            if(year == 0) {

                term = "to date"

                }else{

                term = year
                }

            quicktip_completion_yearly.update({
                title: {
                    text: `Top 5 Quick Tips Completions of ${term}`
                },
                series: [{
                    dataSorting: {
                        enabled: true,
                        matchByName: true
                    },
                    data: [
                        $.ajax({
                            url: '{{ url('/admin/reports/graph/yearlyQuickTipCRresults') }}?cyear=' +
                                year,
                            type: 'GET',
                            dataType: 'json',
                            success: function(res) {
                                $.each(res, function(key, value) {
                                    quicktip_completion_yearly.series[0].addPoint([value
                                        .name, parseInt(
                                            value.total)
                                    ]);
                                })
                            }
                        })
                    ],
                    name: 'Completions',
                }]
            }, true, false, {
                duration: 800
            });
        });
    </script>

    <script>
        const quicktip_yearly = Highcharts.chart('quicktip_yearly_container', {
            chart: {
                type: 'column'
            },
            title: {
                text: '{{ $yearlyQuickTipEnrollmentGraph['chart_title'] }}',

            },

            plotOptions: {
                series: {
                    grouping: false,
                    borderWidth: 0
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                shared: true,
                headerFormat: '<span style="font-size: 15px">{point.point.name}</span><br/>',
                pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y} </b><br/>'
            },
            xAxis: {
                type: 'category',
                accessibility: {
                    description: 'Courses'
                },

            },
            yAxis: [{
                title: {
                    text: 'Numbers'
                },
                showFirstLabel: false
            }],
            series: [{
                color: 'rgb(82, 14, 51)',
                pointPlacement: -0.2,
                linkedTo: 'main',
                dataSorting: {
                    enabled: true,
                    matchByName: true
                },
                data: [
                    @foreach ($yearlyQuickTipEnrollmentGraph['enrollment_data'] as $key => $value)
                        {
                            name: '{{ $value['name'] }}',
                            y: {{ $value['total'] }}
                        },
                    @endforeach
                ],
                name: 'Enrollments'
            }],
            exporting: {
                allowHTML: true
            }
        });


        var select = document.getElementById('quicktip-dashboard-year');

        select.addEventListener('change', (e) => {
            var year = e.target.value;

            var term;
            if(year == 0) {

                term = "to date"

                }else{

                term = year
                }

            quicktip_yearly.update({
                title: {
                    text: `Top 5 quick tip enrollments of ${term}`
                },
                series: [{
                    dataSorting: {
                        enabled: true,
                        matchByName: true
                    },
                    data: [
                        $.ajax({
                            url: '{{ url('/admin/reports/graph/yearlyQuickTipEResults') }}?eyear=' +
                                year,
                            type: 'GET',
                            dataType: 'json',
                            success: function(res) {
                                $.each(res, function(key, value) {
                                    quicktip_yearly.series[0].addPoint([value
                                        .name, parseInt(
                                            value.total)
                                    ]);
                                })
                            }
                        })
                    ],
                    name: 'Enrollments',
                }]
            }, true, false, {
                duration: 800
            });
        });
    </script>








@endpush
