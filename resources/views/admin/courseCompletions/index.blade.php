@extends('twill::layouts.main')

@push('extra_css')
    <style>
        .filter__more {
            display: block !important;
            overflow: visible !important;
        }

        .filter__toggle.button.button--ghost {
            display: none !important;
        }


        .container {
            width: 100% !important;
        }

        .a17 .vselect__field .dropdown-toggle {
            height: 43px !important;
        }
        .date_range_picker{
            height: 43px !important;
            width:15%;
        }
        .filter-select{
            height: 43px !important;
            margin-bottom:2rem;
            min-width:10%;
        }
        .filter .filter__moreInner > div{
            display:contents !important;
        }

        .filter .filter__moreInner > div > .vselectOuter {
        margin-right: 20px;
        width: 32%;
        }
         
        .filter .filter__moreInner > select {
        margin-right: 20px;
        width: 9%;
        }

        .filter .filter__moreInner > div > label {
        margin-right: 20px;
        width: unset;
        }

    </style>
@endpush

@section('appTypeClass', 'body--listing')

@php
$translate = $translate ?? false;
$translateTitle = $translateTitle ?? ($translate ?? false);
$reorder = $reorder ?? false;
$nested = $nested ?? false;
$bulkEdit = $bulkEdit ?? true;
$create = $create ?? false;
$skipCreateModal = $skipCreateModal ?? false;

$requestFilter = json_decode(request()->get('filter'), true) ?? [];


@endphp

@push('extra_css')
    @if (app()->isProduction())
        <link href="{{ twillAsset('main-listing.css') }}" rel="preload" as="style" crossorigin />
    @endif
    @unless(config('twill.dev_mode', false))
        <link href="{{ twillAsset('main-listing.css') }}" rel="stylesheet" crossorigin />
    @endunless
@endpush

@push('extra_js_head')
    @if (app()->isProduction())
        <link href="{{ twillAsset('main-listing.js') }}" rel="preload" as="script" crossorigin />
    @endif
@endpush

@section('content')
    <div class="listing">
        <div class="listing__nav">
            <div class="container" ref="form" >
                <a17-filter v-on:submit="filterListing" v-bind:closed="hasBulkIds"
                    initial-search-value="{{ $filters['search'] ?? '' }}" :clear-option="true"
                    v-on:clear="clearFiltersAndReloadDatas">
                    <a17-table-filters slot="navigation"></a17-table-filters>

                    @forelse($hiddenFilters as $filter)

                        @if ($loop->first)
                            <div slot="hidden-filters">
                        @endif

                        @if (isset(${$filter . 'List'}) && $filter != 'completion_date' && $filter != 'enrollment_date')
                        @if($filter === 'course_id')
                        @php
                        $list = ${$filter . 'List'};
                        $options =
                            is_object($list) && method_exists($list, 'map')
                                ? $list
                                    ->map(function ($label, $value) {
                                        return [
                                            'value' => $label->id,
                                            'label' => $label->name,
                                        ];
                                    })
                                    ->values()
                                    ->toArray()
                                : $list;

                        $selectedIndex = isset($requestFilter[$filter]) ? array_search($requestFilter[$filter], array_column($options, 'value')) : false;
                    @endphp

                        @else
                         @php
                             $list = ${$filter . 'List'};
                             $options =
                                 is_object($list) && method_exists($list, 'map')
                                     ? $list
                                         ->map(function ($label, $value) {
                                             return [
                                                 'value' => $label->id,
                                                 'label' => $label->title,
                                             ];
                                         })
                                         ->values()
                                         ->toArray()
                                     : $list;

                             $selectedIndex = isset($requestFilter[$filter]) ? array_search($requestFilter[$filter], array_column($options, 'value')) : false;
                         @endphp

                         @endif

                            @if($filter === 'job_role_id')
                            <label>{{ 'Job Role' }}</label>
                            @elseif ($filter === 'country_id')
                            <label>{{ 'Country' }}</label>
                            @elseif ($filter === 'branch_id')
                                <label>{{ 'Branch' }}</label>
                            @elseif($filter === 'status_type')
                                <label>{{ 'Status' }}</label>
                            @elseif($filter === 'course_id')
                                <label>{{ 'Course' }}</label>
                            @elseif($filter === 'company_id')
                                <label>{{ 'Subsidiary' }}</label>

                            @endif
                              
                            <select name='{{$filter}}' class="filter-select" id="{{$filter}}">
                                <option value=""> Select </option>
                              @foreach($options as $key => $value)
                             <option value="{{$value['value']}}">{{$value['label']}}</option>
                              
                              @endforeach
                            </select>
                           
                            {{-- <a17-vselect name="{{ $filter }}" :options="{{ json_encode($options) }}"
                                @if ($selectedIndex !== false) :selected="{{ json_encode($options[$selectedIndex]) }}" @endif
                                placeholder="Select" ref="filterDropdown[{{ $loop->index }}]"></a17-vselect> --}}
                        @else
                            @if ($filter === 'completion_date')
                                <label>{{ 'Completion Date' }}</label>
                            @elseif($filter === 'enrollment_date')
                                <label>{{ 'Enrollment Date' }}</label>

                            @endif
                            <input type="text" name="{{ $filter }}" class="date_range_picker"/>
                            {{-- <a17-datepicker name="{{ $filter }}" placeholder="Completion Date">
                            </a17-datepicker> --}}

                        @endif

                        @if ($loop->last)

            </div>

            @endif
        @empty
            @hasSection('hiddenFilters')
                <div slot="hidden-filters">
                    @yield('hiddenFilters')
                </div>
            @endif
            @endforelse

            @if ($create)
                <div slot="additional-actions">
                    <a17-button variant="validate" size="small"
                        @if ($skipCreateModal) href={{ $createUrl ?? '' }} el="a" @endif
                        @if (!$skipCreateModal) v-on:click="create" @endif>
                        {{ twillTrans('twill::lang.listing.add-new-button') }}
                    </a17-button>
                    @foreach ($filterLinks as $link)
                        <a17-button el="a" href="{{ $link['url'] ?? '#' }}" download="{{ $link['download'] ?? '' }}"
                            rel="{{ $link['rel'] ?? '' }}" target="{{ $link['target'] ?? '' }}"
                            variant="small secondary">{{ $link['label'] }}</a17-button>
                    @endforeach
                </div>
            @elseif(isset($filterLinks) && count($filterLinks))
                <div slot="additional-actions">
                    @foreach ($filterLinks as $link)
                        <a17-button el="a" href="{{ $link['url'] ?? '#' }}" download="{{ $link['download'] ?? '' }}"
                            rel="{{ $link['rel'] ?? '' }}" target="{{ $link['target'] ?? '' }}"
                            variant="small secondary">{{ $link['label'] }}</a17-button>
                    @endforeach
                </div>
            @endif

            </a17-filter>

        </div>
        @if ($bulkEdit)
            <a17-bulk></a17-bulk>
        @endif

    </div>



    <div class="container" style="margin-bottom: 3%;">
        <div class="row">
           <div class="col-12">
              <form action="{{ route('admin.courseCompletion.export') }}" method="POST">
                @csrf
                <input type="hidden" value="{{json_encode($tableData)}}" name="tableData">

                <button type="submit" class="button button--small button--validate" style="float:right; margin-right:20px;">Export</button>

              </form>

            {{-- <a size="small" class="button button--small button--validate" href="{{ route('admin.courseCompletion.export', 1) }}" style="float:right; margin-right:20px;">Export </a> --}}
           </div>
        </div>
    </div>

    @if ($nested)
        <a17-nested-datatable :draggable="{{ $reorder ? 'true' : 'false' }}" :max-depth="{{ $nestedDepth ?? '1' }}"
            :bulkeditable="{{ $bulkEdit ? 'true' : 'false' }}"
            empty-message="{{ twillTrans('twill::lang.listing.listing-empty-message') }}">
        </a17-nested-datatable>
    @else
        <a17-datatable :draggable="{{ $reorder ? 'true' : 'false' }}"
            :bulkeditable="{{ $bulkEdit ? 'true' : 'false' }}"
            empty-message="{{ twillTrans('twill::lang.listing.listing-empty-message') }}">
        </a17-datatable>
    @endif

    @if ($create)
        <a17-modal-create ref="editionModal" form-create="{{ $storeUrl }}" v-on:reload="reloadDatas"
            @if ($customPublishedLabel ?? false) published-label="{{ $customPublishedLabel }}" @endif
            @if ($customDraftLabel ?? false) draft-label="{{ $customDraftLabel }}" @endif>
            <a17-langmanager></a17-langmanager>
            @partialView(($moduleName ?? null), 'create', ['renderForModal' => true])
        </a17-modal-create>
    @endif

    <a17-dialog ref="warningDeleteRow" modal-title="{{ twillTrans('twill::lang.listing.dialogs.delete.title') }}"
        confirm-label="{{ twillTrans('twill::lang.listing.dialogs.delete.confirm') }}">
        <p class="modal--tiny-title"><strong>{{ twillTrans('twill::lang.listing.dialogs.delete.move-to-trash') }}</strong>
        </p>
        <p>{{ twillTrans('twill::lang.listing.dialogs.delete.disclaimer') }}</p>
    </a17-dialog>

    <a17-dialog ref="warningDestroyRow" modal-title="{{ twillTrans('twill::lang.listing.dialogs.destroy.title') }}"
        confirm-label="{{ twillTrans('twill::lang.listing.dialogs.destroy.confirm') }}">
        <p class="modal--tiny-title">
            <strong>{{ twillTrans('twill::lang.listing.dialogs.destroy.destroy-permanently') }}</strong>
        </p>
        <p>{{ twillTrans('twill::lang.listing.dialogs.destroy.disclaimer') }}</p>
    </a17-dialog>
    </div>
@stop

@section('initialStore')

    window['{{ config('twill.js_namespace') }}'].CMS_URLS = {
    index: @if (isset($indexUrl)) '{{ $indexUrl }}'
    @else
        window.location.href.split('?')[0] @endif,
    publish: '{{ $publishUrl }}',
    bulkPublish: '{{ $bulkPublishUrl }}',
    restore: '{{ $restoreUrl }}',
    bulkRestore: '{{ $bulkRestoreUrl }}',
    forceDelete: '{{ $forceDeleteUrl }}',
    bulkForceDelete: '{{ $bulkForceDeleteUrl }}',
    reorder: '{{ $reorderUrl }}',
    create: '{{ $createUrl ?? '' }}',
    feature: '{{ $featureUrl }}',
    bulkFeature: '{{ $bulkFeatureUrl }}',
    bulkDelete: '{{ $bulkDeleteUrl }}'
    }

    window['{{ config('twill.js_namespace') }}'].STORE.form = {
    fields: []
    }

    window['{{ config('twill.js_namespace') }}'].STORE.datatable = {
    data: {!! json_encode($tableData) !!},
    columns: {!! json_encode($tableColumns) !!},
    navigation: {!! json_encode($tableMainFilters) !!},
    filter: { status: '{{ $filters['status'] ?? ($defaultFilterSlug ?? 'all') }}' },
    page: '{{ request('page') ?? 1 }}',
    maxPage: '{{ $maxPage ?? 1 }}',
    defaultMaxPage: '{{ $defaultMaxPage ?? 1 }}',
    offset: '{{ request('offset') ?? ($offset ?? 60) }}',
    defaultOffset: '{{ $defaultOffset ?? 60 }}',
    sortKey: '{{ $reorder ? request('sortKey') ?? '' : request('sortKey') ?? '' }}',
    sortDir: '{{ request('sortDir') ?? 'asc' }}',
    baseUrl: '{{ rtrim(config('app.url'), '/') . '/' }}',
    localStorageKey:
    '{{ isset($currentUser) ? $currentUser->id : 0 }}__{{ $moduleName ?? Route::currentRouteName() }}'
    }

    @if ($create && ($openCreate ?? false))
        window['{{ config('twill.js_namespace') }}'].openCreate = {!! json_encode($openCreate) !!}
    @endif
@stop

@push('extra_js')
    <script src="{{ twillAsset('main-listing.js') }}" crossorigin></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <script type="text/javascript">
        $(function() {

          $('input[name="completion_date"]').daterangepicker({
            autoUpdateInput: false,
            opens: "center",
              locale: {
                  cancelLabel: 'Clear'
              }
          });

          $('input[name="completion_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
          });

          $('input[name="completion_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });

        });
        </script>

<script type="text/javascript">
    $(function() {

      $('input[name="enrollment_date"]').daterangepicker({
        autoUpdateInput: false,
        opens: "center",
          locale: {
              cancelLabel: 'Clear',
              customRangeLabel: "Custom",
          }
      });

      $('input[name="enrollment_date"]').on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
      });

      $('input[name="enrollment_date"]').on('cancel.daterangepicker', function(ev, picker) {
          $(this).val('');
      });

    });
    </script>


<script type="text/javascript">
 $(document).ready(function(){

    $('#company_id').on('change', function() {
        var companyID = $(this).val();
           if(companyID) {
               $.ajax({
                   url: '/admin/getInfo/'+companyID,
                   type: "GET",
                   data : {"_token":"{{ csrf_token() }}"},
                   dataType: "json",
                   success:function(data)
                   {
                    console.log(data.branch);
                     if(data.branch){
                        $('#branch_id').empty();
                        $('#branch_id').append('<option value=" " hidden>Select Branch</option>'); 
                        $.each(data.branch, function(key, branch){
                            $('select[name="branch_id"]').append('<option value="'+ branch.id +'">' + branch.title+ '</option>');
                        });
                    }else{
                        $('#branch_id').empty();
                    }

                    if(data.jobRole){
                        $('#job_role_id').empty();
                        $('#job_role_id').append('<option value=" " hidden>Select Job Role</option>'); 
                        $.each(data.jobRole, function(key, role){
                            $('select[name="job_role_id"]').append('<option value="'+ role.id +'">' + role.title+ '</option>');
                        });


                    }else{

                        $('#job_role_id').empty();

                    }
                 }
               });
           }else{
             $('#branch').empty();
           }
});

});


</script>
 
@endpush
