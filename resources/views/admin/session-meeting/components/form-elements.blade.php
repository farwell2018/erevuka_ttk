
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('topic'), 'has-success': fields.topic && fields.topic.valid }">
    <label for="topic" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.session-meeting.columns.topic') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <textarea class="form-control" v-model="form.topic" v-validate="'required'" id="topic" name="topic"></textarea>
        </div>
        <div v-if="errors.has('topic')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('topic') }}</div>
    </div>
</div>
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('agenda'), 'has-success': fields.agenda && fields.agenda.valid }">
    <label for="agenda" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.session-meeting.columns.agenda') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <quill-editor v-model="form.agenda" :options="wysiwygConfig" id="agenda" name="agenda"/>
            {{-- <textarea class="form-control" v-model="form.agenda" v-validate="'required'" id="agenda" name="agenda"></textarea> --}}
        </div>
        <div v-if="errors.has('agenda')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('agenda') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('start_time'), 'has-success': fields.start_time && fields.start_time.valid }">
    <label for="start_time" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.session-meeting.columns.start_time') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
          
            <datetime v-model="form.start_time" :config="datetimePickerConfig" v-validate="'required|date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('enrolled_at'), 'form-control-success': fields.enrolled_at && fields.enrolled_at.valid}" id="start_time" name="start_time" placeholder="{{ trans('admin.session-meeting.columns.start_time') }}"></datetime>


        {{-- <input type="text" v-model="form.start_time" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('start_time'), 'form-control-success': fields.start_time && fields.start_time.valid}" id="start_time" name="start_time" placeholder="{{ trans('admin.session-meeting.columns.start_time') }}"> --}}
        <div v-if="errors.has('start_time')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('start_time') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('duration'), 'has-success': fields.duration && fields.duration.valid }">
    <label for="duration" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">Duration</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">

            
        <input type="text" v-model="form.duration" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('duration'), 'form-control-success': fields.duration && fields.duration.valid}" id="duration" name="duration" placeholder="Duration">
        <div v-if="errors.has('duration')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('duration') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('session_image'), 'has-success': fields.session_image && fields.session_image.valid }">
    <label for="session_image" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.session-meeting.columns.session_image') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="file" v-on:change="" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('session_image'), 'form-control-success': fields.session_image && fields.session_image.valid}" id="session_image" name="session_image" placeholder="{{ trans('admin.session-meeting.columns.session_image') }}">
        <div v-if="errors.has('session_image')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('session_image') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('session_video'), 'has-success': fields.session_video && fields.session_video.valid }">
    <label for="session_video" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.session-meeting.columns.session_video') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="file"  v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('session_video'), 'form-control-success': fields.session_video && fields.session_video.valid}" id="session_video" name="session_video" placeholder="{{ trans('admin.session-meeting.columns.session_video') }}">
        <div v-if="errors.has('session_video')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('session_video') }}</div>
    </div>
</div>


