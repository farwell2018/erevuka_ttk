@extends('twill::layouts.form')

@section('contentFields')
   
    @formField('input', [
        'name' => 'contact_first_name',
        'label' => ' Contact First Name',
        'translated' => true,
        'maxlength' => 100
    ])

    @formField('input', [
        'name' => 'contact_last_name',
        'label' => ' Contact last Name',
        'translated' => true,
        'maxlength' => 100
    ])

@formField('input', [
    'name' => 'contact_email',
    'label' => ' Contact Email Address',
    'translated' => true,
    'maxlength' => 100
])

@formField('input', [
    'name' => 'secondcontact_first_name',
    'label' => ' Second Contact First Name',
    'translated' => true,
    'maxlength' => 100
])

@formField('input', [
    'name' => 'secondcontact_last_name',
    'label' => ' Second Contact last Name',
    'translated' => true,
    'maxlength' => 100
])

@formField('input', [
'name' => 'secondcontact_email',
'label' => ' Second Contact Email Address',
'translated' => true,
'maxlength' => 100
])


@formField('medias', [
'name' => 'cover',
'label' => 'Company Logo',
 'note' => 'Shown on certificate'
])

@formField('block_editor', [
    'blocks' => ['title', 'quote', 'text', 'image', 'grid', 'test', 'publications', 'news']
])
@stop

@section('fieldsets')
<a17-fieldset title="Domains" id="domains" :open="true">
 @formField('repeater', ['type' => 'domain'])

</a17-fieldset>   

@endsection

