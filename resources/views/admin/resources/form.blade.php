@extends('twill::layouts.form')

@section('contentFields')

@formField('select', [
	'name' => 'content_bucket_id',
	'label' => 'Resource Content Bucket',
	'placeholder' => 'Select Resource Content Bucket',
	'options' => collect($contentBucketList ?? ''),
])
@formField('select', [
	'name' => 'resource_type_id',
	'label' => 'Resource Category',
	'placeholder' => 'Select Resource Category',
	'options' => collect($resourceTypeList ?? ''),
])

@formField('select', [
	'name' => 'course_id',
	'label' => 'Resource course',
	'placeholder' => 'Select Resource related course',
	'options' => collect($courseList ?? ''),
])

@formField('select', [
	'name' => 'resource_category',
	'label' => 'Resource Type',
	'placeholder' => 'Select if course or quick tip',
	'options' => collect($resourceCategoryList ?? ''),
])

@formField('multi_select', [
	'name' => 'related_courses',
	'label' => 'Select related course or quicktip for this resource',
	'placeholder' => 'Select related course or quicktip for this resource',
    'min'=> 1,
    'unpack' => false,
    'searchable'=>true,
	'options' => collect($courseList ?? ''),
])

@formField('wysiwyg', [
        'name' => 'description',
        'label' => 'Short Description',
        'placeholder' => 'Introduction Text',
		'maxlength' => 2000,
		'translated' => true,
])

@formField('medias',[
    'name' => 'resource_image',
    'label' => 'Resources image',
    'note' => 'This dimensions for images are 380*200 ',
])


@formField('files', [
	'name' => 'resource_video',
	'label' => 'Video',
	'note' => 'This applies for video resources',
])

@formField('input', [
        'name' => 'video_length',
        'label' => 'Resource Video length',
		'note' => 'The length of the video in minutes',
        'maxlength' => 100
    ])

@formField('repeater', ['type' => 'resource-document'])
@stop
