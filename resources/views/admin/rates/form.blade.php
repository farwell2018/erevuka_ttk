@extends('twill::layouts.form')

@section('contentFields')
    @formField('input', [
        'name' => 'range',
        'label' => 'Range',
        'maxlength' => 100
    ])

    @formField('input', [
        'name' => 'price',
        'label' => 'Price',
        'maxlength' => 100
    ])

    @formField('input', [
        'name' => 'currency',
        'label' => 'Currency',
        'maxlength' => 100
    ])
@stop
