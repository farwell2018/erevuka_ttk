@extends('twill::layouts.form')

@section('contentFields')
	@formField('input', [
		'name' => 'firstname',
		'label' => 'First Name',
		'maxlength' => 100
	])
	@formField('input', [
		'name' => 'lastname',
		'label' => 'Last Name',
		'maxlength' => 100
	])
	@formField('wysiwyg', [
        'name' => 'description',
        'label' => 'Testimonial',
        'placeholder' => 'Testimonial',
		'translated' => true,
	])
@stop