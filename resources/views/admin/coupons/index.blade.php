@extends('layouts.admin.codes')
@push('extra_css')
    <link href="{{ asset('/css/reports.css') }}" rel="stylesheet">
    <style>
        .report_visual {
            margin-bottom: 30px;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('customPageContent')
    <div class="listing">

        <div id="container-fluid">

            <div class="col-12">
            <div class="row">

           
            <div class="col  mb-3">
    <form action="{{ route('admin.promo.codes') }}" method="GET" class="form-inline">
        <div class="form-group mr-2 mb-0">
            <label for="search" class="mr-2">Search:</label>
            <input type="text" class="form-control form-control-sm" name="search" value="{{ request('search') }}" placeholder="Enter search query">
        </div>
        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
        
    </form>


    
</div>
<a href="{{route('admin.coupons.index')}}"  class=" btn btn-primary btn-sm d-flex justify-content-end mt-2"><i class="fa fa-plus "></i> Add New</a>
</div>
                <div class="row">
              
                @if (session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif
                    <div class="col">

                        <table class="table" style="text-align: center">
                            <thead></thead>
                            <tr class="tablehead">
                                <th style="font-weight:bold;"> #</th>
                                <th style="text-align: left;padding-left:2.5%; font-weight:bold;"> Title</th>
                                <th style="text-align: left;padding-left:2.5%;width:20%;font-weight:bold;"> Related user </th>
                                <th style="text-align: left;padding-left:2.5%;width:20%;font-weight:bold;"> Promotion Code </th>
                                <th style="text-align: left;padding-left:2.5%;width:20%;font-weight:bold;"> Created On</th>
                                <th style="text-align: left;padding-left:2.5%;width:20%;font-weight:bold;"> Promotion Code Total Use </th>


                            </tr>
                            </thead>
                            <tbody>
                                @php $i = 1; @endphp
                                @if(count($codes)>0)
                                @foreach ($codes as $code)
                                    <tr class="tablerow">
                                        <td class="tablecell" style="width:5%">
                                            {{ $i++ }}</td>
                                            <td class="tablecell" style="width:5%">
                                            {{ $code->title}}</td>
                                        <td class="tablecell" style="text-align:left; padding-left:2.5%; width:10%"><a
                                                href="{{ route('admin.user.promo.codes', [$code->code]) }}">{{ $code->name }}</a>
                                        </td>
                                        <td class="tablecell" style= "text-align:left; padding-left:2.5%; width:5%">
                                            {{ $code->code}}</td>
                                        <td class="tablecell" style="text-align:left; padding-left:2.5%">
                                            {{ $code->created_at}}</td>

                                        <td class="tablecell" style="text-align:left; padding-left:2.5%">
                                        <a
                                                href="{{ route('admin.user.promo.codes', [$code->code]) }}">{{ $code->count }}</a></td>

                                    </tr>
                                @endforeach
                                @else
                                <tr class="tablerow">
                                        <td class="tablecell" colspan="10" style="width:10%">
                                        Records Not found for this resource
                                        </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>

                        {!! $codes->links() !!}
                    </div>
                </div>

            </div>

        </div>
    </div>
@stop
@push('extra_js')
    <script src="{{ twillAsset('main-free.js') }}" crossorigin></script>
@endpush
