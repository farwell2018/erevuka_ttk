@extends('layouts.admin.codes')
@push('extra_css')
@php
use App\Repositories\UserRepository;
 $userList = app(UserRepository::class)->where('published', 1)->where('id', '!=', 1)->orderBy('name')->pluck('name', 'id');


@endphp
    <link href="{{ asset('/css/reports.css') }}" rel="stylesheet">
    <style>
        .report_visual {
            margin-bottom: 30px;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('customPageContent')
<div class="listing">
    <div id="container-fluid">
        <div class="col-6">
            <div class="row">
                <div class="col  mb-3">
                    <form action="{{ route('admin.coupons.store') }}">
                        <div class="form-group row align-items-center">
                            <label for="title" class="col-form-label text-md-right">Title</label>
                            <input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}" placeholder="Title">
                        </div>

                        <div class="form-group row align-items-center">
                            <label for="user_id">User</label>
                            <input type="text" list="users" id="user-input" name="user_id" class="form-control" value="{{ old('user_id') }}" placeholder="Search user">
                            <datalist id="users">
                                <!-- Populate the datalist options with PHP/blade -->
                                @foreach($userList as $id => $name)
                                <option value="{{ $name }}" data-value="{{ $id }}">

                                @endforeach
                            </datalist>
                        </div>

                        <div class="form-group row align-items-center">
                            <label for="prefix" class="col-form-label text-md-right">Prefix</label>
                            <input type="text" class="form-control" id="prefix" value="{{ old('prefix') }}" name="prefix" placeholder="eg TTK">
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@push('extra_js')
    <script src="{{ twillAsset('main-free.js') }}" crossorigin></script>
    <script>
    // Get the input and select elements
    const userInput = document.getElementById('user-input');
    const userSelect = document.getElementById('user_id');

    // Add an event listener to the input element
    userInput.addEventListener('input', () => {
        // Get the selected option data-value attribute
        const option = document.querySelector(`#users option[value="${userInput.value}"][data-value]`);

        const value = option ? option.getAttribute('data-value') : '';

        // Update the select element value
        userSelect.value = value;
    });
</script>


@endpush

