@extends('layouts.admin.daycare_codes')
@push('extra_css')
    <link href="{{ asset('/css/reports.css') }}" rel="stylesheet">
    <style>
        .report_visual {
            margin-bottom: 30px;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('customPageContent')
    <div class="listing">

        <div id="container-fluid">

            <div class="col-12">
            
                <div class="row">
                    
                    <div class="col">
                    <b>PROMO CODE:</b> <button>{{$promocode}}</button> <span class="mr-2"><b>DAYCARE:</b><button> {{$owner->name ?? ''}}</button></span>
                   

                    <button id="exportButton" class=" btn btn-primary btn-sm d-flex justify-content-end float-right mb-2"><i class="fa fa-file-excel-o mx-2 my-1" ></i> Export</button>


                        <table class="table" style="text-align: center">
                            <thead></thead>
                            <tr class="tablehead">
                                <th style="font-weight:bold;"> #</th>
                                <th style="text-align: left;padding-left:2.5%;font-weight:bold;"> Namez </th>
                                <th style="text-align: left;padding-left:2.5%;font-weight:bold;"> Phone </th>
                                <th style="text-align: left;padding-left:2.5%;width:20%;font-weight:bold"> Date Purchased</th>
                                <th style="text-align: left;padding-left:2.5%;width:20%;font-weight:bold"> Subscription status</th>
                            



                            </tr>
                            </thead>
                            <tbody>
                               
                                @php $i = 1; 
                         
                                @endphp
                                @if(count($users) > 0)
                                @foreach ($users as $completion)
                                    <tr class="tablerow">
                                        <td class="tablecell">{{ $i++ }}</td>
                                        <td class="tablecell" style="text-align:left; padding-left:2.5%">
                                            {{ $completion->name ?? '' }}
                                        </td>
                                        <td class="tablecell" style="text-align:left; padding-left:2.5%">
                                        {{ $completion->phone_locale  }}{{$completion->phone}}
                                        </td>
                                        <td class="tablecell" style="text-align:left; padding-left:2.5%">
                                            {{ $completion->subscription_start_date ?? '' }}</td>
                                        <td class="tablecell" style="text-align:left; padding-left:2.5%">
                                            {{ $completion->subscription  ==1 ? 'Active' : 'Inactive' }}</td>
                                        

                                    </tr>
                                @endforeach
                                @else
                                <tr class="tablerow">
                                <td colspan="5" class="tablecell"> No records found for this promo code</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>


                    </div>
                </div>

            </div>

        </div>
    </div>
@stop
@push('extra_js')
    <script src="{{ twillAsset('main-free.js') }}" crossorigin></script>
    <!-- Include SheetJS library from CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.16.8/xlsx.full.min.js"></script>

<!-- Add event listener to export button -->
<script>
  // Function to export table data to Excel
  function exportToExcel() {
    // Get table element
    var table = document.querySelector('table');

    // Convert table to worksheet
    var ws = XLSX.utils.table_to_sheet(table);

    // Create workbook and add worksheet
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

    // Export workbook to Excel file
    XLSX.writeFile(wb, "daycare_code.xlsx");
  }

  // Add event listener to export button
  document.getElementById('exportButton').addEventListener('click', exportToExcel);
</script>

@endpush
