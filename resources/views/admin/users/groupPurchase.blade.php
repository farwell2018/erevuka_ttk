@extends('layouts.admin.purchases')
@push('extra_css')
    <link href="{{ asset('/css/reports.css') }}" rel="stylesheet">
    <style>
        .report_visual {
            margin-bottom: 30px;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('customPageContent')
    <div class="listing">

        <div id="container-fluid">

            <div class="col-12">
                <div class="row">
                    <div class="col">

                        <table class="table" style="text-align: center">
                            <thead></thead>
                            <tr class="tablehead">
                                <th> #</th>
                                <th style="text-align: left;padding-left:2.5%;font-weight:bold;"> Purchased By</th>
                                <th style="text-align: left;padding-left:2.5%;width:20%;font-weight:bold"> Contact </th>
                                <th style="text-align: left;padding-left:2.5%;width:20%;font-weight:bold"> Total number of codes</th>


                            </tr>
                            </thead>
                            <tbody>
                                @php $i = 1; @endphp
                                @foreach ($purchases as $completion)
                                
                                    <tr class="tablerow">
                                       
                                        <td class="tablecell" style="width:5%">
                                            {{ $i++ }}</td>
                                            @if(isset($completion->users->first_name) && isset($completion->users->first_name))
                                        <td class="tablecell" style="text-align:left; padding-left:2.5%; width:10%"><a
                                                href="{{ route('admin.user.group.purchase', [$completion->users->id]) }}">{{ $completion->users->first_name . ' ' . $completion->users->last_name }}</a>
                                        </td>
                                        @else
                                   
                                   <td class="tablecell" style="text-align:left; padding-left:2.5%; width:10%" >Unavailable</td>
                              
                               @endif
                               @if(isset($completion->users->phone_locale))
                                        <td class="tablecell" style="text-align:left; padding-left:2.5%">
                                            {{ $completion->users->phone_locale . '' . $completion->users->phone }}</td>
                                @else
                                <td class="tablecell" style="text-align:left; padding-left:2.5%; width:10%" >Unavailable</td>
                                @endif

                                        <td class="tablecell" style="text-align:left; padding-left:2.5%">
                                            {{ $completion->count }}</td>

                                            

                                    </tr>
                                   
                                @endforeach
                            </tbody>
                        </table>

                        {!! $purchases->links() !!}
                    </div>
                </div>

            </div>

        </div>
    </div>
@stop
@push('extra_js')
    <script src="{{ twillAsset('main-free.js') }}" crossorigin></script>
@endpush
