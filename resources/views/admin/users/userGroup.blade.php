@extends('layouts.admin.purchases')
@push('extra_css')
    <link href="{{ asset('/css/reports.css') }}" rel="stylesheet">
    <style>
        .report_visual {
            margin-bottom: 30px;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('customPageContent')
    <div class="listing">

        <div id="container-fluid">

            <div class="col-12">
                <div class="row">
                    <div class="col">

                        <table class="table" style="text-align: center">
                            <thead></thead>
                            <tr class="tablehead">
                                <th> #</th>
                                <th style="text-align: left;padding-left:2.5%"> Code </th>
                                <th style="text-align: left;padding-left:2.5%;width:20%"> Date Purchased</th>
                                <th style="text-align: left;padding-left:2.5%;width:20%"> Status</th>
                                <th style="text-align: left;padding-left:2.5%;width:20%"> Date Used</th>



                            </tr>
                            </thead>
                            <tbody>
                                @php $i = 1; @endphp
                                @foreach ($purchases as $completion)
                                    <tr class="tablerow">
                                        <td class="tablecell">{{ $i++ }}</td>
                                        <td class="tablecell" style="text-align:left; padding-left:2.5%">
                                            {{ $completion->unique_code }}
                                        </td>
                                        <td class="tablecell" style="text-align:left; padding-left:2.5%">
                                            {{ $completion->created_at }}</td>
                                        <td class="tablecell" style="text-align:left; padding-left:2.5%">
                                            {{ $completion->status == 1 ? 'Used' : '' }}</td>
                                        <td class="tablecell" style="text-align:left; padding-left:2.5%">
                                            {{ $completion->date_used }}</td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>


                    </div>
                </div>

            </div>

        </div>
    </div>
@stop
@push('extra_js')
    <script src="{{ twillAsset('main-free.js') }}" crossorigin></script>
@endpush
