@extends('twill::layouts.form')

@section('contentFields')
	@formField('medias', [
		'name' => 'cover',
		'label' => 'Sponsors Image',
		'withVideoUrl' => false
	])
    @formField('wysiwyg', [
        'name' => 'description',
        'label' => 'Short Description',
        'placeholder' => 'Introduction Text',
		'maxlength' => 300,
		'translated' => true,
	])
	
	@formField('input', [
		'name' => 'link',
		'label' => 'Link to sponsors site',
		'translated' => false,
	])

	@formField('input', [
		'name' => 'link_text',
		'label' => 'Link text',
		'translated' => true,
	])
@stop
