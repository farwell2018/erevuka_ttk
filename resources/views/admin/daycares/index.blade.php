@extends('layouts.admin.daycares')
@push('extra_css')
    <link href="{{ asset('/css/reports.css') }}" rel="stylesheet">
    <style>
        .report_visual {
            margin-bottom: 30px;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('customPageContent')
    <div class="listing">

        <div id="container-fluid">
            

            <div class="col-12">
                <div class="row">
                @if (session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
     @elseif(session()->has('error'))
     <div class="alert alert-success">
        {{ session()->get('error') }}
    </div>
@endif
                </div>
            <div class="row">

           
            <div class="col  mb-3">
    <form action="" method="GET" class="form-inline">
        <div class="form-group mr-2 mb-0">
            <label for="search" class="mr-2">Search:</label>
            <input type="text" class="form-control form-control-sm" name="search" value="{{ request('search') }}" placeholder="Enter search query">
        </div>
        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
        
    </form>

    


    
</div>

<div class="col  mb-3">
<form class="form-inline" method="post" action="{{route('admin.daycare.upload')}}" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    

    <div class="form-group mr-2 mb-0">
      
      <input type="file" name="file" class="form-control space-top-1">
    </div>
        <button type="submit" class="btn btn-primary btn-sm">Upload
        </button>
</form>
</div>
<a href="{{ route('admin.daycare.create')}}"  class=" btn btn-primary btn-sm d-flex justify-content-end mt-2 mb-3"><i class="fa fa-plus mr-2 mt-1 "></i> Add New</a>
</div>
                <div class="row">
                    <div class="col">

                        <table class="table" style="text-align: center">
                            <thead></thead>
                            <tr class="tablehead">
                                <th style="font-weight:bold;"> #</th>
                                <th style="font-weight:bold;text-align: left;padding-left:2.5%; width:10%"> Daycare ID</th>
                                <th style="font-weight:bold;text-align: left;padding-left:2.5%;width:10%"> Name </th>
                                <th style="font-weight:bold;text-align: left;padding-left:2.5%;width:20%"> Contact Person </th>
                                <th style="font-weight:bold;text-align: left;padding-left:2.5%;width:20%"> Location</th>
                                <th style="font-weight:bold;text-align: left;padding-left:2.5%;width:20%"> Cohort </th>
                                <th style="font-weight:bold;text-align: left;padding-left:2.5%;width:20%"> Joining Date </th>
                               


                            </tr>
                            </thead>
                            <tbody>
                            @php $i = 1; @endphp
                            @if(count($daycares) >0)
                                @foreach ($daycares as $daycare)
                                    <tr class="tablerow">
                                        <td class="tablecell" style="width:5%">
                                            {{ $i++ }}</td>
                                            <td class="tablecell" style="width:5%">
                                            <a
                                                href="{{ route('admin.daycare.users', [$daycare->id]) }}">
                                            {{ $daycare->daycare_id}}</a></td>
                                        <td class="tablecell" style="text-align:left; padding-left:2.5%; width:10%">
                                        {{$daycare->name}}
                                        </td>
                                        <td class="tablecell" style= "text-align:left; padding-left:2.5%; width:5%">
                                            {{ $daycare->contact_name}}</td>
                                        <td class="tablecell" style="text-align:left; padding-left:2.5%">
                                            {{ $daycare->location ?? '-'}}</td>
                                            <td class="tablecell" style="text-align:left; padding-left:2.5%">
                                            {{ $daycare->cohort ?? 0}}</td>

                                        <td class="tablecell" style="text-align:left; padding-left:2.5%">
                                       {{ $daycare->joining_date  }}</td>
                                       <td class="text-center">
                                        <div class="btn-group" role="group" aria-label="Second group">
                                            <a href="{{ route('admin.daycare.show', [$daycare->id]) }}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                                            <a href="{{ route('admin.daycare.destroy', [$daycare->id]) }}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </td>

                                       

                                    </tr>
                                @endforeach
                                @else

                                <tr class="tablerow">
                                        <td class="tablecell" colspan="10" style="width:5%">
                                        </td>
                                </tr>
                                @endif
                               
                            </tbody>
                        </table>
                        {!! $daycares->links() !!}

                    
                    </div>
                </div>

            </div>

        </div>
    </div>
@stop
@push('extra_js')
    <script src="{{ twillAsset('main-free.js') }}" crossorigin></script>
    
@endpush
