@extends('layouts.admin.daycares')
@push('extra_css')
    <link href="{{ asset('/css/reports.css') }}" rel="stylesheet">
    <style>
        .report_visual {
            margin-bottom: 30px;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('customPageContent')
    <div class="listing">

        <div id="container-fluid">
        <div class="col  mb-3">
    <form action="{{ route('admin.promo.codes') }}" method="GET" class="form-inline">
        <div class="form-group mr-2 mb-0">
            <label for="search" class="mr-2">Search:</label>
            <input type="text" class="form-control form-control-sm" name="search" value="{{ request('search') }}" placeholder="Enter search query">
        </div>
        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
        
    </form>


    
</div>

            <div class="col-12">
                <div class="row">
                    <div class="col">

                        <table class="table" style="text-align: center">
                            <thead></thead>
                            <tr class="tablehead">
                                <th style="font-weight:bold;"> #</th>
                                <th style="text-align: left;padding-left:2.5%;font-weight:bold;"> Name </th>
                                <th style="text-align: left;padding-left:2.5%;width:20%;font-weight:bold;"> Date Purchased</th>
                                <th style="text-align: left;padding-left:2.5%;width:20%;font-weight:bold;"> Subscription status</th>
                            



                            </tr>
                            </thead>
                            <tbody>
                               
                                @php $i = 1; 
                         
                                @endphp
                                @if(count($daycareUsers) > 0)
                                @foreach ($daycareUsers as $completion)
                                    <tr class="tablerow">
                                        <td class="tablecell">{{ $i++ }}</td>
                                        <td class="tablecell" style="text-align:left; padding-left:2.5%">
                                            {{ $completion->name }}
                                        </td>
                                        <td class="tablecell" style="text-align:left; padding-left:2.5%">
                                            {{ $completion->phone }}</td>
                                        <td class="tablecell" style="text-align:left; padding-left:2.5%">
                                            {{ $completion->subscription  ==1 ? 'Active' : 'Inactive' }}</td>
                                        

                                    </tr>
                                @endforeach
                                @else
                                <tr class="tablerow">
                                <td colspan="5" class="tablecell"> No records found for this daycare</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        {!! $daycareUsers->links() !!}

                    </div>
                </div>

            </div>

        </div>
    </div>
@stop
@push('extra_js')
    <script src="{{ twillAsset('main-free.js') }}" crossorigin></script>
@endpush
