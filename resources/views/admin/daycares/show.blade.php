@extends('layouts.admin.daycares')
@push('extra_css')
@php
use App\Repositories\UserRepository;
 $userList = app(UserRepository::class)->where('published', 1)->where('id', '!=', 1)->pluck('name', 'id');


@endphp
    <link href="{{ asset('/css/reports.css') }}" rel="stylesheet">
    <style>
        .report_visual {
            margin-bottom: 30px;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('customPageContent')
<div class="listing">
    <div id="container-fluid">
        <div class="col-6">
            <div class="row">
                <div class="col  mb-3">
                    <form action="{{ route('admin.daycare.update',[$daycare->id]) }}">
                    <div class="form-group row align-items-center">
                            <label for="daycare_id" class="col-form-label text-md-right">Daycare id</label>
                            <input type="text" class="form-control" id="daycare_id" name="daycare_id" value="{{$daycare->daycare_id}}" placeholder="Daycare id">
                        </div>
                        <div class="form-group row align-items-center">
                            <label for="title" class="col-form-label text-md-right">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{$daycare->name}}" placeholder="Name">
                        </div>

                        <div class="form-group row align-items-center">
                            <label for="user_id">Contact Name</label>
                            <input type="text" list="users" id="user-input" name="user_id" class="form-control" value="{{$daycare->contact_name}}" placeholder="Search user">
                            <datalist id="users">
                                <!-- Populate the datalist options with PHP/blade -->
                                @foreach($userList as $id => $name)
                                <option value="{{ $name }}" data-value="{{ $id }}">

                                @endforeach
                            </datalist>
                        </div>
                        <div class="form-group row align-items-center">
                            <label for="phone" class="col-form-label text-md-right">Contact Number</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="{{$daycare->contact_number}}" placeholder="phone">
                        </div>
                        <div class="form-group row align-items-center">
                            <label for="location" class="col-form-label text-md-right">Location</label>
                            <input type="text" class="form-control" id="location" name="location" value="{{$daycare->location}}" placeholder="Location">
                        </div>

                        <div class="form-group row align-items-center">
                            <label for="cohort" class="col-form-label text-md-right">Cohort</label>
                            <input type="text" class="form-control" id="cohort" value="{{$daycare->cohort}}" name="cohort" placeholder="cohort">
                        </div>
                        <div class="form-group row align-items-center">
                            <label for="joining_date" class="col-form-label text-md-right">Joining Date</label>
                            <input type="text" class="form-control" id="joining_date" name="joining_date" value="{{$daycare->joining_date}}" placeholder="1/25/2023">
                        </div>

                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@push('extra_js')
    <script src="{{ twillAsset('main-free.js') }}" crossorigin></script>
    <script>
    // Get the input and select elements
    const userInput = document.getElementById('user-input');
    const userSelect = document.getElementById('user_id');

    // Add an event listener to the input element
    userInput.addEventListener('input', () => {
        // Get the selected option data-value attribute
        const option = document.querySelector(`#users option[value="${userInput.value}"][data-value]`);

        const value = option ? option.getAttribute('data-value') : '';

        // Update the select element value
        userSelect.value = value;
    });


</script>


@endpush

