@extends('twill::layouts.form')

@section('contentFields')
@formField('input', [
    'name' => 'icon_name',
    'label' => 'Icon Name',
])

@formField('multi_select', [
    'name' => 'user_role',
    'label' => 'User Role',
    'options' => $user_roles
])
@stop
