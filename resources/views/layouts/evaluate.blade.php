<?php
    header("Cache-Control: no-cache, must-revalidate");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">
    <meta name="keywords" content="quality childcare (vol 900), child safety locks for doors (vol700), Health safety and nutrition for the young kid (vol200),free child safety kit (vol200),paleo diet for kids (vol30),childhood dreams(vol 400),child development babies dolls (vol 150),child development programs (vol 70),importance of nurture in child development (vol 40),effects of daycare on child development (vol 20),child development stories (vol 0-10),finance digitalization(vol 600),finance digitalization(vol 600),record books(vol 450),daycare start up cost(vol 40),how to run successful daycare (vol 10),decorating kitchen shelves (vol 250),preventing fires in the kitchen (vol 150),revenue based loans (vol 100),how to build a kitchen (vol 100),peer learning (vol 500),review questions (vol 450),peer survey (20)">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-store, no-cache, must-revalidate" />
    <meta http-equiv="expires" content="-1" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
     
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="refresh" content="{{ config('session.lifetime') * 60 }}">
    
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
<script type = "text/javascript" src = "//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js" ></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/27089a3c31.js" crossorigin="anonymous"></script>

    <!-- Styles -->
    <link rel="preload stylesheet" href="{{ asset('css/app.css') }}" as="style" type="text/css">
    <link rel="preload stylesheet" href="{{ asset('css/main.css') }}" as="style">
    @yield('css')
</head>
<body >
    <div id="app">

      @include('layouts.partials._header')


        @include('includes.modals.courseerrors')
        @include('includes.modals.coursemessages')
        @include('includes.modals.messages')
        @include('includes.modals.errors')
        @include('includes.modals.notification')
        @include('includes.modals.registerLogin')
        @include('includes.modals.ticket')
        <main class="py-4">


            @yield('content')

        </main>

<!-- Back to top button -->
{{-- <a id="support" data-toggle="modal" data-target="#ticketModal"><span>Support</span></a> --}}
<a id="back-top"><span>Back Up</span></a>

     @include('layouts.partials._footer')

    </div>
  @yield('js')
<script>
  var $star_rating = $('.star-rating .fa');

  var SetRatingStar = function() {
    return $star_rating.each(function() {
      if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
        return $(this).removeClass('fa-star-o').addClass('fa-star');
      } else {
        return $(this).removeClass('fa-star').addClass('fa-star-o');
      }
    });
  };

  $star_rating.on('click', function() {
    console.log($star_rating.siblings('input.rating-value').val($(this).data('rating')));
    $star_rating.siblings('input.rating-value').val($(this).data('rating'));
    return SetRatingStar();
  });

  SetRatingStar();
  $(document).ready(function() {

  });

  </script>
 <!-- <script src="{{ asset('js/app.js') }}" type="text/javascript" ></script> -->
</body>
</html>