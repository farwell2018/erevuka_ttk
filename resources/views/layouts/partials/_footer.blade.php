@php
use App\Models\Socialmedia;
use App\Models\Menus;
use App\Models\FooterText;

$facebook = 'facebook';
$twitter = 'twitter';
$youtube = 'youtube';
$linkedin = 'linkedin';

$socialfacebook = Socialmedia::where('key', $facebook)
    ->published()
    ->first();
$socialtwitter = Socialmedia::where('key', $twitter)
    ->published()
    ->first();
$socialyoutube = Socialmedia::where('key', $youtube)
    ->published()
    ->first();
$sociallinkedin = Socialmedia::where('key', $linkedin)
    ->published()
    ->first();
   

$mainmenus = Menus::where('menu_type', 1)
    ->published()
    ->orderBy('position', 'asc')
    ->get();

$mainoffice = FooterText::where('id', 1)
    ->published()
    ->first();

$fieldoffice = FooterText::where('id', 2)
    ->published()
    ->first();

@endphp
@if ((new \Jenssegers\Agent\Agent())->isDesktop())

    <footer>
        <div class="container-fluid">
            <div class="row ">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3 ">
                            <a class="navbar-brand nav-brand-v1" href="{{ url('/') }}">
                                <img src="{{ asset('images/icons/logo.png') }}"
                                    alt="{{ config('app.name', 'Laravel') }}">
                            </a>
                            <div class="footer-span ">

                                </br>

                                @if ($socialfacebook)
                                    <a href="{{ $socialfacebook->description }}" target="_blank"><img
                                            src="{{ asset('images/icons/Facebook.svg') }}" alt="facebookicon"
                                            title="Follow us on Facebook"></a>
                                @else
                                    <a href="https://www.facebook.com/TinyTotos/"><img src="{{ asset('images/icons/Facebook.svg') }}" alt="facebookicon"
                                            title="Follow us on Facebook"></a>
                                @endif
                                @if ($socialtwitter)
                                    <a href="{{ $socialtwitter->description }}" target="_blank"><img
                                            src="{{ asset('images/icons/Twitter.svg') }}" alt="twittericon"
                                            title="Follow us on Twitter"></a>
                                @else
                                    <a href="https://twitter.com/tinytotos?lang=en"><img src="{{ asset('images/icons/Twitter.svg') }}" alt="twittericon"
                                            title="Follow us on Twitter"></a>
                                @endif

                                @if ($sociallinkedin)
                                    <a href="{{ $sociallinkedin->description }}" target="_blank"><img
                                            src="{{ asset('images/icons/Linkedin.svg') }}" alt="linkedinicon"
                                            title="Follow us on Linkedin"></a>
                                @else
                                    <a href="https://www.linkedin.com/company/tiny-totos-kenya"><img src="{{ asset('images/icons/Linkedin.svg') }}" alt="linkedinicon"
                                            title="Follow us on Linkedin"></a>
                                @endif


                                @if ($socialyoutube)
                                    <a href="{{ $socialyoutube->description }}" target="_blank"><img
                                            src="{{ asset('images/icons/YouTube.svg') }}" alt="youtubeicon"
                                            title="Follow us on Youtube"></a>
                                @else
                                    <a href="https://www.youtube.com/watch?v=vvfb4QGM1zs"><img src="{{ asset('images/icons/YouTube.svg') }}" alt="youtubeicon"
                                            title="Follow us on Youtube"></a>
                                @endif
                            </div>

                        </div>

                        <div class="col-md-3 ">
                            <div class="footer-heading">
                                <span>Menu</span>
                            </div>

                            <ul class="footer-ul">
                                <?php foreach ($mainmenus as $el) : ?>

                                <li><a href="{{ route('pages', ['key' => $el['key']]) }}"> {{ $el['title'] }}</a>
                                </li>
                                <?php endforeach; ?>


                            </ul>
                        </div>

                        <div class="col-md-3 ">
                            @if ($mainoffice)
                                <div class="footer-heading">
                                    <span>{{ $mainoffice->title }}</span>
                                </div>
                                <div class="footer-description">
                                    {!! $mainoffice->description !!}
                                </div>
                            @endif
                        </div>

                        <div class="col-md-3 ">
                            @if ($fieldoffice)
                                <div class="footer-heading">
                                    <span>{{ $fieldoffice->title }}</span>
                                </div>
                                <div class="footer-description">
                                    {!! $fieldoffice->description !!}
                                </div>
                            @endif
                        </div>

                    </div>


                </div>


            </div>
        </div>

        {{-- <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="footer-text">
                    <ul>
                        <li><small>Copyright &copy; <?php echo date('Y'); ?> Erevuka | All Rights Reserved</small>&nbsp;&nbsp;&nbsp;<small class="bordered-left">Powered By <a href="https://farwell-consultants.com"  aria-label="Farwell Innovations(opens in new tab)"  target="_blank">Farwell Innovations Ltd</a></small></li>
                    </ul>
                </div>
            </div>
        </div> --}}
        </div>
    </footer>
@endif
@if ((new \Jenssegers\Agent\Agent())->isMobile())


    <footer style="">
        <div class="container-fluid">
            <div class="row ">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-4 col-6 ">
                            <a class=" nav-brand-v1 footer-navbar-brand footer-heading" href="{{ url('/') }}">
                                <img src="{{ asset('images/icons/logo.png') }}"
                                    alt="{{ config('app.name', 'Laravel') }}">
                            </a>
                            <div class="footer-span ">

                            @if ($socialfacebook)
                                    <a href="{{ $socialfacebook->description }}" target="_blank"><img
                                            src="{{ asset('images/icons/Facebook.svg') }}" alt="facebookicon"
                                            title="Follow us on Facebook"></a>
                                @else
                                    <a href="https://www.facebook.com/TinyTotos/"><img src="{{ asset('images/icons/Facebook.svg') }}" alt="facebookicon"
                                            title="Follow us on Facebook"></a>
                                @endif
                                @if ($socialtwitter)
                                    <a href="{{ $socialtwitter->description }}" target="_blank"><img
                                            src="{{ asset('images/icons/Twitter.svg') }}" alt="twittericon"
                                            title="Follow us on Twitter"></a>
                                @else
                                    <a href="https://twitter.com/tinytotos?lang=en"><img src="{{ asset('images/icons/Twitter.svg') }}" alt="twittericon"
                                            title="Follow us on Twitter"></a>
                                @endif

                                @if ($sociallinkedin)
                                    <a href="{{ $sociallinkedin->description }}" target="_blank"><img
                                            src="{{ asset('images/icons/Linkedin.svg') }}" alt="linkedinicon"
                                            title="Follow us on Linkedin"></a>
                                @else
                                    <a href="https://www.linkedin.com/company/tiny-totos-kenya"><img src="{{ asset('images/icons/Linkedin.svg') }}" alt="linkedinicon"
                                            title="Follow us on Linkedin"></a>
                                @endif


                                @if ($socialyoutube)
                                    <a href="{{ $socialyoutube->description }}" target="_blank"><img
                                            src="{{ asset('images/icons/YouTube.svg') }}" alt="youtubeicon"
                                            title="Follow us on Youtube"></a>
                                @else
                                    <a href="https://www.youtube.com/watch?v=vvfb4QGM1zs"><img src="{{ asset('images/icons/YouTube.svg') }}" alt="youtubeicon"
                                            title="Follow us on Youtube"></a>
                                @endif
                            </div>


                        </div>

                        <div class=" col-sm-4 col-6 ">
                            @if ($mainoffice)
                                <div class="footer-heading">
                                    <span>{{ $mainoffice->title }}</span>
                                </div>
                                <div class="footer-description">
                                    {!! $mainoffice->description !!}
                                </div>
                            @endif


                        </div>
                        <div class="col-sm-4  d-none d-sm-block ">
                            @if ($fieldoffice)
                                <div class="footer-heading">
                                    <span>{{ $fieldoffice->title }}</span>
                                </div>
                                <div class="footer-description">
                                    {!! $fieldoffice->description !!}
                                </div>
                            @endif
                        </div>



                    </div>


                </div>


            </div>
        </div>

        {{-- <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="footer-text">
                <ul>
                    <li><small>Copyright &copy; <?php echo date('Y'); ?> Erevuka | All Rights Reserved</small>&nbsp;&nbsp;&nbsp;<small class="bordered-left">Powered By <a href="https://farwell-consultants.com"  aria-label="Farwell Innovations(opens in new tab)"  target="_blank">Farwell Innovations Ltd</a></small></li>
                </ul>
            </div>
        </div>
    </div> --}}
        </div>
    </footer>

@endif
