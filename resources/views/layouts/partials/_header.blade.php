<?php
use App\Models\Menus;

$mainmenus = Menus::where('menu_type', 1)
    ->published()
    ->orderBy('position', 'asc')
    ->get();
$sidemenus = Menus::where('menu_type', 2)
    ->published()
    ->orderBy('position', 'asc')
    ->get();
?>
@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    <nav class="navbar sticky-top navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>

                <div id="navbar-close" class="hidden">
                    <span class="glyphicon glyphicon-remove"></span>
                </div>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset('images/icons/logo.png') }}" alt="{{ config('app.name', 'Laravel') }}">
            </a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                </ul>
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->


                    <?php foreach ($mainmenus as $el) : ?>

                    <li class="nav-item">

                        @if (count($el->childs))

                            @if (Auth::check() && Auth::user()->role == 1 || Auth::check() && Auth::user()->role == 2 || Auth::check() && Auth::user()->role == 3 || Auth::check() && Auth::user()->role == 4)
                            <a id="exploreDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ $el['title'] }}</a>
                                <div class="dropdown-menu dropdown-menu-right-connect profile-menu connect-menu"
                                    aria-labelledby="navbarDropdown">
                                    <span class="dropdown-menu-arrow"></span>
                                    <?php foreach ($el->childs as $child) : ?>
                                    @if (\Route::current()->getName() == $child['route'])
                                        <a class="nav-link active dropdown-item"
                                            href="{{ route('pages', ['key' => $child['key']]) }}">
                                            {{ $child['title'] }}</a>
                                    @else
                                        <a class="nav-link dropdown-item"
                                            href="{{ route('pages', ['key' => $child['key']]) }}">
                                            {{ $child['title'] }}</a>
                                    @endif
                                    <?php endforeach; ?>

                                </div>

                            @elseif(Auth::check() && Auth::user()->role === "ADMIN" || Auth::check() && Auth::user()->role == "SUPERADMIN")
                                    <a id="exploreDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ $el['title'] }}</a>
                                <div class="dropdown-menu dropdown-menu-right-connect profile-menu"
                                    aria-labelledby="navbarDropdown">
                                    <span class="dropdown-menu-arrow"></span>
                                    <?php foreach ($el->childs as $child) : ?>
                                    @if (\Route::current()->getName() == $child['route'])
                                        <a class=" active dropdown-item"
                                            href="{{ route('pages', ['key' => $child['key']]) }}">
                                            {{ $child['title'] }}</a>
                                    @else
                                        <a class=" dropdown-item"
                                            href="{{ route('pages', ['key' => $child['key']]) }}">
                                            {{ $child['title'] }}</a>
                                    @endif
                                    <?php endforeach; ?>

                                </div>


                            @else
                            <a id="exploreDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ $el['title'] }}</a>
                                <div class="dropdown-menu dropdown-menu-right-connect profile-menu"
                                    aria-labelledby="navbarDropdown">
                                    <span class="dropdown-menu-arrow"></span>
                                    <?php foreach ($el->childs as $child) : ?>
                                    @if (\Route::current()->getName() == $child['route'])
                                        <a class="dropdown-item"
                                            href="{{ route('pages', ['key' => $child['key']]) }}">
                                            {{ $child['title'] }}</a>
                                    @else
                                        <a class="dropdown-item"
                                            href="{{ route('pages', ['key' => $child['key']]) }}">
                                            {{ $child['title'] }}</a>
                                    @endif
                                    <?php endforeach; ?>

                                </div>
                            @endif
                        @else
                            @if (\Route::current()->getName() == $el['key'] || strpos(\Request::url(), $el['key']) !== false)
                                <a class="nav-link active" href="{{ route('pages', ['key' => $el['key']]) }}">
                                    {{ $el['title'] }}</a>
                            @else
                                <a class="nav-link" href="{{ route('pages', ['key' => $el['key']]) }}">
                                    {{ $el['title'] }}</a>
                            @endif
                        @endif
                    </li>
                    <?php endforeach; ?>

                    @if (!Auth::check())
                        <li class="nav-item section-auth-2">

                            @if (Route::current()->getName() === 'register')
                                <button type="button" class="btn btn-overall btn_register_active"
                                    onclick="location.href='{{ route('register') }}'">Register</i></button>
                            @else
                                <button type="button" class="btn btn-overall btn_register"
                                    onclick="location.href='{{ route('register') }}'">Register</i></button>
                                {{-- <a class="nav-link" href="{{ route('login') }}/">{{ __('Register') }}</a> --}}
                            @endif
                        </li>
                        <li class="nav-item section-auth">

                            @if (Route::current()->getName() === 'login')
                                <button type="button" class="btn btn-overall btn_login_active"
                                    onclick="location.href='{{ route('login') }}'">Login </button>
                            @else
                                <button type="button" class="btn btn-overall btn_login"
                                    onclick="location.href='{{ route('login') }}'">Login </button>
                            @endif
                        </li>
                    @else

                    <li class="nav-item section-auth" style="margin-top: 5px;display:initial"> <a href="{{ route('profile.courses') }}" class="btn btn-overall purple" style=" padding: 0.375rem 0.75rem;border-radius: 0;
                        font-size: 16px;"> My Enrolled Courses</a></li>

                        <li class="nav-item dropdown profile-dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                @if (Auth::user()->profile_pic != null)
                                    @php $ppic = Auth::user()->profile_pic; @endphp
                                @else
                                    @php $ppic = 'images.jpg'; @endphp
                                @endif
                                <img src="{{ asset('uploads/' . $ppic) }}" alt="{{ Auth::user()->name }}"> &nbsp;
                                {{ Auth::user()->name }}
                            </a>



                            <div class="dropdown-menu dropdown-menu-right profile-menu"
                                aria-labelledby="navbarDropdown">
                                <span class="dropdown-menu-arrow"></span>

                                <a class="dropdown-item" href="{{ route('profile.courses') }}">My Enrolled Courses</a>
                                <a class="dropdown-item" href="{{ route('profile.achievements') }}">My
                                    Achievements</a>


                                <a class="dropdown-item menu-breaker" href=""><span></span></a>
                                <a class="dropdown-item" href="{{ route('profile.edit') }}/">Edit Profile</a>
                                @if (Auth::user()->is_admin)
                                    <a class="dropdown-item" href="/admin" target="_blank">Admin Section</a>
                                @endif
                                {{-- @if (Auth::user()->is_company_admin)
                                    <a class="dropdown-item " href="/admin" target="_blank">Admin Dashboard</a>
                                @endif --}}
                                <a class="dropdown-item " href="{{ route('logout') }}/" onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                    <span>{{ __('Logout') }}</span>
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </form>
                            </div>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    @elseif ((new \Jenssegers\Agent\Agent())->isTablet())
    <nav class="navbar sticky-top navbar-expand-lg navbar-light bg-white shadow-sm">
    <a class="navbar-brand" href="{{ url('/') }}">
    <img src="{{ asset('images/icons/logo.png') }}" alt="{{ config('app.name', 'Laravel') }}">
</a>
<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <!-- <span class="navbar-toggler-icon"></span> -->
    <span class="fa fa-bars" style="color:#520e33;"></span>

</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
        <!-- Authentication Links -->


        <?php foreach ($mainmenus as $el) : ?>

        <li class="nav-item">

            @if (count($el->childs))

                @if (Auth::check() && Auth::user()->role == 1)
                    @if (\Route::current()->getName() == 'caring-for-your-child' || strpos(\Request::url(), 'caring-for-your-child') !== false)
                        <a class="nav-link active"
                            href="{{ route('pages', ['key' => 'caring-for-your-child']) }}"> Caring for
                            your
                            child</a>
                    @else
                        <a class="nav-link"
                            href="{{ route('pages', ['key' => 'caring-for-your-child']) }}"> Caring for
                            your
                            child</a>
                    @endif
                @else
                    <a id="exploreDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ $el['title'] }}</a>
                    <div class="dropdown-menu   profile-menu connect-menu"
                        aria-labelledby="navbarDropdown">
                        <span class="dropdown-menu-arrow"></span>
                        <?php foreach ($el->childs as $child) : ?>
                        @if (\Route::current()->getName() == $child['route'])
                            <a class="nav-link active dropdown-item"
                                href="{{ route('pages', ['key' => $child['key']]) }}">
                                {{ $child['title'] }}</a>
                        @else
                            <a class="nav-link dropdown-item"
                                href="{{ route('pages', ['key' => $child['key']]) }}">
                                {{ $child['title'] }}</a>
                        @endif
                        <?php endforeach; ?>

                    </div>
                @endif
            @else
                @if (\Route::current()->getName() == $el['key'] || strpos(\Request::url(), $el['key']) !== false)
                    <a class="nav-link active" href="{{ route('pages', ['key' => $el['key']]) }}">
                        {{ $el['title'] }}</a>
                @else
                    <a class="nav-link" href="{{ route('pages', ['key' => $el['key']]) }}">
                        {{ $el['title'] }}</a>
                @endif
            @endif
        </li>
        <?php endforeach; ?>

        @if (!Auth::check())
            <li class="nav-item section-auth-2">

                @if (Route::current()->getName() === 'register')
                    <button type="button" class="btn btn-overall btn_register_active"
                        onclick="location.href='{{ route('register') }}'">Register</i></button>
                @else
                    <button type="button" class="btn btn-overall btn_register"
                        onclick="location.href='{{ route('register') }}'">Register</i></button>
                    {{-- <a class="nav-link" href="{{ route('login') }}/">{{ __('Register') }}</a> --}}
                @endif
            </li>
            <li class="nav-item section-auth-2">

                @if (Route::current()->getName() === 'login')
                    <button type="button" class="btn btn-overall btn_login_active"
                        onclick="location.href='{{ route('login') }}'">Login </button>
                @else
                    <button type="button" class="btn btn-overall btn_login"
                        onclick="location.href='{{ route('login') }}'">Login </button>
                @endif
            </li>
        @else
            <li class="nav-item dropdown profile-dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    @if (Auth::user()->profile_pic != null)
                        @php $ppic = Auth::user()->profile_pic; @endphp
                    @else
                        @php $ppic = 'images.jpg'; @endphp
                    @endif
                    <img src="{{ asset('uploads/' . $ppic) }}" alt="{{ Auth::user()->name }}"> &nbsp;
                    {{ Auth::user()->name }}
                </a>

                <div class="dropdown-menu dropdown-menu-right profile-menu" aria-labelledby="navbarDropdown">
                    <span class="dropdown-menu-arrow"></span>

                    <a class="dropdown-item" href="{{ route('profile.courses') }}">My Enrolled Courses</a>
                    <a class="dropdown-item" href="{{ route('profile.achievements') }}">My
                        Achievements</a>


                    <a class="dropdown-item menu-breaker" href=""><span></span></a>
                    <a class="dropdown-item" href="{{ route('profile.edit') }}/">Edit Profile</a>
                    @if (Auth::user()->is_admin)
                        <a class="dropdown-item" href="/admin" target="_blank">Admin Section</a>
                    @endif
                    @if (Auth::user()->is_company_admin)
                        <a class="dropdown-item " href="/admin" target="_blank">Faclilitator Dashboard</a>
                    @endif
                     <a class="dropdown-item " href="{{ route('logout') }}/" onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                        <span>{{ __('Logout') }}</span>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                        style="display: none;">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form> 
                </div>
            </li>
        @endif
    </ul>
</div>
</nav>
@else
    <nav class="navbar sticky-top navbar-expand-lg navbar-light bg-white shadow-sm">

        <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <!-- <span class="navbar-toggler-icon"></span> -->
            <span class="fa fa-bars" style="color:#520e33;"></span>

        </button>
    <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ asset('images/icons/logo.png') }}" alt="{{ config('app.name', 'Laravel') }}">
        </a>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->


                <?php foreach ($mainmenus as $el) : ?>

                <li class="nav-item">

                    @if (count($el->childs))

                        @if (Auth::check() && Auth::user()->role == 1)
                            @if (\Route::current()->getName() == 'caring-for-your-child' || strpos(\Request::url(), 'caring-for-your-child') !== false)
                                <a class="nav-link active"
                                    href="{{ route('pages', ['key' => 'caring-for-your-child']) }}"> Caring for
                                    your
                                    child</a>
                            @else
                                <a class="nav-link"
                                    href="{{ route('pages', ['key' => 'caring-for-your-child']) }}"> Caring for
                                    your
                                    child</a>
                            @endif
                        @else
                            <a id="exploreDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ $el['title'] }}</a>
                            <div class="dropdown-menu dropdown-menu-right-connect profile-menu"
                                aria-labelledby="navbarDropdown">
                                <span class="dropdown-menu-arrow"></span>
                                <?php foreach ($el->childs as $child) : ?>
                                @if (\Route::current()->getName() == $child['route'])
                                    <a class=" active dropdown-item"
                                        href="{{ route('pages', ['key' => $child['key']]) }}">
                                        {{ $child['title'] }}</a>
                                @else
                                    <a class=" dropdown-item"
                                        href="{{ route('pages', ['key' => $child['key']]) }}">
                                        {{ $child['title'] }}</a>
                                @endif
                                <?php endforeach; ?>

                            </div>
                        @endif
                    @else
                        @if (\Route::current()->getName() == $el['key'] || strpos(\Request::url(), $el['key']) !== false)
                            <a class="nav-link active" href="{{ route('pages', ['key' => $el['key']]) }}">
                                {{ $el['title'] }}</a>
                        @else
                            <a class="nav-link" href="{{ route('pages', ['key' => $el['key']]) }}">
                                {{ $el['title'] }}</a>
                        @endif
                    @endif
                </li>
                <?php endforeach; ?>

                @if (!Auth::check())
                    <li class="nav-item section-auth-2">

                        @if (Route::current()->getName() === 'register')
                            <button type="button" class="btn btn-overall btn_register_active"
                                onclick="location.href='{{ route('register') }}'">Register</i></button>
                        @else
                            <button type="button" class="btn btn-overall btn_register"
                                onclick="location.href='{{ route('register') }}'">Register</i></button>
                            {{-- <a class="nav-link" href="{{ route('login') }}/">{{ __('Register') }}</a> --}}
                        @endif
                    </li>
                    <li class="nav-item section-auth">

                        @if (Route::current()->getName() === 'login')
                            <button type="button" class="btn btn-overall btn_login_active"
                                onclick="location.href='{{ route('login') }}'">Login </button>
                        @else
                            <button type="button" class="btn btn-overall btn_login"
                                onclick="location.href='{{ route('login') }}'">Login </button>
                        @endif
                    </li>
                @else
                    <li class="nav-item dropdown profile-dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            @if (Auth::user()->profile_pic != null)
                                @php $ppic = Auth::user()->profile_pic; @endphp
                            @else
                                @php $ppic = 'images.jpg'; @endphp
                            @endif
                            <img src="{{ asset('uploads/' . $ppic) }}" alt="{{ Auth::user()->name }}"> &nbsp;
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right profile-menu" aria-labelledby="navbarDropdown">
                            <span class="dropdown-menu-arrow"></span>

                            <a class="dropdown-item" href="{{ route('profile.courses') }}">My Enrolled Courses</a>
                            <a class="dropdown-item" href="{{ route('profile.achievements') }}">My
                                Achievements</a>


                            <a class="dropdown-item menu-breaker" href=""><span></span></a>
                            <a class="dropdown-item" href="{{ route('profile.edit') }}/">Edit Profile</a>
                            @if (Auth::user()->is_admin)
                                <a class="dropdown-item" href="/admin" target="_blank">Admin Section</a>
                            @endif
                            @if (Auth::user()->is_company_admin)
                                <a class="dropdown-item " href="/admin" target="_blank">Faclilitator Dashboard</a>
                            @endif
                             <a class="dropdown-item " href="{{ route('logout') }}/" onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                                <span>{{ __('Logout') }}</span>
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                style="display: none;">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form> 
                        </div>
                    </li>
                @endif
            </ul>
        </div>
        {{-- @if (Auth::check())
            <div class="navbar-text">
                    <i class="fa fa-power-off" aria-hidden="true"><a class="logout-link" href="{{ route('logout') }}/" onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                                <span class="logout-text">{{ __('Logout') }}</span>
                            </a></i>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>
@endif --}}
    </nav>


@endif
