<?php
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">

    <meta name="keywords"
        content="quality childcare (vol 900), child safety locks for doors (vol700), Health safety and nutrition for the young kid (vol200),free child safety kit (vol200),paleo diet for kids (vol30),childhood dreams(vol 400),child development babies dolls (vol 150),child development programs (vol 70),importance of nurture in child development (vol 40),effects of daycare on child development (vol 20),child development stories (vol 0-10),finance digitalization(vol 600),finance digitalization(vol 600),record books(vol 450),daycare start up cost(vol 40),how to run successful daycare (vol 10),decorating kitchen shelves (vol 250),preventing fires in the kitchen (vol 150),revenue based loans (vol 100),how to build a kitchen (vol 100),peer learning (vol 500),review questions (vol 450),peer survey (20)">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-store, no-cache, must-revalidate" />
    <meta http-equiv="expires" content="-1" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="refresh" content="{{ config('session.lifetime') * 60 }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <script rel="preload" src="{{ asset('js/app.js') }}" as="script"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/27089a3c31.js" crossorigin="anonymous"></script>

    <!-- Styles -->
    <link rel="preload stylesheet" href="{{ asset('css/app.css') }}" as="style" type="text/css">
    <link rel="preload stylesheet" href="{{ asset('css/animate.css') }}" as="style" type="text/css">
    <link rel="preload stylesheet" href="{{ asset('css/main.css') }}" as="style">

    <link rel="stylesheet" href="{{ asset('assets/owl-carousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/owl-carousel/assets/owl.theme.default.min.css') }}">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/css/star-rating.min.css" />

    <link rel="preload stylesheet" href="{{ asset('emojis/emoji-picker-main/lib/css/emoji.css') }}" as="style"
        type="text/css">
    <link rel="preload stylesheet" href="{{ asset('emojis/emoji-picker-main/lib/css/emoji.css') }}" as="style"
        type="text/css">

    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <link href="{{ asset('css/mobile.css') }}" rel="stylesheet" media="screen and (max-width: 840px)">
    @endif

    <!-- Google Tag Manager -->
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'G-H8TMVGQ4CN');
    </script>
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-N6QJWJD');
    </script>
    <!-- End Google Tag Manager -->

    @yield('css')
</head>

<body>
    <div id="app">



        @include('layouts.partials._header')

        @if (Session::has('course_success'))
            <script>
                jQuery(document).ready(function($) {

                    $("#CourseSuccess").addClass('show');
                });
            </script>
        @elseif(Session::has('course_errors'))
            <script>
                jQuery(document).ready(function($) {
                    $("#CourseErrors").addClass('show');
                });
            </script>
        @endif

        @include('includes.modals.courseerrors')
        @include('includes.modals.coursemessages')
        @yield('content')

        <!-- Back to top button -->

        @include('layouts.partials._footer')
    </div>
    <script src="{{ asset('assets/jquery-ui-1.12.1/external/jquery/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/jquery-ui-1.12.1/jquery-ui.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/owl-carousel/owl.carousel.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

    <script src="{{ asset('emojis/emoji-picker-main/lib/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('emojis/emoji-picker-main/lib/js/util.js') }}" type="text/javascript"></script>
    <script src="{{ asset('emojis/emoji-picker-main/lib/js/jquery.emojiarea.js') }}" type="text/javascript"></script>
    <script src="{{ asset('emojis/emoji-picker-main/lib/js/emoji-picker.js') }}" type="text/javascript"></script>

    <script src="{{ asset('emojis/emoji-picker-main/lib/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('emojis/emoji-picker-main/lib/js/util.js') }}" type="text/javascript"></script>
    <script src="{{ asset('emojis/emoji-picker-main/lib/js/jquery.emojiarea.js') }}" type="text/javascript"></script>
    <script src="{{ asset('emojis/emoji-picker-main/lib/js/emoji-picker.js') }}" type="text/javascript"></script>

    @yield('js')




    <script>
        function myFunction() {
            var elmnt = document.getElementById("home-scroll");
            elmnt.scrollIntoView();
        }


        $(function() {

            $('#navbarSupportedContent')
                .on('shown.bs.collapse', function() {
                    $('.navbar-toggler-icon').addClass('hidden');
                    $('#navbar-close').removeClass('hidden');
                })
                .on('hidden.bs.collapse', function() {
                    $('.navbar-hamburger').removeClass('hidden');
                    $('#navbar-close').addClass('hidden');
                });

        });

        jQuery(document).ready(function($) {
            function getCookie(name) {
                let matches = document.cookie.match(new RegExp(
                    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
                ));
                return matches ? decodeURIComponent(matches[1]) : undefined;
            }

            was_reloaded = getCookie('was_reloaded')
            console.log(was_reloaded)
            if (was_reloaded != 'yes') {
                document.cookie = "was_reloaded=yes; path=/; max-age=3600;"
                location.reload();
            }
        });
    </script>
    <script>
        (function(d) {
            var s = d.createElement("script");
            s.setAttribute("data-account", "kyz0QDqj5Z");
            s.setAttribute("src", "https://cdn.userway.org/widget.js");
            (d.body || d.head).appendChild(s);
        })(document)
    </script><noscript>Please ensure Javascript is enabled for purposes of <a
            href="https://userway.org">website accessibility</a></noscript>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-L24Z1F65RD"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-L24Z1F65RD');
    </script>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N6QJWJD" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
</body>
