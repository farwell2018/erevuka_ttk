@php
    $activeRoute = Route::currentRouteName();
@endphp
<nav class="nav">
    <div class="container">
        <ul class="nav__list">
            <li class="nav__item {{ $activeRoute == 'admin.daycare.codes' ? 's--on' : '' }}">
                <a href="{{ route('admin.daycare.codes') }}" class="">
                    {{ __('Daycare Codes') }}
                </a>
            </li>

        </ul>
    </div>
</nav>
