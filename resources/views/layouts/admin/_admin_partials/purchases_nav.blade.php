@php
    $activeRoute = Route::currentRouteName();
@endphp
<nav class="nav">
    <div class="container">
        <ul class="nav__list">
            <li class="nav__item {{ $activeRoute == 'admin.group.purchase' ? 's--on' : '' }}">
                <a href="{{ route('admin.group.purchase') }}" class="">
                    {{ __('Group Purchases') }}
                </a>
            </li>

        </ul>
    </div>
</nav>
