@php
    $activeRoute = Route::currentRouteName();
@endphp
<nav class="nav">
    <div class="container">
        <ul class="nav__list">
            <li class="nav__item {{ $activeRoute == 'admin.daycare.index' ? 's--on' : '' }}">
                <a href="{{ route('admin.daycare.index') }}" class="">
                    {{ __('Daycares') }}
                </a>
            </li>

        </ul>
    </div>
</nav>
