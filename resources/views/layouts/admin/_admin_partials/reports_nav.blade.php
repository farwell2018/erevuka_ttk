@php
	$activeRoute = Route::currentRouteName();
@endphp
<nav class="nav">
	<div class="container">
    <ul class="nav__list">
		<li class="nav__item {{$activeRoute == 'admin.graph.dashboard' ? 's--on' : ''}}">
            <a href="{{ route('admin.graph.dashboard') }}" class="">
                {{ __('Dashboard') }}
            </a>
        </li>

		<li class="nav__item {{$activeRoute == 'admin.reports.courseCompletions.index' ? 's--on' : ''}}">
            <a href="{{ route('admin.reports.courseCompletions.index') }}">
                {{ __('Course Completion') }}
            </a>
        </li>

      
		

    </ul>
	</div>
</nav>