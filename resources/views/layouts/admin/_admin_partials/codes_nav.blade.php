@php
    $activeRoute = Route::currentRouteName();
@endphp
<nav class="nav">
    <div class="container">
        <ul class="nav__list">
            <li class="nav__item {{ $activeRoute == 'admin.promo.codes' ? 's--on' : '' }}">
                <a href="{{ route('admin.promo.codes') }}" class="">
                    {{ __('Promo Codes') }}
                </a>
            </li>

        </ul>
    </div>
</nav>
