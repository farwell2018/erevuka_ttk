
@extends('layouts.app_no_js')

@section('content')
<section class="auth-page ">
<div class="bg-overlay">
    <div class="row justify-content-center">
        <div class="col-lg-6 auth-login auth-register w-100 d-none d-md-block">
            {{-- <div class="card">
            <h1 class="text-white">Register</h1>
            <div class="card-body"> 
                    @include('site.includes.auth.register')
            </div>
            </div> --}}
        </div>
        <div class="col-lg-6">
            <div class="card login reset">

                <div class="card-body">
                    <h1 style="font-size: 38px;text-align: center;">Reset Password</h1>
                     <div class="col-md-12">
                     
                        <div class="form-login">
                            <form method="POST" action="{{ route('password.update') }}">
                                @csrf
        
                                <input type="hidden" name="token" value="{{ $token }}">
        
                                <input id="email" type="hidden" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
        
                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
        
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>
        
                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-overall btn_register">
                                            {{ __('Reset Password') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                       </div>
                      

                     </div>
                  
                    
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</section>
@endsection


@section('js')
<script>
    function getNewVal(item)
   {

    if(item.value == 1 || item.value == 2 || item.value == 3  || item.value == 4){
        document.getElementById("parent").style.display = "block";
        document.getElementById("partners").style.display = "none"; 
    }
    else{
        document.getElementById("parent").style.display = "none";
        document.getElementById("partners").style.display = "block"; 
    }
    
   }
   </script>

<script>
$(document).on('click','#checkbox_info',function(){
  var isChecked = $(this).is(':checked');
  if(isChecked == true){
       $('#platforminfo').val('1');
    }
    else if($(this).prop("checked") == false){
        $('#platforminfo').val('0'); 
    }
  
});



$(document).on('click','#checkbox_contact',function(){
  var isChecked = $(this).is(':checked');
  if(isChecked == true){
       $('#consentinfo').val('1');
    }
    else if($(this).prop("checked") == false){
        $('#consentinfo').val('0'); 
    }
  
});
</script>


@endsection