
@if(session()->has('login_error'))
    <div class="form-group row">
        <div class="col-md-12">
            <div class="form-check">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session()->get('login_error') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            </div>
        </div>
    </div>
    @php session()->forget('login_error') @endphp
@endif
<form method="POST" action="{{ route('user.login') }}">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group form-group row justify-content-center">
        <div class="col-md-10">
            <div class="form-check">
                <label> Username or Email  <label>
            <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" placeholder="Username or Email" autocomplete="off">
            @error('username')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
        </div>
    </div>

    <div class="form-group form-group row justify-content-center">
        <div class="col-md-10">
            <div class="form-check input-group mb-3">
                <label> Password  <label>
            <input id="login_password" type="password" class="pass form-control @error('login_password') is-invalid @enderror" placeholder="Password" name="login_password">
            <div class="input-group-append pass-view">
            <i class="far fa-eye"></i>
            <i class="far fa-eye-slash" style="display: none;"></i>
            </div>
            @error('login_password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
        </div>
    </div>

    <div class="form-group form-group row justify-content-center">
        <div class="col-md-10">
            <div class="form-check">
              <input class="form-check-input" type="checkbox" name="remember" id="remember" />
              <label class="form-check-label text-small" for="remember">Remember me</label>
            </div>
        </div>
    </div>
    <div class="form-group form-group row">
        <div class="col-md-8 offset-md-2">
            <button type="submit" class="btn btn-overall white">Login <i aria-hidden="true" class="fas fa-long-arrow-alt-right"></i></button>
        </div>
    </div>
    <div class="form-group form-group row">
        <div class="col">
            <button type="submit" class="btn btn-overall white">Sign in with Google</button>
        </div>
        <div class="col">
            <button type="submit" class="btn btn-overall white">Sign in with Linkedin</button>
        </div>
    </div>
    <div class="form-group form-group row justify-content-center mb-0">
        <div class="col-md-10">
            <a class="form-check-label text-white text-small" href="/password/reset">Trouble logging in?</a>
        </div>
    </div>
</form>
