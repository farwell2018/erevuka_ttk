@php
    $flashMessage = session()->get('site_errors');
@endphp
    
<div class="modal fade bd-example-modal-lg" id="ModalErrors" tabindex="-1" role="dialog" aria-labelledby="ModalErrors" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content error-modal">
            <div class="modal-header">
                @if(isset($flashMessage['title']))
                <h5 class="modal-title" id="ModalErrorsLabel">{{ $flashMessage['title'] }}</h5>
                @endif
            </div>
            <div class="clearfix">
                <div class="divider div-dark div-dot-arsenic"></div>
            </div>
            <div class="modal-body">
                @if(isset($flashMessage['content']))
                {!! $flashMessage['content'] !!}
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dismiss-modal modal-accept"  data-dismiss="modal"  onclick="document.getElementById('ModalErrors').style.display='none'">Dismiss</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

