<div class="modal fade" id="confrimchannelsModal" tabindex="-1" role="dialog" aria-labelledby="confrimchannelsModal" aria-hidden="true">

  <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
    <div class="modal-content">
      <div id="first_form">

       <?php

        if(isset( $invoice_number) && isset($fullAmount) && isset($phone)):?>

        <?php


          $fields =  array("live"=> "1",
                          "oid"=> $invoice_number,
                          "inv"=> $invoice_number,
                          "ttl"=> $fullAmount,
                          "tel"=> $phone,
                          "eml"=> Auth::user()->email,
                          "vid"=> "farwell",
                          "curr"=> "KES",
                          "p1"=>  $invoice_number,
                          "p2"=> "",
                          "p3"=> "",
                          "p4"=> "100",
                          "cbk"=> 'https://dev.erevuka.org/profile/ipay/status/',
                          "cst"=> "1",
                          "crl"=> "2"
                          );

          $datastring =  $fields['live'].$fields['oid'].$fields['inv'].$fields['ttl'].$fields['tel'].$fields['eml'].$fields['vid'].$fields['curr'].$fields['p1'].$fields['p2'].$fields['p3'].$fields['p4'].$fields['cbk'].$fields['cst'].$fields['crl'];
          $hashkey ="poaQcvXamn781HpaMM";//use "demoCHANGED" for testing where vid is set to "demo"

          $generated_hash = hash_hmac('sha1',$datastring , $hashkey);
          ?>

          <FORM action="https://payments.ipayafrica.com/v3/ke" class="grey">
            <div class="checkout-panel">
              <div class="panel-body">
                <h2 class="title">Checkout</h2>

                <div class="progress-line">
                  <div class="step active"></div>
                  <div class="step active"></div>
                  <div class="step"></div>
                </div>
                <div class="payment-method">
                  <div class="row">
                    <div class="col-6">
                        <p> Your phone number has been submitted, kindly proceed to make payment</p>

           <?php
                foreach ($fields as $key => $value) {

                    echo ' <input name="'.$key.'" type="text" value="'.$value.'"  id="'.$key.'" class="form-control" style="border-radius:10px;"readonly>';
                }
               ?>
           <INPUT name="hsh" type="hidden" value="<?php echo $generated_hash ?>">
           </div>
           </div>
           </div>
             <div class="form-group flex-group2"style="align-content: center;">
                 <div style="width: 30%;display: flex;margin-top: 2.5em;align-content: center;">
                   <button class="btn btn-primary" type="submit" style="background-color:#23245f;">{{ __('Proceed to pay') }}</button>
                   <!-- <button type="submit" class="btn btn-overall purple"  >{{ __('Proceed to pay') }}</button>&nbsp;&nbsp;
                   <button type="button" class="btn-arsenic-inverse btn-box btn-reset" data-dismiss="modal" style="padding: 0.2rem;border-radius:17px;">Close</button> -->
           </div>
         </div>
           </div>
         </div>
       </div>

        </FORM>
       <?php endif;?>
      </div>
    </div>
  </div>
</div>

@section('js')
<script type="text/javascript">

  function openCity(evt, cityName) {
    document.getElementById('payment_method').value= cityName;

  $("#Airtel").removeClass("block");
  $("#Airtel").addClass("tabcontent");
  $("#Mpesa").removeClass("block");
  $("#Mpesa").addClass("tabcontent");
  $("#Visa").removeClass("block");
  $("#Visa").addClass("tabcontent");
  $("#Equitel").removeClass("block");
  $("#Equitel").addClass("tabcontent");

  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  tablinks = document.getElementsByClassName("method");



  for (i = 0; i < tabcontent.length; i++) {

    if(cityName === tabcontent[i].id){
      tabcontent[i].className = "block";
    }else{
      tabcontent[i].className.replace("block", "tabcontent");
    }
  }
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace("active", "");

  }
  document.getElementById(cityName).className.replace("block", "tabcontent");
  evt.currentTarget.className += "active";
}

</script>

<script type="text/javascript">

function paymentSubmit() {
   if($('#payment_method').val() === 'Mpesa'){
    $('#pay-form').submit();
   }else{
  //console.log('yes');
  $('#errorMsg').html('');
  $('#responseMsg').html(' ');
  var url = "{{route('ipay.pay')}}";
  var payment_method = $('#payment_method').val();
  var total = $('#total_amount').val();
  var invoice_number = $('#invoice_number').val();
  var MpesaNumber = $('#MpesaNumber').val();
  var AirtelNumber = $('#AirtelNumber').val();
  var EquitelNumber = $('#EquitelNumber').val();
  var cardnumber = $('#cardnumber').val();
  var carddate = $('#carddate').val();
  var cardverification = $('#cardverification').val();
  var cardholder = $('#cardholder').val();
  var token =  "{{ csrf_token()}}";

  $.ajax({
      url: url,
      type: 'POST',
      data:{'payment_method':payment_method, 'amount':total, '_token':token, 'invoice_number':invoice_number, 'MpesaNumber':MpesaNumber, 'AirtelNumber':AirtelNumber, 'EquitelNumber':EquitelNumber,'cardnumber':cardnumber, 'carddate':carddate, 'cardverification':cardverification, 'cardholder':cardholder},
      success: function(res)
      {
          if(res.payment_method === 'Airtel'){
            $('#paybill').append(res.paybill);
            $('#account').append(res.account);
            $('#amount').append(res.amount);

            // var link = '../../ipay/status/'+res.orderid;

              $('#confrim_o').val(res.orderid);

             $('#first_form').css('display','none');
              $('#airtel_form').css('display','block');

          }else if(res.payment_method === 'Equitel'){
            $('#equitel_paybill').append(res.paybill);
            $('#equitel_account').append(res.account);
            $('#equitel_amount').append(res.amount);

            // var link = '../../ipay/status/'+res.orderid;

            $('#confrim_o').val(res.orderid);

            // $("#equitel_confirm").attr('href', link);

            $('#first_form').css('display','none');
             $('#equitel_form').css('display','block');
          }

      },

  });


}
}
</script>
@endsection
