<div class="modal fade" id="youtube" tabindex="-1" role="dialog" aria-labelledby="youtube" aria-hidden="true">

  <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onclick="videoPause()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row justify-content-center" id="iframe-cont" data-iframe="/videos/Erevuka-video.mp4">

        </div>
      </div>
    </div>
  </div>
</div>
