@extends('layouts.email')

@section('content')

<p style="font-family:Helvetica Neue, sans-serif;font-size: 28px;line-height:1.6;font-weight:normal;margin:0 0 30px;padding:0;color:#c0bac4;text-align:center;">User Verification Codes</p>
<hr class="line-footer">
<p class="bigger-bold" style="font-family: Helvetica Neue, sans-serif;text-align:center;font-size: 22px;">Hello </p>

<p style="font-family: Helvetica Neue, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">
    Thank you for purchasing a monthly subscription.</p>

  <p style="font-family: Helvetica Neue, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">
      Below are the codes to be used by each individual user while enrolling to the courses and quicktips.</p>


@foreach($codes as $code)
<p style="font-family: Helvetica Neue, sans-serif;margin: 30px 0 30px; text-align:center;color:#520e33;font-size:18px;font-weight: bold;">{{$code->unique_code}}</p>
@endforeach

<p style="font-family: Helvetica Neue, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">
 Kindly ignore this email incase you did not make this request.
</p>
<p style="font-family: Helvetica Neue, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 20px 0 0;padding: 0;color:#7C7C7C;text-align:center;">
Best Regards,
</p>
<p style="font-family:Helvetica Neue, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 0 0 10px;padding: 0;color:#7C7C7C;text-align:center;">
  Mtotolearn Team
</p>

<p style="font-family:Helvetica Neue, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 0 0 10px;padding: 0;color:#520e33;text-align:center;">
learn@tinytotos.com <br>
 +254 790 489 493
</p>


@endsection
