@extends('layouts.email')

@section('content')


<p style="font-family:poppins, sans-serif;font-size: 28px;line-height:1.6;font-weight:normal;margin:0 0 30px;padding:0;color:#F18A21;text-align:center;">Account Verification</p>
<hr class="line-footer">
<p class="bigger-bold" style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">Hello {{ ucwords(strtolower($name)) ?: '' }},</p>

<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">
    Your account on {{ config('app.name', 'The Challenging Patriarchy Program') }} has been created.</p>

  <p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">
      In order to complete your registration and log in, please the activation code below to activate your account .</p>
  
<p style="font-family: poppins, sans-serif;margin: 30px 0 30px; text-align:center;color:#23245F;font-size:18px;font-weight: bold;">{{$code}}</p>

<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">
 Kindly ignore this email incase you did not make this request.
</p>
<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 20px 0 0;padding: 0;color:#404040;text-align:center;">
Best Regards,
</p>
<p style="font-family:poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 0 0 10px;padding: 0;color:#404040;text-align:center;">
The Tiny Totos Program
</p>

<p style="font-family:poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 0 0 10px;padding: 0;color:#662F8E;text-align:center;">
 support@farwell-consultants.com
</p>


@endsection
