@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level === 'error')
# @lang('Whoops!')
@else
# @lang('Hello!')
@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $key=>$line)
@if($key===0)
<p style="font-size: 16px;">{{ $line }}</p>
@else
<p style="font-size: 16px;">{{ $line }}</p>
@endif
@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
switch ($level) {
    case 'success':
    case 'error':
        $color = $level;
        break;
    default:
        $color = 'primary';
}
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@lang("Incase you’re unable to click on the link, you may copy and paste the link into your browser.")
<p style="font-size: 16px;width:100vw;">
    <a href="{{$actionUrl}}" target="_blank" style="padding: 15px;">{{$actionUrl}}</a>
</p>
@endisset


{{-- Outro Lines --}}
@foreach ($outroLines as $line)
<p>{!! $line !!}</p>
@endforeach


{{-- Subcopy --}}
@isset($actionText)
@slot('subcopy')
<p style="color:#3d4852;line-height:1.5em;margin-top:0;text-align:center;font-size:14px">
    {{-- Salutation --}}
    @if (! empty($salutation))
    {{ $salutation }}
    @else
    @lang('In case you need any help, please contact support ')
    <br />
    @lang('and we will be happy to assist.')
    <br />
    <a href="mailto:{{config('app-constants.support_email')}}" target="_blank">{{config('app-constants.support_email')}}</a>
    @endif
</p>
@endslot
@endisset

@endcomponent