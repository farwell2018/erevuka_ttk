
@extends('layouts.app')

@section('js')
<script src="{{asset('js/auth.js')}}"></script>
@endsection
@section('content')
<section class="auth-page verify-page">
<div class="bg-overlay">
    <div class="row justify-content-center">
        <div class="col-lg-6 auth-login auth-register w-100 d-none d-md-block" style="padding: 21% 0;">
            {{-- <div class="card">
            <h1 class="text-white">Register</h1>
            <div class="card-body"> 
                    @include('site.includes.auth.register')
            </div>
            </div> --}}
        </div>
        <div class="col-lg-6">
            <div class="card login reset">

                <div class="card-body">
                     <div class="col-md-12">
                      <div class="row">

                       <div class="col-md-2"></div>

                       <div class="col-md-8">
                        <h1>Verify Account</h1>
                        <p>Registration successfully submitted, please enter the verification code sent to phone number or email</p>
                        <div class="form-login">
                            <form  method="POST" action="{{ route('user.verification') }}" id="registerForm">
                                @csrf
                                 <input type="hidden" name="user" value="{{$user_id}}">
                               
                                <div class="form-group row justify-content-center">
                                    <div class="col-md-10">
                                        <div class="form-check">
                                        <input id="token" type="text" class="form-control" name="token" placeholder="{{__('00-00-00')}}" autocomplete="off" value="{{ old('token') }}" autofocus>
                                        @error('token')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-center">
            
                                    <button type="submit" class="btn btn-overall btn_register col-lg-4">
                                        {{ __('Submit') }}
                                        <i class="fa fa-arrow-right"></i>
                                    </button>
                                </div>
                                
                            </form>
                       </div>
                       </div>
                      

                       <div class="col-md-2"></div>

                      </div>

                     </div>
                  
                    
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</section>
@endsection


@section('js')
<script>
    $(document).ready(function(){
        $('input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){
               $('#course_information').val('1');
            }
            else if($(this).prop("checked") == false){
                $('#course_information').val(' '); 
            }
        });
    });
</script>


@endsection