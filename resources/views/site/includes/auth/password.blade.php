@if(session()->has('login_message'))
    <div class="form-group row">
        <div class="col-md-12">
            <div class="form-check">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session()->get('login_message') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            </div>
        </div>
    </div>
@endif

<form method="POST" action="{{ route('password.email') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group row">
        <div class="col-md-8 offset-md-2">
            <div class="form-check">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required placeholder="Your email address" autocomplete="off">

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col-md-12">
            <button type="submit" class="btn btn-overall purple">
                {{ __('Send Password Reset Link') }}
            </button>
        </div>
    </div>
</form>
