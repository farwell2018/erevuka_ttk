@if(session()->has('register_error'))
    <div class="form-group row">
        <div class="col-md-12">
            <div class="form-check">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session()->get('register_error') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            </div>
        </div>
    </div>
    @php session()->forget('register_error') @endphp
@endif
@if(session()->has('register_success'))
    <div class="form-group row">
        <div class="col-md-12">
            <div class="form-check">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session()->get('register_success') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            </div>
        </div>
    </div>
    @php session()->forget('register_success') @endphp
@endif
<form method="POST" action="{{ route('user.create') }}" id="registerForm">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="col-md-12">
        <div class="row">

             <div class="col-md-6 no-padding-right">
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> First Name <span style="color:red">*</span> </label>
                        <input id="first_name" type="text" class="form-control" name="first_name" autocomplete="off" value="{{ old('first_name') }}" placeholder="First Name">
                        @error('first_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
             </div>

            <div class="col-md-6 no-padding-left">
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Last Name <span style="color:red">*</span> </label>
                        <input id="last_name" type="text" class="form-control" name="last_name"  autocomplete="off" value="{{ old('last_name') }}" placeholder="Last Name">
                        @error('last_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-6 no-padding-right">
        <div class="form-group ">
            <div class="form-check">
                <label> Who am I <small>(Please select the role that describes you best)</small><span style="color:red">*</span></label>
                <select class="form-control downArrow "  name="role" id="role" onchange="getNewVal(this);">
                    <option value="">{{__('')}}</option>
                    @foreach ($roles as $key => $value)
                        <option value="{{$value->id}}">{{$value->title}}</option>
                    @endforeach
                </select>
                @error('mobile_number_country')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
        </div>

    </div>
   
<div class="parent" id="parent" style="display:none">
     <div class="col-md-12">
    
            <div class="form-group row justify-content-center">
                <div class="col-md-12">
                    <div class="form-check">
                        <label> Phone Number <span style="color:red">*</span></label>
                    <div class="input-group">
                        <div class="input-group-prepend" style="width:20%;">
                           
                            <input type="text" value="+254" name="mobile_number_country" class="form-control no-radius-right">

                            
                            @error('mobile_number_country')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
            
                        <input id="mobile_number" type="text"
                            class="form-control @error('mobile_number') border-red-500 @enderror no-radius-left" name="mobile_number"
                            value="{{ old('mobile_number') }}" placeholder="700000000"  autocomplete="mobile_number" >
                            @error('mobile_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    </div>
                </div>
            </div>
     </div>
  
      <div class="col-md-12">
          <div class="row">
              <div class="col-md-6 no-padding-right">
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Password <span style="color:red">*</span></label>
                            <div class="input-group mb-3">
                        <input id="password" type="password"  class="pass form-control" name="password"  autocomplete="new-password" placeholder="Password">
                        <div class="input-group-append pass-view">
                        <i class="far fa-eye"></i>
                        <i class="far fa-eye-slash" style="display: none;"></i>
                        </div>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                        </div>
                    </div>
                </div>
              </div>

              <div class="col-md-6 no-padding-left">
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check ">
                             <label> Confirm Password <span style="color:red">*</span></label>
                          <div class="input-group mb-3">
                        <input id="password-confirm" type="password"  class="pass form-control" name="password_confirmation"  autocomplete="new-password" placeholder="Confirm Password">
                        <div class="input-group-append pass-view">
                        <i class="far fa-eye"></i>
                        <i class="far fa-eye-slash" style="display: none;"></i>
                        </div>
                        @error('password_confirmation')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                    </div>
                </div>
              </div>
          </div>
      </div>
      <div class="col-md-12">
        <div class="form-group  recieve-information">
            <input type="hidden" id="platforminfo"name="register_receive_information" />
            <input type="checkbox"  name="register_receive_course_information" id="checkbox_info" >
            <label for="exampleCheck1" class="form-check-label" style="display: initial">Yes I want to receive updates</label>
            <br>
            <input type="hidden" id="consentinfo" name="contact_consent" />
            <input type="checkbox"  name="register_contact_consent" id="checkbox_contact" >
            <label for="exampleCheck2" class="form-check-label" style="display: initial">I agree to all the terms and conditions</label>
        </div>
      </div>
    </div>

    <div class="partners" id="partners" style="display:none">
        <div class="col-md-12">
            
        <div class="row">
            <div class="col-6">
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Phone Number <span style="color:red">*</span></label>
                        <div class="input-group">
                            <div class="input-group-prepend" style="width:20%;">
                               
                                <input type="text" value="+254" name="partners_mobile_number_country" class="form-control no-radius-right">
    
                                
                                @error('mobile_number_country')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            </div>
                
                            <input id="mobile_number" type="text"
                                class="form-control @error('mobile_number') border-red-500 @enderror no-radius-left" name="partners_mobile_number"
                                value="{{ old('mobile_number') }}" placeholder="700000000"  autocomplete="mobile_number" >
                                @error('mobile_number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-6">
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Email Address <span style="color:red">*</span></label>
                        <input id="email" type="email" class="form-control" name="partners_registration_email"  autocomplete="off" value="{{ old('registration_email') }}" placeholder="Email Address">
                        @error('registration_email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>

            </div>
        </div>
             
           
               
        </div>
     
         <div class="col-md-12">
             <div class="row">
                 <div class="col-md-6">
                   <div class="form-group row justify-content-center">
                       <div class="col-md-12">
                           <div class="form-check">
                               <label> Password <span style="color:red">*</span></label>
                               <div class="input-group mb-3">
                           <input id="password" type="password"  class="pass form-control" name="partners_password"  autocomplete="new-password" placeholder="Password">
                           <div class="input-group-append pass-view">
                           <i class="far fa-eye"></i>
                           <i class="far fa-eye-slash" style="display: none;"></i>
                           </div>
                           @error('password')
                               <span class="invalid-feedback" role="alert">
                                   <strong>{{ $message }}</strong>
                               </span>
                           @enderror
                       </div>
                           </div>
                       </div>
                   </div>
                 </div>
   
                 <div class="col-md-6">
                   <div class="form-group row justify-content-center">
                       <div class="col-md-12">
                           <div class="form-check ">
                                <label> Confirm Password <span style="color:red">*</span></label>
                             <div class="input-group mb-3">
                           <input id="password-confirm" type="password"  class="pass form-control" name="partners_password_confirmation"  autocomplete="new-password" placeholder="Confirm Password">
                           <div class="input-group-append pass-view">
                           <i class="far fa-eye"></i>
                           <i class="far fa-eye-slash" style="display: none;"></i>
                           </div>
                           @error('password_confirmation')
                               <span class="invalid-feedback" role="alert">
                                   <strong>{{ $message }}</strong>
                               </span>
                           @enderror
                           </div>
                       </div>
                       </div>
                   </div>
                 </div>
             </div>
         </div>
         <div class="col-md-12">
           <div class="form-group  recieve-information">
               <input type="hidden" id="platforminfo"name="partners_register_receive_information" />
               <input type="checkbox"  name="register_receive_course_information" id="checkbox_info" >
               <label for="exampleCheck1" class="form-check-label" style="display: initial">Yes I want to receive updates</label>
               <br>
               <input type="hidden" id="consentinfo" name="partners_contact_consent" />
               <input type="checkbox"  name="register_contact_consent" id="checkbox_contact" >
               <label for="exampleCheck2" class="form-check-label" style="display: initial">I agree to all the terms and conditions</label>
           </div>
         </div>
       </div>




      <div class="form-group form-group row">
        <div class="col ">
            <button type="submit" id="register_id"class="btn btn-overall btn_register mt-2 ml-5"> Register <i aria-hidden="true" class="fas fa-long-arrow-alt-right"></i></button>
        </div>
    </div>
    <div class="form-group form-group row justify-content-center ">
       
            <p class="have-account text-center" >Already have an account? <a class="form-check-label " href="{{route('login')}}">Log in</a><p><br/>
            
    </div>

    <div class="form-group form-group row justify-content-center ">
        <p class="have-account text-center" > Already registered but not yet activated your account? Click <a class="form-check-label text-green " href="{{route('resend.code')}}">here</a> to request a fresh activation code<p>
    </div>
    
</form>
