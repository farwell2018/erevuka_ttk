@php
$banner=  asset('images/banners/subscription.png');
@endphp
<div class="jarallax">
@if((new \Jenssegers\Agent\Agent())->isDesktop() ||(new \Jenssegers\Agent\Agent())->isTablet())
    <img class="jarallax-img" src="{{$image}}" alt="">
    @else
    <img class="jarallax-img" src="{{$banner}}" alt="">
@endif
    <div class="content">
      <div class="wrap">
        {{-- @if((new \Jenssegers\Agent\Agent())->isDesktop())
        <h1>{!! $text !!}</h1>
        @else
        <h3>{!! $text !!}</h3>
        @endif
       --}}
      </div>
      <div class = "ik-hr-white">
        <span class="hr-inner">
          <span class="hr-inner-style"></span>
        </span>
      </div>
    </div>
    <!-- <a href="#next-section" title="" class="scroll-down-link " aria-hidden="true" >
      <i class="fas fa-chevron-down"></i>
    </a> -->
</div>
<div id="next-section"></div>
