@php
    session_start();
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
    $_SESSION['course'] = $course->id;
    
    use App\Models\Mpesa;
    if (Auth::user()) {
        $subscribed = Mpesa::where('user_id', Auth::user()->id)->first();
    }
    
@endphp

<div class="jarallax">
    @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <img class="jarallax-img" src="{{ $image }}" alt="">
            <div class="content">
                <div class="wrap  mt-2">
                @if (Auth::user())
                    @if ( Auth::user()->subscription != 1)
                        <br>
                        <p>
                            <a href="{{ route('subscriptions') }}" style="background:#ff6d00; color:#fff" class="btn btn-overall">Enroll for this course
                                <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                            </a>
                        </p>


                        <p>
                            <a style="color:#fff;" data-code="{{ $course->id }}" href="#codeModal"
                                style="font-weight:600;border-radius: 25px; position:absolute;  "
                                class="btn btn-overall purple  open-CodeDialog btn text-white mt-2"
                                title="Enroll with code"> Enroll with code </a>
                        </p>
                    @else
                        <h1 class="mt-2">{!! $text !!}</h1>
                        {{-- <p class="span-space"> <i class="fa fa-clock-o" style="font-size:16px"></i> {{$course->effort}}</p> --}}
                        @guest
                            <p>
                                <a href="{{ route('user.login') }}" class="btn btn-overall "  style="color:#fff; background:#ff6d00;">Enroll for this course <i
                                        class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                                </a>
                            </p>
                        @else
                     
                            @if (!$licensed)
                                <p><a class="btn btn-overall" style="color:#fff; background:#ff6d00;"
                                        href="{{ route('course.enroll', $course->course_id) }}/">Enroll into Course</a></p>
                            @else
                                @if ($enrolled)
                                    <p><a class="btn btn-overall" style="color:#fff; background:#ff6d00;"
                                            href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}"
                                            target="_blank">Take Course</a></p>
                                @else
                                    <p><a class="btn btn-overall " style="color:#fff; background:#ff6d00;"
                                            href="{{ route('course.enroll', $course->course_id) }}/">Enroll into Course</a>
                                    </p>
                                @endif
                            @endif
                        @endguest
                    @endif
                @endif
           

        </div>
        <div class="ik-hr-white">
            <span class="hr-inner">
                <span class="hr-inner-style"></span>
            </span>
        </div>
    </div>
    @else

    <img class="jarallax-img" src="{{ $image }}" alt="">
            <div class="content">
                <div class="wrap text-center mt-2">
                @if (Auth::user())
                    @if ( Auth::user()->subscription != 1)
                        <br>
                        <p>
                            <a href="{{ route('subscriptions') }}" class="btn btn-overall purple" style=" min-width: max-content;">Enroll for this course
                                <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                            </a>
                        </p>


                        <p>
                            <a style="color:#fff" data-code="{{ $course->id }}" href="#codeModal"
                                style="font-weight:600;border-radius: 25px; position:absolute;min-width: max-content; "
                                class="btn btn-overall purple open-CodeDialog btn text-white mt-2"
                                title="Enroll with code"> Enroll with code </a>
                        </p>
                    @else
                        <h1 class="mt-2">{!! $text !!}</h1>
                        {{-- <p class="span-space"> <i class="fa fa-clock-o" style="font-size:16px"></i> {{$course->effort}}</p> --}}
                        @guest
                            <p>
                                <a href="{{ route('user.login') }}" class="btn btn-overall purple" style=" min-width: max-content;">Enroll for this course <i
                                        class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                                </a>
                            </p>
                        @else
                            @if (!$licensed)
                                <p><a class="btn btn-overall purple" style=" min-width: max-content;"
                                        href="{{ route('course.enroll', $course->course_id) }}/">Enroll into Course</a></p>
                            @else
                                @if ($enrolled)
                                    <p><a class="btn btn-overall purple"
                                            href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}"
                                            target="_blank">Take Course</a></p>
                                @else
                                    <p><a class="btn btn-overall purple" style=" min-width: max-content;"
                                            href="{{ route('course.enroll', $course->course_id) }}/">Enroll into Course</a>
                                    </p>
                                @endif
                            @endif
                        @endguest
                    @endif
                @endif
           

        </div>
        <div class="ik-hr-white">
            <span class="hr-inner">
                <span class="hr-inner-style"></span>
            </span>
        </div>
    </div>

    @endif
    
</div>


<div class="modal fade " id="codeModal" tabindex="-1" role="dialog" aria-labelledby="codeModal" aria-hidden="true"
    style="margin-top: 15%;">
    <div class="modal-dialog modal-lg" style="width:450px">
        <div class="modal-content">

            <div class="modal-body" style="text-align:center;padding:0">

                <div class="col-md-12 col-12 bg-teal-green" style="padding: 40px 20px;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                        style="margin-right: 20px;font-size: 1.5rem;color:#fff">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <h5 class="modal-title" id="exampleModalLabel" style="margin-top:30px;color:#fff">Enrollment Code
                    </h5>

                    <form method="POST" action="{{ route('course.purchasedCode') }}" class="grey" id="groupPurchase">
                        @csrf
                        <input type="hidden" name="course" value="" id="confirm_course">
                        <div class="form-group editable">

                            <label for="confirm_course" style="font-size:15px; text-align:center;color:#000;"> Please
                                enter the code shared with you to access this module to proceed.</label>

                            <input type="text" name="confirmation_code" value="" id="course"
                                class="form-control">
                        </div>
                        <div class="form-group ">
                            <div class="form-group flex-group">
                                <button type="submit" class="btn btn-overall purple" id="pricing-form-submit">
                                    {{ __('Submit') }}
                                </button>&nbsp;&nbsp;
                                <button type="button" class="btn-overall btn-cancel" style="border-radius: 25px;"
                                    data-dismiss="modal" style="min-width: 96px; padding: 0.4rem 1rem;">Close</button>
                            </div>
                        </div>
                    </form>



                </div>

            </div>



        </div>
       
    </div>
</div>
</div>

</div>

<div id="next-section"></div>
<script>
    $(document).on("click", ".open-CodeDialog", function(e) {
        e.preventDefault();
        var _self = $(this);
        var courseId = _self.data('code');
        console.log(courseId);
        $("#confirm_course").val(courseId);
        $(_self.attr('href')).modal('show');
    });
</script>
