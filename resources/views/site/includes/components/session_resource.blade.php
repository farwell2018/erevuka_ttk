


{{-- <div class="alert alert-danger print-error-msg" style="display:none">
    <ul></ul>
</div> --}}
    <form action="" method="POST" enctype="multipart/form-data" id="file-upload-form">

        <div class="form-group row error-display" style="display:none">
            <div class="col-md-12">
                <div class="form-check">
                <div class="alert alert-danger alert-dismissible fade print-error-msg " role="alert"  id="print-error-upload">
                    <ul></ul>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                </div>
            </div>
        </div>
        
        
        <div class="form-group row success-display" style="display:none">
            <div class="col-md-12">
                <div class="form-check">
                <div class="alert alert-success alert-dismissible fade  print-success-msg" role="alert" id="print-success-upload">
                    
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                </div>
            </div>
        </div>

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input id="session_id" type="hidden" name="session_id" value="{{$pageItem->id}}" />

        <div class="form-group row justify-content-center">
            <div class="col-md-12">
                <div class="form-check">
                    <label> Title </label>
                <input id="title" type="text" class="form-control" name="title" autocomplete="off" value="{{ old('title') }}" autofocus>
                @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
        </div>
    
    
        <div class="form-group row justify-content-center">
            <div class="col-md-12">
                <div class="form-check">
                    <label> Description </label>
                    <textarea id="description" type="text"  name="description" class="md-textarea form-control" rows="5"></textarea>
              
                @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
        </div>
          
            <div class="form-group row justify-content-center">
                <div class="col-md-12">
                <div class="file-background">
                    <div class="preview-zone hidden">
                        <div class="box box-solid">
                          <div class="box-header with-border">
                            <div><b>Preview</b></div>
                            <div class="box-tools pull-right">
                              <button type="button" class="btn btn-overall btn_take_course remove-preview">
                                 Remove upload
                              </button>
                            </div>
                          </div>
                          <div class="box-body"></div>
                        </div>
                      </div>
              <div class="dropzone-wrapper">

                <label class="dropzone-desc" for="file-upload">
                    <h3> Upload your files </h3>
                     Supports: PDF and Word files
                    <br /><br />Drag and drop files here
                    <br />or
                    <br /><br /><span id="file-upload-btn" class="button">Browse files</span>
                    <br /><br />Maximum file size: 8Mb
                </label>
               
                <input type="file" name="fileUpload" class="dropzone" id="file-upload">
              </div>
            </div>

            <span class="invalid-feedback" role="alert" id="image_error" style="display:none">
                
            </span>
       
          </div>
        </div>
       
        
  
        <div class="row">
          <div class="col-md-12">
            <button type="submit" class="btn btn-overall btn_upload" id="saveLevels" style="left: 38%;">Submit Resource</button>
          </div>
        </div>
      
    </form>
