@if((new \Jenssegers\Agent\Agent())->isDesktop())
<div class="container">
    <div class="card upcoming-card mb-3" >
        <div class="row g-0">
          <div class="col-md-2 no-padding-right">
            @if( $item->hasImage('event_image'))
            <img src="{{$item->image("event_image", "default")}}" alt="{{$item->title}}" class="img-fluid "/>
            @endif
          </div>
          <div class="col-md-8 no-padding-left no-padding-right">
            <div class="card-body">
              <h5 class="card-title">{{ $item->title}}</h5>
              <p class="card-text">
                <?php
                    $str = $item->short_description;
                    if (strlen($str) > 200) {
                        $str = substr($str, 0, 200) . '...';
                    }?>
                    {!!  $str !!}
              </p>
              <p class="readmore " style="margin-bottom:0">
                <a href="{{route('event.details',[$item->id])}}" class="btn btn-overall btn_event">Event Details </a>
              </p>
            </div>
          </div>
          <div class="col-md-2 no-padding-left ">
            <div class="event_date_section d-flex flex-column  justify-content-center">
                <p class="event-date">{{$item->present()->day}}<p>
                <p class="event_month">{{$item->present()->month}}</p>

            </div>
          </div>
        </div>
      </div>
</div>

@elseif((new \Jenssegers\Agent\Agent())->isMobile())

<div class="course-card mt-3 sessions-card">
  <article>
    <div class="thumbnail">
      @if( $item->hasImage('event_image'))
      <img src="{{$item->image("event_image", "default")}}" alt="{{$item->title}}" class="img-fluid "/>
      @endif
        
    </div>

    <div class="course-card-content session-card-content">
      <p class=" text-grey session-card-calendar">

        <span class="session-calendar"> <i class="fa fa-calendar" aria-hidden="true"></i> {{$item->present()->date}} | {{$item->present()->time}} </span>
        
      </p>
      <p class="title ">
        {{ $item->title}}</p>
      <p class="description " style="text-align:left">
        <?php
                    $str = $item->short_description;
                    if (strlen($str) > 200) {
                        $str = substr($str, 0, 200) . '...';
                    }?>
                    {!!  $str !!}
      </p>
      <p class="readmore " style="margin-bottom:0">
        <a href="{{route('event.details',[$item->id])}}" class="btn btn-overall btn_event">Event Details </a>
      </p>
    </div>

  </article>
</div>


@endif
