@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    
        <div class="row justify-content-center">
            <div class="card resource-type-filter" >
                <div class="col-12">
                    <div class="row justify-content-center">

                        <div class="col-12">
                            <form action="{{ route('pages', ['key' => $key]) }}" method="POST">
                                @csrf
                                <div class="row justify-content-center">
                                    <div class="col-1  mt-4 mb-4 col-spacing">
                                        <img src="{{ asset('images/icons/filter_icon.svg') }}"
                                            style="float: right;" />
                                    </div>

                                    <div class="col-8 no-padding-right ">
                                        <div class="row">

                                            <div class="col-3 mt-4 mb-4 ">
                                                <label class="filter_by"> Find Resource by</label>
                                            </div>
                                            <div class="col-3">
                                                <select name="category" id="" class="form-control mt-4 mb-4 " >
                                                    <option value=" ">Category</option>
                                                 
                                                    @foreach($resourceTypes as $theme)
                                                    <option value="{{$theme->id}}"> {{$theme->title}}</option>
                                                @endforeach
                                                  
                                                </select>

                                                
                                                @error('category')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                            </div>
                                            <div class="col-3">
                                                <select name="type" id="" class="form-control mt-4 mb-4 " >
                                                    <option value=" ">Type</option>
                                                    @foreach($resourceCategorySlugs as $theme)
                                                    <option value="{{$theme->id}}"> {{$theme->title}}</option>
                                                @endforeach
                                                  
                                                </select>

                                                
                                                @error('type')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                            </div>

                                            <div class="col-3">
                                                 <input type="text" placeholder="Search" name="free_search" class="form-control mt-4 mb-4" />
                                                @error('free_search')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                            </div>

                                        </div>


                                    </div>

                                    <div class="col-3 ">
                                        <button type="submit"
                                            class="btn btn-overall btn_register mt-4 mb-4">Search</button>
                                        <button type="reset" class="btn btn-overall btn_cancel mt-4 mb-4"
                                            onClick="window.location.href=window.location.href">Reset</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
 @elseif ((new \Jenssegers\Agent\Agent())->isTablet())
 <div class="row justify-content-center">
            <div class="card resource-tablet-type-filter" >
                <div class="col-12">
                    <div class="row justify-content-center">

                        <div class="col-12">
                            <form action="{{ route('pages', ['key' => $key]) }}" method="POST">
                                @csrf
                                <div class="row justify-content-center">
                                    <div class="col-1  mt-2 mb-2 col-spacing">
                                        <img src="{{ asset('images/icons/filter_icon.svg') }}"
                                            style="float: right;" />
                                    </div>

                                    <div class="col-7 no-padding-right ">
                                        <div class="row">

                                            <div class="col-2 mt-2 mb-2 pb-2 ">
                                                <label class="filter_by"> Find Resource by</label>
                                            </div>
                                            <div class="col-3">
                                                <select name="category" id="" class="form-control mt-2 mb-2 " >
                                                    <option value=" ">Category</option>
                                                 
                                                    @foreach($resourceTypes as $theme)
                                                    <option value="{{$theme->id}}"> {{$theme->title}}</option>
                                                @endforeach
                                                  
                                                </select>

                                                
                                                @error('category')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                            </div>
                                            <div class="col-3">
                                                <select name="type" id="" class="form-control mt-2 mb-2 " >
                                                    <option value=" ">Type</option>
                                                    @foreach($resourceCategorySlugs as $theme)
                                                    <option value="{{$theme->id}}"> {{$theme->title}}</option>
                                                @endforeach
                                                  
                                                </select>

                                                
                                                @error('type')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                            </div>

                                            <div class="col-3">
                                                 <input type="text" placeholder="Search" name="free_search" class="form-control mt-2 mb-2" />
                                                @error('free_search')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                            </div>

                                        </div>


                                    </div>

                                    <div class="col-4">
                                        <button type="submit"
                                            class="btn btn-overall btn_register mt-2 mb-2">Search</button>
                                        <button type="reset" class="btn btn-overall btn_cancel mt-2 mb-2"
                                            onClick="window.location.href=window.location.href">Reset</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>

@else
  
        <div class="card resource-filter mb-2 mt-5">
            <div class="col-12">
                <div class="row">

                    <div class="col-12">
                        <form action="{{ route('pages', ['key' => $key]) }}" method="POST">
                            @csrf
                            <div class="row">

                                <div class="col-sm-3 col-xs-12">
                                    <select name="category" id="" class="form-control mt-4 mb-2 minimal" >
                                        <option value= " "> Category</option>
                                        @foreach($resourceTypes as $theme)
                                        <option value="{{$theme->id}}"> {{$theme->title}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-3 col-xs-12">
                                    <select name="type" id="" class="form-control mt-4 mb-2 minimal" >
                                        <option value=" "> Type</option>
                                        @foreach($resourceCategorySlugs as $theme)
                                        <option value="{{$theme->id}}"> {{$theme->title}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-3 col-xs-12">
                                    <input type="text" placeholder="Search" name="free_search" class="form-control mt-4 mb-4" />
                                    @error('free_search')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                                <div class="col-sm-3 col-xs-12">
                                    <button type="submit" class=" text-white btn btn-overall mt-4 mb-4" style="background:#5d2941;">Search</button>
                                    <button type="reset" class="btn btn-overall btn_cancel mt-4 mb-4"
                                        onClick="window.location.href=window.location.href">Reset</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>

        </div>
 


@endif
