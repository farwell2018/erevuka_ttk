@php
use App\Models\Resource;
use App\Models\Course;
 $configLms   = config()->get("settings.lms.live");
 $main_course = Resource::where('course_id',$course->id)->first();
 $resources   = Resource::leftJoin('courses', 'resources.course_id', '=', 'courses.id')
              ->select('resources.*', 'courses.*')->where(function($query) {
                $query->where('resources.resource_category','=', 2);
              })->where(function($query) use($main_course) {
                $query->where('resources.content_bucket_id',$main_course->content_bucket_id);
              })->published()->paginate(3);
@endphp


        <section class="detailed-tree">
          <div class="start">            
           <h4 class="font-weight-bold courses-related" style="color:#520e33; font-weight:200">Related Quicktips</h4>
         

           @foreach($resources as $course)
           <div class="course-card" >
  <article style="border:0px;">
    <div class="thumbnail">

      <a href="/profile/course/detail/{{$course->id}}">
        <img src="{{asset($course['course_image_uri'])}}" alt="{{$course->name}}" />
        <div class="overlay">
          <div class="overlay-inside" data-toggle="tooltip" data-placement="top" title="{{$course['title']}}">
            <i class="fas fa-share"></i>
          </div>
        </div>
      </a>
    
    </div>

    <div class="course-card-content">
      <p style="font-size:15px">
        <?php
       
        $str = $course['short_description'];
        if (strlen($str) > 50) {
          $str = substr($str, 0, 50) . '...';
        }
        echo $str;
       
        ?>
        <a style="font-size:15px" href="/profile/course/detail/{{$course->id}}">Read More</i></a>
      </p>
    </div>


    @if(isset($uwishlist))
    

    @endif
  </article>
</div>
@endforeach

<div class="pagination">
{!! $resources->links() !!}  
</div>
<share-modal></share-modal>
       
        </div>
        </section>
      
      