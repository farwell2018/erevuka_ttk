

   <section class="general-section ">
    <div class="container-fluid">
    <div class="col-12 ">
        <div class="row">
        <div class="hrHeading">
            <h2 class="line-header  child_header_inner">
                {{ $pageItem->title }}
            </h2>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="whatYouGetListing ">
                   {!!$pageItem->description!!}
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="video-wrapper">
                    <video controls playsinline muted id="bgvid">
                        <source src="{{$pageItem->file("resource_video")}}" type="video/mp4">
                    </video>
                 
                </div>
    
            </div>
        </div>
    </div>
    </div>
   
        @if((new \Jenssegers\Agent\Agent())->isDesktop())
        <div class="col-12">
        <div class="replies mt-3">
            <span class="resource-span-icon" ><img src="{{asset('images/resource_categories/'.$pageItem->type->icon_name)}}" title="{{$pageItem->type->title}}"/> {{$pageItem->type->title}}</span>

                <span style="display:inline-block" class="like-share">
                <share-action email="{{$pageItem->share['email']}}" facebook="{{$pageItem->share['facebook']}}" linkedin="{{$pageItem->share['linkedin']}}" url="{{route('resource.details',$pageItem->id)}}"></share-action>
                </span>

                <span title="Likes"  onClick="saveLikeDislike({{$pageItem->id}},'like')" class="mr-3  mr-1-sm like-share" >
                    <span class="like-count" id="like-{{$pageItem->id}}" >Like <i class="fa fa-heart"></i></span>
                </span>
        </div>
    </div>
        @else 
        <div class="replies mt-3">
            
                <span class="resource-span-icon" ><img src="{{asset('images/resource_categories/'.$pageItem->type->icon_name)}}" title="{{$pageItem->type->title}}"/> {{$pageItem->type->title}}</span>
            
                <span class="like-share">
                    <share-action email="{{$pageItem->share['email']}}" facebook="{{$pageItem->share['facebook']}}" linkedin="{{$pageItem->share['linkedin']}}" url="{{route('resource.details',$pageItem->id)}}"></share-action>
                    </span>
    
                    <span title="Likes"  onClick="saveLikeDislike({{$pageItem->id}},'like')" class="like-share like-mobile" >
                        <span class="like-count" id="like-{{$pageItem->id}}" >Like <i class="fa fa-heart"></i></span>
                    </span>
           
                
        </div>
        @endif
    
    @if($pageItem->documents)
    <div>
        <table class="downloable-table">
        @foreach($pageItem->documents as $document)
        @include('site.includes.components.resources.downloable',['document'=>$document])
       @endforeach
    </table>
    </div>
    @endif
</section>
