<div class="course-card">
  <article>
    <div class="thumbnail">

      <a href="/courses/detail/{{$course->slug}}">
        <img src="{{asset($course['course_image_uri'])}}" alt="{{$course->name}}" />
        <div class="overlay">
          <div class="overlay-inside" data-toggle="tooltip" data-placement="top" title="{{$course['title']}}">
            <i class="fas fa-share"></i>
          </div>
        </div>
      </a>
    
    </div>

    <div class="course-card-content">
      <p class="title text-center">
        <a href="/courses/detail/{{$course->slug}}" class="text-center text-purple">{{ $course['name'] }}</a></p>
      <p class="description text-grey text-center">
        <?php
       
        $str = $course['short_description'];
        if (strlen($str) > 60) {
          $str = substr($str, 0, 60) . '...';
        }
        echo $str;
        ?>
      </p>
      <p class="price text-grey  text-center">
        {{--@if($course->start >= Carbon\Carbon::today()->toDateString())
        Available from: {{Carbon\Carbon::parse($course->start)->isoFormat('MMMM Do YYYY')}}
        @else
        Available from: {{Carbon\Carbon::parse($course->start)->isoFormat('MMMM Do YYYY')}}
        @endif--}}
      </p>
      <p class="readmore text-center">
        <a href="/courses/detail/{{$course->slug}}">Read More <i class="fas fa-arrow-right"></i></a>
      </p>
    </div>


    @if(isset($uwishlist))
    

    @endif
  </article>
</div>

<share-modal></share-modal>
