@php
session_start();
    header("Cache-Control: no-cache, must-revalidate");
    header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
$_SESSION['course'] = $course->id;

  use  App\Models\Resource;
   $resource = Resource::where('course_id', $course->id)->first();
   use App\Models\Mpesa;

  $subscribed = Auth::user()->subscription;

 

@endphp

<div class="jarallax">
@if((new \Jenssegers\Agent\Agent())->isDesktop())
    <img class="jarallax-img" src="{{$image}}" alt="">
    <div class="content">
      <div class="wrap mt-2">
      
      @if($subscribed != 1)
      <br>
        <p>
           <a href = "{{route('subscriptions')}}"
           class="btn btn-overall "  style="color:#fff; background:#ff6d00;">Enjoy this Quicktip
           </a></p>

          
          
           <p>
            <a style="color:#fff" data-code="{{$course->id}}" href="#codeModal" style="font-weight:400;border-radius: 25px; position:absolute;  background:#ff6d00;" class="btn btn-overall purple open-CodeDialog btn text-white mt-2" title="Enroll with code"> Enroll with code </a>
          </p>


           @else
      
        <h1 class="mt-2">{!! $text !!}</h1>
        @if($resource->resource_category == 1)
       @if(!$licensed)
          <a href="{{route('course.enroll',$course->course_id)}}/" class="btn btn-overall "  style="color:#fff; background:#ff6d00;" >Enroll Into Course</a>
          @else
      @if($enrolled)
          
        <a href="{{ $configLms['LMS_BASE']  . '/courses/' . $course->course_id . '/courseware'}}" class="btn btn-overall "  style="color:#fff; background:#ff6d00;">Take Course </a>

     @else

      <a href="{{route('course.enroll',$course->course_id)}}/"    class="btn btn-overall "  style="color:#fff; background:#ff6d00;">Take Course</a>

      @endif
      @endif
        <button type="button" class="btn btn-overall "  style="color:#fff; background:#ff6d00;" >Take Course</button>
      @elseif($resource->resource_category == 2)
      @if(!$licensed)
          <a href="{{route('course.enroll',$course->course_id)}}/" class="btn btn-overall "  style="color:#fff; background:#ff6d00;" >Enroll Into Course </a>
          @else
      @if($enrolled)
          
        <a href="{{ $configLms['LMS_BASE']  . '/courses/' . $course->course_id . '/courseware'}}" class="btn btn-overall "  style="color:#fff; background:#ff6d00;" >Take Course  </a>

     @else

      <a href="{{route('course.enroll',$course->course_id)}}/" class="btn btn-overall "  style="color:#fff; background:#ff6d00;" >Take Course</a>

      @endif
      
     
      @endif
      
      @endif
@endif

       
      </div>
      <div class = "ik-hr-white">
        <span class="hr-inner">
          <span class="hr-inner-style"></span>
        </span>
      </div>
    </div>
  
    @else

    <img class="jarallax-img" src="{{$image}}" alt="">
    <div class="content">
      <div class="wrap">
      
      @if($subscribed != 1)
      <br>
      <br>
        <p>
           <a href = "{{route('subscriptions')}}"
             class="btn text-white mt-5 "  style="font-weight:400;border-radius: 50px; position:absolute;  background:#ff6d00;  min-width: max-content;">Enjoy this Quicktip
           </a></p>

   
          <br>
           <br>
           <br>
           <p>
            <a style="color:#fff" data-code="{{$course->id}}" href="#codeModal" style="font-weight:600;border-radius: 50px; position:absolute;  background:#ff6d00; min-width: max-content;" class="btn btn-overall purple open-CodeDialog btn text-white mt-2" title="Enroll with code"> Enroll with code </a>
          </p>
          <br>


           @else
      
        {{--<h1 class="font-weight-bold mt-2"><b>{!! $text !!}</b></h1>--}}
        @if($resource->resource_category == 1)
       @if(!$licensed)
          <a href="{{route('course.enroll',$course->course_id)}}/" class="btn text-white "   style="font-weight:600;border-radius: 50px; position:absolute;  background:#ff6d00; min-width: max-content;" >Enroll Into Course</a>
          @else
      @if($enrolled)
          
        <a href="{{ $configLms['LMS_BASE']  . '/courses/' . $course->course_id . '/courseware'}}" class="btn text-white "   style="font-weight:600;border-radius: 50px; position:absolute;  background:#ff6d00; min-width: max-content;" >Take Course </a>

     @else

      <a href="{{route('course.enroll',$course->course_id)}}/" class="btn text-white "   style="font-weight:600;border-radius: 50px; position:absolute;  background:#ff6d00;min-width: max-content;" >Take Course</a>

      @endif
      @endif
        <button type="button" class="btn text-white "   style="font-weight:600;border-radius: 50px; position:absolute;  background:#ff6d00; min-width: max-content;" >Take Course</button>
      @elseif($resource->resource_category == 2)
      @if(!$licensed)
          <a href="{{route('course.enroll',$course->course_id)}}/" class="btn text-white "   style="font-weight:600;border-radius: 50px; position:absolute;  background:#ff6d00;min-width: max-content;" >Enroll Into Course </a>
          @else
      @if($enrolled)
          
        <a href="{{ $configLms['LMS_BASE']  . '/courses/' . $course->course_id . '/courseware'}}" class="btn text-white "   style="font-weight:600;border-radius: 50px; position:absolute;  background:#ff6d00; min-width: max-content;" >Take Course  </a>

     @else

      <a href="{{route('course.enroll',$course->course_id)}}/"  class="btn text-white "   style="font-weight:600;border-radius: 50px; position:absolute;  background:#ff6d00; min-width: max-content;" >Take Course</a>

      @endif
      
      @endif
      
      @endif
@endif

       
      </div>
      <div class = "ik-hr-white">
        <span class="hr-inner">
          <span class="hr-inner-style"></span>
        </span>
      </div>
    </div>


    
</div>
@endif




<!--modal-->

<div class="modal fade " id="codeModal" tabindex="-1" role="dialog" aria-labelledby="codeModal" aria-hidden="true" style="margin-top: 15%;">
    <div class="modal-dialog modal-lg" style="width:450px">
        <div class="modal-content">

            <div class="modal-body" style="text-align:center;padding:0">

                <div class="col-md-12 col-12 bg-teal-green" style="padding: 40px 20px;">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 20px;font-size: 1.5rem;color:#fff">
                      <span aria-hidden="true">&times;</span>
                  </button>

                  <h5 class="modal-title" id="exampleModalLabel" style="margin-top:30px;color:#fff">Enrollment Code</h5>

                <form method="POST" action="{{route('course.purchasedCode')}}" class="grey" id="groupPurchase">
                    @csrf
                    <input type="hidden" name="course" value="" id="confirm_course">
                    <div class="form-group editable">

                      <label for="confirm_course" style="font-size:15px; text-align:center;color:#000;"> Please enter the code shared with you to access this module to proceed.</label>

                     <input type="text" name="confirmation_code" value="" id="course" class="form-control">
                    </div>
                    <div class="form-group ">
                        <div class="form-group flex-group">
                            <button type="submit" class="btn btn-overall purple"  id="pricing-form-submit" >
                                {{ __('Submit') }}
                            </button>&nbsp;&nbsp;
                            <button type="button"  style="border-radius: 25px;" class="btn-overall btn-cancel" data-dismiss="modal" style="min-width: 96px; padding: 0.4rem 1rem;">Close</button>
                        </div>
                    </div>
                </form>



              </div>

              </div>



            </div>
           
        </div>
    </div>

<!--end-modal-->
<div id="next-section"></div>
<script>
       $(document).on("click", ".open-CodeDialog", function (e) {
        e.preventDefault();
         var _self = $(this);
         var courseId = _self.data('code');
         console.log(courseId);
         $("#confirm_course").val(courseId);
         $(_self.attr('href')).modal('show');
       });
       </script>
