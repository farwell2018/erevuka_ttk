

<section class="general-section">
    <div class="col-md-12 ">
		<div class="hrHeading">
			<h2  class="header-with-line about-sidebar-title"> Previous Session </h2>
            <hr>
		</div>
		<div class="row">
			<div class="col-lg-12">
                @foreach($previous as $item)
				<div class="card upcoming-card upcoming-side mb-3" >
                    <div class="row g-0">
                      <div class="col-md-12 ">
                        @if( $item->hasImage('session_image'))
                        <img src="{{$item->image("session_image", "default")}}" alt="{{$item->topic}}" class="img-fluid upcoming-image"/>
                      
                        @endif
                      
                      </div>
                      <div class="col-md-12 ">
                        <div class="card-body">
                            <p class=" text-grey session-card-calendar">
          
                                <span class="session-calendar"> <i class="fa fa-calendar" aria-hidden="true"></i> {{Carbon\Carbon::parse($item->start_time)->isoFormat('Do MMMM YYYY')}} </span>
                                
                      
                                <span class="session-time"> <i class="fa fa-calendar" aria-hidden="true"></i> {{Carbon\Carbon::parse($item->start_time)->format('H:i A')}} </span>
                               
                              </p>
                          <p class="card-title">{{ $item->topic}}</p>
                        
                          <p class="readmore ">
                            <span><a href="{{route('session.details',[$item->id])}}" class="btn btn-overall btn_watch_video">Watch Video</a></span> 
                  
                            <span><a href="{{route('session.details',[$item->id])}}" class="btn btn-overall btn_take_course">Self paced session</a></span>
                          </p>
                  
                        </div>
                      </div>
                    </div>
                  </div>
            @endforeach
			</div>
		</div>
	</div>

</section>