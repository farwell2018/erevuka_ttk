@if((new \Jenssegers\Agent\Agent())->isDesktop())
<div class="container-fluid ">
  <div class="row justify-content-center" >
    <div class="card resource-filter home-resource-filter mb-3 mt-4">
      <div class="col-12">
        <div class="row justify-content-center">
       
            
              <div class="col-12">
                <form action="{{route('home')}}" method="POST">
                    @csrf
                    <div class="row justify-content-center">
                      <div class="col-1  mt-4 mb-4 col-spacing">
                        <img src="{{asset('images/icons/filter_icon.svg')}}" style="float: right;"/>
                      </div>
                     
                      <div class="col-8 col-spacing">

                        <select name="category" id="" class="form-control mt-4 mb-4 ">
                            <option>Filter by Resource Category</option>
                            @foreach($resourceTypes as $theme)
                            <option value="{{$theme->id}}"> {{$theme->title}}</option>
                           @endforeach
                          </select>
                      </div>
                      
                      <div class="col-2 col-spacing">
                        <button type="submit" class="btn btn-overall btn_register mt-4 mb-4">Search</button>
                      </div>
                    </div>
                  </form>
    
              </div>
           </div>

      </div>
    </div>
    </div>
</div>
@elseif((new \Jenssegers\Agent\Agent())->isTablet())
<div class="container-fluid ">
  <div class="row justify-content-center" >
    <div class="card resource-filter home-resource-filter mb-3 mt-4">
      <div class="col-12">
        <div class="row justify-content-center">
       
            
              <div class="col-12">
                <form action="{{route('home')}}" method="POST">
                    @csrf
                    <div class="row justify-content-center">
                      <div class="col-1  mt-4 mb-4 col-spacing">
                        <img src="{{asset('images/icons/filter_icon.svg')}}" style="float: right;"/>
                      </div>
                     
                      <div class="col-8 col-spacing">

                        <select name="category" id="" class="form-control mt-4 mb-4 ">
                            <option>Filter by Resource Category</option>
                            @foreach($resourceTypes as $theme)
                            <option value="{{$theme->id}}"> {{$theme->title}}</option>
                           @endforeach
                          </select>
                      </div>
                      
                      <div class="col-2 col-spacing">
                        <button type="submit" class="btn btn-overall btn_register mt-4 mb-4">Search</button>
                      </div>
                    </div>
                  </form>
    
              </div>
           </div>

      </div>
    </div>
    </div>
</div>

@else 
<div class="container-fluid">
 
  <div class="card resource-filter home-resource-filter mb-3 mt-3">
    <div class="col-12">
      <div class="row">
     
            <div class="col-12">
              <form action="{{route('home')}}" method="POST">
                  @csrf
                  <div class="row">
                   
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <select name="category" id="" class="form-control mt-4 mb-2 minimal">
                          <option>Filter by Resource Category</option>
                          @foreach($resourceTypes as $theme)
                          <option value="{{$theme->id}}"> {{$theme->title}}</option>
                         @endforeach
                        </select>
                        
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                      <button type="submit" class="btn btn-overall btn_register btn-xs-mobile mt-1 mb-2">Search</button>
                    </div>
                  </div>
                </form>
  
            </div>
         </div>

    </div>
    
  </div>
</div>


@endif