@php
 $configLms = config()->get("settings.lms.live");   

@endphp
        <section class="detailed-tree">
          <div class="start">            
           {{-- @if($course->start >= Carbon\Carbon::today()->toDateString())
            <p class="span-space"><strong> Available from:</strong> {{Carbon\Carbon::parse($course->start)->isoFormat('MMMM Do YYYY')}}</p>
            @else
            @if($course->end <= Carbon\Carbon::today()->toDateString())
          <p class="span-space"><strong>Ended:</strong> {{Carbon\Carbon::parse($course->end)->isoFormat('MMMM Do YYYY')}}</p>
            @else
          <p class="span-space"><strong> Available from:</strong> {{Carbon\Carbon::parse($course->start)->isoFormat('MMMM Do YYYY')}}</p>
            @endif
            @endif--}}
           <p class="span-space"><strong>Effort:</strong> {{$course->effort}}</p>
          

       
      @guest
         <p>
           <a href = "{{route('user.login')}}"
             class="btn btn-overall purple">Enrol for this course <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
           </a></p>
      @else
          @if(!$licensed)
          <p><a class="btn btn-overall purple" href="{{route('course.enroll',$course->course_id)}}/">Enroll into Course</a></p>
          @else
      @if($enrolled)
          
        <p><a class="btn btn-overall purple" href="{{ $configLms['LMS_BASE']  . '/courses/' . $course->course_id . '/courseware'}}" target="_blank">Take Course</a></p>

      @else
      <p><a class="btn btn-overall purple" href="{{route('course.enroll',$course->course_id)}}/">Enroll into Course</a></p>

      @endif
      @endif
      @endguest
       
        </div>
        </section>
      
      