<div class="container" style="margin-top: 15px;">
    <div class="row">
      <div class="col-12">
        <div class="comments">
            <h4>New Comment</h4>
          <div class="comment-box add-comment" style="">
            <span class="commenter-pic">
              <img src="{{ asset('uploads/images.jpg') }}" class="img-fluid">
            </span>
            <span class="commenter-name" style="width: 100%;
            padding-left: 75px;
            position: absolute;
            top: 20px;
            left: 0px">
            <form method="POST" action="" id="comment-discuss">
               
                <input type="text" name="id" value="{{request()->id}}" hidden>
                <textarea class="form-control text-input" name="comment" placeholder="What are your thoughts?" data-emojiable="true" data-emoji-input="unicode">Add a comment</textarea>
              {{-- <input type="text"  class="form-control text-input" data-emojiable="true" data-emoji-input="unicode" placeholder="What are your thoughts?" name="comment"> --}}
              
              <div class="submitbuttons">
                {{-- <a href="{{url()->previous()}}" class="btn btn-outline-dark btn-cancel btn-sm">Cancel</a> --}}
                <button type="submit" class="button button-submit btn-sm">Submit Comment</button>
            </div>                                      
            </span>
          </div>
        </form>
        
        </div>
      </div>
    </div>
  </div>
  