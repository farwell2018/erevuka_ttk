      <ul class="nav nav-pills course-tabs" role="tablist">
      <?php foreach (config('app-constants.profile') as $el) : ?>

        <li class="nav-item">
          @if (\Route::current()->getName() == $el['route'] || (strpos(\Request::url(), $el['route']) !== false))
        <a class="nav-link active first-tab" href="{{route($el['route'])}}/">{{$el['title']}}</a>
        @else
        <a class="nav-link first-tab" href="{{route($el['route'])}}/">{{$el['title']}}</a>
        @endif
      </li>
      <?php endforeach; ?>
      </ul>