<div class="course-card mt-3 sessions-card ">
  <article>
    <div class="thumbnail">

      @if( $item->hasImage('resource_image'))
      @if($item->course_id)
      <a href="{{route('course.detail',['slug'=>$item->course_id])}}"><img src="{{$item->image("resource_image", "default")}}" alt="{{$item->title}}" /></a>
      @if($item->resource_category == 2)
      <a href="{{route('course.detail',['slug'=>$item->course_id])}}"><button class="btn course-btn font-weight-bold"><i class="fas fa-play"></i>Watch</button></a>
      @endif
      @else
      <a href="{{route('resource.details',['id'=>$item->id])}}"><img src="{{$item->image("resource_image", "default")}}" alt="{{$item->title}}" /></a>
      @if($item->resource_category == 2)
      <a href="{{route('resource.details',['id'=>$item->id])}}"> <button class="btn course-btn font-weight-bold "><i class="fas fa-play"></i>Watch</button></a>
      @endif
      @endif
      @endif
        <div class="overlay">
            @if($item->course_id)

             @if($item->resource_category == 1)
          <div class="overlay-inside" data-toggle="tooltip" data-placement="top" title="{{$item->title}}">
            <i class="pro-icon icon-pro "></i>
          </div>
          @elseif($item->resource_category == 2)
          <div class="overlay-inside" data-toggle="tooltip" data-placement="top" title="{{$item->title}}">
            <i class="pro-icon icon-tip"></i>
          </div>
          @endif
          @endif
        </div>
    </div>

    <div class="course-card-content session-card-content ">
    {{-- @if($item->course_id)
      <p class="title "><a href="{{route('course.detail',['slug'=>$item->course_id])}}" class=" text-purple">{{ $item->title}}</a></p>

      @else
      <p class="title "><a href="{{route('resource.details',['id'=>$item->id])}}" class=" text-purple">{{ $item->title}}</a></p>
      @endif--}}

      <?php
        $str = $item['description'];
        if (strlen($str) > 50) {
          $str = truncateString($str, 50);
        }
        
        ?>
      @if($item->course_id)
      {!!$str!!}<small class="lead" style="color:#5b9fdd;"><a href="{{route('course.detail',['slug'=>$item->course_id])}}">Read More</a></small>
      @else 
      {!!$str!!}<small class="lead" style="color:#5b9fdd;"><a href="{{route('resource.details',['id'=>$item->id])}}" >Read More</a></small>
      @endif

    </div>
    <div class="card-footer ">

      @if(!empty($item->enrollment))
       @if($item->enrollment != 1 )
      {{--<div class="overlay-inside-right" data-toggle="tooltip" data-placement="top" title="">
        <i class="progress-icon icon-not-started text-primary" title="Not Started"></i>
        <span class=" text-grey session-card-calendar progress-resource">{{ "Not started" }}</span>

      </div>--}}

       @else

      @if(!empty($item->completion))
      @if($item->completion == 1)
      <div class="overlay-inside-right" data-toggle="tooltip" data-placement="top" title="">
        <i class="progress-icon icon-completed" title="Completed"></i>
        <span class=" text-grey session-card-calendar progress-resource">{{ "Completed" }}</span>

      </div>
     
      @else
      <div class="overlay-inside-right" data-toggle="tooltip" data-placement="top" title="">
        <i class="progress-icon icon-retake" title="Retake Course"></i>
        <span class=" text-grey session-card-calendar progress-resource">{{ "Completed but Failed" }}</span>
      </div>
      @endif
     @else
     <div class="overlay-inside-right" data-toggle="tooltip" data-placement="top" title="">
      <i class="progress-icon icon-progress" title="In Progress"></i>
      <span class=" text-grey session-card-calendar progress-resource">{{ "In Progress" }}</span>
    </div>

     @endif

  @endif
   @endif

              <p class=" text-grey session-card-calendar overlay-inside-right" >

                  <span class="session-calendar">{{$item->video_length}} Min </span>
                 {{-- <span class="session-time">  {{Carbon\Carbon::parse($item->created_at)->isoFormat('Do MMMM YYYY')}}</span>--}}
                  <!-- <span class="card-span-icon" ><img src="{{asset('images/resource_categories/'.$item->type->icon_name)}}" title="{{$item->type->title}}"/></span> -->
                </p>

  </div>
  </article>
</div>
