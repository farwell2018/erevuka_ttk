@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    <div class="card enrolled-card mx-2 more-spacing">
        <div class="card-body">
            <div class="row">
                <div class="col-3 pe-0">
                    <div class="responsive-rectangle">
                        <div class="image-wrapper">
                            <img src="{{ $license->course->course_image_uri }} " alt="">
                        </div>
                    </div>


                </div>

                <div class="col-9 ps-0">
                    <div class="enrolled-card-info ps-3 pt-3">
                        <div class="row">
                            <div class="col-6 col-sm-5">
                                <div class="title"><a target="_blank"
                                        href="{{ $configLms['LMS_BASE'] . '/courses/' . $license->course->course_id . '/courseware' }}">{{ $license->course->name }}</a>
                                </div>
                                <div class="sub-title"> Start Date:
                                    {{ Carbon\Carbon::parse($license->course->start)->isoFormat('Do MMMM YYYY') }} |
                                    {{ $license->getEnrollments($license->course->id) }}
                                    {{ $license->getEnrollments($license->course->id) > 1 ? 'people have enrolled' : 'person has enrolled' }} |
                                    {{ $license->getCompleted($license->course->id) > 1 ? 'people have completed' : 'person has completed' }}
                                </div>
                            </div>
                            <div class="col-6 col-sm-7">
                                <button class="btn btn-overall btn_border_primary mt-2">Status:
                                    {{ $license->status }}</button>

                                @if ($license->grade >= $passMark &&
                                    $license->status === 'Completed' &&
                                    $license->getEvaluationStatus($license->course->id, $license->user->id) == 0)
                                    <a href="{{ route('course.evaluate', ['courseid' => $license->course->id]) }}"
                                        alt="evaluate"
                                        class="btn btn-overall btn_border_secondary btn_solid_secondary_special mt-2 me-4 float-end">
                                        Evaluate & download badge </a>
                                @elseif($license->grade >= $passMark &&
                                    $license->status === 'Completed' &&
                                    $license->getEvaluationStatus($license->course->id, $license->user->id) > 0)
                                    <a href="{{ $license->action }}" alt="download"
                                        class="btn btn-overall btn_solid_secondary mt-2 me-4 float-end" target="_blank"> Download
                                        badge </a>
                                @elseif($license->grade < $passMark && $license->status === 'Completed but Failed')
                                    <a href="{{ $license->action }}" alt="download" target="_blank"
                                        class="btn btn-overall btn_solid_tertiary mt-2 me-4 float-end"> Retake course
                                    </a>
                                @else
                                    <a href="{{ $license->action }}" alt="download" target="_blank"
                                        class="btn btn-overall btn_solid_additional mt-2 me-4 float-end"> Continue with
                                        course </a>
                                @endif




                            </div>

                        </div>

                    </div>
                    <hr>
                    <div class="enrolled-card-info  ">
                        <div class="row">
                            <div class="col-3 stats ps-0 pe-0">
                                <div>
                                    <h5 class="innerstats"><i class="icon-calendar" title="clock"></i> <span>Enrollment
                                            Date</span></h5>
                                    <p>{{ Carbon\Carbon::parse($license->enrolled_at)->isoFormat('Do MMMM YYYY') }}
                                    </p>
                                </div>
                            </div>
                            <div class="col-3 stats ps-0 pe-0">
                                <div>
                                    <h5 class="innerstats"><i class="icon-calendar" title="clock"></i><span> Course Completion Date </span></h5>
                                    @if(!empty($license->completion_date))
                                    <p>{{Carbon\Carbon::parse($license->completion_date)->isoFormat('Do MMMM YYYY') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-3 stats ps-0 pe-0">
                                <div>
                                    <h5 class="innerstats"><i class="progress-icon icon-tick"
                                            title="clock"></i><span>Prev Score</span></h5>
                                    <p>{{ 0 }}</p>
                                </div>
                            </div>

                            <div class="col-3 stats ps-0 pe-0">
                                <div>
                                    <h5 class="innerstats"><i class="progress-icon icon-tick" title="clock"></i><span>
                                            Current Score</span></h5>
                                    <p>{{ $license->grade * 100 }}%</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

@elseif((new \Jenssegers\Agent\Agent())->isTablet())
@else
    <div class="card enrolled-card">
        <div class="card-body">
            <div class="row">
                <div class="col-12 ">
                    <div class="responsive-rectangle">
                        <div class="image-wrapper">
                            <img src="{{ $license->course->course_image_uri }} " alt="">
                        </div>
                    </div>


                </div>

                <div class="col-12 ">
                    <div class="enrolled-card-info ps-2 pt-2">
                        <div class="row">
                            <div class="col-12">
                                <div class="title"><a target="_blank"
                                        href="{{ $configLms['LMS_BASE'] . '/courses/' . $license->course->course_id . '/courseware' }}">{{ $license->course->name }}</a>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-12">

                                    <h5 class="innerstats"><i class="icon-calendar" title="clock"></i> <span>Enrollment
                                            Date:</span>
                                        <span>{{ Carbon\Carbon::parse($license->enrolled_at)->isoFormat('Do MMMM YYYY') }}</span>
                                    </h5>

                                </div>
                                <div class="col-12">
                                    <h5 class="innerstats"><i class="icon-calendar" title="clock"></i><span> Course Completion
                                            Date: </span>
                                            @if(!empty($license->completion_date))
                                            <span>{{Carbon\Carbon::parse($license->completion_date)->isoFormat('Do MMMM YYYY') }}</span>
                                           
                                            @endif
                                        
                                    </h5>
                                </div>
                                <div class="col-12">

                                    <h5 class="innerstats"><i class="progress-icon icon-tick" title="clock"></i><span>
                                            Current Score</span> <span>{{ $license->grade * 100 }}%</span></h5>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-overall btn_border_primary mt-2">Status:
                                        {{ $license->status }}</button>
                                </div>

                                <div class="col-12">
                                    @if ($license->grade >= $passMark &&
                                        $license->status === 'Completed' &&
                                        $license->getEvaluationStatus($license->course->id, $license->user->id) == 0)
                                        <a href="{{ route('course.evaluate', ['courseid' => $license->course->id]) }}"
                                            alt="evaluate" class="btn btn-overall btn_border_secondary mt-2"> Evaluate &
                                            download badge </a>
                                    @elseif($license->grade >= $passMark &&
                                        $license->status === 'Completed' &&
                                        $license->getEvaluationStatus($license->course->id, $license->user->id) > 0)
                                        <a href="{{ $license->action }}" alt="download"
                                            class="btn btn-overall btn_solid_secondary mt-2" target="_blank"> Download badge </a>
                                    @elseif($license->grade < $passMark && $license->status === 'Completed but failed')
                                        <a href="{{ $license->action }}" alt="download" target="_blank"
                                            class="btn btn-overall btn_solid_tertiary mt-2 "> Retake course </a>
                                    @else
                                        <a href="{{ $license->action }}" alt="download" target="_blank"
                                            class="btn btn-overall btn_solid_additional mt-2 "> Continue with course </a>
                                    @endif


                                </div>


                            </div>

                        </div>

                    </div>
                    {{-- <hr>
                   <div class="enrolled-card-info  ">
                   
                   </div> --}}
                </div>

            </div>

        </div>
    </div>

@endif
