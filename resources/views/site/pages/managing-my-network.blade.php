@extends('layouts.app_no_js')

@section('title','Background')

@section('content')
@php
use App\Models\ResourceType;
if(Auth::check()){
  $role = Auth::user()->role;
$resourceTypes = ResourceType::where('user_role','LIKE',"%{$role}%")->get();
}
if(Auth::check() && $role =='SUPERADMIN')
{
  $resourceTypes = ResourceType::orderBy('position','asc')->get();
}


@endphp
<div class="background-page">
    <section class="general-section business-general top-general" style="margin-bottom:0">
     
        <div class="container-fluid">
            <div class="row alignment-class">
        <div class="col-12 ">
            <div class="hrHeading">
                <h2 class="line-header network_header_inner">
                    {{ $pageItem->title }}
                </h2>
            </div>
                <div class="col-12">
                <div class="whatYouGetListing gray">

<span id="lessText"> {!! truncateString($pageItem->description,200) !!}</span>
<a href="#">Show More</a>
</div>

<div class="whatYouGetListing orange" style="display: none;">
<span id="moreText">{!! $pageItem->description !!}</span>
<a href="#">Show Less</a>
</div>
                </div>
             
        </div>
        </div>
        </div>
        
    </section>

    @if ((new \Jenssegers\Agent\Agent())->isDesktop())
    <section class="general-section ">
      <div class="container-fluid">
        @include('site.includes.components.courses_filter',['resourceTypes' => $resourceTypes,'resourceCategorySlugs'=> $resourceCategorySlugs,'key'=>$pageItem->key])
      </div>
    </section>
    @elseif ((new \Jenssegers\Agent\Agent())->isTablet())
    <section class="business-general ">
      <div class="container-fluid">
        @include('site.includes.components.courses_filter',['resourceTypes' => $resourceTypes,'key'=>$pageItem->key])
      </div>
    </section>
     
    @else 
    <section class="business-general ">
      <div class="container-fluid">
        @include('site.includes.components.courses_filter',['resourceTypes' => $resourceTypes, 'resourceCategorySlugs'=> $resourceCategorySlugs,'key'=>$pageItem->key])
      </div>
    </section>

    @endif

    
    @if((new \Jenssegers\Agent\Agent())->isDesktop())
    <div class="container-fluid p-0">
    @elseif ((new \Jenssegers\Agent\Agent())->isTablet())
    <div class="container-fluid p-0">
    @else 
    <div class="container-fluid">
    @endif
 
    {!! $pageItem->renderBlocks(true, [], ['type' => $resourcetype, 'search' => $freeSearch,'slug_type'=>$slug_type]) !!}


</div>

</div>
@endsection

@section('js')

<script>
    $(document).ready(function() {
  
  // Get current page URL
  var url = window.location.href;
  
  
  
  // remove # from URL
  url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
  
  // remove parameters from URL
  url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
  
  // select file name
  url = url.split('/')[4];
  
  
  // console.log(url);
  
  // Loop all menu items
  $('.navbar-nav .nav-item').each(function(){
  
   // select href
   var href = $(this).find('a').attr('href');
  
   link = href.split('/')[4];
   
   // Check filename
   if(link === 'managing-my-network'){
  
    // Add active class
    $(this).addClass('active');
   }
  });
  });
  </script>

<script type="text/javascript">
// Hide the extra content initially:
            $('.read-more-content').addClass('hide_content')
            $('.read-more-show, .read-more-hide').removeClass('hide_content')
 
            // Set up the toggle effect:
            $('.read-more-show').on('click', function(e) {
              $(this).next('.read-more-content').removeClass('hide_content');
              $(this).addClass('hide_content');
              e.preventDefault();
            });
            $('.read-more-hide').on('click', function(e) {
              var p = $(this).parent('.read-more-content');
              p.addClass('hide_content');
              p.prev('.read-more-show').removeClass('hide_content'); // Hide only the preceding "Read More"
              e.preventDefault();
            });

            $('oragnge').hide();
    $('.gray a').click(function() {
      $('.gray').hide();
      $('.orange').show();
    });
    $('.orange a').click(function() {
      $('.gray').show();
      $('.orange').hide();
    })
</script>



@endsection