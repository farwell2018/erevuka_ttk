@extends('layouts.app')
@section('title','Background')
@section('content')
<div class="background-page">
    @if( $pageItem->hasImage('hero_image'))
    @php $image = $pageItem->image("hero_image", "default") ; 
    $text = $pageItem->header_title;
    @endphp
    @include('site.includes.components.parallax-o',
        [
            'image'=> $image,
            'text'=>$text
        ])
    @endif
</div>
<div class="clearfix">
    <br /><br />
</div>
<div class="body">
    @include('site.includes.components.discuss')
</div>
@endsection

