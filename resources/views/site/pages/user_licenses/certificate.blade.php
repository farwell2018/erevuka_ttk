<html>

<head>
    <style>
    @font-face {
   
    font-family: 'CenturyGothic';
    src: url('{{ storage_path("fonts/Century_Gothic.ttf") }}') format("truetype");
    font-weight: normal; 
    font-style: normal; 
    font-variant: normal;
}
        @page {
            size: a4 landscape;
            margin: 0;
            padding: 0; // you can set margin and padding 0
        }

        body {
            padding-top: 0 !important;
            padding-bottom: 0 !important;
            padding-top: 0 !important;
            padding-bottom: 0 !important;
            margin: 0 !important;
            width: 100% !important;
            -webkit-text-size-adjust: 100% !important;
            -ms-text-size-adjust: 100% !important;
            -webkit-font-smoothing: antialiased !important;
            font-family: 'CenturyGothic';

        }

        table {
            page-break-inside: auto;
            border: 0;
            font-family: 'CenturyGothic';
        }

        tr {
            page-break-inside: avoid;
            page-break-after: auto;
            border: 0;
            font-family: 'CenturyGothic';
        }

        th {
            border: 0;
            font: inherit;
            font-size: 100%;
            margin: 0;
            padding: 0;
            text-align: left;
            color: #1D2F5D;
            font-family: 'CenturyGothic';


        }

        .tableContent img {
            border: 0 !important;
            display: block !important;
            outline: none !important;
        }

        a {
            color: #382F2E;
        }

        a.blend {
            color: #fff;
        }

        p,
        h1,
        h2,
        ul,
        ol,
        li,
        div {
            margin: 0;
            padding: 0;
        }

        h1,
        h2 {
            font-weight: normal;
            background: transparent !important;
            border: none !important;
        }

        .header {
            margin-top: 4rem;
            margin-bottom: 2rem;
        }

        @media only screen and (max-width: 480px) {

            table[class="MainContainer"],
            td[class="cell"] {
                width: 100% !important;
                height: auto !important;
            }

            td[class="specbundle"] {
                width: 100% !important;
                float: left !important;
                font-size: 1.5em;
                line-height: 17px !important;
                display: block !important;
                padding-bottom: 15px !important;
            }

            td[class="specbundle2"] {
                width: 80% !important;
                float: left !important;
                font-size: 1.3em;
                line-height: 17px !important;
                display: block !important;
                padding-bottom: 10px !important;
                padding-left: 10% !important;
                padding-right: 10% !important;
            }

            td[class="spechide"] {
                display: none !important;
            }

            img[class="banner"] {
                width: 100% !important;
                height: auto !important;
            }

            td[class="left_pad"] {
                padding-left: 15px !important;
                padding-right: 15px !important;
            }

        }

        @media only screen and (max-width: 540px) {

            table[class="MainContainer"],
            td[class="cell"] {
                width: 100% !important;
                height: auto !important;
            }

            td[class="specbundle"] {
                width: 100% !important;
                float: left !important;
                font-size: 13px !important;
                line-height: 17px !important;
                display: block !important;
                padding-bottom: 15px !important;
            }

            td[class="specbundle2"] {
                width: 80% !important;
                float: left !important;
                font-size: 13px !important;
                line-height: 17px !important;
                display: block !important;
                padding-bottom: 10px !important;
                padding-left: 10% !important;
                padding-right: 10% !important;
            }

            td[class="spechide"] {
                display: none !important;
            }

            img[class="banner"] {
                width: 100% !important;
                height: auto !important;
            }

            td[class="left_pad"] {
                padding-left: 15px !important;
                padding-right: 15px !important;
            }

        }

        .contentEditable h2.big,
        .contentEditable h1.big {
            font-size: 1.4em !important;
        }

        .big-bold {
            font-size: 1.2em !important;
            font-weight: 900;
        }

        .bigger-bold {
            font-size: 1.3em !important;
            font-weight: 900;
        }

        .contentEditable h2.bigger,
        .contentEditable h1.bigger {
            font-size: 37px !important;
        }

        td,
        table {
            vertical-align: top;
        }

        td.middle {
            vertical-align: middle;
        }

        a.link1 {
            font-size: 13px;
            color: #27A1E5;
            line-height: 24px;
            text-decoration: none;
        }

        a {
            text-decoration: none;
        }

        a.btn-drk-left {
            color: #ffffff;
            border-radius: 25px;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            background: #000151;
            padding: 10px 20px;
            font-weight: 900;
        }

        .link2 {
            color: #ffffff;
            border-top: 10px solid #27A1E5;
            border-bottom: 10px solid #27A1E5;
            border-left: 18px solid #27A1E5;
            border-right: 18px solid #27A1E5;
            border-radius: 3px;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            background: #27A1E5;
        }

        .link3 {
            color: #555555;
            border: 1px solid #cccccc;
            padding: 10px 18px;
            border-radius: 3px;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            background: #ffffff;
        }

        .link4 {
            color: #27A1E5;
            line-height: 24px;
        }


        p {
            font-size: 18px;
            line-height: 1.7em;
        }

        .contentEditable li {}

        .appart p {}

        .bgItem {
            background: #ffffff;
        }

        .bgBody {
            background: #ffffff;
        }

        img {
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
            width: auto;
            /* max-width: 100%; */
            clear: both;
            display: block;
            float: none;
        }

        img.logo {
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
            float: none;
            display: initial;
            padding: 0 20px;
        }
        .center {
            display: block;
            margin-left: auto;
            margin-right: auto;

        }

        .left {
            display: block;
            float: left;
            margin: 2rem;

        }

        .right {
            display: block;
            float: right;
            margin: 5rem;

        }

        img {
            min-width: 100px;
        }

        hr.class-1 {
        border-top: 0.5px solid #ced4da;
    }

    </style>

</head>

<body paddingwidth="0" paddingheight="0" style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; background-image: url('https://ttk.farwell-consultants.com/images/certificateAssets/certificate_design.png');
    background-size:100% 100%;
    background-repeat: no-repeat;" offset="0" toppadding="0" leftpadding="0">

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>

                                <td style="text-align:center"><img src="{{ asset('images/icons/logo.png') }}"
                                        style="margin-top:3.9rem;width:214px;"></td>
                            </tr>
                            <tr>
                                <td style="text-align:center">
                                    <h2 style="color:#58595B; text-transform:none; font-size:62px; padding-bottom:0;  "
                                        class="center"> CERTIFICATE </h2>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center">
                                    <p style="color:#58595B;font-size:40px" class="center"> of Completion
                                    <p>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center">
                                    <img src="{{ asset('images/certificateAssets/Linedivider.png') }}"
                                        style="margin-top:0.5rem;">
                                </td>
                            </tr>
                        </tbody>
                    </table>


                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align:center">
                                <p style="color:#58595B;margin-top: 5px; font-size:24px">This certificate is presented
                                    to</p>
                                <p style="color:#58595B;font-weight: bold;font-size:28px;">{{ $name }}</p>
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center">
                                <p style="color:#58595B;font-size:24px">for participating in the course</p>
                                <p style="color:#58595B;font-size:28px"><b> {!! wordwrap($course_name, 40, "<br />\n") !!}</b> </p>

                               
                            </td>
                        </tr>

                       

                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                       
                    </table>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>

                            <tr>
                                <td style="text-align:left; width:50%">

                                    <p style="font-size:20px; height:auto; margin-left:58%;margin-bottom:0;margin-top:4rem">
                                        {{ $date }}</p>
                                        <hr class="class-1" style="margin-left:56%; width:30%;"/>
                                    <p style="margin-left:66%; line-height:1em;font-size:20px;">Date</p>
                                </td>
                                <td>
                                    <hr class="class-1" style="margin-left:10%; width:30%;margin-top:6.5rem">
                                    <p style="margin-left:18%; line-height:1em;font-size:20px;">Signature</p>

                                </td>

                            </tr>
                           
                        </tbody>
                    </table>

                </td>
            </tr>
        </tbody>
    </table>
    <p style="line-height:1em;font-size:11px; position:absolute; bottom:40px;right:12rem; color:#6f727a;"> Serial Number: {{ $cert_number }} </p>
</body>

</html>
