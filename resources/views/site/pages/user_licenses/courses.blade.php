@extends('layouts.app')

@section('title', 'My Profile')

@section('content')
    <div class="courses-page">
        @include('site.includes.components.parallax-o', [
            'image' => asset('images/banners/course_header.jpg'),
            'text' => 'My Profile',
        ])

        @component('site.includes.components.breadcrumbs')
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="{{ route('profile.courses') }}" class="active">My Courses</a>
                </li>
            </ol>
        @endcomponent



        <div class="profile-row">

            <div class="row">

                <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
                    @include('site.includes.components.timeline')

                </div>

                
                <div class="col-lg-12  col-md-12 col-sm-12 col-12">
  
<div class="col-lg-12  col-md-12 col-sm-12 col-12">               
  <div class="row alignment-class">
      <div class="col-12">
          <div class="whatYouGetListing mt-4">
            <p>Track your progress on all courses enrolled into on this page. For all completed courses, you can also give feedback and download a certificate of completion on this page.</p>
          </div>
      </div>


  </div>
        </div>
  


</div>
                <div class="offset-md-2 col-lg-10  col-md-10 col-sm-12 col-12">
                    @include(
                        'site.includes.components.user-courses-filter'
                    )
                </div>
            </div>

            <div class="row no-gutters">


                <div class="col-lg-12 col-md-12 col-sm-12 col-12 d-flex p-3">
                    <div class="container-fluid">

                        <!-- Filter section -->
                        <!-- End of filter section -->
                        <!-- Course list -->
                        <div class="tab-content">
                            <div id="home" class="container-fluid tab-pane active">
                                <div class="course-card detail-card">
                                    <article>
                                        <div class="course-content">
                                            <div class="help">
                                            </div>
                                            @if (!count($licenses))
                                                <div class="row justify-content-md-center no-gutters">
                                                    <div class="text-center col-12">
                                                        <p>
                                                            Your list of courses is currently empty.
                                                        </p>

                                                    </div>
                                                </div>
                                            @endif
                                            @php $count = 0;
                                                
                                                $today = Carbon\Carbon::today()->format('d-m-y');
                                                
                                            @endphp
                                            @foreach ($licenses as $license)
                                                @php $count ++; 
                                                $configLms = config()->get("settings.lms.live");
                                                @endphp
                                                 


                                                <my-course class="d-none d-lg-flex"
                                                    cert_title="{{ $license->course->name }}"
                                                    title="{{ $license->course->name }}"
                                                    image="{{ $license->course->course_image_uri }}"
                                                    date="{{ Carbon\Carbon::parse($license->enrolled_at)->isoFormat('Do MMMM YYYY') }}"
                                                    status="{{ $license->status }}" progress="{{ $license->grade * 100 }}%"
                                                    prev-score="0" current-score="{{ $license->grade }}"
                                                    action="{{ $license->action }}"
                                                    link="{{ $configLms['LMS_BASE']. '/courses/' . $license->course->course_id . '/courseware' }}"
                                                    start="{{ Carbon\Carbon::parse($license->course->start)->format('M jS Y') }}"
                                                    end="{{ ($license->completion_date)? Carbon\Carbon::parse($license->completion_date)->format('M jS Y') : 'Not Complete'}}"
                                                    enrolled="{{ $license->getEnrollments($license->course->course_id) }}"
                                                    owner="{{ Auth::user()->name }}" count="{{ $count }}"
                                                    evaluate="{{ $license->getEvaluationStatus($license->course->course_id, $license->user->id) }}"
                                                    evalaction="{{ route('course.evaluate', ['id' => $license->course->course_id]) }}"
                                                    today="{{ $today }}"></my-course>


                                                <my-course-mobile class="d-inline-block d-lg-none "
                                                    cert_title="{{ $license->course->name }}"
                                                    title="{{ $license->course->name }}"
                                                    image="{{ $license->course->course_image_uri }}"
                                                    date="{{ Carbon\Carbon::parse($license->enrolled_date)->format('M jS Y') }}"
                                                    status="{{ $license->status }}" progress="{{ $license->grade * 100 }}"
                                                    prev-score="0" current-score="{{ $license->grade }}"
                                                    action="{{ $license->action }}"
                                                    start="{{ Carbon\Carbon::parse($license->course->start)->format('M jS Y') }}"
                                                    end="{{ Carbon\Carbon::parse($license->course->end)->format('M jS Y') }}"
                                                    enrolled="{{ $license->getEnrollments($license->course->id) }}"
                                                    evaluate="{{ $license->getEvaluationStatus($license->course->course_id, $license->user->id) }}"
                                                    evalaction="{{ route('course.evaluate', ['id' => $license->course->course_id]) }}"
                                                    count="{{ $count }}" today="{{ $today }}">
                                                </my-course-mobile>
                                            @endforeach

                                        </div>
                                    </article>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>


        </div>
    </div>
    <!-- End of future courses list -->

    <div class="clearfix"><br /></div>
@endsection
