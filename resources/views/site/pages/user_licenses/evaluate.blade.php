@extends('layouts.evaluate')


@section('title','Evaluate')


@section('content')
<div class="courses-page">
  @include('site.includes.components.parallax-o',[
  'image'=>asset("images/banners/course_header.jpg"),
  'text'=>'Course Evaluation'
  ])

  @component('includes.components.breadcrumbs')
  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="{{ route('home') }}">Home</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">
      <a href="{{route('course.evaluate',['id'=> $id])}}" class="active">Course Evaluation</a>
    </li>
  </ol>
  @endcomponent
  @if(Session::has('redirectMessage'))
      
        <script>
        jQuery(document).ready(function($){
         $("#notificationModal").addClass('show');
      });
        </script>
        @php Session::forget('redirectMessage') @endphp
      @else
            {{-- <script>
        window.addEventListener('load', function() {
            if(!window.location.hash) {
                window.location = window.location + '#/';
                window.location.reload();
            }
        });
      </script> --}}
      @endif
<div class="evaluate-section">
    <div class="row justify-content-center about-row">

      <div class="col-md-12">
      <p style="padding-top:20px; ">Thank you for you for participating in this 5 minute evaluation.  Your responses will directly affect how we undertake similar initiatives in future so thoughtful responses are appreciated.  Should you wish to provide feedback in person.</a></p></div>
        <div class="col-md-12">
          
         
         
          @if($sections) 
    
          {!! $sections->renderBlocks(false,[], ['id' => $id]) !!}

        @endif
        </div>
    </div>
</div>

</div>
</div>
<!-- End of future courses list -->

<div class="clearfix"><br /></div>
@endsection
