@extends('layouts.app')

@section('title','My Courses')
@php
$profile_panels = [
['id' => '1', 'name' => 'View Activity Feed' , 'level' => '0','icon'=>'our-reach-icon.svg', 'icon-active'=>'our-reach-icon-active.svg'],
['id' => '2', 'name' => 'View CoPs' , 'level' => '0','icon'=>'our-reach-icon.svg', 'icon-active'=>'our-reach-icon-active.svg'], 
['id' => '3', 'name' => 'View Current Projects' , 'level' => '0','icon'=>'our-reach-icon.svg', 'icon-active'=>'our-reach-icon-active.svg']
];
@endphp
@section('content')
<div class="courses-page">
    @if($user->id == Auth::user()->id)
    @php $text = 'My Profile'; @endphp
    @else 
    @php $text = $user->name; @endphp
    @endif
  

  @include('site.includes.components.parallax',[
  'image'=>asset("images/banners/profile_header.png"),
  'text'=> $text
  ])

  @component('site.includes.components.breadcrumbs')
  <ol class="breadcrumb">
    <li class="breadcrumb-item active" aria-current="page">
    @if ($user->id == Auth::user()->id)
      <a href="{{ url()->current() }}" class="active">My Activity Feed</a>
    @else 
    <a href="{{ url()->current() }}" class="active">Activity Log</a>
    @endif
    </li>
  </ol>
  @endcomponent
  <div class="clearfix">
    <br /> 
</div>

@if((new \Jenssegers\Agent\Agent())->isDesktop())

<div class="col-md-12">     
    <div class="row alignment-class-connect profile-top">
       <div class="col-12">
           <div class="row">
            <div class="col-3">
                <div id="ppic">
                    @if($user->profile_pic)
                    @php $image = asset('uploads/'.$user->profile_pic);@endphp
                    @else 
                    @php $image = asset('images/profile/images.jpg');@endphp
                    @endif
                <div class="profile-image" style="background-image:url('{{$image}}')"></div>
                  
                </div>
            </div>
           <div class="col-9">
               <div class="profile-details">
                <h1>{{$user->name}}</h1>
                 <p class="more-information"><img src="{{asset('images/'.$flag)}}" width='30px'/> <span>{{$country->name['common']}}</span>
                
                 <span class="more-information-spacing "> <a data-target="#mySuperpower" data-toggle="modal" href="#mySuperpower" class="profile-superpower">View Her super powers <i class="fa fa-eye"></i></a></span>
                </p>

                <p class="more-information"> <span><b>{{count($user->getMyFollowers($user->id))}}</b> Followers</span>
                
                    <span class="more-information-spacing"><b> {{count($user->getMyFollowing($user->id))}}</b> Following</span>

                    <span class="profile-buttons">@if($user->id == Auth::user()->id)
                        <a href="{{ route('profile.edit') }}" class="btn btn-overall btn_login"> Edit Profile </a>
                       @else 
                       @if($user->getCommunityLink($user->id) == 0)
                      
                       <a href="{{ route('profile.myFollow', [$user->id,0])}}" class="btn btn-overall btn_register"> Follow </a>
                       @else 
                        <button class="btn btn-overall btn_register">Following</button>
                       @endif
                       @endif  
                    </span>
                   </p>
                  
               </div>
           
           </div>

           </div>
      
       </div>
    </div>
    <div class="row alignment-class-connect profile-lower">
      <div class="col-12">
         <div class="row">
            <div class="col-3">
                @include('site.includes.components.connect.side_menu')
            </div>
              <div class="col-md-9 col-12">
               
                <div class="card connect-card">
                  <article>
                      <div class="row p-3">
                        <div class="col-8">
                           
                        
                            <ul class="nav nav-pills nav-fill session-tabs group-tabs profile-tabs" role="tablist">
                              @foreach ($profile_panels as $key => $item)
      
                              @if ($key === array_key_last($profile_panels))
                          
                                  <li role="presentation"
                                      class=" nav-item {{ $item['id'] == 1 ? 'active' : '' }} tabz-spacing">
                                      <a href="#home{{ $item['id'] }}" aria-controls="home" role="tab"
                                          data-toggle="tab"> <span class="group-icon forum-icon"></span> {!! $item['name'] !!}</a>
                      
                                  </li>
                         
                              @elseif($key === array_key_first($profile_panels))
                                  <li role="presentation"
                                      class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} border-right">
                                      <a href="#home{{ $item['id'] }}" aria-controls="home" role="tab"
                                          data-toggle="tab" style="text-align: center;
                                      ">  <span class="group-icon activity-icon"></span> {!! $item['name'] !!}</a>
                                  </li>
      
                              @else
                                  <li role="presentation"
                                      class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} border-right tabz-spacing">
                                      <a href="#home{{ $item['id'] }}" aria-controls="home" role="tab"
                                          data-toggle="tab"><span class="group-icon resource-icon"></span> {!! $item['name'] !!}</a>
                                  </li>
                              @endif
                          @endforeach
                          </ul>
                          {{-- <a href="#" class="view-project"><span class="profile-icon icon-project"></span> <span class="view-project-span">View Current Project </span> </a> --}}
                          </div>
                          <div class="col-4">
                          @php 
                            $u_agent = $_SERVER['HTTP_USER_AGENT'];
                            $phone_code = $country->idd['root'].''.$country->idd['suffixes'][0];
                            
                            $phone_number = $phone_code.''.$user->phone;

                            if(preg_match('/Firefox/i',$u_agent))
                            {
                                $whatsapp = "https://web.whatsapp.com/send?phone=".$phone_number;
                            }else{
                                $whatsapp = "https://api.whatsapp.com/send?phone=".$phone_number;
                            }


                            
                          @endphp
                         
                        @if($user->getCommunityLink($user->id) == 2 && $user->contact_consent == 1)
                        <a  style="color:#30BF39" href="{{$whatsapp}}" target="_blank">
                            <i class="fa fa-whatsapp " aria-hidden="true" style="font-size:1.6rem;padding-top:0"></i> 
                           </a>
                             
                             <a href="mailto:{{$user->email}}" style="color:#404040">
                               <i class="fa fa-envelope" aria-hidden="true" style="font-size:1.6rem;padding-top:0"></i>
                             </a>
                         
                         @endif
                            
                          </div>
                      </div>
                  </article>
              </div>


              <div role="tabpanel " class="session_details-panel mt-4">
                   
                <div class="tab-content" >
                    @foreach ($profile_panels as $item)
                        <div role="tabpanel" class="tab-pane {{ $item['id'] == 1 ? 'active' : '' }}" id="home{{ $item['id'] }}" class="active">
                           

                                @if($item['id'] == 1)
                                @include('site.includes.components.connect.profile_activity')
                               
                                @elseif($item['id'] == 2)
                                <div class="row">
                                @foreach ($groups as $group)
                                <div class="col-md-4 col-12">
                                @include('site.includes.components.connect.profile_group',['item'=>$group])
                                </div>
                                @endforeach
                                </div>
                               @elseif($item['id'] == 3)

                               {!!$user->current_project!!}
                                @endif

                            
                        </div>
                    @endforeach
                </div>
            </div>
               
              
          </div>
         </div>
      
    
    </div>
    </div>
</div>


@else
<div class="col-md-12">     
  <div class="row ">
    <div class="col-12">
      @include('site.includes.components.connect.side_menu')
    </div>  
  </div>
  
</div>
<div class="col-md-12 col-12">
  <div class="row alignment-class-connect profile-top">
   
         <div class="col-4 " style="padding-left:0 !important">
            
                 @if($user->profile_pic)
                 @php $image = asset('uploads/'.$user->profile_pic);@endphp
                 @else 
                 @php $image = asset('images/profile/images.jpg');@endphp
                 @endif
             <div class="profile-image" style="background-image:url('{{$image}}')"></div>
               
             
         </div>
        <div class="col-8" style="padding-left: 0;padding-right: 0;">
            <div class="profile-details">
             <h3>{{$user->name}}</h3>
              <p class="more-information"><img src="{{asset('images/'.$flag)}}" width='30px'/> <span>{{$country->name['common']}}</span>
             
              <span class="more-information-spacing "> <a data-target="#mySuperpower" data-toggle="modal" href="#mySuperpower" class="profile-superpower">View Her super powers <i class="fa fa-eye"></i></a></span>
             </p>

             <p class="more-information"> <span><b>{{count($user->getMyFollowers($user->id))}}</b> Followers</span>
             
                 <span class="more-information-spacing"><b> {{count($user->getMyFollowing($user->id))}}</b> Following</span>

                 <span class="profile-buttons">@if($user->id == Auth::user()->id)
                     <a href="{{ route('profile.edit') }}" class="btn btn-overall btn_login"> Edit Profile </a>
                    @else 
                    @if($user->getCommunityLink($user->id) == 0)
                   
                    <a href="{{ route('profile.myFollow', [$user->id,0])}}" class="btn btn-overall btn_register"> Follow </a>
                    @else 
                     <button class="btn btn-overall btn_register">Following</button>
                    @endif
                    @endif  
                 </span>
                </p>
               
            </div>
        
        </div>
        <div class="col-12">
          @php 
            $u_agent = $_SERVER['HTTP_USER_AGENT'];
            $phone_code = $country->idd['root'].''.$country->idd['suffixes'][0];
            
            $phone_number = $phone_code.''.$user->phone;

            if(preg_match('/Firefox/i',$u_agent))
            {
                $whatsapp = "https://web.whatsapp.com/send?phone=".$phone_number;
            }else{
                $whatsapp = "https://api.whatsapp.com/send?phone=".$phone_number;
            }


            
          @endphp
         
        @if($user->getCommunityLink($user->id) == 2 && $user->contact_consent == 1)
        <a  style="color:#30BF39" href="{{$whatsapp}}" target="_blank">
            <i class="fa fa-whatsapp " aria-hidden="true" style="font-size:1.6rem;padding-top:0"></i> 
           </a>
             
             <a href="mailto:{{$user->email}}" style="color:#404040">
               <i class="fa fa-envelope" aria-hidden="true" style="font-size:1.6rem;padding-top:0"></i>
             </a>
         
         @endif
            
          </div>

 </div>
</div>

<div class="col-12">
  <div class="card connect-card">
    <article>
        <div class="row ">
          <div class="col-12">
             
          
            <ul class="nav nav-pills session-tabs group-tabs" role="tablist">
              @foreach ($profile_panels as $key => $item)

              @if ($key === array_key_last($profile_panels))
              
                  <li role="presentation"
                      class=" nav-item {{ $item['id'] == 1 ? 'active' : '' }} tabz-spacing">
                      <a href="#home{{ $item['id'] }}" aria-controls="home" role="tab"
                          data-toggle="tab">{!! $item['name'] !!}</a>
      
                  </li>
               
              @elseif($key === array_key_first($profile_panels))
                  <li role="presentation"
                      class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} border-right">
                      <a href="#home{{ $item['id'] }}" aria-controls="home" role="tab"
                          data-toggle="tab" style="text-align: center;
                      ">{!! $item['name'] !!}</a>
                  </li>

              @else
                  <li role="presentation"
                      class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} border-right tabz-spacing">
                      <a href="#home{{ $item['id'] }}" aria-controls="home" role="tab"
                          data-toggle="tab">{!! $item['name'] !!}</a>
                  </li>
              @endif
          @endforeach
          </ul>
            {{-- <a href="#" class="view-project"><span class="profile-icon icon-project"></span> <span class="view-project-span">View Current Project </span> </a> --}}
            </div>
     
        </div>
    </article>
</div>


<div role="tabpanel " class="session_details-panel mt-4">
     
  <div class="tab-content" >
      @foreach ($profile_panels as $item)
          <div role="tabpanel" class="tab-pane {{ $item['id'] == 1 ? 'active' : '' }}" id="home{{ $item['id'] }}" class="active">
             

                  @if($item['id'] == 1)
                  @include('site.includes.components.connect.profile_activity')
                 
                  @elseif($item['id'] == 2)
      
                  
                    @foreach ($groups as $group)
                    
                    @include('site.includes.components.connect.profile_group',['item'=>$group])
                   
                    @endforeach
                   
                

                 @elseif($item['id'] == 3)

                 {!!$user->current_project!!}

                  @endif

              
          </div>
      @endforeach
  </div>
</div>


</div>





@endif

</div>
<div class="clearfix"><br /></div>


<div class="modal fade" id="mySuperpower" tabindex="-1" role="dialog" aria-labelledby="mySuperpowerLabel" aria-hidden="true">
    <div class="modal-dialog superpower-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header " >
          <h5 class="modal-title " id="mySuperpowerLabel" >Super Powers</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          {!!$user->super_power!!}
        </div>
        
      </div>
    </div>
  </div>
@endsection

@section('js')
<script>
  $(document).ready(function() {

// Get current page URL
var url = window.location.href;

// remove # from URL
url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));

// remove parameters from URL
url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));

// select file name
url = url.split('/')[4];


// console.log(url);

// Loop all menu items
$('.list-group .list-nav').each(function(){

 // select href
 var href = $(this).find('a').attr('href');

 link = href.split('/')[4];


 console.log(link);
 
 // Check filename
 if(link === 'community'){

  // Add active class
  $(this).addClass('active');
 }
});
});
</script>

@endsection