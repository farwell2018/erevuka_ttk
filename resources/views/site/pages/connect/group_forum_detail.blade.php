@extends('layouts.app_no_js')

@section('title', 'My Courses')

@section('css')
<style>
/* .emoji-picker-icon {
    position: absolute;

    font-size: 20px;
    opacity: 0.7;
    z-index: 100;
    transition: none;
    color: black;
    -moz-user-select: none;
    -khtml-user-select: none;
    -webkit-user-select: none;
    -o-user-select: none;
    user-select: none;
    border: none;
    border-radius: 3px;
     padding: 0.4rem; 
}
.main-message .emoji-picker-icon{
    right: 68%;
    top:50%;
} */
.emoji-wysiwyg-editor::before {
  color: grey;
}
.emoji-wysiwyg-editor[data-placeholder]:not([data-placeholder=""]):empty::before {
  content: attr(data-placeholder);
}
.emoji-wysiwyg-editor::before {
  content: 'Whats' ' new?';
}
.main-message  .emoji-wysiwyg-editor{
    /* height:150px; */
    /* border: 1px solid #00000029; */
}
  .lead.emoji-picker-container {
    /* width: 300px; */
    margin-bottom: 0;
    display: block;
    input {
      width: 100%;
      height: 50px;
    }
}
</style>

@endsection
@section('content')
    <div class="courses-page">
        @include('site.includes.components.parallax',[
        'image'=>asset("images/banners/profile_header.png"),
        'text'=>'Connect'
        ])

        @component('site.includes.components.breadcrumbs')
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('connect.groups') }}">CoPs</a>
                </li>
                @if((new \Jenssegers\Agent\Agent())->isDesktop())

                <li class="breadcrumb-item active" aria-current="page">
                    <a href="{{ url()->current() }}" class="active">Issue Based Collaborative Learning Networks</a>
                </li>
                @else 
        
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="{{ url()->current() }}" class="active">IBCLN</a>
                </li>
                @endif
            </ol>
        @endcomponent

        <div class="profile-row">
            
            <div class="row alignment-class-connect">
                <div class="col-md-3">
                    @include('site.includes.components.connect.side_menu')
                </div>
                <div class="col-md-7">
              
                    @unless(empty($activities))
                        @foreach ($activities as $activity)
                            <div class="card shadow connectCard ">
                                <div class="card-body d-flex connectCardBody">
                                    <div class="d-flex">
                                        {{-- <div class="commentImg">
                                            @if ($activity->user->profile_pic)
                                                <img src="{{ asset('uploads/' . $activity->user->profile_pic) }}"
                                                    class="img-fluid connectImg" />
                                            @else
                                                <img src="{{ asset('uploads/images.jpg') }}" class="img-fluid connectImg" />
                                            @endif
                                        </div> --}}
                                        <div class="upperRow">
                                            <div class="commentsContainer">
                                                <div class="upperCommentRow">
                                                    <span class="name">
                                                        {{ $activity->title }}
                                                        <small class="time">
                                                            {{ \Carbon\Carbon::parse($activity->created_at)->diffForHumans() }}
                                                        </small>
                                                    </span>
                                                    {{-- <span class="time">
                                                        {{ \Carbon\Carbon::parse($activity->created_at)->diffForHumans() }}
                                                    </span> --}}
                                                </div>
                                                <div class="commentMain">
                                                    <p class="connect-font">{!! $activity->description !!}</p>
                                                </div>
                                                {{-- <div class="commentImgInner">
                                                    @unless(empty($activity->images))
                                                        @foreach (json_decode($activity->images) as $image)
                                                            <img src="{{ asset('images/activity/' . $image) }}"
                                                                class="img-fluid main-Commentimg" />
                                                        @endforeach
                                                    @endunless
                                                </div> --}}
                                                <div class="d-flex align-items-center" style="">
                                                    <small class="float-left">
                                                        <span title="Likes" class=""
                                                            onclick="sendLike({{ $activity->id }}, 'like')">
                                                            <i class="fa fa-thumbs-up" aria-hidden="true"></i> <span
                                                                class="activity-like-count " id="like-{{ $activity->id }}"
                                                                style="font-size:14px">{{ $activity->reactions->sum('likes') }}</span>
                                                        </span>
                                                        <span title="Dislikes" class=""
                                                            onclick="sendLike({{ $activity->id }}, 'dislike')">
                                                            <i class="fa fa-thumbs-down" aria-hidden="true"></i> <span
                                                                class="activity-like-count " id="dislike-{{ $activity->id }}"
                                                                style="font-size:14px">{{ $activity->reactions->sum('dislikes') }}</span>
                                                        </span>
                                                        {{-- <a href="javascript:;"onclick="showHide('reply-{{ $activity->id }}');showHideReply('response-{{ $activity->id }}');" class="replylink" >Reply</a> --}}
                                                        <span>
                                                            <img src="{{ asset('svgs/comment.svg') }}" id=""
                                                                class="img-fluid inputImg"
                                                                onclick="showHide('reply-{{ $activity->id }}');showHideReply('response-{{ $activity->id }}');"
                                                                width="20" /><span
                                                                class="like-count">{{ $activity->replies->count() }}</span>
                                                        </span>

                                                        </form>
                                                    </small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <form action="{{ route('group.forum.reply.create', [$activity->id]) }}" method="POST"
                                        enctype="multipart/form-data" style="width: 100%">
                                        @csrf
                                        
                                        <div class="container-fluid mt-3" id="reply-{{ $activity->id }}"
                                            style="display: none;">
                                            <div class="main  d-flex">
                                                <div class="card " style="border: 0px;width:100%">
                                                    <div class="row no-gutters">
                                                        {{-- <div class="col-2 col-md-1">
                                                            @if ($activity->user->profile_pic)
                                                                <img src="{{ asset('uploads/' . $activity->user->profile_pic) }}"
                                                                    class="card-img avatar-small rounded-circle disc-img" />
                                                            @else
                                                                <img src="{{ asset('uploads/images.jpg') }}"
                                                                    class="card-img avatar-small rounded-circle disc-img" />
                                                            @endif
                                                        </div> --}}
                                                        <div class="col-10">

                                                            <div class="card-body card-small pb-0">
                                                                <div class="card-text">
                                                                    <input type="text" name="id" value="5" hidden=""
                                                                        id="replycomment_id">
                                                                        <textarea id="description" class="form-control" name="description" required rows="6"></textarea>
                                                                <div class="submitbuttons">
                                                                    <button type="button"
                                                                        class="btn btn-outline-dark btn-cancel"
                                                                        onclick="showHide('reply-{{ $activity->id }}')">Cancel</button>
                                                                    <button type="submit"
                                                                        class="btn connect-button">Reply</button>
                                                                    {{-- <a href="javascript:;" onclick="submitReply()"
                                                                        class="button button-submit"> Submit Comment</a> --}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="card-body pt-0" id="response-{{ $activity->id }}" style="display: none">
                                        {{-- <summary>Show Replies</summary> --}}
                                        <span class="showReplies pt-0">
                                            @unless(empty($activity->replies))
                                            @foreach($activity->replies as $reply)
                                                <div class="card-body d-flex flex-row pt-0" id="response-{{ $activity->id  }}">
                                                <div class="commentImg">
                                                    @if ($reply->user->profile_pic)
                                                        <img src="{{ asset('uploads/' . $reply->user->profile_pic) }}"
                                                            class="img-fluid connectImg" />
                                                    @else
                                                        <img src="{{ asset('uploads/images.jpg') }}"
                                                            class="img-fluid connectImg" />
                                                    @endif
                                                </div>
                                            <div class="upperRow">
                                                <div class="commentsContainer">
                                                <div class="responseCommentRow">
                                                    <span class="name">
                                                    {{ $reply->user->username }}
                                                     <small class="time">
                                                          {{ \Carbon\Carbon::parse($reply->created_at)->diffForHumans() }}
                                                     </small>
                                                    </span>
                                                    {{-- <span class="time">
                                                    {{ \Carbon\Carbon::parse($reply->created_at)->diffForHumans() }}
                                                    </span> --}}
                                                </div>
                                                <div class="commentMain">
                                                    <p>{!! $reply->comment !!}</p>
                                                </div>
                                            
                                                {{-- <div class="commentImgInner">
                                                    @unless(empty($reply->images))
                                                    @foreach(json_decode($reply->images) as $image)
                                                <img src="{{ asset('images/activity/' . $image) }}" class="img-fluid main-Commentimg"/>
                                                    @endforeach
                                                @endunless
                                                </div> --}}
                                                 <div class="" style="">
                                                                <small class="float-left">
                                                                    <span title="Likes" class="mr-1-sm"onclick="sendReplyLike({{ $activity->id }}, {{ $reply->id }},'like')"  >
                                                                        <i class="fa fa-thumbs-up" aria-hidden="true"></i> <span class="activity-like-count " id="like-reply-{{$reply->id}}" style="font-size:14px">{{ $reply->replyReactions->sum('likes') }}</span>
                                                                    </span>
                                                                    <span title="Dislikes" class="mr-1-sm" onclick="sendReplyLike({{ $activity->id }}, {{ $reply->id }},'dislike')">
                                                                        <i class="fa fa-thumbs-down" aria-hidden="true"></i> <span class="activity-like-count " id="dislike-reply-{{$reply->id}}" style="font-size:14px">{{ $reply->replyReactions->sum('dislikes') }}</span>
                                                                    </span>
                                                                    {{-- <a href="javascript:;"onclick="showHide('reply-{{ $activity->id }}');showHideReply('response-{{ $activity->id }}');" class="replylink" >Reply</a> --}}
                                                                     <img src="{{ asset('svgs/comment.svg') }}" id="" class="img-fluid inputImg"onclick="showHide('response-reply-{{ $reply->id }}')"  width="20"/><span class="like-count">{{ $reply->replyReplies->count() }}</span>
                                                                    </form>
                                                                </small>
                                                            </div>
                                                    {{-- <div class="lowerCommentRow">
                                                    <input type="text" name="like" id="likeInput" class="d-none">
                                                    <label for="likeInput">  <img src="{{ asset('svgs/like.svg') }}" class="img-fluid inputImg" width="20"/><span class="like-count">{{ $activity->reactions->sum('likes') }}</span></label>
                                                    <input type="text" name="like" id="DislikeInput" class="d-none">
                                                    <label for="DislikeInput">  <img src="{{ asset('svgs/dislike.svg') }}" class="img-fluid inputImg" width="20"/><span class="dislike-count">{{ $activity->reactions->sum('dislikes') }}</span></label>
                                                    <img src="{{ asset('svgs/comment.svg') }}" id="" class="img-fluid inputImg"onclick="showHide('response-{{ $reply->id }}')"  width="20"/><span class="like-count">{{ $reply->replyReplies->count() }}</span>
                                                </div> --}}
                                                </div>
                                            </div>
                                            
                                            </div>
                                            <form action="{{ route('group.forum.reply.reply.create', [$activity->id, $reply->id]) }}" method="POST">
                                                @csrf
                                                <div class="reply-reply"id="response-reply-{{ $reply->id }}" style="display:none ">
                                                {{-- <div class="d-flex">
                                                <div class="connectImgLeft">
                                                        <img src="{{ asset('uploads/' . $activity->user->profile_pic) }}" class="img-fluid connectImg"/>
                                                    </div>
                                                        <div class="inputRight">
                                                        <input type="text" name="comment" class="connectInput" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" placeholder="Reply" data-emojiable="true" data-emoji-input="unicode">
                                                        <label for="imageInput"><img src="{{ asset('svgs/images.svg') }}" class="img-fluid inputImg" width="20"/></label>
                                                            <input name="images[]" type="file" id="imageInput" multiple>
                                                        <label for="gifInput"><img src="{{ asset('svgs/gif.svg') }}" class="img-fluid inputImg" width="20"/></label>
                                                            <input name="gifs[]" type="file" id="gifInput" multiple>
                                                        <img src="{{ asset('svgs/emoji.svg') }}" class="img-fluid inputImg inputIcon" width="20"/>
                                                    </div>
                                                </div> --}}
                                                     <div class="container-fluid mt-3" id="response-reply-{{ $reply->id }}"
                                                                                        style="display: block;">
                                                                                        <div class="main  d-flex">
                                                                                            <div class="card " style="border: 0px;width:100%">
                                                                                                <div class="row no-gutters">
                                                                                                    <div class="col-2 col-md-1">
                                                                                                        @if ($reply->user->profile_pic)
                                                                                                            <img src="{{ asset('uploads/' . $reply->user->profile_pic) }}"
                                                                                                                class="card-img avatar-small rounded-circle disc-img" />
                                                                                                        @else
                                                                                                            <img src="{{ asset('uploads/images.jpg') }}"
                                                                                                                class="card-img avatar-small rounded-circle disc-img" />
                                                                                                        @endif
                                                                                                    </div>
                                                                                                    <div class="col-10">
                                            
                                                                                                        <div class="card-body card-small pb-0">
                                                                                                            <div class="card-text">
                                                                                                                <input type="text" name="id" value="5" hidden=""
                                                                                                                    id="replycomment_id">
                                                                                                                    <textarea id="description-reply" class="form-control" name="description-reply" required rows="6"></textarea>
                                                                                                            </div>
                                                                                                            {{-- <label for="imageInput"><img
                                                                                                                    src="{{ asset('svgs/images.svg') }}"
                                                                                                                    class="img-fluid inputImg" width="20" /></label>
                                                                                                            <input name="images[]" type="file" id="imageInput" multiple>
                                                                                                            <label for="gifInput"><img src="{{ asset('svgs/gif.svg') }}"
                                                                                                                    class="img-fluid inputImg" width="20" /></label>
                                                                                                            <input name="gifs[]" type="file" id="gifInput" multiple> --}}
                                                                                                            {{-- <img src="{{ asset('svgs/emoji.svg') }}"
                                                                                                                class="img-fluid inputImg inputIcon" width="20" /> --}}
                                                                                                            <div class="submitbuttons">
                                                                                                                <button type="button"
                                                                                                                    class="btn btn-outline-dark btn-cancel"
                                                                                                                    onclick="showHide('response-reply-{{ $reply->id }}">Cancel</button>
                                                                                                                <button type="submit"class="btn connect-button">Reply</button>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                            
                                                    @unless(empty($reply->replyReplies))
                                            @foreach($reply->replyReplies as $reply)
                                                <div class="card-body d-flex pt-0">
                                                <div class="commentImg">
                                                @if ($reply->user->profile_pic)
                                                        <img src="{{ asset('uploads/' . $reply->user->profile_pic) }}"
                                                            class="img-fluid connectImg" />
                                                    @else
                                                        <img src="{{ asset('uploads/images.jpg') }}"
                                                            class="img-fluid connectImg" />
                                                    @endif
                                                </div>
                                            <div class="upperRow">
                                                <div class="commentsContainer">
                                                <div class="responseCommentRow">
                                                    <span class="name">
                                                    {{ $reply->user->username }}
                                                     <small class="time">
                                                          {{ \Carbon\Carbon::parse($reply->created_at)->diffForHumans() }}
                                                     </small>
                                                    </span>
                                                    {{-- <span class="time">
                                                    {{ \Carbon\Carbon::parse($reply->created_at)->diffForHumans() }}
                                                    </span> --}}
                                                </div>
                                                <div class="commentMain">
                                                    <p>{!! $reply->comment !!}</p>
                                                </div>
                                            
                                                {{-- <div class="commentImgInner">
                                                    @unless(empty($reply->images))
                                                    @foreach(json_decode($reply->images) as $image)
                                                <img src="{{ asset('images/activity/' . $image) }}" class="img-fluid main-Commentimg"/>
                                                    @endforeach
                                                @endunless
                                                </div> --}}
                                            </div>
                                            </div>
                                                </div>
                                            @endforeach
                                            @endunless
                                                </div>
                                            
                                            </form>
                                            
                                            
                                            @endforeach
                                            @endunless
                                        </span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endunless
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')

    <script>
        $(function() {
            // Initializes and creates emoji set from sprite sheet
            window.emojiPicker = new EmojiPicker({
                emojiable_selector: '[data-emojiable=true]',
                assetsPath: '{{ asset('emojis/lib/img/') }}',
                popupButtonClasses: 'fa fa-smile-o',
                position: "bottom"
            });

            window.emojiPicker.discover();
        });
    </script>
    <script>
        function showHide(id) {
            $("#" + id).toggle();
        };

        function showHideReply(id) {
            $("#" + id).toggle();
        };
    </script>
    <script>
        function sendLike(id, data) {
            $.ajax({
                url: "{{ url('/profile/connect/group/forum/reactions/') }}/" + id,
                type: "post",
                dataType: 'json',
                data: {
                    data,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(response) {
                    console.log(response)
                    document.getElementById("like-" + id).innerHTML = response.likes;
                    document.getElementById("dislike-" + id).innerHTML = response.dislikes;
                },
                error: function(response) {
                    // console.log(response)
                }
            });
        };
    </script>
    <script>
        function sendReplyLike(activityId, replyId, data) {
            console.log(activityId, replyId, data)
            $.ajax({
                url: "{{ url('/profile/connect/group/forum/reply/reacttions/create/') }}/" + activityId + "/" + replyId,
                type: "post",
                dataType: 'json',
                data: {
                    data,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(response) {
                    console.log(document.getElementById("dislike-" + replyId))
                    document.getElementById("like-reply-" + replyId).innerHTML = response.likes;
                    document.getElementById("dislike-reply-" + replyId).innerHTML = response.dislikes;
                },
                error: function(response) {
                    console.log(response)
                }
            });
        }
    </script>

<script>
    $(document).ready(function() {
  
  // Get current page URL
  var url = window.location.href;
  
  // remove # from URL
  url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
  
  // remove parameters from URL
  url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
  
  // select file name
  url = url.split('/')[4];
  
  
  // console.log(url);
  
  // Loop all menu items
  $('.list-group .list-nav').each(function(){
  
   // select href
   var href = $(this).find('a').attr('href');

   
  
   link = href.split('/')[4];
  
  
   console.log(link);
   
   // Check filename
   if(link === 'groups'){
  
    // Add active class
    $(this).addClass('active');
   }
  });
  });
  </script>

<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

    <script>
        CKEDITOR.replace( 'description');
        CKEDITOR.replace( 'description-reply');
    
    </script>
@endsection
