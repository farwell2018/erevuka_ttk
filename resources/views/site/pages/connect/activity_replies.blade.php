@unless(empty($activity->replies))
@foreach($activity->replies as $reply)
    <div class="card-body d-flex flex-row pt-0" id="response-{{ $activity->id  }}">
    <div class="commentImg">
        @if ($reply->user->profile_pic)
            <img src="{{ asset('uploads/' . $reply->user->profile_pic) }}"
                class="img-fluid connectImg" />
        @else
            <img src="{{ asset('uploads/images.jpg') }}"
                class="img-fluid connectImg" />
        @endif
    </div>
<div class="upperRow">
    <div class="commentsContainer">
    <div class="responseCommentRow">
        <span class="name">
        {{ $reply->user->username }}
         <small class="time">
              {{ \Carbon\Carbon::parse($reply->created_at)->diffForHumans() }}
         </small>
        </span>
        {{-- <span class="time">
        {{ \Carbon\Carbon::parse($reply->created_at)->diffForHumans() }}
        </span> --}}
    </div>
    <div class="commentMain">
        <p>{{ $reply->comment }}</p>
    </div>

    <div class="commentImgInner">
        @unless(empty($reply->images))
        @foreach(json_decode($reply->images) as $image)
    <img src="{{ asset('images/activity/' . $image) }}" class="img-fluid main-Commentimg"/>
        @endforeach
    @endunless
    </div>
     <div class="" style="">
                    <small class="float-left">
                        <span title="Likes" class="mr-1-sm"onclick="sendReplyLike({{ $activity->id }}, {{ $reply->id }},'like')"  >
                            <i class="fa fa-thumbs-up" aria-hidden="true"></i> <span class="activity-like-count " id="like-reply-{{$reply->id}}" style="font-size:14px">{{ $reply->replyReactions->sum('likes') }}</span>
                        </span>
                        <span title="Dislikes" class="mr-1-sm" onclick="sendReplyLike({{ $activity->id }}, {{ $reply->id }},'dislike')">
                            <i class="fa fa-thumbs-down" aria-hidden="true"></i> <span class="activity-like-count " id="dislike-reply-{{$reply->id}}" style="font-size:14px">{{ $reply->replyReactions->sum('dislikes') }}</span>
                        </span>
                        {{-- <a href="javascript:;"onclick="showHide('reply-{{ $activity->id }}');showHideReply('response-{{ $activity->id }}');" class="replylink" >Reply</a> --}}
                         <img src="{{ asset('svgs/comment.svg') }}" id="" class="img-fluid inputImg"onclick="showHide('response-reply-{{ $reply->id }}')"  width="20"/><span class="like-count">{{ $reply->replyReplies->count() }}</span>
                        </form>
                    </small>
                </div>
        {{-- <div class="lowerCommentRow">
        <input type="text" name="like" id="likeInput" class="d-none">
        <label for="likeInput">  <img src="{{ asset('svgs/like.svg') }}" class="img-fluid inputImg" width="20"/><span class="like-count">{{ $activity->reactions->sum('likes') }}</span></label>
        <input type="text" name="like" id="DislikeInput" class="d-none">
        <label for="DislikeInput">  <img src="{{ asset('svgs/dislike.svg') }}" class="img-fluid inputImg" width="20"/><span class="dislike-count">{{ $activity->reactions->sum('dislikes') }}</span></label>
        <img src="{{ asset('svgs/comment.svg') }}" id="" class="img-fluid inputImg"onclick="showHide('response-{{ $reply->id }}')"  width="20"/><span class="like-count">{{ $reply->replyReplies->count() }}</span>
    </div> --}}
    </div>
</div>

</div>
@include('site.pages.connect.reply_replies')
@endforeach
@endunless
