@extends('layouts.app_no_js')

@section('title', 'Courses')
@php

$details = [['id' => '1', 'name' => 'Description'], ['id' => '2', 'name' => 'Course Outline']];

$configLms = config()->get("settings.lms.live");
@endphp
@section('content')
<style>
    .row-description {
    padding: 2px 20px;
    /* color:#520e33;  */
    font-weight:400
}

.row-description .about h2,  .faq h2,  .prerequisites h2 {
    text-align: left;
    color: #520e33;
    font-size: 26px;
    font-weight:bold
}
</style>
    <div class="courses-page ttk-course-details">
        @php
        use  App\Models\Resource;
   $resource = Resource::where('course_id', $course->id)->first();
        @endphp

    @if($resource->resource_category ==1)
    @include('site.includes.components.parallax', [
            'image' => asset('images/banners/course_header.jpg'),
            'text' => $course->name,
        ])
                   @else
                   @include('site.includes.components.parallax-related_course', [
            'image' => asset('images/banners/course_header.jpg'),
            'text' => $course->name,
        ])
                     @endif
       
        
    
 



        @component('site.includes.components.breadcrumbs')
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item " aria-current="page">
                    <a href="{{ route('pages', ['key' => 'caring-for-your-child']) }}"> Caring for your child</a>
                </li>

                <li class="breadcrumb-item active" aria-current="page">
                    <a href="/course/detail/{{ $course->id }}" class="active">{{ $course->name }}</a>
                </li>
            </ol>

            <div class="d-block d-sm-none justify-content-center" style="margin-left:15px">
                @guest
                    <p>
                        <a href="{{ route('user.login') }}" class="btn btn-overall purple" style="font-size: 14px;">Enrol for this
                            course <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                        </a>
                    </p>
                @else
                    @if (!$licensed)
                        <p><a class="btn btn-overall purple" href="{{ route('course.enroll', $course->course_id) }}/"
                                style="font-size: 14px;">Enroll into
                                Course</a></p>
                    @else
                        @if ($enrolled)
                            <p><a class="btn btn-overall purple" href="{{ $configLms['LMS_BASE']  . '/courses/' . $course->course_id . '/courseware'}}" target="_blank">Take Course</a></p>
                        @else
                            <p><a class="btn btn-overall purple" href="{{ route('course.enroll', $course->course_id) }}/"
                                    style="font-size: 14px;">Enroll
                                    into
                                    Course</a></p>
                        @endif
                    @endif
                @endguest


            </div>
        @endcomponent



        @if (Session::has('course_success'))
            <script>
                jQuery(document).ready(function($) {
                    console.log("it has");
                    $("#CourseSuccess").addClass('show');
                });
            </script>
        @elseif(Session::has('course_errors'))
            <script>
                jQuery(document).ready(function($) {
                    $("#CourseErrors").addClass('show');
                });
            </script>
        @else
            <script>
                window.addEventListener('load', function() {
                    if (!window.location.hash) {
                        window.location = window.location + '#/';
                       
                    }
                });
            </script>
        @endif
        <div class="row courses-detail-row">
            <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
                <ul class="nav nav-pills course-tabs" role="tablist"  style="visibility:collapse">
                    @foreach ($details as $key => $item)
                        @if ($key === array_key_first($details))
                            <li class="nav-item active">
                                <a class="nav-link first-tab" data-toggle="pill" href="#home">Description</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#menu1">Course Outline</a>
                            </li>
                        @endif
                    @endforeach
                </ul>

            </div>
        </div>



        <div class="container-fluid">
            <div class="row column-reverse">
                <div class="col-lg-3 col-md-3 col-sm-12 col-12">

                @if($resource->resource_category ==1)
                   @include('site.includes.components.course-actions')
                   @else
                     @include('site.includes.components.course_related')
                     @endif

                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-12">
                    <div class="tab-content">
                        <div id="home" class="container-fluid tab-pane active">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="course-card detail-card-related">
                                        <article>
                                       
                                            <div class="course-card-content">
                                               

                                                <div class="row-description gray">

                                                <span id="lessText"> {!! truncateString($course->overview,100) !!}</span>
                                                <a href="#" style="color:#5eadcc">Show More</a>
                                                </div>

                                                <div class="row-description orange" style="display: none;">
                                                <span id="moreText">{!! $course->overview !!}</span>
                                                <a href="#" style="color:#5eadcc">Show Less</a>
                                                </div>
                                            </div>
                                           
                                        </article>
                                    </div>
                                </div>
                            </div>
                            @if($course->more_info)
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="course-card detail-card-related">
                                   
                                        <article>
                                            <div class="course-card-content">


                                                <div class="row-description blue">
                                                   
                                                <span id="lessText"> {!! truncateString($course->more_info,100) !!}</span>
                                                <a href="#" style="color:#5eadcc">Show More</a>
                                                </div>

                                                <div class="row-description black" style="display: none;">
                                                <span id="moreText">{!! $course->more_info !!}</span>
                                                <a href="#" style="color:#5eadcc">Show Less</a>
                                               
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                     

                    </div>
                    @if ($course->course_video)
                        <div class="video-wrapper">
                            <video controls playsinline id="bgvid">
                                <source src="{{ url('course_videos/' . $course->course_video) }}" type="video/mp4">
                            </video>
                        </div>
                        <!-- {!! $course->course_video_uri !!} -->
                    @endif


                    <div class="clearfix" style="margin-bottom: 5px;"></div>
                </div>
            </div>
        </div>
    </div>


    

@endsection
@section('js')
    <script>
        $(document).ready(function() {
            $('.show-table').removeClass('hidden-xs');

        });


        $(document).ready(function() {

            // Get current page URL
            var url = window.location.href;



            // remove # from URL
            url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));

            // remove parameters from URL
            url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));

            // select file name
            url = url.split('/')[4];




            // Loop all menu items
            $('.navbar-nav .nav-item').each(function() {

                // select href
                var href = $(this).find('a').attr('href');

                link = href.split('/')[3];

            
                // Check filename
                if (link === 'caring-for-your-child') {

                    // Add active class
                    $(this).addClass('active');
                }
            });
        });
    </script>
   <script type="text/javascript">
// Hide the extra content initially:
            $('.read-more-content').addClass('hide_content')
            $('.read-more-show, .read-more-hide').removeClass('hide_content')
 
            // Set up the toggle effect:
            $('.read-more-show').on('click', function(e) {
               
              $(this).next('.read-more-content').removeClass('hide_content');
              $(this).addClass('hide_content');
              e.preventDefault();
            });
            $('.read-more-hide').on('click', function(e) {
                
              var p = $(this).parent('.read-more-content');
              p.addClass('hide_content');
              p.prev('.read-more-show').removeClass('hide_content'); // Hide only the preceding "Read More"
              e.preventDefault();
            });
            $('orange').hide();
    $('.gray a').click(function() {
      $('.gray').hide();
      $('.orange').show();
    });
    $('.orange a').click(function() {
      $('.gray').show();
      $('.orange').hide();
    })


    $('black').hide();
    $('.blue a').click(function() {
      $('.blue').hide();
      $('.black').show();
    });
    $('.black a').click(function() {
      $('.blue').show();
      $('.black').hide();
    })
</script>



@endsection