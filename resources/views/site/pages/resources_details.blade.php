
@php
use Carbon\Carbon;
@endphp

@extends('layouts.app')

@section('title','Session')



@section('content')

    <section class="general-section business-general top-general">
    <div class="col-md-12">
        @if((new \Jenssegers\Agent\Agent())->isDesktop())
        <div class="row alignment-class-sessions">
         @else 
         <div class="row alignment-class-sessions" style="margin-right: -15px;margin-left: -15px;">
         @endif
            <div class="col-md-9 col-sm-7 ">
                            
                @include('site.includes.components.previous_detail',
                [
                'pageItem'=>$pageItem,
                ])
               
            </div> 
            @if((new \Jenssegers\Agent\Agent())->isDesktop())
            <div class="col-md-3 col-sm-5">

                @include('site.includes.components.related',
                [
                'related'=>$related,
                ])
            </div>

            @endif
        </div>
    </section>
   
@endsection

@section('js')
<script>

    function saveLikeDislike(comment,type){
      var vm=$(this);

      $.ajax({
         type:'POST',
         url: "{{ route('save_likedislike') }}",
          data: {post: comment,  type: type , token:"{{ csrf_token() }}" },
          dataType:'json',
          beforeSend:function(){
          vm.addClass('disabled');
            },
          success: (data) => {
            if(data.bool==true){
                    vm.removeClass('disabled').addClass('active');
                    vm.removeAttr('id');
                    if(type === 'like'){
                        var _prevCount= $("#like-"+comment).text();
                    _prevCount++;
                    $("#like-"+comment).text(_prevCount);
                    }
                }
          },
          
      });

    }

    </script>

<script>
    $(document).ready(function() {
  
  // Get current page URL
  var url = window.location.href;
  
  
  
  // remove # from URL
  url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
  
  // remove parameters from URL
  url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
  
  // select file name
  url = url.split('/')[4];
  
  
  // console.log(url);
  
  // Loop all menu items
  $('.navbar-nav .nav-item').each(function(){
  
   // select href
   var href = $(this).find('a').attr('href');
  
   link = href.split('/')[3];

  
   
   // Check filename
   if(link === 'caring-for-your-child'){
  
    // Add active class
    $(this).addClass('active');
   }
  });
  });
  </script>


@endsection
