@extends('layouts.app_no_js')

@section('title', 'Background')

@section('content')
    <style>
        .subscription-filter {
            background: #fff 0 0 no-repeat padding-box;
            box-shadow: 4px 4px 8px rgba(0, 0, 0, 0.302);
            border-radius: 10px;
        }

        .subscription-filter-mobile {
            background: #fff 0 0 no-repeat padding-box;
            box-shadow: 4px 4px 8px rgba(0, 0, 0, 0.302);
            width: 90%
        }

        .card.subscription-filter:hover {
            background-color: #f8f9fa;
            box-shadow: 0 0.25rem 0.75rem rgba(0, 0, 0, 0.1);
            position: relative;
        }

        .textbox {
            position: absolute;
            top: -15px;
            left: 15%;
            padding: 2px;
            background-color: #520e33;
            color: white;
            font-weight: bold;
        }

        .currency {
            font-size: 12px;
            font-weight: 700;
            color: #000;
        }

        .figure {
            font-size: 18px;
            font-weight: 700;
            color: #000;

        }

        .description {
            font-size: 16px;
            line-height: 24px;
            font-weight: 400;
        }

        .form-control-mobile {
            display: block;
            width: 150%;
            height: calc(1.5em + 0.75rem + 2px);
            padding: 0.375rem 0.75rem;
            font-size: 15px;
            line-height: 1.5;
            color: #888;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: 0;
            margin: 15px;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }

        .modal {
            text-align: center;
        }

        @media screen and (min-width: 768px) {
            .modal:before {
                display: inline-block;
                vertical-align: middle;
                content: " ";
                height: 100%;
            }
        }

        .modal-dialog {
            display: inline-block;
            text-align: left;
            vertical-align: middle;
        }
    </style>
    <div class="background-page">
        @php
            $image = asset('images/banners/subscription.png');
            $text = 'Subscriptions';
        @endphp
        @include('site.includes.components.parallax-subscription', [
            'image' => $image,
            'text' => $text,
        ])

        @component('site.includes.components.breadcrumbs')
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="{{ url()->current() }}" class="active">{!! 'Subscriptions' !!}</a>
                </li>
            </ol>
        @endcomponent

        <div class="clearfix">
            <br /> <br /> <br /> <br />
        </div>

        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
            <div class="container-fluid pt-5 pb-5">





            @php $individual = \App\Models\Rate::where('range','=', '')->pluck('price');
   
            @endphp
                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-8">
                        <div class="card subscription-filter">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-4">
                                        <h4 class="card-title mt-4 text-center">Individual Subscription</h4>
                                        <p class="card-text text-center"><span class="currency">Ksh</span> <span
                                                class="figure"> {{$individual[0]}}.00</span> <span class="currency"> / Monthly</span> </p>

                                        <div class="wrap text-center description">
                                            <p>Enter your phone number and the promo code, then click <strong>"Make
                                                    Payment."</strong> </p>

                                            <!-- <p>You will receive a prompt on your phone to enter your MPESA PIN to complete the purchase.</p> -->
                                        </div>


                                    </div>
                                    <div class="col-8">
                                        <br>
                                        <form class="form-horizontal" method="POST" action="{{ route('ipay.pay') }}">
                                            @csrf
                                            <div class="form-group">

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="phone" name="phone"
                                                        placeholder=" Enter phone number e.g 254721123456">
                                                </div>
                                            </div>
                                            <div class="form-group">

                                                <div class="col-sm-8">
                                                    <input type="text" hidden class="form-control" id="amount"
                                                        name="amount" value="{{$individual[0]}}" placeholder="Enter amount">
                                                </div>
                                            </div>
                                            <!-- <div class="form-group">
                    
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="account" name="account" placeholder="Enter store or till number">
                            </div>
                        </div> -->
                                            <div class="form-group">

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="coupon" name="coupon"
                                                        placeholder="Enter promo code">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-overall purple"
                                                        style="padding: 0.375rem 0.75rem;border-radius:0; font-weight:700; "
                                                        onclick="paymentSubmit()">Make payment</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>



                            </div>
                        </div>
                        <div class="col-2"></div>
                    </div>
                </div>



            </div>
        @else
            <div class="container-fluid">
               
                <div class="row">
                    <div class="card subscription-filter-mobile ml-4">
                        <div class="card-body">
                            <h4 class="card-title mt-4 text-center">Individual Subscription</h4>
                            <p class="card-text text-center"><span class="currency">Ksh</span> <span class="figure">
                            {{$individual[0]}}.00</span> <span class="currency"> / Monthly</span> </p>

                            <div class="wrap text-center description">
                                <p>Enter your phone number and the promo code, then click <strong>"Make Payment."</strong>
                                </p>

                                <!-- <p>You will receive a prompt on your phone to enter your MPESA PIN to complete the purchase.</p> -->
                            </div>

                            <div class="row">

                                <form class="form-horizontal" method="POST" action="{{ route('ipay.pay') }}">
                                    @csrf
                                    <div class="form-group">

                                        <input type="text" class="form-control-mobile" id="phone" name="phone"
                                            placeholder=" Enter phone number e.g 254721729078">

                                    </div>
                                    <div class="form-group">


                                        <input type="text" hidden  class="form-control-mobile" id="amount" value="{{$individual[0]}}" name="amount"
                                            placeholder="Enter amount">

                                    </div>
                                    <!-- <div class="form-group">

                                        <input type="text" class="form-control-mobile" id="account" name="account"
                                            placeholder="Enter store or till number">

                                    </div> -->
                                    <div class="form-group">

                                        <input type="text" class="form-control-mobile" id="coupon" name="coupon"
                                            placeholder="Enter promo code ">

                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-overall purple"
                                                style="padding: 0.375rem 0.75rem;border-radius:0; font-weight:700; "  onclick="paymentSubmit()">Make
                                                payment</button>
                                        </div>
                                    </div>
                                </form>

                            </div>

                        </div>
                    </div>

                </div>
            </div>
        @endif


        <div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog"
            aria-labelledby="confirmationModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h6 class="modal-title " id="confirmationModalLabel" style="color: #520e33"><strong>Confirm
                                payment on your phone</strong></h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>You will receive a prompt on your phone</p>
                        <p> if you had a promo code, please check for the new amount after clicking "Subscribe". Enter your
                            MPESA PIN on your phone to confirm transaction</p>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancelButton">Cancel</button> -->
                        <button type="button"
                            style="background-color:#F2BD38; border:#F2BD38; color:#520e33; font-weight:700"
                            class="btn btn-primary pull-end" id="confirmButton">Close</button>
                    </div>
                </div>
            </div>
        </div>




    @endsection

    @section('js')
        <script src="https://cdnjs.cloudfstartlare.com/ajax/libs/axios/1.2.1/axios.min.js"></script>


        <script type="text/javascript">
            function paymentSubmit() {

                $('#error_message').empty();

                if ($('#phone').val() == '') {

                    $('#error_message').append('Please fill in the phone number before submitting');

                } else {

                    $('#pay-form').submit();
                }
            }
        </script>


    @endsection
