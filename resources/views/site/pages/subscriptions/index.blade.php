@extends('layouts.app_no_js')

@section('title','Background')

@section('content')
<style>
    .subscription-filter{
        
   
    background: #fff 0 0 no-repeat padding-box;
    box-shadow: 4px 4px 8px rgba(0, 0, 0, 0.302);
    min-height:350px;
    

    }

    .subscription-filter-mobile{
        
   
        background: #fff 0 0 no-repeat padding-box;
        box-shadow: 4px 4px 8px rgba(0, 0, 0, 0.302);
        width:90%
    
        }

    .card.subscription-filter:hover {
    background-color: #f8f9fa;
    box-shadow: 0 0.25rem 0.75rem rgba(0, 0, 0, 0.1);
    position: relative;
  }

 

  .currency{
font-size: 12px;
font-weight: 700;
color: #000;
  }
  .figure{
    font-size: 18px;
    font-weight: 700;
color: #000;

  }
  .description{
    font-size: 16px;
    line-height: 24px;
    font-weight: 400;
  }

  .plan-option{
  
     color:#530C33;
      font-weight:400
  }
  select > .plan-option{
  background: #fff;
  color:#530C33;
   font-weight:700
}



 
</style>
<div class="background-page">
    @php
    $image=  asset('images/banners/subscription.png');
    $text = 'Subscriptions';
    @endphp
    @include('site.includes.components.parallax-subscription',[
    'image'=> $image,
    'text'=>$text

 

    ])
   
    @component('site.includes.components.breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">
            <a href="{{ url()->current() }}" class="active">{!! 'Subscriptions' !!}</a>
        </li>
    </ol>
    @endcomponent

    <div class="clearfix">
        <br /> <br />  <br /> <br />
    </div>
    @php $individual = \App\Models\Rate::where('range','=', '')->pluck('price');
   
    @endphp
    @if((new \Jenssegers\Agent\Agent())->isDesktop())
        <div class="container-fluid pt-5 pb-5">
       
              

            <div class="row ">
                <div class="col-md-2"></div>
                <div class="col-md-3">
                <div class="card subscription-filter">
            <div class="card-body">
                
               
                <h4 class="card-title mt-4 text-center">Individual Subscription</h4>
                <p class="card-text text-center"><span class="currency">Ksh</span> <span class="figure"> {{$individual[0]}}.00</span>   <span class="currency"> / Monthly</span> </p>
                
                <div class="wrap text-center description">
                <p>Unlocks access to our full content bank.</p>

                <p>Upon completing the transaction, you will be able to access the courses and take the training.</p>
                </div>

                
                <div class="col-sm-offset-2 col-sm-10 text-center ml-4">
                        <a href="{{route('single')}}" class="btn btn-overall purple" style="padding: 0.385rem 0.75rem;border-radius:0; font-weight:700; ">Subscribe Now</a>
                        </div>
         
            </div>
                </div>
                </div>

                <div class="col-md-1"></div>
                <div class="col-md-3">
                <div class="card subscription-filter">
            <div class="card-body">
                
               
                <h4 class="card-title mt-4 text-center">Group Subscription</h4>
                <p class="card-text text-center"> <span class="figure"> 1- 200 Users</span>    </p>
                
                <div class="wrap text-center description">
                <p>Purchase multiple licenses to be accessed by 1-200 users as indicated in the price plan below.</p>

              
                </div>
                <form method="POST" action="{{route('uwishlist.purchase-group')}}" class="grey">
                      @csrf
                <div class="col-sm-offset-2 col-sm-10 text-center ml-2 mb-4">
                @php $rates = \App\Models\Rate::where('range','!=', '')->get();@endphp
                <select id="plan-select" name="bundles" class="form-select   form-select-sm text-center"  style="padding: 0.285rem 0.55rem; background:#D0D0D0; color:#530C33; ">
                    <option class="plan-option" value=" "><strong>Select plan</strong></option>
                    @foreach (  $rates as $rate)
                    <option class="plan-option" value="{{$rate->id}}">{{$rate->range}} {{$rate->currency}} {{$rate->price}}</option>
                    @endforeach
                  
                   </select>
                   @if ($errors->has('plan-select'))
                              <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('plan-select') }}</strong>
                            </span>
                          @endif
                </div>
               
                <div class="col-sm-offset-2 col-sm-10 text-center ml-4">
                <button  type="submit"  class="btn btn-overall purple" style=" background:#FE6D01;padding: 0.385rem 0.75rem;border-radius:0; font-weight:700; ">Subscribe Now</button>
                </div>
                </form>
           
            </div>
                </div>
                </div>
            </div>
        </div>
        @else
        <div class="container-fluid">
        <p class="text-center py-2 text-bold">You can pay for the courses via MPESA.</p>
        <div class="row">
        <div class="card subscription-filter-mobile ml-4">
            <div class="card-body">
            <h4 class="card-title mt-4 text-center">Individual Subscription</h4>
            <p class="card-text text-center"><span class="currency">Ksh</span> <span class="figure"> {{$individual[0]}}.00</span>   <span class="currency"> / Monthly</span> </p>
                
                <div class="wrap text-center description">
                <p>This enables access to all courses for one person.</p>

                <p>Upon completing the transaction, you will be able to access the courses and take the training.</p>
                </div>

                
                <div class="col-sm-offset-2 col-sm-10 text-center ml-4">
                        <a href="{{route('single')}}" class="btn btn-overall purple" style="padding: 0.385rem 0.75rem;border-radius:0; font-weight:700; ">Subscribe Now</a>
                  </div>

            </div>
        </div>

        </div>
        <br>
        <br>
        <div class="row">
        <div class="card subscription-filter-mobile ml-4">
            <div class="card-body">
                
               
                <h4 class="card-title mt-4 text-center">Group Subscription</h4>
                <p class="card-text text-center"><span class="currency">Ksh</span> <span class="figure"> 500.00</span>   <span class="currency"> / Monthly</span> </p>
                
                <div class="wrap text-center description">
                <p>This allows you to purchase multiple licenses for all courses to be accessed by 1-200 users as indicated in the price plan below.</p>

                <p>Once you complete the transaction you will be sent codes via email to share with your team members so that they can enroll and take the modules.</p>
                </div>

                <div class="col-sm-offset-2 col-sm-10 text-center ml-2 mb-4">
                @php $rates = \App\Models\Rate::where('range','!=', '')->get();@endphp
                <select id="plan-select" name="bundles" class="form-select   form-select-sm text-center"  style="padding: 0.285rem 0.55rem; background:#D0D0D0; color:#530C33; ">
                    <option value=" " class="plan-option"><strong>Select plan</strong></option>
                    @foreach (  $rates as $rate)
                    <option class="plan-option" value="{{$rate->id}}">{{$rate->range}} {{$rate->currency}} {{$rate->price}}</option>
                    @endforeach
                  
                   </select>

                   @if ($errors->has('plan-select'))
                              <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('plan-select') }}</strong>
                            </span>
                          @endif
                </div>
               
                <div class="col-sm-offset-2 col-sm-10 text-center ml-4">
                <button type="submit"  id="subscribe-button" class="btn btn-overall purple" style=" background:#FE6D01;padding: 0.385rem 0.75rem;border-radius:0; font-weight:700; ">Subscribe Now</button>
                </div>
           
            </div>
                </div>

        </div>
        <br>
        <br>
        </div>
        @endif



    


@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/1.2.1/axios.min.js"></script>
<script>
  $(document).ready(function() {
    $("#subscribe-button").click(function(event) {
      var planValue = $("#plan-select option:selected").data("value");
      var url = "{{ route('group') }}?plan=" + planValue;
      window.location.href = url;
      event.preventDefault();
    });
  });
</script>
@endsection
