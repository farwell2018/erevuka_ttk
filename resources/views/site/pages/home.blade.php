@extends('layouts.app_no_js')

@section('content')
    @php
    $variant = $pageItem->variant;
    $url = $pageItem->url;
    $linkText = $pageItem->link_text;
    @endphp

    @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <section>
            <div class="row ">
                <div class="col-md-12">
                        @php $images = $pageItem->files @endphp

                        @if ($images)
                            <div id="carouselExampleIndicators" class="carousel slide first-slider" data-ride="carousel" data-interval="false">

                                <div class="carousel-inner">

                                    @foreach ($images as $key => $item)

                                        <div class="carousel-item item {{ $key == 0 ? ' active' : '' }}">

                                          <img src="{{ asset('storage/uploads/' . $item->uuid) }}"
                                                    alt="{{ $pageItem->title }}" />

                                        </div>

                                    @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                    data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                    data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>


                            <div id="carouselTextIndicators" class="carousel slide second-slider" data-ride="carousel" data-interval="false">

                                <div class="carousel-inner">


                                        <div class="carousel-item item active">

                                            <h1 class="special-h1"><strong>{!! $pageItem->header_title !!}</strong></h1>
                                            {!! $pageItem->description !!}

                                        @if ($variant && $variant === 'brown_bg')

                                                <a id="tour_videos" data-toggle="modal" data-target="#tourModal" >
                                                    <span>Watch</span>
                                                </a>

                                        @elseif ($variant && $variant === 'white_bg_brown_border')

                                        <a id="tour_videos" data-toggle="modal" data-target="#tourModal" >
                                            <span>Watch</span>
                                        </a>
                                        @endif

                                        </div>

                                        {{-- <div class="carousel-item item second-item ">

                                            <h1 class="play-header">{!! $linkText !!}</h1>
                                            <a href="{{  $url }}" target="_blank"><img src="{{ asset('images/icons/android_app.svg') }}" alt="Android"></a>
                                        </div> --}}



                                </div>
                                <a class="carousel-2-control-prev" href="#carouselTextIndicators" role="button"
                                    data-slide="prev">
                                    <span class="carousel-2-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-2-control-next" href="#carouselTextIndicators" role="button"
                                    data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        @else
                        @endif


                </div>

            </div>
            <button onclick="myFunction()" title="" class="btn-arrow arrow bounce d-none d-md-block" aria-hidden="true" style="" id="arrow-button">
                <i class="fa fa-arrow-down fa-2x"></i>
            </button>
        </section>

        @elseif ((new \Jenssegers\Agent\Agent())->isTablet())
        <section>
            <div class="row ">
                <div class="col-sm-12">
                    <div class="home-banner">
                        @php $images = $pageItem->files @endphp

                        @if ($images)
                            <div id="carouselExampleIndicators" class="carousel slide first-slider" data-ride="carousel" data-interval="false">

                                <div class="carousel-inner">

                                    @foreach ($images as $key => $item)

                                        <div class="carousel-item item {{ $key == 0 ? ' active' : '' }}">

                                          <img src="{{ asset('storage/uploads/' . $item->uuid) }}"
                                                    alt="{{ $pageItem->title }}" />

                                        </div>

                                    @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                    data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                    data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>


                            <div id="carouselTextIndicators" class="carousel slide second-slider" data-ride="carousel" data-interval="false">

                                <div class="carousel-inner">


                                        <div class="carousel-item item active">

                                            <h1 class="special-h1"><strong>{!! $pageItem->header_title !!}</strong></h1>
                                            {!! $pageItem->description !!}

                                        @if ($variant && $variant === 'brown_bg')

                                                <a id="tour_videos" data-toggle="modal" data-target="#tourModal" >
                                                    <span>Watch</span>
                                                </a>

                                        @elseif ($variant && $variant === 'white_bg_brown_border')

                                        <a id="tour_videos" data-toggle="modal" data-target="#tourModal" >
                                            <span>Watch</span>
                                        </a>
                                        @endif

                                        </div>

                                        {{-- <div class="carousel-item item second-item ">

                                            <h1 class="play-header">{!! $linkText !!}</h1>
                                            <a href="{{$url  }}" target="_blank"><img src="{{ asset('images/icons/android_app.svg') }}" alt="Android"></a>
                                        </div> --}}

                                </div>
                                <a class="carousel-2-control-prev" href="#carouselTextIndicators" role="button"
                                    data-slide="prev">
                                    <span class="carousel-2-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-2-control-next" href="#carouselTextIndicators" role="button"
                                    data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        @else
                        @endif


                    </div>
                </div>
            </div>
            <button onclick="myFunction()" title="" class="btn-arrow arrow bounce d-none d-md-block" aria-hidden="true" style="" id="arrow-button">
                <i class="fa fa-arrow-down fa-2x"></i>
            </button>
        </section>
    @else
        <section>
            <div class="row ">
                <div class="col-xs-12">
                    <div class="home-banner mobile-banner">
                        @php $images = $pageItem->files @endphp

                        @if ($images)
                            <div id="carouselExampleIndicators" class="carousel slide first-slider" data-ride="carousel" data-interval="false">

                                <div class="carousel-inner">

                                    @foreach ($images as $key => $item)

                                        <div class="carousel-item item {{ $key == 0 ? ' active' : '' }}">

                                          <img src="{{ asset('storage/uploads/' . $item->uuid) }}"
                                                    alt="{{ $pageItem->title }}" />

                                        </div>

                                    @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                    data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                    data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>


                            <div id="carouselTextIndicators" class="carousel slide second-slider" data-ride="carousel" data-interval="false">

                                <div class="carousel-inner">


                                        <div class="carousel-item item active">

                                            <h1 class="special-h1"><strong>{!! $pageItem->header_title !!}</strong></h1>
                                            {!! $pageItem->description !!}

                                        @if ($variant && $variant === 'brown_bg')

                                                <a id="tour_videos" data-toggle="modal" data-target="#tourModal" >
                                                    <span>Watch</span>
                                                </a>

                                        @elseif ($variant && $variant === 'white_bg_brown_border')

                                        <a id="tour_videos" data-toggle="modal" data-target="#tourModal" >
                                            <span>Watch</span>
                                        </a>
                                        @endif

                                        </div>

                                        {{-- <div class="carousel-item item second-item ">

                                            <h1 class="play-header">{!! $linkText !!}</h1>
                                            <a href="{{ $url }}" target="_blank"><img src="{{ asset('images/icons/android_app.svg') }}" alt="Android"></a>
                                        </div> --}}

                                </div>
                                <a class="carousel-2-control-prev" href="#carouselTextIndicators" role="button"
                                    data-slide="prev">
                                    <span class="carousel-2-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-2-control-next" href="#carouselTextIndicators" role="button"
                                    data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        @else
                        @endif


                    </div>
                </div>



            </div>

        </section>

    @endif


    @if (Auth::user())
        <section class="general-section business-general">
            <div class="container-fluid">
                @include(
                    'site.includes.components.resources.resources_filter',
                    ['resourceTypes' => $resourceTypes]
                )
            </div>
        </section>
    @endif


    {!! $pageItem->renderBlocks(true, [], ['type' => $resourcetype]) !!}

@endsection


@section('js')

<script>
$('.carousel.first-slider').carousel({
  interval: 10000
})

</script> -


<script>
    $('.carousel.second-slider').carousel({
      interval: 6000
    })

    </script>

@endsection
