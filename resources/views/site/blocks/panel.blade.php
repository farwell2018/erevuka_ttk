@php
use App\Models\Session;
use Carbon\Carbon;
@endphp

  <div class="container-fluid">
    <div role="tabpanel " class="session-panel">
        <div class="col-md-12">
          <div class="row">
            @if((new \Jenssegers\Agent\Agent())->isDesktop())
            <div class="col-md-10">
        <ul class="nav nav-tabs" role="tablist">
            @foreach ($panels as $key => $item)
                @if($key  === $panels->keys()->last())
                <li role="presentation" class="{{ $item->id == 1 ? 'active' : '' }} tabs-spacing">
                    <a href="#home{{ $item->id }}" aria-controls="home" role="tab" data-toggle="tab">{{ $item->translatedinput('panel_title') }}</a>
                  </li>
                @elseif($key === $panels->keys()->first())
                <li role="presentation" class="active border-right">
                    <a href="#home{{ $item->id }}" aria-controls="home" role="tab" data-toggle="tab">{{ $item->translatedinput('panel_title') }}</a>
                  </li>

                @else
              <li role="presentation" class="{{ $item->id == 1 ? 'active' : '' }} border-right tabs-spacing">
                <a href="#home{{ $item->id }}" aria-controls="home" role="tab" data-toggle="tab">{{ $item->translatedinput('panel_title') }}</a>
              </li>
              @endif
            @endforeach
        </ul></div>
        <div class="col-md-2">
          @if (Route::current()->getName() === 'home')
          
      <a href="{{route('sessions')}}" class="text-grey full-list">See Full Sessions List > </a> 
      @endif
    </div>

    @elseif((new \Jenssegers\Agent\Agent())->isMobile())
    <div class="col-md-2">
      @if (Route::current()->getName() === 'home')
      
  <a href="{{route('sessions')}}" class="text-grey full-list mb-3">See Full Sessions List > </a> 
  @endif
</div>
    <div class="col-md-10">
      <ul class="nav nav-tabs" role="tablist">
          @foreach ($panels as $key => $item)
              @if($key  === $panels->keys()->last())
              <li role="presentation" class="{{ $item->id == 1 ? 'active' : '' }} tabs-spacing">
                  <a href="#home{{ $item->id }}" aria-controls="home" role="tab" data-toggle="tab">{{ $item->translatedinput('panel_title') }}</a>
                </li>
              @elseif($key === $panels->keys()->first())
              <li role="presentation" class="active border-right">
                  <a href="#home{{ $item->id }}" aria-controls="home" role="tab" data-toggle="tab">{{ $item->translatedinput('panel_title') }}</a>
                </li>

              @else
            <li role="presentation" class="active border-right tabs-spacing">
              <a href="#home{{ $item->id }}" aria-controls="home" role="tab" data-toggle="tab">{{ $item->translatedinput('panel_title') }}</a>
            </li>
            @endif
          @endforeach
      </ul></div>
    @endif
    </div>
  </div>
        <div class="tab-content">
         
      
         @foreach ($panels as $key => $item)

        
         @if (Route::current()->getName() === 'home')

         @if((new \Jenssegers\Agent\Agent())->isDesktop())
         @if($key === $panels->keys()->first())
         @php $first = $item->id; @endphp
         @php $sessions = Session::where('start_time', '>=', Carbon::now())->limit(4)->get(); @endphp
         @else
         @php $sessions = Session::where('start_time', '<', Carbon::today()->toDateString())->limit(4)->get(); @endphp
         @endif
         @endif
         @if((new \Jenssegers\Agent\Agent())->isMobile())
                @if($key === $panels->keys()->first())
                @php $first = $item->id; @endphp
                @php $sessions = Session::where('start_time', '>=', Carbon::now())->limit(1)->get(); @endphp
                @else
                @php $sessions = Session::where('start_time', '<', Carbon::today()->toDateString())->limit(1)->get(); @endphp
                @endif
          @endif
          @elseif(Route::current()->getName() === 'sessions')

          @if($key === $panels->keys()->first())
          @php $first = $item->id; @endphp
          @php $sessions = Session::where('start_time', '>=', Carbon::now())->get(); @endphp
          @else
          @php $sessions = Session::where('start_time', '<', Carbon::today()->toDateString())->get(); @endphp

          @endif


          @endif
          @if (Route::current()->getName() === 'home')
              <div role="tabpanel" class="tab-pane {{ $key === $panels->keys()->first() ? 'active' : '' }}" id="home{{ $item->id }}" class="active">
          @elseif(Route::current()->getName() === 'sessions')

          <div role="tabpanel" class="tab-pane {{ $key === $panels->keys()->first() ? 'active' : '' }}" id="home{{ $item->id }}" class="active">
          @endif
                <div class="row">
                @foreach($sessions as $session)
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        @include('site.includes.components.session-card',['session'=>$session])
                    </div>
                   
                @endforeach
            </div>
              </div>
         @endforeach
        </div>
      </div>


  </div>
    