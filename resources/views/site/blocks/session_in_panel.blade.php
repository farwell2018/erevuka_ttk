


@if (Route::current()->getName() === 'home')
@if((new \Jenssegers\Agent\Agent())->isDesktop())
<section class="home-sessions" id="home-scroll">

<section  class="general-section" style="margin:0">
    <div class="container-fluid">
        <div class="row justify-content-center alignment-class">
            <div class="col-md-12 ">
                <div class="banner-section-text text-center">
                 
                  <h1>{!! $block->translatedinput('title') !!}</h1>
                   
                </div>
            </div>
            <div class="col-md-12 ">
               
          
              @include('site.blocks.panel',
               [
                  'panels'=>$block->children,
              ])
       
            </div>
        </div>

    </div>


</section>

</section>

@else 
<section class="home-sessions" id="home-scroll">

    <section  class="general-section" style="margin:0">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="banner-section-text text-center">
                     
                      <h1>{!! $block->translatedinput('title') !!}</h1>
                       
                    </div>
                </div>
                <div class="col-md-12 ">
                   
    
                  @include('site.blocks.panel',
                   [
                      'panels'=>$block->children,
                  ])
           
                </div>
            </div>
    
        </div>
    
    
    </section>
    
    </section>
    @endif

@else
@if((new \Jenssegers\Agent\Agent())->isDesktop())
<section  class="general-section">
    <div class="container-fluid">
        <div class="row justify-content-center alignment-class-sessions">
            <div class="col-md-12 ">
                <div class="banner-section-text text-center">
      
                    <br> <br>
                   
                </div>
            </div>
            <div class="col-md-12 ">
               
          
              @include('site.blocks.panel',
               [
                  'panels'=>$block->children,
              ])
       
            </div>
        </div>

    </div>

</section>

@else 

<section  class="general-section">
   
            <div class="col-md-12 ">
                <div class="banner-section-text text-center">
      
                    <br> <br>
                   
                </div>
            </div>
           
               
          
              @include('site.blocks.panel',
               [
                  'panels'=>$block->children,
              ])
       
  
       

</section>


@endif


@endif

