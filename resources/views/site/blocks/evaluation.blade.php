
@if((new \Jenssegers\Agent\Agent())->isDesktop())
<section class="general-section" style="margin:0;">
    <div class="col-md-12 ">

			<div class="col-lg-12">
				<div class="row  mb-5" >
                    <div class="col-12 ">
						<div class="ml-5" style="margin-top:2rem">

                            <form action="{{route('course.evaluate.store')}}" method="post" id="editEvaluationForm" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <input type="hidden" name="course_id" value="{{$id}}" id="course_ev">

                                <section>


                            @foreach ($block->children as $item)


                            @include('site.blocks.evaluation_repeater', ['item' => $item])
                        @endforeach
                    </section>


                    <br>
                    <div class="form-group form-group row justify-content-center">
                 <div class="col-md-6 offset-md-4">
                     <button type="submit" class="btn btn-overall btn_solid_primary">Submit <i aria-hidden="true" class="fas fa-long-arrow-alt-right"></i></button>
                 </div>
             </div>

                   </form>
						{{-- @php dd($block->translatedinput('url_video'));@endphp --}}
						</div>
					</div>

			</div>
		</div>
	</div>

</section>

@else

<section class="general-section" style="margin:0;">

		<div class="row  mb-5" >
                    <div class="col-12 ">
						<div class="ml-1">

                            <form action="{{route('course.evaluate.store')}}" method="post" id="editEvaluationForm" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <input type="hidden" name="course_id" value="{{$id}}" id="course_ev">

                                <section>


                            @foreach ($block->children as $item)


                            @include('site.blocks.evaluation_repeater', ['item' => $item])
                        @endforeach
                    </section>


                    <br>
                    <div class="form-group form-group row justify-content-center">
                 <div class="col-md-6 offset-md-4">
                     <button type="submit" class="btn btn-overall btn_solid_primary">Submit <i aria-hidden="true" class="fas fa-long-arrow-alt-right"></i></button>
                 </div>
             </div>

                   </form>
						{{-- @php dd($block->translatedinput('url_video'));@endphp --}}
						</div>
					</div>

			</div>

</section>

@endif
