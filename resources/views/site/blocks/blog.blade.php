@php
use App\Repositories\Blogpository;
use App\Models\Blog;
if (Route::current()->getName() === 'home'){
 if((new \Jenssegers\Agent\Agent())->isDesktop()){
    $blogs  = Blog::published()->orderBy('position', 'desc')->limit(4)->get();
 }elseif((new \Jenssegers\Agent\Agent())->isMobile()){

    $blogs  = Blog::published()->orderBy('position', 'desc')->limit(1)->get();
 }


}else{
    $blogs  = Blog::published()->orderBy('position', 'desc')->paginate(12);  
}
@endphp

@if(Route::current()->getName() === 'home')
<section class="home-sessions" id="home-scroll">
    <section class="general-section">
        <div class="container-fluid">
            <div class="row justify-content-center alignment-class">
                <div class="col-md-12 ">
                    <div class="banner-section-text text-center">
                     
                      <h1>{!! $block->translatedinput('title') !!}</h1>
                       
                    </div>
                </div>
                @if((new \Jenssegers\Agent\Agent())->isDesktop())
                <div class="col-10"></div>
                <div class="col-2 ">
                    <div class="row alignment-class">
                    @if (Route::current()->getName() === 'home')
                    <a href="{{route('blogs')}}" class="text-grey full-list mb-3">See Full Blog List > </a> 
                    @endif
                    </div>
                  </div>
               @else 
               <div class="col-7"></div>
                <div class="col-5">
                    <div class="row ">
                    @if (Route::current()->getName() === 'home')
                    <a href="{{route('blogs')}}" class="text-grey full-list mb-3">See Full Blog List > </a> 
                    @endif
                    </div>
                  </div>
    
    
               @endif
               
            </div>
    


        <div class="col-12 ">
            <div class="">

             
            <div class="row">
                <div class="col-12">
                    <div class="whatYouGetListing text-center">
                        {!! $block->translatedinput('description') !!}
                        
                    </div>
                </div>
               
            </div>
        </div>
        </div>
        @if((new \Jenssegers\Agent\Agent())->isDesktop())
        <div class="row alignment-class">
        @else 
        <div class="row ">
        @endif
            @foreach($blogs as $blog)
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                @include('site.includes.components.blogs',['item'=>$blog])
            </div>
           @endforeach
        </div>
        
        @if (Route::current()->getName() === 'home')
    
        @else
        <div class="row justify-content-center  pagination-spacing">{!! $blogs->render() !!}</div>
        @endif
    </div>
    </section>
    



</section>
@else 
<section class="general-section">
    <div class="container-fluid">
    <div class="col-12 ">
        <div class="">
		<div class="hrHeading">
			<h2 class="line-header text-center">
				{{ $block->translatedinput('title') }}
			</h2>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="whatYouGetListing text-center">
					{!! $block->translatedinput('description') !!}
					
				</div>
			</div>
            @if((new \Jenssegers\Agent\Agent())->isDesktop())
            <div class="col-10"></div>
            <div class="col-2 ">
                <div class="row alignment-class">
                @if (Route::current()->getName() === 'home')
                <a href="{{route('blogs')}}" class="text-grey full-list mb-3">See Full List > </a> 
                @endif
                </div>
              </div>
           @else 
           <div class="col-8"></div>
            <div class="col-4">
                <div class="row ">
                @if (Route::current()->getName() === 'home')
                <a href="{{route('blogs')}}" class="text-grey full-list mb-3">See Full List > </a> 
                @endif
                </div>
              </div>


           @endif
		</div>
    </div>
	</div>
    @if((new \Jenssegers\Agent\Agent())->isDesktop())
    <div class="row alignment-class">
    @else 
    <div class="row ">
    @endif
        @foreach($blogs as $blog)
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            @include('site.includes.components.blogs',['item'=>$blog])
        </div>
       @endforeach
    </div>
    
    @if (Route::current()->getName() === 'home')

    @else
    <div class="row justify-content-center  pagination-spacing">{!! $blogs->render() !!}</div>
    @endif
</div>
</section>



@endif
