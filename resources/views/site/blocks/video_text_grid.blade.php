@if((new \Jenssegers\Agent\Agent())->isDesktop())
<section class="general-section" style="background: #F5FFFD; margin:0;">
    <div class="col-md-12 ">
		<div class="hrHeading " style="padding-top: 2%;">
			<h2 class="line-header text-center">
				{{ $block->translatedinput('title') }}
			</h2>
		</div>
			<div class="col-lg-12">
				<div class="row mt-3 mb-5" >
                    <div class="col-5 ">
						<div class="ml-5" style="margin-top:8rem">
							<h2 >{{$block->translatedinput('textTitle')}}</h2>
							{!!$block->translatedinput('textdescription')!!}

                            @foreach ($block->children as $block)
									@include('site.blocks.cta_link', $block)
								@endforeach

							
						</div>
					</div>
					<div class="col-6 ">
						<video controls playsinline muted id="bghid">
                            <source src="{{ asset($block->translatedinput('url_video')) }}" type="video/mp4">
                        </video>
					</div>
                       
			</div>
		</div>
	</div>

</section>

@else 

<section class="general-section" style="background: #F5FFFD; margin:0;">
    <div class="col-md-12 ">
		<div class="hrHeading " style="padding-top: 2%;">
			<h2 class="line-header text-center">
				{{ $block->translatedinput('title') }}
			</h2>
		</div>
			<div class="col-lg-12">
				<div class="row mt-3 mb-5" >
                    <div class="col-5 col-12">
						<div >
							<h2 >{{$block->translatedinput('textTitle')}}</h2>
							{!!$block->translatedinput('textdescription')!!}
							@foreach ($block->children as $block)
							@include('site.blocks.cta_link', $block)
						@endforeach
						</div>
					</div>
					<div class="col-6 col-12 mt-3">
						<video controls playsinline muted id="bghid">
                            <source src="{{ asset($block->translatedinput('url_video')) }}" type="video/mp4">
                        </video>
					</div>
                       
			</div>
		</div>
	</div>

</section>


@endif