@php
$variant = $block->input('block_variant');
$icon = $block->input('block_icon');
$background = $block->input('block_background_color');
$text_color = $block->input('block_text_color');
@endphp

@if($background)
<section class="main-page" style="background-color:<?php echo $background;?>" >
@else
<section class="main-page">
@endif
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if($text_color)
            <div class="header-section" style="color: <?php echo $text_color;?> !important">
            <h1 style="color: <?php echo $text_color;?> !important"> <i class="fa <?php echo $icon;?>"></i>{!!$block->translatedinput('title')!!}</h1>
            {!!$block->translatedinput('description')!!}
          </div>
          @else 
          <div class="header-section">
            <h1> <i class="fa <?php echo $icon;?>"></i>{!!$block->translatedinput('title')!!}</h1>
            {!!$block->translatedinput('description')!!}
          </div>
        @endif
           
        </div>
    </div>
</section>