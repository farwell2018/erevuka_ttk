@php

use App\Models\Resource;
use App\Models\ContentBucketRole;

if(Auth::check()){
    if(Auth::user()->role === "ADMIN" || Auth::check() && Auth::user()->role == "SUPERADMIN"){
    $check= true;

}else{

    $check = ContentBucketRole::where('role_id',Auth::user()->role)->where('content_bucket_id', $block->content['content_bucket'])->first();
}

}else{
    $check= true;
}

@endphp

@if($check)


@php
if (Route::current()->getName() === 'home') {
    if ((new \Jenssegers\Agent\Agent())->isMobile()) {
        if (!empty($type)) {
         
            $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->where('resource_type_id', $type)
                ->published()
                ->latest()
                ->limit(4)
                ->get();
            $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->where('resource_type_id', $type)
                ->published()
                ->latest()
                ->get();
        } else {
            $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->published()
                ->latest()
                ->limit(4)
                ->get();
            $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->published()
                ->latest()
                ->get();
        }
    } elseif ((new \Jenssegers\Agent\Agent())->isTablet()) {
        if (!empty($type)) {
            $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->where('resource_type_id', $type)
                ->published()
                ->latest()
                ->limit(4)
                ->get();
            $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->where('resource_type_id', $type)
                ->published()
                ->latest()
                ->get();
        } else {
            $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->published()
                ->latest()
                ->limit(4)
                ->get();
            $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->published()
                ->latest()
                ->get();
        }
    } else {
        if (Auth::check()) {
            if (!empty($type)) {
                $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                    ->where('resource_type_id', $type)
                    ->published()
                    ->latest()
                    ->limit(8)
                    ->get();
                $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                    ->where('resource_type_id', $type)
                    ->published()
                    ->latest()
                    ->get();
            } else {
                $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                    ->published()
                    ->latest()
                    ->limit(8)
                    ->get();
                $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                    ->published()
                    ->latest()
                    ->get();
            }
        } else {
            if (!empty($type)) {
                $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                    ->where('resource_type_id', $type)
                    ->published()
                    ->latest()
                    ->limit(4)
                    ->get();
                $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                    ->where('resource_type_id', $type)
                    ->published()
                    ->latest()
                    ->get();
            } else {
                $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                    ->published()
                    ->latest()
                    ->limit(4)
                    ->get();
                $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                    ->published()
                    ->latest()
                    ->get();
            }
        }
    }
} else {
    if ((new \Jenssegers\Agent\Agent())->isMobile()) {
        if (!empty($type)) {
            $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->where('resource_type_id', $type)
                ->published()
                ->latest()
                ->get();
            $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->where('resource_type_id', $type)
                ->published()
                ->latest()
                ->get();
        } elseif (!empty($search)) {
            $resources = Resource::whereHas('translations', function ($query) use ($search) {
                $query->where('title', 'like', '%' . $search . '%');
            })
                ->published()
                ->latest()
                ->get();
            $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->where('resource_type_id', $type)
                ->published()
                ->latest()
                ->get();
        } elseif (!empty($slug_type)) {
            $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->where('resource_category', $slug_type)
                ->published()
                ->latest()
                ->get();
            $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->where('resource_category', $slug_type)
                ->published()
                ->latest()
                ->get();
        }  else {
            $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->published()
                ->latest()
                ->get();
            $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->published()
                ->latest()
                ->get();
        }
    } elseif ((new \Jenssegers\Agent\Agent())->isTablet()) {
        if (!empty($type)) {
            $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->where('resource_type_id', $type)
                ->published()
                ->latest()
                ->get();
            $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->where('resource_type_id', $type)
                ->published()
                ->latest()
                ->get();
        } elseif (!empty($search)) {
            $resources = Resource::whereHas('translations', function ($query) use ($search) {
                $query->where('title', 'like', '%' . $search . '%');
            })
                ->published()
                ->latest()
                ->get();

            $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->where('resource_type_id', $type)
                ->published()
                ->latest()
                ->get();
        } elseif (!empty($slug_type)) {
            $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->where('resource_category', $slug_type)
                ->published()
                ->latest()
                ->get();
            $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->where('resource_category', $slug_type)
                ->published()
                ->latest()
                ->get();
        }  else {
            $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->published()
                ->latest()
                ->get();
            $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->published()
                ->latest()
                ->get();
        }
    } else {
        if (Auth::check()) {
            if (!empty($type)) {
                $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                    ->where('resource_type_id', $type)
                    ->published()
                    ->latest()
                    ->get();
                $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                    ->where('resource_type_id', $type)
                    ->published()
                    ->latest()
                    ->get();
            } elseif (!empty($search)) {
                $resources = Resource::whereHas('translations', function ($query) use ($search) {
                    $query->where('title', 'like', '%' . $search . '%');
                })
                    ->published()
                    ->latest()
                    ->get();

                $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                    ->where('resource_type_id', $type)
                    ->published()
                    ->latest()
                    ->get();
            } elseif (!empty($slug_type)) {
            $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->where('resource_category', $slug_type)
                ->published()
                ->latest()
                ->get();
            $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->where('resource_category', $slug_type)
                ->published()
                ->latest()
                ->get();
        }  else {
                $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                    ->published()
                    ->latest()
                    ->get();
                $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                    ->published()
                    ->latest()
                    ->get();
            }
        } else {
            if (!empty($type)) {
                $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                    ->where('resource_type_id', $type)
                    ->published()
                    ->latest()
                    ->get();
                $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                    ->where('resource_type_id', $type)
                    ->published()
                    ->latest()
                    ->get();
            } elseif (!empty($search)) {
                $resources = Resource::whereHas('translations', function ($query) use ($search) {
                    $query->where('title', 'like', '%' . $search . '%');
                })
                    ->published()
                    ->latest()
                    ->get();

                $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                    ->where('resource_type_id', $type)
                    ->published()
                    ->latest()
                    ->get();
            } elseif (!empty($slug_type)) {
            $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->where('resource_category', $slug_type)
                ->published()
                ->latest()
                ->get();
            $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                ->where('resource_category', $slug_type)
                ->published()
                ->latest()
                ->get();
        }  else {
                $resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                    ->published()
                    ->latest()
                    ->get();
                $all_resources = Resource::whereIn('content_bucket_id', $block->content['content_bucket'])
                    ->published()
                    ->latest()
                    ->get();
            }
        }
    }
}


@endphp


@if (Auth::user())
    @php $c = array_intersect(Auth::user()->contentBuckets(),$block->content['content_bucket']);@endphp

    @if (count($c) > 0)

        @if (Route::current()->getName() === 'home')
            <section class="general-section">
                <div class="container-fluid mobile-container-fluid">
                    <div class="col-12 ">
                        <div class="">
                            <div class="hrHeading">
                                <h2 class="line-header text-center business_header special-h1">
                                    {{ $block->translatedinput('title') }}
                                </h2>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="whatYouGetListing text-center">
                                        {{-- {!! $block->translatedinput('description') !!} --}}

                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="col-12 ">
                    <div class="">
                        
                        <div class="row alignment-class">
                            <div class="col-12">
                                <div class="whatYouGetListing mt-4">
                                <p>Tiny Totos focuses on the business of childhood.If childcare businesses set up to look after children are not well run, if they do not give good service, if their service is not priced<a href="/managing-my-business">  Read More</a></p>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                    @if ((new \Jenssegers\Agent\Agent())->isDesktop())
                        <div class="row alignment-class mt-3">
                        @else
                            <div class="row mt-3">
                    @endif

                    @if (count($resources) > 0)
                        @foreach ($resources as $resource)
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                @include('site.includes.components.resources', [
                                    'item' => $resource,
                                ])
                            </div>
                        @endforeach
                    @else
                        <p class="text-center"> There are no Managing my Business resources in the selected category
                        </p>
                    @endif
                </div>
                @if (count($all_resources) > 4)
                    <p class="text-center "><a class="view_all_resources"
                            href="{{ route('pages', ['key' => 'caring-for-your-child']) }}"> View all</a>
                    </p>
                @endif
                </div>
            </section>
        @else
            <section class="general-section">
                <div class="container-fluid mobile-container-fluid">

                    @if ((new \Jenssegers\Agent\Agent())->isDesktop())
                        <div class="row alignment-class mt-3">
                        @else
                            <div class="row mt-3">
                    @endif
                    @if (count($resources) > 0)
                        @foreach ($resources as $resource)
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                @include('site.includes.components.resources', [
                                    'item' => $resource,
                                ])
                            </div>
                        @endforeach
                    @else
                        <p class="text-center"> There are no Managing my Business resources in the selected category
                        </p>
                    @endif
                </div>


                </div>
            </section>
        @endif
    @endif
@else
    @if (Route::current()->getName() === 'home')
        <section class="general-section">
            <div class="container-fluid mobile-container-fluid">
                <div class="col-12 ">
                    <div class="">
                        <div class="hrHeading">
                            <h2 class="line-header text-center business_header special-h1">
                                {{ $block->translatedinput('title') }}
                            </h2>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="whatYouGetListing text-center">
                                {{-- {!! $block->translatedinput('description') !!} --}}

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                
                
                <div class="col-12 ">
                    <div class="">
                        
                        <div class="row alignment-class">
                            <div class="col-12">
                                <div class="whatYouGetListing mt-4">
                                <p>Tiny Totos focuses on the business of childhood.If childcare businesses set up to look after children are not well run, if they do not give good service, if their service is not priced<a href="/managing-my-business">  Read More</a></p>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                @if ((new \Jenssegers\Agent\Agent())->isDesktop())
                    <div class="row alignment-class mt-3">
                    @else
                        <div class="row mt-3">
                @endif

                @if (count($resources) > 0)
                    @foreach ($resources as $resource)
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            @include('site.includes.components.resources', [
                                'item' => $resource,
                            ])
                        </div>
                    @endforeach
                @else
                @endif
            </div>


            </div>
        </section>
    @else
        <section class="general-section" style="margin-top:0">
            <div class="container-fluid mobile-container-fluid">

                @if ((new \Jenssegers\Agent\Agent())->isDesktop())
                    <div class="row alignment-class ">
                    @else
                        <div class="row ">
                @endif

                @foreach ($resources as $resource)
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        @include('site.includes.components.resources', [
                            'item' => $resource,
                        ])
                    </div>
                @endforeach

            </div>


            </div>
        </section>




    @endif

@endif
@endif