@php
use App\Models\Resource;
$resources = Resource::latest()->published()
    ->take(1)
    ->get();
@endphp
@if (Request::path() === 'about')
    @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <section class="general-section">
            <div class="row alignment-class-about">
                <div class="col-lg-9">
                    <div class="whatYouGetListing">
                        <div class="col-md-12 ">
                            <div class="hrHeading">
                                <h2 class="line-header ">
                                    {{ $block->translatedinput('title') }}
                                </h2>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    

                                    <div class="whatYouGetListing mt-4 gray">

                                <span id="lessText"> {!! truncateString($block->translatedinput('description'),200) !!}</span>
                                <a href="#">Show More</a>
                                </div>

                                <div class="whatYouGetListing mt-4 orange" style="display: none;">
                                <span id="moreText">{!! $block->translatedinput('description') !!}</span>
                                <a href="#">Show Less</a>
                                </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>


                <div class="col-md-3 d-sm-none d-md-block">

                    @include('site.includes.components.upcoming', [
                        'resources' => $resources,
                    ])

                </div>
            </div>

        </section>
    @else
        <section class="general-section">
            <div class="row alignment-class-about">
                <div class="col-12">
                    <div class="whatYouGetListing">
                        <div class="col-md-12 ">
                            <div class="hrHeading mt-4">
                                <h2 class="line-header ">
                                    {{ $block->translatedinput('title') }}
                                </h2>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    

                                    <div class="whatYouGetListing mt-4 gray">

                                <span id="lessText"> {!! truncateString($block->translatedinput('description'),200) !!}</span>
                                <a href="#">Show More</a>
                                </div>

                                <div class="whatYouGetListing mt-4 orange" style="display: none;">
                                <span id="moreText">{!! $block->translatedinput('description') !!}</span>
                                <a href="#">Show Less</a>
                                </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>


                {{-- <div class="col-md-3 d-sm-none d-md-block">

                    @include('site.includes.components.upcoming', [
                        'resources' => $resources,
                    ])

                </div> --}}
            </div>

        </section>
    @endif
@else
    <section class="general-section">
        <div class="row">
            <div class="col-lg-12">
                <div class="whatYouGetListing">
                    <div class="col-md-12 ">
                        <div class="hrHeading">
                            <h2 class="line-header ">
                                {{ $block->translatedinput('title') }}
                            </h2>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="whatYouGetListing ">

                                    {!! $block->translatedinput('description') !!}

                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

        </div>

    </section>
@endif
@section('js')
<script type="text/javascript">
    $('orange').hide();
    $('.gray a').click(function() {
      $('.gray').hide();
      $('.orange').show();
    });
    $('.orange a').click(function() {
      $('.gray').show();
      $('.orange').hide();
    })
</script>
@endsection