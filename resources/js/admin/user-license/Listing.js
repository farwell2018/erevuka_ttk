import AppListing from '../app-components/Listing/AppListing';

Vue.component('user-license-listing', {
    mixins: [AppListing]
});