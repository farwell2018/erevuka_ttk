import AppForm from '../app-components/Form/AppForm';

Vue.component('course-form', {
    mixins: [AppForm],
    props:[
        'sessions'
    ],
    data: function() {
        return {
            form: {
                name:  '' ,
                short_description:  '' ,
                overview:  '' ,
                more_info:  '' ,
                effort:  '' ,
                start:  '' ,
                end:  '' ,
                enrol_start:  '' ,
                enrol_end:  '' ,
                price:  '' ,
                course_image_uri:  '' ,
                course_video_uri:  '' ,
                job_group_id:  '' ,
                status:  false ,
                course_category_id:  '' ,
                slug:  '' ,
                order_id:  '' ,
                course_video:  '' ,
                sessions_id:  '' ,
                mobile_available: '',
                
            }
        }
    }

});