require('./bootstrap');

const AOS = require('aos/dist/aos')
var Promise = require('es6-promise').Promise;
const ClassicEditor = require('@ckeditor/ckeditor5-build-classic');

window.Vue = require('vue');

let axios = require('axios');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


Vue.component('course-filter', require('./components/CourseFilter.vue').default);
Vue.component('my-course-filter', require('./components/MyCourseFilter.vue').default);
Vue.component('my-course', require('./components/MyCourse.vue').default);
Vue.component('my-course-mobile', require('./components/MyCourseMobile.vue').default);
Vue.component('my-achievements', require('./components/MyAchievements.vue').default);
Vue.component('my-achievements-mobile', require('./components/MyAchievementsMobile.vue').default);
Vue.component('ppic-form', require('./components/PpicForm.vue').default);
Vue.component('ppic', require('./components/ppic.vue').default);
Vue.component('my-community', require('./components/MyCommunity.vue').default);
Vue.component('my-community-filter', require('./components/MyCommunityFilter.vue').default);
Vue.component('training-feed', require('./components/TrainingFeed.vue').default);
Vue.component('training-feed-mobile', require('./components/TrainingFeedMobile.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

window.onload = function () {
  const app = new Vue({
    el: '#app'
  })

  var ready = (callback) => {
    if (document.readyState != "loading") callback();
    else document.addEventListener("DOMContentLoaded", callback);
  }
  
  ready(() => { 
      ClassicEditor
          .create(document.querySelector('.wysiwyg'))
          .then( editor => {
            editor.ui.view.editable.element.style.height = '200px';
        } )
          .catch(error => {
              console.log("error", error)
          });

      ClassicEditor
          .create(document.querySelector('.current-wysiwyg'))
          .then( editor => {
            editor.ui.view.editable.element.style.height = '200px';
        } )
          .catch(error => {
              console.log("error", error)
          });
  });

}
