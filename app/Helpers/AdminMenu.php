<?php
if (!function_exists('adminMenu')) {
	function adminMenu()
	{
		return [


			'content' => [
				'title' => 'Content',
				'route' => 'admin.content.pages.index',
				'primary_navigation' => [
					'pages' => [
						'title' => 'Pages',
						'module' => true,

					],
					'menuses' => [
						'title' => 'Menus',
						'module' => true,
						'secondary_navigation' => [
							'menuses' => [
								'title' => 'Menus',
								'module' => true
							],
							'menuTypes' => [
								'title' => 'Menu Types',
								'module' => true,

							],
						]

					],
					'footerTexts' => [
						'title' => 'FooterTexts',
						'module' => true
					],

					//  'socialmedia' => [
					// 	'title' => 'Social Media Links',
					// 	 'module' => true,

					// ],
					// 'projecttopics' => [
					// 	'title' => 'Project Topics',
					// 	 'module' => true,

					// ],
				],
			],

			'resources' => [
				'title' => 'Resources',
				'route' => 'admin.resources.resources.index',
				'primary_navigation' => [

					'resources' => [
						'title' => 'Resources',
						'module' => true,

					],
					'resourceTypes' => [
						'title' => 'Resource Categories',
						'module' => true,

					],

					'resourceCategories' => [
						'title' => 'Resource Type',
						'module' => true
					]

				],
			],



			'evaluation' => [
				'title' => 'Evaluation',
				'route' => 'admin.evaluation.userEvaluations.index',
				'primary_navigation' => [
					'userEvaluations' => [
						'title' => 'User Evaluations',
						'module' => true,
					],

					'evaluations' => [
						'title' => 'Evaluation Questions',
						'module' => true
					],
					'evaluationOptions' => [
						'title' => 'Evaluation Options',
						'module' => true
					],

				]
			],


			'featureWeeks' => [
				'title' => 'Feature of the Weeks',
				'module' => true
			],



			// 'sessions'=> [
			// 	'title' =>'Sessions',
			// 	 'route' => 'admin.sessions.sessions.index',
			// 	 'primary_navigation' => [
			// 		 'sessions' => [
			// 			 'title' => 'Sessions',
			// 			  'module' => true,

			// 		 ],

			// 		 'sessionResources' => [
			// 			 'title' => 'Sessions Resources',
			// 			  'module' => true,

			// 		 ],
			// 	 ]
			//  ],


			// 'groups'=> [
			// 	'title' =>'Groups',
			// 	 'route' => 'admin.groups.groups.index',
			// 	 'primary_navigation' => [
			// 		 'groups' => [
			// 			 'title' => 'Groups',
			// 			  'module' => true,

			// 		 ],

			// 		 'groupTypes' => [
			// 			 'title' => 'Group Types',
			// 			  'module' => true,

			// 		 ],
			// 		 'groupFormats' => [
			// 			'title' => 'Group Formats',
			// 			 'module' => true,

			// 		],
			// 		'groupThemes' => [
			// 			'title' => 'Group Themes',
			// 			'module' => true
			// 		]
			// 	 ]
			//  ],

			// 'Sessions' => [
			// 	'title' => 'Sessions',
			// 	'route' => 'admin.sessionmeetings.index',

			// ],

			'courses' => [
				'title' => 'Courses',
				'route' => 'admin.courses.index',

			],


			'contentBuckets' => [
				'title' => 'Content Buckets',
				'module' => true,

			],

			'roles' => [
				'title' => 'Roles',
				'module' => true,

			],

			// 'coupons' => [
			// 	'title' => 'Promo Codez',
			// 	'module' => true,
			// ],
			'promoCodes' => [
				'title' => 'Field Staff Codes',
				'route' => 'admin.promo.codes',
				'primary_navigation' => [
					'groupPurchase' => [
						'title' => 'Field Staff Codes',
						'route' => 'admin.promo.codes',
					],


				]
			],
			'dayCares' => [
				'title' => ' Daycares',
				'route' => 'admin.daycare.index',
				'primary_navigation' => [
					'daycares' => [
						'title' => ' Daycares',
						'route' => 'admin.daycare.index',
					],


				]
			],

			'dayCarePromoCodes' => [
				'title' => ' Daycare Codes',
				'route' => 'admin.daycare.codes',
				'primary_navigation' => [
					'groupPurchase' => [
						'title' => ' Daycare Promo Codes',
						'route' => 'admin.daycare.codes',
					],


				]
			],
			'rates' => [
				'title' => 'Purchase Rates',
				'module' => true,
			],

			'groupPurchase' => [
				'title' => 'Group Purchases',
				'route' => 'admin.group.purchase',
				'primary_navigation' => [
					'groupPurchase' => [
						'title' => 'Group Purchase',
						'route' => 'admin.group.purchase',
					],


				]
			],


			'usersUpload' => [
				'title' => 'Upload Users',
				'route' => 'admin.usersUpload',
			],

		];
	}
}

if (!function_exists('facilitatorAdminMenu')) {
	function facilitatorAdminMenu()
	{
		return [

			'sessions' => [
				'title' => 'Sessions',
				'route' => 'admin.sessions.sessions.index',
				'primary_navigation' => [
					'sessions' => [
						'title' => 'Sessions',
						'module' => true,

					],

					'sessionResources' => [
						'title' => 'Sessions Resources',
						'module' => true,

					],
				]
			],

			'groups' => [
				'title' => 'Groups',
				'route' => 'admin.groups.groups.index',
				'primary_navigation' => [
					'groups' => [
						'title' => 'Groups',
						'module' => true,

					],

					'groupTypes' => [
						'title' => 'Group Types',
						'module' => true,

					],
					'groupFormats' => [
						'title' => 'Group Formats',
						'module' => true,

					],
				]
			],

			'resources' => [
				'title' => 'Resources',
				'route' => 'admin.resources.resources.index',


			],

			'courses' => [
				'title' => 'Courses',
				'route' => 'admin.courses.index',

			],



		];
	}
}
