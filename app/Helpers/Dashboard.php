<?php

if (!function_exists('providerDashboard')) {
    function providerDashboard()
    {
		$user = (auth()->guard('twill_users')->user()) ? auth()->guard('twill_users')->user()->id : null;
        return  [
			'modules' => [
				'profile' => [
					'name' => 'users',
					'label' => 'My Profile',
					'label_singular' => 'My Profile',
					'route' => 'admin.users.edit',
					'routeParams' => ['user'=>$user],
					'search' => true,
					'count' => true,
					'create' => false,
					'countScope'=>['id'=>$user],
				],
				'financingProviders' => [
					'name' => 'financingProviders',
					'label' => 'Financing Providers',
					'label_singular' => 'Financing Provider',
					'routePrefix' => 'financing',
					'activity' => true,
					'search' => true,
					'draft' => true,
					'count' => true,
					'create' => false,
				],
				'bdsProviders' => [
					'name' => 'bdsProviders',
					'label' => 'Business Development Support Providers',
					'label_singular' => 'Business Development Support Provider',
					'routePrefix' => 'businesssupport',
					'activity' => true,
					'search' => true,
					'draft' => true,
					'count' => true,
					'create' => false,
				],
			],
			'analytics' => ['enabled' => false],
    		'search_endpoint' => 'admin.search',
		];
    }
}

if (!function_exists('fpProviderDashboard')) {
    function fpProviderDashboard()
    {
		$user = (auth()->guard('twill_users')->user()) ? auth()->guard('twill_users')->user()->id : null;
        return  [
			'profile' => [
				'name' => 'users',
				'label' => 'My Profile',
				'label_singular' => 'My Profile',
				'route' => 'admin.users.edit',
				'routeParams' => ['user'=>$user],
				'search' => true,
				'count' => true,
				'create' => false,
				'countScope'=>['id'=>$user],
			],
			'modules' => [
				'financingProviders' => [
					'name' => 'financingProviders',
					'label' => 'Financing Providers',
					'label_singular' => 'Financing Provider',
					'routePrefix' => 'financing',
					'activity' => true,
					'search' => true,
					'draft' => true,
					'count' => true,
					'create' => false,
				],
			],
			'analytics' => ['enabled' => false],
    		'search_endpoint' => 'admin.search',
		];
    }
}

if (!function_exists('bdsProviderDashboard')) {
    function bdsProviderDashboard()
    {
		$user = (auth()->guard('twill_users')->user()) ? auth()->guard('twill_users')->user()->id : null;
        return  [
			'modules' => [
				'profile' => [
					'name' => 'users',
					'label' => 'My Profile',
					'label_singular' => 'My Profile',
					'route' => 'admin.users.edit',
					'routeParams' => ['user'=>$user],
					'search' => true,
					'count' => true,
					'create' => false,
					'countScope'=>['id'=>$user],
				],
				'bdsProviders' => [
					'name' => 'bdsProviders',
					'label' => 'Business Development Support Providers',
					'label_singular' => 'Business Development Support Provider',
					'routePrefix' => 'businesssupport',
					'activity' => true,
					'search' => true,
					'draft' => true,
					'count' => true,
					'create' => false,
				],
			],
			'analytics' => ['enabled' => false],
    		'search_endpoint' => 'admin.search',
		];
    }
}

if (!function_exists('adminDashboard')) {
    function adminDashboard() {

		return  [
			'modules' => [
				'financingProviders' => [
					'name' => 'financingProviders',
					'label' => 'Financing Providers',
					'label_singular' => 'Financing Provider',
					'routePrefix' => 'financing',
					'activity' => true,
					'search' => true,
					'draft' => true,
					'count' => true,
					'create' => false,
				],
				'bdsProviders' => [
					'name' => 'bdsProviders',
					'label' => 'Business Development Support Providers',
					'label_singular' => 'Business Development Support Provider',
					'routePrefix' => 'businesssupport',
					'activity' => true,
					'search' => true,
					'draft' => true,
					'count' => true,
					'create' => false,
				],
				'providerRequests' => [
					'name' => 'providerRequests',
					'label' => 'Applications & Requests',
					'label_singular' => 'Applications & Requests',
					'routePrefix' => 'requests',
					'activity' => true,
					'search' => true,
					'draft' => true,
					'count' => true,
					'create' => false,
				],
				'news' => [
					'name' => 'news',
					'label' => 'Content Management Actions',
					'label_singular' => 'Content Management Actions',
					'routePrefix' => 'cms',
					'activity' => true,
					'search' => true,
					'draft' => true,
					'count' => true,
					'create' => false,
				],
				'users' => [
					'name' => 'users',
					'label' => 'Userlist',
					'label_singular' => 'Userlist',
					'search' => true,
					'count' => true,
					'create' => false,
				],
				'reports' => [
					'name' => 'reports',
					'label' => 'Reports & Analytics',
					'label_singular' => 'Reports & Analytics',
					'route' => 'admin.enterprise.dashboard',
					'search' => true,
					'count' => true,
					'create' => false,
				],
			],
			'analytics' => ['enabled' => false],
    		'search_endpoint' => 'admin.search',
		];
    }
}

if (!function_exists('countryAdminDashboard')) {
    function countryAdminDashboard() {
		return  [
			'modules' => [
				'financingProviders' => [
					'name' => 'financingProviders',
					'label' => 'Financing Providers',
					'label_singular' => 'Financing Provider',
					'routePrefix' => 'financing',
					'activity' => true,
					'search' => true,
					'draft' => true,
					'count' => true,
					'create' => false,
				],
				'bdsProviders' => [
					'name' => 'bdsProviders',
					'label' => 'Business Development Support Providers',
					'label_singular' => 'Business Development Support Provider',
					'routePrefix' => 'businesssupport',
					'activity' => true,
					'search' => true,
					'draft' => true,
					'count' => true,
					'create' => false,
				],
				'providerRequests' => [
					'name' => 'providerRequests',
					'label' => 'Applications & Requests',
					'label_singular' => 'Applications & Requests',
					'routePrefix' => 'requests',
					'activity' => true,
					'search' => true,
					'draft' => true,
					'count' => true,
					'create' => false,
				],
				'news' => [
					'name' => 'news',
					'label' => 'Content Management Actions',
					'label_singular' => 'Content Management Actions',
					'routePrefix' => 'cms',
					'activity' => true,
					'search' => true,
					'draft' => true,
					'count' => true,
					'create' => false,
				],
				'reports' => [
					'name' => 'reports',
					'label' => 'Reports & Analytics',
					'label_singular' => 'Reports & Analytics',
					'route' => 'admin.enterprise.dashboard',
					'search' => true,
					'count' => true,
					'create' => false,
				],
			],
			'analytics' => ['enabled' => false],
    		'search_endpoint' => 'admin.search',
		];
    }
}
