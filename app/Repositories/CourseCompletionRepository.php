<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\CourseCompletion;
use App\Models\User;
use App\Models\Course;
use App\Models\UserLicense;
use DB;
use PDO;
use Auth;
use Carbon\Carbon;

class CourseCompletionRepository extends ModuleRepository
{
    

    public function __construct(CourseCompletion $model)
    {
        $this->model = $model;
    }



    /**
     * @param \Illuminate\Database\Query\Builder $query
     * @param array $scopes
     * @return \Illuminate\Database\Query\Builder
     */
    public function filter($query, array $scopes = [])
    {


        $likeOperator = $this->getLikeOperator();

        foreach ($this->traitsMethods(__FUNCTION__) as $method) {
            $this->$method($query, $scopes);
        }

        unset($scopes['search']);

        if (isset($scopes['exceptIds'])) {
            $query->whereNotIn($this->model->getTable() . '.id', $scopes['exceptIds']);
            unset($scopes['exceptIds']);
        }
        foreach ($scopes as $column => $value) {

            if ($column === 'status_type') {

                if ($value === '1') {

                    $query->where('score', '>=', 0.8);
                } elseif ($value === '2') {

                    $query->whereBetween('score', [0.2, 0.79]);
                } elseif ($value === '3') {

                    $query->where('score', '<', 0.2);
                }
            } elseif ($column === 'completion_date') {

                $range = explode(' - ', $value);

                $query->whereBetween('completion_date', [Carbon::parse($range[0]), Carbon::parse($range[1])]);

            } elseif ($column === 'enrollment_date') {

                $range = explode(' - ', $value);

                $query->whereBetween('enrollment_date', [Carbon::parse($range[0]), Carbon::parse($range[1])]);
            }
            
            else {
                if (method_exists($this->model, 'scope' . ucfirst($column))) {

                    $query->$column();
                } else {

                   
                    if (is_array($value)) {
                        $query->whereIn($column, $value);
                    } elseif ($column[0] == '%') {
                        $value && ($value[0] == '!') ? $query->where(substr($column, 1), "not $likeOperator", '%' . substr($value, 1) . '%') : $query->where(substr($column, 1), $likeOperator, '%' . $value . '%');
                    } elseif (isset($value[0]) && $value[0] == '!') {
                        $query->where($column, '<>', substr($value, 1));
                    } elseif ($value !== '') {
                        $query->where($column, $value);
                    }
                }
            }
        }

        return $query;
    }

    public function getCompleteRegistrations()
    {

        $registrations = array();

        $months = [
            'Jan' => '1', 'Feb' => '2', 'Mar' => '3', 'Apr' => '4', 'May' => '5', 'Jun' => '6',
            'Jul' => '7', 'Aug' => '8', 'Sep' => '9', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12'
        ];

        foreach ($months as $key => $value) {

            $c = User::where('published',1)->where('is_activated',1)->whereMonth('created_at', $value)->whereYear('created_at', '=', date('Y'))->get();

            $registrations[$key] = count($c);
        }


        $data = collect($registrations);

        return $data;
    }


    public function getPendingRegistrations()
    {

        $pending = array();

        $months = [
            'Jan' => '1', 'Feb' => '2', 'Mar' => '3', 'Apr' => '4', 'May' => '5', 'Jun' => '6',
            'Jul' => '7', 'Aug' => '8', 'Sep' => '9', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12'
        ];

        foreach ($months as $key => $value) {

            $c = User::where('published',0)->where('is_activated',0)->whereMonth('created_at', $value)->whereYear('created_at', '=', date('Y'))->get();

            $pending[$key] = count($c);
        }
        $data = collect($pending);

        return $data;
    }



  /** Enrollments Graph Repository */
  
    public function getYearlyTopEnrollments($year,$launchYear)
    {
        $enrollments = array();
        $completed = Course::leftJoin('user_licenses', function($join) use($launchYear, $year) {
                        $join->on('courses.course_id', '=', 'user_licenses.course_id')
                        ->whereYear('user_licenses.created_at', '>=', $launchYear)->whereYear('user_licenses.created_at', '<=', $year);
                        
                        })
                        ->join('resources', function($join){
                            $join->on('courses.id', '=', 'resources.course_id')
                            ->where('resource_category','=',1);
                        })
                        ->selectRaw('courses.name, user_licenses.id, user_licenses.course_id, count(*) as count')
                        ->groupBy('user_licenses.course_id')
                        ->orderBy('count', 'desc')
                        ->take(5)
                        ->get();


        foreach ($completed as $key => $course) {
            $license =  UserLicense::where('course_id', $course->course_id)->get();
           if(count($license) > 0){
            $enrollments[] = [
                'name' => $course->name,
                'total' => count($license)
            ];
           }
          
        }

        $data = collect($enrollments);
        return $data;
    }


    public function getTopYearlyEnrollments($year)
    {

        $launchYear = "2022";
        $eyear = date('Y');
        $enrollments = array();

        if($year == 0){
            $completed = Course::leftJoin('user_licenses', function($join) use($launchYear, $eyear) {
                $join->on('courses.course_id', '=', 'user_licenses.course_id')
                ->whereYear('user_licenses.created_at', '>=', $launchYear)->whereYear('user_licenses.created_at', '<=', $eyear);
                
                })
                ->join('resources', function($join){
                    $join->on('courses.id', '=', 'resources.course_id')
                    ->where('resource_category','=',1);
                })
                ->selectRaw('courses.name, user_licenses.id, user_licenses.course_id, count(*) as count')
                ->groupBy('user_licenses.course_id')
                ->orderBy('count', 'desc')
                ->take(5)
                ->get();
        }else{
            $completed =Course::leftJoin('user_licenses', function($join) use($year) {
                $join->on('courses.course_id', '=', 'user_licenses.course_id')
                ->whereYear('user_licenses.created_at', '<=', $year);
                
                })
                ->join('resources', function($join){
                    $join->on('courses.id', '=', 'resources.course_id')
                    ->where('resource_category','=',1);
                })
                ->selectRaw('courses.name, user_licenses.id, user_licenses.course_id, count(*) as count')
                ->groupBy('user_licenses.course_id')
                ->orderBy('count', 'desc')
                ->take(5)
                ->get();
            
            
            
            // UserLicense::whereYear('created_at', '=', $year)
            // ->selectRaw('id, course_id,count(*) as count')
            // ->groupBy('course_id')
            // ->orderBy('count', 'desc')
            // ->take(5)
            // ->get();

       

        }

        foreach ($completed as $key => $course) {

            $userlicenses = UserLicense::where('course_id', $course->course_id)->get();
           
            if (count($userlicenses) > 0) {
                $enrollments[] = [
                    'name' => $course->name,
                    'total' => count($userlicenses)
                ];
            } else {
                unset($completed[$key]);
            }
        }

       

        // $data = collect();
        return $enrollments;
    }

  /** End of Enrollments Graph Repository */



/** Completions Graph Repository */

    public function getYearlyTopCompletions($year,$launchYear)
    {
        $completions = array();
        $completed =  Course::leftJoin('course_completions', function($join) use($launchYear, $year) {
            $join->on('courses.id', '=', 'course_completions.course_id')
            ->whereYear('course_completions.created_at', '>=', $launchYear)->whereYear('course_completions.created_at', '<=', $year);
            
            })
            ->join('resources', function($join){
                $join->on('courses.id', '=', 'resources.course_id')
                ->where('resource_category','=', 1);
            })
            ->selectRaw('courses.name, course_completions.id, course_completions.course_id, course_completions.score, count(*) as count')
            ->groupBy('course_completions.course_id')
            ->orderBy('count', 'desc')
            ->take(5)
            ->get();

        

        foreach ($completed as $key => $course) {
            $licenses =  CourseCompletion::where('course_id', $course->course_id)->get();
            if(count($licenses) > 0){
                $completions[] = [
                    'name' => $course->name,
                    'total' => count($licenses)
                ];
            }
           
        }
        $data = collect($completions);
        return $data;
    } 

    public function getTopYearlyCompletions($year)
    {
        $launchYear = "2022";
        $eyear = date('Y');
      
        $completions = array();


        if($year == 0){
            $courses = Course::leftJoin('course_completions', function($join) use($launchYear, $eyear) {
                $join->on('courses.id', '=', 'course_completions.course_id')
                ->whereYear('course_completions.created_at', '>=', $launchYear)->whereYear('course_completions.created_at', '<=',  $eyear);
                
                })
                ->join('resources', function($join){
                    $join->on('courses.id', '=', 'resources.course_id')
                    ->where('resource_category','=', 1);
                })
                ->selectRaw('courses.name, course_completions.id, course_completions.course_id, course_completions.score, count(*) as count')
                ->groupBy('course_completions.course_id')
                ->orderBy('count', 'desc')
                ->take(5)
                ->get();
            
            
            // CourseCompletion::whereYear('created_at', '>=', $launchYear)->whereYear('created_at', '<=',  $eyear)
            // ->selectRaw('id, course_id,score, count(*) as count')
            // ->groupBy('course_id')
            // ->orderBy('count', 'desc')
            // ->take(5)
            // ->get();

        }else{
            $courses = Course::leftJoin('course_completions', function($join) use($year) {
                $join->on('courses.id', '=', 'course_completions.course_id')
                ->whereYear('course_completions.created_at', '=', $year);
                
                })
                ->join('resources', function($join){
                    $join->on('courses.id', '=', 'resources.course_id')
                    ->where('resource_category','=', 1);
                })
                ->selectRaw('courses.name, course_completions.id, course_completions.course_id, course_completions.score, count(*) as count')
                ->groupBy('course_completions.course_id')
                ->orderBy('count', 'desc')
                ->take(5)
                ->get();
            
            
            
            
            
            
            // CourseCompletion::whereYear('created_at', '=', $year)
            // ->selectRaw('id, course_id,score, count(*) as count')
            // ->groupBy('course_id')
            // ->orderBy('count', 'desc')
            // ->take(5)
            // ->get();

        }
       
        foreach ($courses as $key => $course) {
            $completed = CourseCompletion::where('course_id', '=', $course->course_id)->get();

            if (count($completed) > 0) {
                $completions[] = [
                    'name' => $course->name,
                    'total' => count($completed)
                ];
            } else {
                unset($courses[$key]);
            }
        }
       
        // $data = collect($completions);
        return $completions;
    }

    /** End of Completions Graph Repository */




    /** Top Ten Users with Completions */

     public function getTopUserCompletions(){
     
        $completions = array();
        $completed = CourseCompletion::where('user_id','!=',1)
             ->selectRaw('id, user_id,score, count(*) as count')
            ->groupBy('user_id')
            ->orderBy('count', 'desc')
            ->take(10)
            ->get();

        

        foreach ($completed as $key => $course) {
            $licenses =  CourseCompletion::where('user_id', $course->user_id)->get();
            $completions[] = [
                'name' => $course->users->first_name.' '.$course->users->last_name,
                'total' => count($licenses)
            ];
        }
        $data = collect($completions);
        return $data;


     }

      /** Enrollments Graph Repository */
  
    public function getYearlyQuickTipEnrollments($year,$launchYear)
    {
        $enrollments = array();
        $completed = Course::leftJoin('user_licenses', function($join) use($launchYear, $year) {
                        $join->on('courses.course_id', '=', 'user_licenses.course_id')
                        ->whereYear('user_licenses.created_at', '>=', $launchYear)->whereYear('user_licenses.created_at', '<=', $year);
                        
                        })
                        ->join('resources', function($join){
                            $join->on('courses.id', '=', 'resources.course_id')
                            ->where('resource_category','=', 2);
                        })
                        ->selectRaw('courses.name, user_licenses.id, user_licenses.course_id, count(*) as count')
                        ->groupBy('user_licenses.course_id')
                        ->orderBy('count', 'desc')
                        ->take(5)
                        ->get();


        foreach ($completed as $key => $course) {
            $license =  UserLicense::where('course_id', $course->course_id)->get();
            if (count($license) > 0) {
            $enrollments[] = [
                'name' => $course->name,
                'total' => count($license)
            ];
        }
        }

        $data = collect($enrollments);
        return $data;
    }


    public function getTopYearlyQuickTipEnrollments($year)
    {

        $launchYear = "2022";
        $eyear = date('Y');
        $enrollments = array();

       
        if($year == 0){
            $completed = Course::leftJoin('user_licenses', function($join) use($launchYear, $eyear) {
                $join->on('courses.course_id', '=', 'user_licenses.course_id')
                ->whereYear('user_licenses.created_at', '>=', $launchYear)->whereYear('user_licenses.created_at', '<=', $eyear);
                
                })
                ->join('resources', function($join){
                    $join->on('courses.id', '=', 'resources.course_id')
                    ->where('resource_category','=',2);
                })
                ->selectRaw('courses.name, user_licenses.id, user_licenses.course_id, count(*) as count')
                ->groupBy('user_licenses.course_id')
                ->orderBy('count', 'desc')
                ->take(5)
                ->get();
        }else{
            $completed =Course::leftJoin('user_licenses', function($join) use($year) {
                $join->on('courses.course_id', '=', 'user_licenses.course_id')
                ->whereYear('user_licenses.created_at', '<=', $year);
                
                })
                ->join('resources', function($join){
                    $join->on('courses.id', '=', 'resources.course_id')
                    ->where('resource_category','=',2);
                })
                ->selectRaw('courses.name, user_licenses.id, user_licenses.course_id, count(*) as count')
                ->groupBy('user_licenses.course_id')
                ->orderBy('count', 'desc')
                ->take(5)
                ->get();
            
        }

        foreach ($completed as $key => $course) {

            $userlicenses = UserLicense::where('course_id', $course->course_id)->get();
           
            if (count($userlicenses) > 0) {
                $enrollments[] = [
                    'name' => $course->name,
                    'total' => count($userlicenses)
                ];
            } else {
                unset($completed[$key]);
            }
        }

       

        // $data = collect();
        return $enrollments;
    }

  /** End of Enrollments Graph Repository */




 /** Completions Graph Repository */

 public function getYearlyQuickTipCompletions($year,$launchYear)
 {
     $completions = array();
     $completed = Course::leftJoin('course_completions', function($join) use($launchYear, $year) {
                $join->on('courses.id', '=', 'course_completions.course_id')
                ->whereYear('course_completions.created_at', '>=', $launchYear)->whereYear('course_completions.created_at', '<=', $year);
                
                })
                ->join('resources', function($join){
                    $join->on('courses.id', '=', 'resources.course_id')
                    ->where('resource_category','=', 2);
                })
                ->selectRaw('courses.name, course_completions.id, course_completions.course_id, course_completions.score, count(*) as count')
                ->groupBy('course_completions.course_id')
                ->orderBy('count', 'desc')
                ->take(5)
                ->get();

      
     
     foreach ($completed as $key => $course) {

        if(empty($course->course_id)){
            unset($completed[$key]);
        }else{
            $licenses =  CourseCompletion::where('course_id', $course->course_id)->get();
         $completions[] = [
             'name' => $course->name,
             'total' => count($licenses)
         ];
        }
        
     }
     $data = collect($completions);
     return $data;
 } 

 public function getTopYearlyQuickTipCompletions($year)
 {
     $launchYear = "2022";
     $eyear = date('Y');
   
     $completions = array();


     if($year == 0){
        $courses = Course::leftJoin('course_completions', function($join) use($launchYear, $eyear) {
            $join->on('courses.id', '=', 'course_completions.course_id')
            ->whereYear('course_completions.created_at', '>=', $launchYear)->whereYear('course_completions.created_at', '<=',  $eyear);
            
            })
            ->join('resources', function($join){
                $join->on('courses.id', '=', 'resources.course_id')
                ->where('resource_category','=', 2);
            })
            ->selectRaw('courses.name, course_completions.id, course_completions.course_id, course_completions.score, count(*) as count')
            ->groupBy('course_completions.course_id')
            ->orderBy('count', 'desc')
            ->take(5)
            ->get();
        
        

    }else{
        $courses = Course::leftJoin('course_completions', function($join) use($year) {
            $join->on('courses.id', '=', 'course_completions.course_id')
            ->whereYear('course_completions.created_at', '=', $year);
            
            })
            ->join('resources', function($join){
                $join->on('courses.id', '=', 'resources.course_id')
                ->where('resource_category','=', 2);
            })
            ->selectRaw('courses.name, course_completions.id, course_completions.course_id, course_completions.score, count(*) as count')
            ->groupBy('course_completions.course_id')
            ->orderBy('count', 'desc')
            ->take(5)
            ->get();
   

    }
    
     foreach ($courses as $key => $course) {
         $completed = CourseCompletion::where('course_id', '=', $course->course_id)->get();

         if (count($completed) > 0) {
             $completions[] = [
                 'name' => $course->name,
                 'total' => count($completed)
             ];
         } else {
             unset($courses[$key]);
         }
     }
    
     // $data = collect($completions);
     return $completions;
 }

 /** End of Completions Graph Repository */








    protected function getLikeOperator()
    {
        if (DB::connection()->getPDO()->getAttribute(PDO::ATTR_DRIVER_NAME) === 'pgsql') {
            return 'ILIKE';
        }

        return 'LIKE';
    }


}
