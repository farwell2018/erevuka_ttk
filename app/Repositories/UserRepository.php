<?php

namespace App\Repositories;
use A17\Twill\Repositories\UserRepository as TwillUserRepository;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use A17\Twill\Models\Group;
use App;
use TaylorNetwork\UsernameGenerator\Generator;
use App\Models\Domain;
use App\Models\TwillUsersAuth;
use A17\Twill\Models\Enums\UserRole;
use App\Edx\EdxAuthUser;

class UserRepository extends TwillUserRepository
{

    public function __construct(User $model)
    {
        $this->model = $model;
	}

	    /**
     * @param string[] $fields
     * @return \A17\Twill\Models\Model
     */
    public function createForRegister($fields)
    {

		return DB::transaction(function () use ($fields) {
            $original_fields = $fields;

            $fields = $this->prepareFieldsBeforeCreate($fields);

            // $generator = new Generator([ 'separator' => '_' ]);
            // $username = $generator->generate($fields['first_name'] . ' ' . $fields['last_name']);

            
            // $usern = EdxAuthUser::where('username', $username)->first();
             if($fields['role'] == 5){
               
              $username = $fields['partners_mobile_number'];

             }else{
               
              $username = $fields['mobile_number'];

             }
            
             
            
            
            if($fields['role'] == 5){
            
              $password = $fields['partners_password'];
              $phone  = $fields['partners_mobile_number'];
              $phone_locale = $fields['partners_mobile_number_country'];
              $email = $fields['partners_registration_email'];
              $info = $fields['partners_register_receive_information'];
              $consent = $fields['partners_contact_consent'];

            }else{
              $email = $username.'@ttk.com';
              $password = $fields['password'];
              $phone  = $fields['mobile_number'];
              $phone_locale = $fields['mobile_number_country'];
              $info = $fields['register_receive_information'];
              $consent = $fields['contact_consent'];
            }

          
            $object = $this->model->create([
				        'name' => $fields['first_name'] . ' ' . $fields['last_name'],
				        'email' => $email,
                'role' => $fields['role'],
                'username'=> $username,
                'password' => Hash::make($password),
                'company_id' => 1,
                'phone' => $phone,
                'phone_locale' => $phone_locale,
                'first_name' => $fields['first_name'],
                'last_name' => $fields['last_name'],
                'is_activated' => 0,
                'platform_information' => $info,
                'contact_consent'=> $consent,
                
			]);
			
      $this->beforeSave($object, $original_fields);

			$this->afterSave($object, $fields);
            
			$object->save();


            return $object;
        }, 3);
	}


    public function createForValidation($fields)
    {
        $verify = TwillUsersAuth::where('user_id', $fields['user'])->where('token', $fields['user'])->first();

       if(!empty($verify)){
        $this->model->where('id',$verify->user_id)->update([
            'is_activated' => 1,
            'registered_at' => Carbon\Carbon::now(),
        ]);

        


       }


    }

    protected function changeCompany($email)
    {
      //Get domain
      $raw_domain = explode("@", $email)[1];
      //Get company with domain
      $domain = Domain::where('name', $raw_domain)->first();
      if ($domain) {
        $company_id = $domain->company_id;
      } else {
        $company_id = null;
      }

      return $company_id;
    }
}
