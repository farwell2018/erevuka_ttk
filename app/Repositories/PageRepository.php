<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\ModuleRepository;
use Illuminate\Support\Arr;
use App\Models\Page;
use App\Models\Menus;
use DB;

class PageRepository extends ModuleRepository
{
    use HandleBlocks, HandleTranslations, HandleSlugs, HandleMedias, HandleFiles, HandleRevisions;   
    
    const PAGE_HOMEPAGE = 'home';
 
    public function __construct(Page $model)
    {
        $this->model = $model;
    }

    public function update($id, $fields)
    {
        
        DB::transaction(function () use ($id, $fields) {
            $object = $this->model->findOrFail($id);

            $this->beforeSave($object, $fields);

            $fields = $this->prepareFieldsBeforeSave($object, $fields);

            $object->fill(Arr::except($fields, $this->getReservedFields()));

            $object->save();

            $menu = Menus::where('key','=', $object->key)->first();
            if(empty($menu)){
               $create = new Menus();
               $create->title = $object->title;
               $create->description = $object->description;
               $create->key = $object->key;
               $create->menu_type = $fields['menu_type'];
               $create->published = true;
               $create->active = 1;
               $create->save();
             }else{
              $update = Menus::where('key','=', $object->key)->first();
              $update->title = $object->title;
              $update->description = $object->description;
              $update->key = $object->key;
              $update->menu_type = $fields['menu_type'];
              $update->published = true;
              $update->active = 1;
              $update->save();
             }
           
            $this->afterSave($object, $fields);
        }, 3);
    }


    public function updateBasic($id, $values, $scopes = [])
    {

       
        return DB::transaction(function () use ($id, $values, $scopes) {
            // apply scopes if no id provided
            if (is_null($id)) {
                $query = $this->model->query();

                foreach ($scopes as $column => $value) {
                    $query->where($column, $value);
                }
            
                $query->update($values);

                $query->get()->each(function ($object) use ($values) {
                    $this->afterUpdateBasic($object, $values);
                });
                
                
                return true;
            }

            // apply to all ids if array of ids provided
            if (is_array($id)) {
                $query = $this->model->whereIn('id', $id);
                $query->update($values);

                $query->get()->each(function ($object) use ($values) {
                    $this->afterUpdateBasic($object, $values);
                });

                return true;
            }

            if (($object = $this->model->find($id)) != null) {
                
                $object->update($values);
                $this->afterUpdateBasic($object, $values);
                 
             $update = Menus::where('key','=', $object->key)->first();

            // dd($object->published);

             if($object->published){

                $update->published = true;
                $update->active = 1;
                $update->save();
             
             }else{
                $update->published = false;
                $update->active = 0;
                $update->save();
             
             }
                return true;
            }

            return false;
        }, 3);
    }




    public function getPage($key, $with = [])
    {
        return $this->getByKey($key, $with);
	}
	public function getByKey($key, $with = [])
    {
        if (
            ($page = $this->model
                ->with($with)
                ->where('key', $key)
                ->first()) == null
        ) {
            if (isset($this->defaultTitle[$key])) {
                $title = $this->defaultTitle[$key];
            } else {
                $title = 'Untitled';
            }

            $page = Page::create([
                'key' => $key,
				'title' => $title,
				'published'=>1,
				'active' => 1
            ]);
        }
        return $page;
    }
}
