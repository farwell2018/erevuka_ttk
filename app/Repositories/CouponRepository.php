<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Coupon;
use App\Models\User;
use Str;

class CouponRepository extends ModuleRepository
{
    use HandleTranslations, HandleSlugs, HandleRevisions;

    public function __construct(Coupon $model)
    {
        $this->model = $model;
    }

    public function update($object, $fields)
    {

        
        $user = $fields['user'];
        $prefix = $fields['prefix']['en'];
        $expirationDate = now()->addDays(30);

        $code = implode('-', [strtoupper($prefix), substr(md5(uniqid(mt_rand(), true)), 0, 6)]);
        $coupon = Coupon::where('id', $object)->first();

        if ($coupon) {
            // If coupon code already exists, generate a new one
            $code = implode('-', [strtoupper($prefix),  substr(md5(uniqid(mt_rand(), true)), 0, 6)]);
            $coupon->code = $code;
            $coupon->user_id = $user;
            $coupon->expiration_date = $expirationDate;
            $coupon->save();
        } else {
            $coupon = new Coupon;
            $coupon->code = $code;
            $coupon->user_id = $user;
            $coupon->expiration_date = $expirationDate;
            $coupon->save();
        }
    }
}
