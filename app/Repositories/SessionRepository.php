<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Session;
use App\Traits\ZoomMeetingTrait;
use GuzzleHttp\Client;
use DB;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;

class SessionRepository extends ModuleRepository
{
    use HandleBlocks, HandleTranslations, HandleSlugs, HandleMedias, HandleFiles, HandleRevisions;
   
    const MEETING_TYPE_INSTANT = 1;
    const MEETING_TYPE_SCHEDULE = 2;
    const MEETING_TYPE_RECURRING = 3;
    const MEETING_TYPE_FIXED_RECURRING_FIXED = 8;

    public $client;
    public $jwt;
    public $headers;

    public function __construct(Session $model)
    {
        $this->model = $model;

        $this->client = new Client();
        $this->jwt = $this->generateZoomToken();
        $this->headers = [
            'Authorization' => 'Bearer '.$this->jwt,
            'Content-Type'  => 'application/json',
            'Accept'        => 'application/json',
        ];
    }


    public function generateZoomToken()
    {
        $key = env('ZOOM_API_KEY', '');
        $secret = env('ZOOM_API_SECRET', '');
        $payload = [
            'iss' => $key,
            'exp' => strtotime('+1 minute'),
        ];

        return \Firebase\JWT\JWT::encode($payload, $secret, 'HS256');
    }


    public function update($id, $fields)
    {
      if($fields['start_time'] < Carbon::now()){

      
        DB::transaction(function () use ($id, $fields) {
  
            $object = $this->model->findOrFail($id);

            $start = $this->toZoomTimeFormat($fields['start_time']);
            
            // $this->beforeSave($object, $fields);

            // $fields = $this->prepareFieldsBeforeSave($object, $fields);
        
            $object->update([
                    'meeting_id' => null,
                    'host_email' => null,
                    'topic' => $fields['title']['en'],
                    'status'=> null,
                    'start_time' => $start,
                    'agenda' => $fields['description'],
                    'description' => $fields['description'],
                    'join_url' => null,
                    'meeting_password' => null,
                    'session_video_url' =>(! empty($fields['session_video_url'])) ? $fields['session_video_url'] : null ,
                    'active' => 0,
                ]);
            // $object->save();
            
             $this->afterSave($object, $fields);

        }, 3);


      }else{
        DB::transaction(function () use ($id, $fields) {
  
            $object = $this->model->findOrFail($id);

            $create = $this->zoomCreate($fields);
            $start = $this->toZoomTimeFormat($fields['start_time']);
            
            $this->beforeSave($object, $fields);

            $fields = $this->prepareFieldsBeforeSave($object, $fields);
           
            $object->update([
                    'meeting_id' => $create['data']['id'],
                    'host_email' => $create['data']['host_email'],
                    'topic' => $create['data']['topic'],
                    'status'=> $create['data']['status'],
                    'start_time' => $start,
                    'agenda' => $create['data']['agenda'],
                    'description' => $create['data']['agenda'],
                    'join_url' => $create['data']['join_url'],
                    'phone_locale' => $create['data']['join_url'],
                    'meeting_password' => $create['data']['password'],
                    'session_video_url' =>(! empty($fields['session_video_url'])) ? $fields['session_video_url'] : null ,
                    'active' => 0,
                ]);
            // $object->save();
            

             $this->afterSave($object, $fields);

        }, 3);

      }
        
    
    }


    public function updateBasic($id, $values, $scopes = [])
    {
        return DB::transaction(function () use ($id, $values, $scopes) {
            // apply scopes if no id provided
            if (is_null($id)) {
                $query = $this->model->query();

                foreach ($scopes as $column => $value) {
                    $query->where($column, $value);
                }

                $query->update($values);

                $query->get()->each(function ($object) use ($values) {
                    $this->afterUpdateBasic($object, $values);
                });

                return true;
            }

            // apply to all ids if array of ids provided
            if (is_array($id)) {
                $query = $this->model->whereIn('id', $id);
                $query->update($values);

                $query->get()->each(function ($object) use ($values) {
                    $this->afterUpdateBasic($object, $values);
                });

                return true;
            }

            if (($object = $this->model->find($id)) != null) {
                dd($id);
                $object->update($values);
                $this->afterUpdateBasic($object, $values);
                return true;
            }

            return false;
        }, 3);
    }


    public function zoomCreate($data)
    {
        $path = 'users/me/meetings';
        $url = $this->retrieveZoomUrl();
        $body = [
            'headers' => $this->headers,
            'body'    => json_encode([
                'topic'      => $data['title']['en'],
                'type'       => self::MEETING_TYPE_SCHEDULE,
                'start_time' => $this->toZoomTimeFormat($data['start_time']),
                'duration'   => $data['duration'],
                'agenda'     => (! empty($data['description'])) ? $data['description'] : null,
                'timezone'     => 'Africa/Nairobi',
                'settings'   => [
                    'host_video'        => true ,
                    'participant_video' => false,
                    'waiting_room'      => true,
                   
                    "join_before_host" => true,
                    
                ],
            ]),
        ];
        
        $response =  $this->client->post($url.$path, $body);

        return [
            'success' => $response->getStatusCode() === 201,
            'data'    => json_decode($response->getBody(), true),
        ];
    }



    public function zoomUpdate($id, $data)
    {

        
        $path = 'meetings/' . $id;
        $url = $this->retrieveZoomUrl();
        
      
        $body = [
            'headers' => $this->headers,
            'body'    => json_encode([
                'topic'      => $data['title'],
                'type'       => self::MEETING_TYPE_SCHEDULE,
                'start_time' => $this->toZoomTimeFormat($data['start_time']),
                'duration'   => $data['duration'],
                'agenda'     => (! empty($data['description'])) ? $data['description'] : null,
                'timezone'     => 'Africa/Nairobi',
                'settings'   => [
                    'host_video'        => ($data['host_video'] == "1") ? true : false,
                    'participant_video' => ($data['participant_video'] == "1") ? true : false,
                    'waiting_room'      => true,
                     "join_before_host" => true,
                ],
            ]),
        ];
        $response =  $this->client->patch($url.$path, $body);

       

        return [
            'success' => $response->getStatusCode() === 204,
            'data'    => json_decode($response->getBody(), true),
        ];
    }

    private function retrieveZoomUrl()
    {
        return env('ZOOM_API_URL', '');
    }

    public function toZoomTimeFormat(string $dateTime)
    {
        try {
            $date = new \DateTime($dateTime);

            return $date->format('Y-m-d\TH:i:s');
        } catch (\Exception $e) {
            Log::error('ZoomJWT->toZoomTimeFormat : '.$e->getMessage());

            return '';
        }
    }

}
