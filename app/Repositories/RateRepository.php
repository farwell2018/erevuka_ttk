<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\Rate;

class RateRepository extends ModuleRepository
{
    

    public function __construct(Rate $model)
    {
        $this->model = $model;
    }
}
