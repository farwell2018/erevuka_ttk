<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use App\Models\JobroleCourse;
use App\Models\UserLicense;
use App\Models\CourseCompletion;

class Course extends Model
{
    //
    use HasBlocks, HasTranslation, HasSlug, HasMedias, HasFiles, HasRevisions, HasPosition;

    protected $fillable = [
        'id',
        'name',
        'short_description',
        'overview',
        'more_info',
        'effort',
        'start',
        'end',
        'enrol_start',
        'enrol_end',
        'price',
        'course_image_uri',
        'course_video_uri',
        'course_category_id',
        'status',
        'slug',
        'order_id',
        'course_video',
        'sessions_id',
        'course_id',
    
    ];

    public $translatedAttributes = [
        'title',
        'description',
        'active',
    ];
    
    public $slugAttributes = [
        'title',
    ];
    
    public $mediasParams = [
        'cover' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
    ];

   
    //Tell laravel more dates
    protected $dates = ['start', 'end', 'enrol_start', 'enrol_end','created_at','updated_at'];

    
    protected $appends = ['resource_url'];

    public function getResourceUrlAttribute()
    {
        return url('/admin/courses/'.$this->getKey());
    }


   

}
