<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ForumReaction extends Model
{
    //
    public const REACTION = 1;

    protected $guarded = [];
}
