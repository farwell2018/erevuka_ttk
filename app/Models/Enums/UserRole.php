<?php

    namespace A17\Twill\Models\Enums;

    use MyCLabs\Enum\Enum;

    class UserRole extends Enum
    {   
        const VIEWONLY = 'View only';
        const PUBLISHER = 'Publisher';
        const ADMIN = 'Admin';
        const Parents = '1';
        const TOT = '2';
        const Agent = '3';
        const Business= '4';
        const Partner = '5';
        const Admin = '8';
    }