<?php

namespace App\Models;


use A17\Twill\Models\Model;

class Rate extends Model 
{
    

    protected $fillable = [
        'published',
        'title',
        'range',
        'price',
        'currency'
    ];
    
}
