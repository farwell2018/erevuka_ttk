<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class Forum extends Model implements Sortable
{
    use HasBlocks, HasTranslation, HasSlug, HasMedias, HasFiles, HasRevisions, HasPosition;

    const TYPE_QUESTION=2;
    const TYPE_DISCUSSION=1;

    protected $fillable = [
        'published',
        'title',
        'description',
        'position',
        'forum_type',
        'forum_topic',
        'created_by',
    ];
    
    public $translatedAttributes = [
        'title',
        'description',
        'active',
    ];
    
    public $slugAttributes = [
        'title',
    ];
    
    public $mediasParams = [
        'cover' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
    ];

    public static function getTypes()
    {
        $types = array(
         array('id' => self::TYPE_QUESTION, 'name'=>'Question'),
         array('id' => self::TYPE_DISCUSSION, 'name'=>'Discussion'),
          );
        return $types;
    }

    public function topic()
    {
        return $this->belongsTo(ForumTopic::class,'forum_topic','id');
    }
 

    public function user()
    {
        return $this->belongsTo(User::class,'created_by','id');
    }

    public function replies()
    {
        return $this->hasMany(ForumReply::class)->latest();
    }

    public function reactions()
    {
        return $this->hasMany(ForumReaction::class);
    }

    /**
     * Process files eg. Images & Gif and save them in publick directory
     * @param files $files
     */
    public static function saveFiles($files)
    {
        return collect($files)->map(function ($file) {
            $fileName = uniqid().$file->getClientOriginalName();
            $fileName = strtolower(str_replace(' ', '_', $fileName));
            $file->move('Forums', $fileName);
            return $fileName;
        });
    }
 
}
