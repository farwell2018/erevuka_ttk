<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class EvaluationOption extends Model implements Sortable
{
    use HasPosition;

    //

    protected $table = 'evaluation_options';


    protected $fillable = [
        'published',
        'title',
        'type',
        'value1',
        'value2',
        'value3',
        'value4',
        'value5',
        'position',
    ];


    public function evaluation()
    {
        return $this->hasOne(Evaluation::class);
    }
}
