<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class Event extends Model implements Sortable
{
    use HasBlocks, HasTranslation, HasSlug, HasMedias, HasFiles, HasRevisions, HasPosition;

    protected $fillable = [
        'published',
        'title',
        'description',
        'external_link',
        'event_date',
        'event_time',
        'position',
    ];
    
    public $translatedAttributes = [
        'title',
        'description',
        'short_description',
        'active',
    ];
    
    public $slugAttributes = [
        'title',
    ];

    protected $dates = [
        'event_date',
        'event_time',
	];
   
    protected $presenter = 'App\Presenters\Front\EventPresenter';

    protected $with = ['translations', 'medias'];
    
    public $mediasParams = [

        'event_image' => [
            'default' => [
                [
                    'name' => 'landscape',
                    
                ]
            ]
        ]
        // 'event_image' => [
        //     'default' => [
        //         [
        //             'name' => 'default',
        //             'ratio' => 16 / 9,
        //         ],
        //     ],
        //     'mobile' => [
        //         [
        //             'name' => 'mobile',
        //             'ratio' => 1,
        //         ],
        //     ],
        //     'flexible' => [
        //         [
        //             'name' => 'free',
        //             'ratio' => 0,
        //         ],
        //         [
        //             'name' => 'landscape',
        //             'ratio' => 16 / 9,
        //         ],
        //         [
        //             'name' => 'portrait',
        //             'ratio' => 3 / 5,
        //         ],
        //     ],
        // ],
    ];
}
