<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use Carbon\Carbon;


class CourseCompletion extends Model
{
    use HasPosition;



    public function courses()
    {
        return $this->belongsTo(Course::class, 'course_id', 'id');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

  
    public function roles()
    {
        return $this->belongsTo(JobRole::class, 'job_role_id', 'id');
    }

    public function getCompletionDateAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }


    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getEnrollmentDateAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

   
    public function getCompletionStatusAttribute(){

        $completed = $this->checkCompletionsStatus($this->course_id, $this->user_id) ;

         return $completed['status'];


      }


      private function checkCompletionsStatus($course_id, $user_id){

       $completion = CourseCompletion::where('course_id', $course_id)->where('user_id', $user_id)->first();

       if($completion){

           if($completion->score >= 0.80){

               $completionStatus = 1;
               $status = "Completed";

           }else{

               $completionStatus = 2;
               $status = 'Completed but failed';
           }
       }else{
           $completionStatus = false;
           $status = 'In Progress';
       }

       return [
       'status' => $status,
       'completionStatus'=>$completionStatus
        ];

   }

}
