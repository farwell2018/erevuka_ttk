<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use App\Models\MyCommunity;
use App\Models\TrainingLike;
use App\Models\TrainingComment;
use Auth;
use App\Edx\EdxAuthUser;
use App\Edx\StudentCourseEnrollment;
use App\Edx\StudentCourseCompletion;
use App\Edx\StudentCourseware;
use App\Models\CourseCompletion;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use Carbon\Carbon;
use App;

class UserLicense extends Model
{
    //
    use HasPosition;

    protected $fillable = [
        'course_id',  'user_id', 'enrolled_at', 'company_license_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['enrolled_at'];

    protected $appends = ['resource_url'];
     /**
     * Get the company that this user belongs to .
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }
    /**
     * Get the company that this user belongs to .
     */
    public function course()
    {
        return $this->belongsTo('App\Models\Course','course_id','course_id');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }


    public function getcompletionDateAttribute()
  {
    $user = User::where('email',Auth::user()->email)->first();
    $quiz = CourseCompletion::where('user_id', $user->id)->where('course_id', $this->course->id)->first();

    if ($quiz) {

      return $quiz->completion_date;

    } else {

      return '';
    }
  }



    public function getResourceUrlAttribute()
    {
        return url('/admin/user-licenses/'.$this->getKey());
    }

    public function getEnrollments($course){
        $enrollments = $this->where('course_id', $course)->get();
       
        return count($enrollments);
  }

  public function getCompleted($course){
    $completions =CourseCompletion::where('course_id', $course)->get();
    return count($completions);
  }

  public function getStatusAttribute()
  {
    
    return  App::environment(['local', 'staging']) ? $this->statusLocal() : $this->statusLive();
  }


  public function statusLocal(){

    if ($this->enrolled_at) {
      $user = User::where('email',Auth::user()->email)->first();
  
      $quiz = CourseCompletion::where('user_id',$user->id)->where('course_id',$this->course->id)->first();
  
      if ($quiz) {
        $score = (float)$quiz->score;
        $pass = (float)0.80;
        $high_low = (float)0.79;


        if ($score >= $pass) {
          return 'Completed';
        } elseif ($score < $high_low) {
          return 'Completed but failed';
        }
      } else {
        return 'In Progress';
      }
   
  } 
  }


  public function statusLive(){

  $email= '';
      if ($this->enrolled_at) {
          $result = $this->getCourseProgress($this->course->course_id,$email);
          return $result['status'];
      } else {
          return 'Pending';
      }

  }


  public function getGradeAttribute()
    {

      return  App::environment(['local', 'staging']) ? $this->gradeLocal() : $this->gradeLive();

         
    }

    public function gradeLocal(){
 
      $user = User::where('email',Auth::user()->email)->first();

      $license = UserLicense::where('user_id',$user->id)->where('course_id',$this->course->course_id)->first();

      if(empty($license->completion_id)){
          $license->completion_id = $this->course->id;
          $license->save();
  
      }

      $quiz = CourseCompletion::where('user_id',$user->id)->where('course_id',$this->course->id)->first();

      if ($quiz) {

        return $quiz->score;
      } else {
        return 0;
      }

    }


  public function gradeLive()
  {

    $user = User::where('email',Auth::user()->email)->first();
     
    $email= '';
     
    $license = UserLicense::where('user_id',$user->id)->where('course_id',$this->course->course_id)->first();

    if(empty($license->completion_id)){
        $license->completion_id = $this->course->id;
        $license->save();

    }

    if ($this->enrolled_at) {
        $result = $this->getCourseProgress($this->course->course_id,$email);

      

        $quiz = CourseCompletion::where('user_id',$user->id)->where('course_id',$this->course->id)->first();

     

        if($result['grade'] > 0){
         if($quiz){
           $quiz->user_id = $user->id;
           $quiz->course_id = $this->course->id;
           $quiz->username = $user->first_name .' '. $user->last_name;
           $quiz->email = $user->email;
           $quiz->score = $result['grade'];
           $quiz->max_score = 1;
           $quiz->completion_date = $result['completion'];
           $quiz->enrollment_date = $result['enrollment'];
           $quiz->save();

         }else{
           $quiz = new CourseCompletion();
           $quiz->user_id = $user->id;
           $quiz->course_id = $this->course->id;
           $quiz->username = $user->first_name .' '. $user->last_name;
           $quiz->email = $user->email;
           $quiz->score = $result['grade'];
           $quiz->max_score = 1;
           $quiz->completion_date = $result['completion'];
           $quiz->enrollment_date = $result['enrollment'];
           $quiz->save();

         }

         return $quiz->score;

       }else{
         return 0;
       }

      }
  }

  
  public function getGrades($email)
  {
       if ($this->enrolled_at) {
          //  $result = $this->getCourseProgresses($this->course->id,$email);
           //var_dump($result);die;
          //  return $result['grade'];
          return 1;
       } else {
           return 0;
       }
  }

  public function getStatuses($email)
  {

       if ($this->enrolled_at) {
          //  $result = $this->getCourseProgresses($this->course->id,$email);
          //  return $result['status'];
          return 1;
       } else {
           return 0;
       }
  }


  public function getActionAttribute()
{
 
 
 return  App::environment(['local', 'staging']) ? $this->actionLocal() : $this->actionLive();


 
}

public function actionLocal(){
  $configLms = config()->get("settings.lms.live");
  if ($this->enrolled_at) {
    $user = User::where('email',Auth::user()->email)->first();

    $quiz = CourseCompletion::where('user_id',$user->id)->where('course_id',$this->course->id)->first();
    if($quiz){
    if($quiz->score >= 0.80 ){

       return route('course.certificate', $this->course->id);
    }
  }
 return $configLms['LMS_BASE'] . '/courses/' . $this->course->course_id . '/courseware';
} else {
   return route('course.enroll', $this->course->id);
}

}



public function actionLive(){

  $configLms = config()->get("settings.lms.live");
  $email= '';
  if ($this->enrolled_at) {
       $result = $this->getCourseProgress($this->course->course_id,$email);
       if($result['grade'] >= 0.70 && $result['status'] == "Completed"){

          return route('course.certificate', $this->course->id);
       }
    return $configLms['LMS_BASE'] . '/courses/' . $this->course->course_id . '/courseware';
  } else {
      return route('course.enroll', $this->course->id);
  }



}

  public function getCommunityStatus($follow){

    $status = MyCommunity::where('user_id',Auth::user()->id)->where('follow_id',$follow)->first();

    if(empty($status['status']) || $status['status'] == 0 ){

      $status['status'] = 0;
    }

    return $status['status'];

  }


  public function getCommunityAction($follow)
  {
    $status = MyCommunity::where('user_id',Auth::user()->id)->where('follow_id',$follow)->first();

  if(empty($status['status']) || $status['status'] == 0){
      $id = 0;
    return route('profile.myFollow', [$follow,$id]);
  }elseif( $status['status'] == 1 && $status['follow_id'] == Auth::user()->id){
    $id = 1;
  return route('profile.myFollow', [$follow,$id]);

}elseif( $status['status'] == 2){
    $id = 2;
  return route('profile.myFollow', [$follow,$id]);
  }

}


public function getCommunityBlockAction($follow)
{
  $status = MyCommunity::where('user_id',Auth::user()->id)->where('follow_id',$follow)->first();
  if(!empty($status)){
    if( $status['status'] == 2){
        $id = 3;
      return route('profile.myFollow', [$follow,$id]);
      }elseif( $status['status'] == 3){
        $id = 4;
      return route('profile.myFollow', [$follow,$id]);
      }
  }
  

}

public function getLikes($like,$courseid)
{
  $likes =TrainingLike::where('like_id',$like)->where('course_id',$courseid)->get();

    if(empty($likes)){
      return 0;
    }
  return count($likes);

}

public function getLikeAction($like,$courseid)
{

  return route('profile.myLike', [$like,$courseid]);

}

public function getFollowers($follow){

 $followers = Mycommunity::where('follow_id',$follow)->get();

  if(empty($followers)){

    return 0;
  }

  return count($followers);

}


public function getCommentCount($follow,$courseid){

 $comments = TrainingComment::where('comment_id',$follow)->where('course_id',$courseid)->whereNull('parent_id')->get();

  if(empty($comments)){
    return 0;
  }
  return count($comments);

}

public function getCommentAction(){
    return route('profile.myComment');

}


private function getCourseProgress($courseId,$email)
{
   if(!empty($email)){
     $user = EdxAuthUser::where('email', $email)->firstOrFail();
   }else{
     $user = EdxAuthUser::where('email', Auth::user()->email)->firstOrFail();
   }
   $enrollment = StudentCourseEnrollment::where(['user_id' => $user->id, 'course_id' => $courseId])->first();
   $completions = StudentCourseCompletion::where(['student_id' => $user->id, 'module_type' => 'problem', 'course_id' => $courseId])->orderBy('id', 'DESC')->first();
    if ($enrollment) {

        $gradeApiObject = $enrollment->getGenCert();
        $completion ='';
 
      if($gradeApiObject[0]->percent >= 0.75){
           $gradeApiObject[0]->letter_grade = 'Completed';
        }elseif($gradeApiObject[0]->percent <= 0.74 && $gradeApiObject[0]->percent >= 0.10){

         $gradeApiObject[0]->letter_grade = 'Completed but Failed';
         $completion = date('Y-m-d H:i:s', strtotime($completions->modified));
         }else{
         
           $gradeApiObject[0]->letter_grade = 'In Progress';
           $completion ='';
       }

        return [
            'status' => $gradeApiObject[0]->letter_grade,
            'grade' => (float)$gradeApiObject[0]->percent,
            'completion' => $completion,
            'enrollment' => $enrollment->created,
        ];
    } else {
        return ['status' => 'Pending', 'grade' => '0'];
    }
}

private function getCourseProgresses($courseId,$email)
{
  $grades=array();
  $max = array();
   if(!empty($email)){
     $user = EdxAuthUser::where('email', $email)->firstOrFail();
   }else{
     $user = EdxAuthUser::where('email', Auth::user()->email)->firstOrFail();
   }

   $enrollment = StudentCourseEnrollment::where(['user_id' => $user->id, 'course_id' => $courseId])->first();
   $completion = StudentCourseCompletion::where(['student_id' => $user->id, 'module_type' => 'problem', 'course_id' => $courseId])->orderBy('id', 'DESC')->first();
    if ($enrollment) {

        $gradeApiObject = $enrollment->getGenCerts($user->id, $courseId);

        if($gradeApiObject[0]->percent >= 0.70){
          $gradeApiObject[0]->letter_grade = 'Completed';
         $completion = date('Y-m-d H:i:s', strtotime($completion->modified));

     }elseif($gradeApiObject[0]->percent <= 0.69 && $gradeApiObject[0]->percent >= 0.10){

        $gradeApiObject[0]->letter_grade = 'Completed but Failed';
         $completion = date('Y-m-d H:i:s', strtotime($completion->modified));

        }else{

          $gradeApiObject[0]->letter_grade = 'In Progress';
         $completion = '';
      }
      return [
        'status' => $gradeApiObject[0]->letter_grade,
        'grade' => (float)$gradeApiObject[0]->percent,
        'completion' => $completion,
        'enrollment' => $enrollment->created,
        ];
    }
     else {
         return ['status' => 'Pending', 'grade' => '0'];
    }
}

public function getEvaluationStatus($courseid, $userid){

  $evaluation  = UserEvaluation::where('user_id','=', $userid)->where('course_id','=',$courseid)->first();

  if(empty($evaluation)){
    return 0;

  }else{

    return 1;
  }

}

}
