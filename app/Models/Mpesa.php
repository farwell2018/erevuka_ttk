<?php

namespace App\Models;
use App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Mpesa extends Model
{
    protected $table='subscriptions';
    
    protected $fillable = [
        'user_id',
        'transaction_id',
        'coupon',
        'subscription_start_date',
        'subscription_end_date',
    ];

    public function user()
{
    return $this->belongsTo(User::class);
}
}
