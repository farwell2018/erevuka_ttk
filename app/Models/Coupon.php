<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class Coupon extends Model implements Sortable
{
    use HasTranslation, HasSlug, HasRevisions, HasPosition;

    protected $fillable = [
        'published',
        'title',
        'description',
        'position',
        'user_id',
        'code',
        'expiration_date'

    ];

    public $translatedAttributes = [
        'title',
        'description',
        'active',
    ];

    public $slugAttributes = [
        'title',
    ];

    protected $presenter = 'App\Presenters\Front\CouponPresenter';

    protected $presenterAdmin = 'App\Presenters\Front\CouponPresenter';

    public function users()
    {
        return $this->belongsTo(User::class,  'user_id', 'id');
    }


    public function getCodeAttributes()
    {

        return $this->code;
    }
}
