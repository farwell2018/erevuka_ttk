<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class Group extends Model implements Sortable
{
    use HasBlocks, HasTranslation, HasSlug, HasMedias, HasFiles, HasRevisions, HasPosition;

    //
    const TYPE_PUBLIC=2;
    const TYPE_PRIVATE=1;

    const TYPE_PARTICIPANTS=2;
    const TYPE_AAC=1;
   
    protected $table = 'groups';

    protected $fillable = [
        'title', 'description', 'group_type_id','group_format','cover_image','created_by','group_theme'
    ];   

    public $translatedAttributes = [
      'title',
      'description',
      'active',
  ];
  
  public $slugAttributes = [
      'title',
  ];
  protected $with = ['translations', 'medias'];

  public $mediasParams = [
      'cover_image' => [
          'default' => [
              [
                  'name' => 'default',
                  'ratio' => 16 / 9,
              ],
          ],
          'mobile' => [
              [
                  'name' => 'mobile',
                  'ratio' => 1,
              ],
          ],
          'flexible' => [
              [
                  'name' => 'free',
                  'ratio' => 0,
              ],
              [
                  'name' => 'landscape',
                  'ratio' => 16 / 9,
              ],
              [
                  'name' => 'portrait',
                  'ratio' => 3 / 5,
              ],
          ],
      ],
  ];

    public function user()
    {
        return $this->belongsTo(User::class , 'created_by', 'id');
    }

    public function groupUsers()
  {
    return $this->hasMany(User::class);
  }


    public static function getFormats()
    {
        $types = array(
         array('id' => self::TYPE_PUBLIC, 'name'=>'Public Group'),
         array('id' => self::TYPE_PRIVATE, 'name'=>'Private Group'),
          );
        return $types;
    }

    public function type()
    {
        return $this->hasOne(GroupType::class,'id','group_type_id');
    }


    public function format()
    {
        return $this->hasOne(GroupFormat::class,'id','group_format');
    }
}
