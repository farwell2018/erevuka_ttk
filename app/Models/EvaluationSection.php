<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EvaluationSection extends Model
{
    //

    protected $table = 'evaluation_sections';


    public function section_evaluations(){
      return $this->hasMany(Evaluation::class,'sectionid','id');
    }
}
