<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Daycare extends Model
{
    protected $table="daycares";

    protected $fillable = ['daycare_id','name','contact_name','contact_number','location','cohort','joining_date'];
}
