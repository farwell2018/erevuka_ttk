<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class UserMessages extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function scopeReceivedMessages($query)
    {
        return $query->where('receiver_id', Auth::id());
    }

    public function scopeSentMessages($query)
    {
        return $query->where('sender_id', Auth::id());
    }

    public function scopeAllMessages($query)
    {
        return $query->where('sender_id', Auth::id())->orWhere('receiver_id', Auth::id());
    }

    public static function createMessage($details = [])
    {
        $message = self::create($details);
        
      
        $receiver = User::where('id', $message->receiver_id)->first();
    
         $message = [
            'type'=> Notification::MESSAGE,
            'subject'=> 'New Message',
            'model_id' => $message->id,
            'message' => 'You have recieved a new message from'.$message->sender->name,
           ];

           $receiver->notify(new \App\Notifications\NewMessageNotification($message));


        return $message;
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'receiver_id', 'id');
    }

    public function conversation()
    {
        return $this->hasMany(Conversation::class, 'user_message_id', 'id');
    }
}
