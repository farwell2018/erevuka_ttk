<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class Evaluation extends Model implements Sortable
{
    use HasBlocks, HasPosition;

    protected $fillable = [
        'published',
        'title',
        'course_id',
        'option_id',
        'position',
    ];

    protected $table = 'evaluations';


    public function scopeActive($query)
    {
      return $query->where('status', '=', 0);
    }

    public function evaluation_sections(){
      return $this->belongsTo(EvaluationSection::class,'sectionid','id');
    }

    public function evaluation_options(){
      return $this->belongsTo(EvaluationOption::class,'optionid','id');
    }


    
    public static function getRadio($option, $item){

      $Eoptions = EvaluationOption::where('id', $option)->first();

      $var = '<div class="radio"><label><input type="radio" name="'.$item.'" value="'.$Eoptions->value1.'"> '.$Eoptions->value1.'</label></div>
              <div class="radio"><label><input type="radio" name="'.$item.'" value="'.$Eoptions->value2.'"> '.$Eoptions->value2.'</label></div>';
          if(!empty($Eoptions->value3)){
            
            $var .=  '<div class="radio"><label><input type="radio" name="'.$item.'" value="'.$Eoptions->value3.'"> '.$Eoptions->value3.'</label></div>';
          }
          if(!empty($Eoptions->value4)){
            $var .=  '<div class="radio"><label><input type="radio" name="'.$item.'" value="'.$Eoptions->value4.'"> '.$Eoptions->value4.'</label></div>';
          }
             if(!empty($Eoptions->value5)){

      $var .= '<div class="radio"><label><input type="radio" name="'.$item.'" value="'.$Eoptions->value5.'"> '.$Eoptions->value5.'</label></div>';
             }


      return $var;
  }
}
