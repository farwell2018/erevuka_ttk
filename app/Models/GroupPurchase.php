<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupPurchase extends Model
{
  protected $table = 'group_purchases';

  protected $fillable = [
    'unique_code', 'user_id'
  ];


  public function users()
  {
    return $this->belongsTo(User::class, 'user_id', 'id');
  }
}
