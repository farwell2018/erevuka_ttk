<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupActivityReply extends Model
{
    //
    use SoftDeletes;

    protected $guarded = [];

    public function replyReplies()
    {
        return $this->hasMany(GroupActivityReplyReplies::class)->latest();
    }

    public function replyReactions()
    {
        return $this->hasMany(GroupActivityReplyReaction::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
