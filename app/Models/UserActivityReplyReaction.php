<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserActivityReplyReaction extends Model
{
    protected $guarded = [];
}
