<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use willvincent\Rateable\Rateable;
use Share;
use Auth;

class Resource extends Model implements Sortable
{
    use HasBlocks, HasTranslation, HasSlug, HasMedias, HasFiles, HasRevisions, HasPosition,Rateable;

    protected $fillable = [
        'published',
        'title',
        'description',
        'position',
        'resource_type_id',
        'resource_theme_id',
        'resource_category',
        'resource_country',
        'cover_image',
        'audio_file',
        'video_file',
        'downloable_file',
        'external_link',
        'content_bucket_id',
        'course_id',
        'video_length',
        'related_courses'
    ];
    
    public $translatedAttributes = [
        'title',
        'description',
        'external_text',
        'external_link',
        'active',
    ];
    
    public $slugAttributes = [
        'title',
    ];
    public $filesParams = ['download_resource','resource_audio','resource_video'];
    
    protected $presenter = 'App\Presenters\Front\ResourcePresenter';

    protected $presenterAdmin = 'App\Presenters\Front\ResourcePresenter';

	protected $with = ['translations', 'medias','files'];

    public $mediasParams = [


        'resource_image' => [
            'default' => [
                [
                    'name' => 'landscape',
                    
                ]
            ]
        ]
    ];


    public function type()
    {
        return $this->hasOne(ResourceType::class,'id','resource_type_id');
    }

    public function getShareAttribute()
    {
        return Share::load(route('resource.details', $this->id), $this->name)->services('email','linkedin','facebook','twitter');
    }

    public function documents()
    {
        return $this->hasMany(ResourceDocument::class)->orderBy('position');
    }


    public function course()
    {
        return $this->belongsTo(Course::class);
    }




    public function getEnrollmentAttribute(){
    

        /** use this when live */
  
      // if($this->course_id === 'course-v1:TowerSacco+TS101+2022_Q1'){
          
      //    $enrolled = Auth::check() ? $this->checkEnrollmentStatus($this->course_id) : false;
  
      //   }else{
  
         /** Local instance */

        if($this->course){

            $enrolled = Auth::check() ? $this->checkLicenseStatus($this->course->course_id) : false;
        }else{

            $enrolled = "none";
        }
       
    //   //   
       return $enrolled;
  
     
    }
  
  
    public function getCompletionAttribute(){
      
      $completed = Auth::check() ? $this->checkCompletionStatus($this->course->id) : false;
  
      return $completed['completionStatus'];
  
  
    }
  
  
    
  
    public function getMandatoryAttribute(){
      // dd(Auth::user());
     $role = JobroleCourse::where('course_id', $this->id)->where('job_role_id', Auth::user()->job_role_id)->first();
      
     if(!empty($role)){
  
      return true;
  
     }else{
  
      return false;
     }
     
   }
  
  
   public static function getNotMandatory(){
      $compulsories = array();
      
      $role = JobroleCourse::where('job_role_id', Auth::user()->job_role_id)->get();
      $courses = Course::published()->where('status','=',1)->orderBy('position','asc')->get();
        
      foreach($role as $comp){
          $compulsories[] = $comp->course_id;
      }
  
      foreach($courses as $key => $l){
          if(in_array($l->id,$compulsories)){
                  unset($courses[$key]);
              }
          }
  
  
          $data = self::paginate($courses);
      
          return $data;
    }
  
  
    public static function paginate($items, $perPage = 8, $page = null, $options = [])
      {
          $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
          $items = $items instanceof Collection ? $items : Collection::make($items);
          return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
      }
  
  
   private function checkLicenseStatus($course_id){
 
      $enrollment = UserLicense::where('course_id', $course_id)->where('user_id', Auth::user()->id)->first();
      if ($enrollment) {
          $enrollmentStatus = true;
      } else {
          $enrollmentStatus = false;
      }
  
      return $enrollmentStatus;
  
  }
  
  
  
  private function checkCompletionStatus($course_id){
  
    
      $completion = CourseCompletion::where('course_id', $course_id)->where('user_id', Auth::user()->id)->first();
  
      if($completion){
       
          if($completion->score >= 0.80){
  
              $completionStatus = 1;
              $status = "Completed";
  
          }else{
  
              $completionStatus = 2;
              $status = 'Completed but failed';
          }
      }else{
          $completionStatus = false;
          $status = 'In Progress';
      }
      
      return [
      'status' => $status,
      'completionStatus'=>$completionStatus
       ];
  
  }
}
