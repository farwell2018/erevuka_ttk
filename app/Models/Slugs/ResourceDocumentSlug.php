<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class ResourceDocumentSlug extends Model
{
    protected $table = "resource_document_slugs";
}
