<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class ResourceThemeSlug extends Model
{
    protected $table = "resource_theme_slugs";
}
