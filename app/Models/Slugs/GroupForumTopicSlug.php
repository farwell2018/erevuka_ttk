<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class GroupForumTopicSlug extends Model
{
    protected $table = "group_forum_topic_slugs";
}
