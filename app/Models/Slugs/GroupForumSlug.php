<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class GroupForumSlug extends Model
{
    protected $table = "group_forum_slugs";
}
