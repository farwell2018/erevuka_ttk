<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class ForumSlug extends Model
{
    protected $table = "forum_slugs";
}
