<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class CouponSlug extends Model
{
    protected $table = "coupon_slugs";
}
