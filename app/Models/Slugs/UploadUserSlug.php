<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class UploadUserSlug extends Model
{
    protected $table = "upload_user_slugs";
}
