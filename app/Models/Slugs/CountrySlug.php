<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class CountrySlug extends Model
{
    protected $table = "country_slugs";
}
