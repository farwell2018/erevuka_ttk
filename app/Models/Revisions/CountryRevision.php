<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class CountryRevision extends Revision
{
    protected $table = "country_revisions";
}
