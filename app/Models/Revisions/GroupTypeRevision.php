<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class GroupTypeRevision extends Revision
{
    protected $table = "group_type_revisions";
}
