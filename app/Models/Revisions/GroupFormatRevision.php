<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class GroupFormatRevision extends Revision
{
    protected $table = "group_format_revisions";
}
