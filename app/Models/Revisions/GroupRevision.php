<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class GroupRevision extends Revision
{
    protected $table = "group_revisions";
}
