<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class CouponRevision extends Revision
{
    protected $table = "coupon_revisions";
}
