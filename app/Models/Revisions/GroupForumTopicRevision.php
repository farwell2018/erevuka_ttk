<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class GroupForumTopicRevision extends Revision
{
    protected $table = "group_forum_topic_revisions";
}
