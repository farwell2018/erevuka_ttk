<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class UploadUserRevision extends Revision
{
    protected $table = "upload_user_revisions";
}
