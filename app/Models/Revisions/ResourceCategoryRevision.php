<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class ResourceCategoryRevision extends Revision
{
    protected $table = "resource_category_revisions";
}
