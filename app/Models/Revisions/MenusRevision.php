<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class MenusRevision extends Revision
{
    protected $table = "menus_revisions";
}
