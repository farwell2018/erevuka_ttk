<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class ForumTopicRevision extends Revision
{
    protected $table = "forum_topic_revisions";
}
