<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupActivity extends Model
{
    //

    use SoftDeletes;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function replies()
    {
        return $this->hasMany(GroupActivityReply::class)->latest();
    }

    public function reactions()
    {
        return $this->hasMany(GroupActivityReaction::class);
    }

    /**
     * Process files eg. Images & Gif and save them in publick directory
     * @param files $files
     */
    public static function saveFiles($files)
    {
        return collect($files)->map(function ($file) {
            $fileName = uniqid().$file->getClientOriginalName();
            $fileName = strtolower(str_replace(' ', '_', $fileName));
            $file->move('Groups/activity', $fileName);
            return $fileName;
        });
    }
}
