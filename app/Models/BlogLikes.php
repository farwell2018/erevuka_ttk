<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogLikes extends Model
{
    //

    protected $fillable = [
        'comment_id', 'user_id', 'like','dislike'
    ];
}
