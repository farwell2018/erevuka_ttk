<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\Resource;

class ResourceTranslation extends Model
{
    protected $baseModuleModel = Resource::class;
}
