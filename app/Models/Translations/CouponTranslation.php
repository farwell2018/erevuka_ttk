<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\Coupon;

class CouponTranslation extends Model
{
    protected $baseModuleModel = Coupon::class;
}
