<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\Group;

class GroupTranslation extends Model
{
    protected $baseModuleModel = Group::class;
}
