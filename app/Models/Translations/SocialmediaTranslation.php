<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\Socialmedia;

class SocialmediaTranslation extends Model
{
    protected $baseModuleModel = Socialmedia::class;
}
