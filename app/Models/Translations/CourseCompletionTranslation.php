<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\CourseCompletion;

class CourseCompletionTranslation extends Model
{
    protected $baseModuleModel = CourseCompletion::class;
}
