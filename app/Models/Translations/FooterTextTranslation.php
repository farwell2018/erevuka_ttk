<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\FooterText;

class FooterTextTranslation extends Model
{
    protected $baseModuleModel = FooterText::class;
}
