<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\ResourceCategory;

class ResourceCategoryTranslation extends Model
{
    protected $baseModuleModel = ResourceCategory::class;
}
