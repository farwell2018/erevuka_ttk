<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\ResourceTheme;

class ResourceThemeTranslation extends Model
{
    protected $baseModuleModel = ResourceTheme::class;
}
