<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupUser extends Model
{
    //
    protected $table = 'group_users';
    protected $fillable = [
        'groups_id', 'twill_users_id','joined','position'
    ]; 

    public function cop()
    {
        return $this->belongsTo(Group::class, 'groups_id','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'twill_users_id','id');
    }

}
