<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class Blog extends Model implements Sortable
{
    use HasBlocks, HasTranslation, HasSlug, HasMedias, HasFiles, HasRevisions, HasPosition;

    protected $fillable = [
        'published',
        'title',
        'description',
        'position',
    ];
    
    public $translatedAttributes = [
        'title',
        'description',
        'active',
    ];
    
    public $slugAttributes = [
        'title',
    ];
    
    public $filesParams = ['download_resource'];
    
    protected $with = ['translations', 'medias','files'];

    public $mediasParams = [
        'blog_image' => [
            'default' => [
                [
                    'name' => 'landscape',
                    
                ]
            ]
         ],

         'inner_image' => [
            'default' => [
                [
                    'name' => 'landscape',
                    
                ]
            ]
        ]
    ];


    public function comments(){

        return $this->hasMany(BlogComment::class)->orderBy('created_at','DESC');
    }
}
