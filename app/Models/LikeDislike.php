<?php

namespace  App\Models;

use Illuminate\Database\Eloquent\Model;

class LikeDislike extends Model
{
    //
    protected $fillable = [
        'comment_id', 'user_id', 'like','dislike'
    ];
    
}
