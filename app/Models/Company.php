<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class Company extends Model implements Sortable
{
    use HasTranslation, HasSlug, HasMedias, HasPosition, HasBlocks;

    protected $fillable = [
        'published',
        'name',
        'contact_first_name',
        'contact_last_name',
        'contact_email',
        'secondcontact_first_name',
        'secondcontact_last_name',
        'secondcontact_email',
        'position',
    ];
    
    public $translatedAttributes = [
        'title',
        'active',
    ];
    
    public $slugAttributes = [
        'name',
    ];
    
    public $mediasParams = [
        'cover' => [
            'desktop' => [
                [
                    'name' => 'desktop',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
    ];

//-------- Relationships

    public function domain(){
        return $this->hasMany(Domain::class)->orderBy('position');
    }
}
