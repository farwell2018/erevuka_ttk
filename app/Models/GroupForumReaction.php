<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupForumReaction extends Model
{
    //
    public const REACTION = 1;

    protected $guarded = [];
}
