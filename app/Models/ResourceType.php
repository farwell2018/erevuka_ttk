<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use App\Models\Translations\RoleTranslation;

class ResourceType extends Model implements Sortable
{
    use HasBlocks, HasTranslation, HasSlug, HasMedias, HasFiles, HasRevisions, HasPosition;

    protected $fillable = [
        'published',
        'title',
        'icon_name',
        'user_role',
        'description',
        'position',
    ];
    
    public $translatedAttributes = [
        'title',
        'description',
        'active',
    ];
    
    public $slugAttributes = [
        'title',
    ];
    
    public $mediasParams = [
        'cover' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
    ];


    public function getUserRoleAttribute($value)
{
    return collect(json_decode($value))->map(function($item) {
        return ['id' => $item];
    })->all();
}

public function setUserRoleAttribute($value)
{
    $this->attributes['user_role'] = collect($value)->filter()->values();
}

    public function groupResource(){
 
        return $this->hasMany(GroupResource::class,'resource_type_id','id')->latest();
    }

    public function roles() {
        return $this->hasMany(Role::class);
    }


   public function getContent(){

    $output ='';

    foreach($this->groupResource as $resource){
    
        $output='<tr>
        <td style="width: 611px;"><strong>'.$resource->title.'</td>
        <td style="width: 683.183px;">'.$resource->description.'</td>';
        if($resource->resource_type_id == 1){
        
            $output.=' <td style="width: 325.817px;">&nbsp;
            <p><a class="link" href="" target="_blank" rel="noopener"><i class="fa fa-file"></i> Read Article</a></p>
            </td>';

        }else{
             if(!empty($resource->audio_file)){
             
             $link =  asset("Groups/audio_file/".$resource->audio_file);

             }elseif(!empty($resource->video_file)){

            $link =  asset("Groups/video_file/".$resource->video_file);

             }elseif(!empty($resource->downloable_file)){

                $link = asset("Groups/downloable_file/".$resource->downloable_file);
             }


            $output.=' <td style="width: 325.817px;">&nbsp;
            <p><a class="link" href="'.$link.'" target="_blank" rel="noopener"><i class="fa fa-download"></i> Download Resource</a></p>
            </td>';

        }
       
        $output.='</tr>';

    }

   
    return $output;

   }
}
