<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $guarded = [];

    public static function createConversation($details = [])
    {
        $message = self::create($details);
        
        $receiver = User::where('id', $message->receiver_id)->first();
        $sender = User::where('id', $message->sender_id)->first();
        $message = [
           'type'=> Notification::MESSAGE,
           'subject'=> 'New Message',
           'model_id' => $message->id,
           'message' => 'You have recieved a new message from'.$sender->name,
          ];

          $receiver->notify(new \App\Notifications\NewMessageNotification($message));

        return $message;
    }
}
