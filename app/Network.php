<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Network extends Model
{

    protected $table='network';
    public function referrer()
    {
        return $this->belongsTo(User::class, 'parent_user_id');
    }
}
