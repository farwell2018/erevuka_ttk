<?php

namespace App\Edx;

use Illuminate\Database\Eloquent\Model;

class CourseModesCourseMode extends Model
{
    //
    //set connection for model
  protected $connection = 'edx_mysql';

  //Set table for model
  protected $table = 'course_modes_coursemode';

    //Disable timestamps
    public $timestamps = false;

}
