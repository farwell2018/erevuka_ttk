<?php

namespace App\Edx;

use Illuminate\Database\Eloquent\Model;

class CourseOverviewsCourseOverview extends Model
{
    //
    //set connection for model
  protected $connection = 'edx_mysql';

  //Set table for model
  protected $table = 'course_overviews_courseoverview';

    //Disable timestamps
    public $timestamps = false;

}
