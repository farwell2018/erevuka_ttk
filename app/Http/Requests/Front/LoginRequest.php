<?php

namespace App\Http\Requests\Front;

use A17\Twill\Http\Requests\Admin\Request;

class LoginRequest extends Request
{
    public function rulesForCreate()
    {
        return [
          'username' => ['required'],
          'login_password' => ['required'],
        ];
    }

    public function rulesForUpdate()
    {
        return [];
    }
}
