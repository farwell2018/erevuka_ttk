<?php

namespace App\Http\Requests\Front;

use A17\Twill\Http\Requests\Admin\Request;

class RegisterRequest extends Request
{
    public function rulesForCreate()
    {

        

        if(Request::input('role') == 5){
            $rules = [
                'first_name' => ['required','max:255'],
                'last_name' => ['required'],
                'role'=>['required'],
                'partners_registration_email' => ['string', 'email', 'max:255', 'unique:twill_users,email'],
                'partners_mobile_number'=>['required','unique:twill_users,phone'],
                'partners_mobile_number_country'=>['required','not_in:0'],
                'partners_password' => ['required', 'string', 'min:8','confirmed'],
                'partners_register_receive_information' => [],
                'partners_contact_consent' => [],
      
                 ];
        

        }else{
            $rules = [
                'first_name' => ['required','max:255'],
                 'last_name' => ['required'],
                 'role'=>['required'],
                 'mobile_number'=>['required','unique:twill_users,phone'],
                 'mobile_number_country'=>['required','not_in:0'],
                 'password' => ['required', 'string', 'min:8','confirmed'],
                 'register_receive_information' => [],
                 'contact_consent' => [],
       
              ];
           
        }

        return $rules;
    }

    public function rulesForUpdate()
    {
        return [];
    }
}
