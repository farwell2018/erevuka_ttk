<?php

namespace App\Http\Requests\Admin\SessionMeeting;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateSessionMeeting extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.session-meeting.edit', $this->sessionMeeting);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'meeting_id' => ['sometimes', 'string'],
            'host_email' => ['sometimes', 'string'],
            'topic' => ['sometimes', 'string'],
            'status' => ['sometimes', 'string'],
            'start_time' => ['sometimes', 'string'],
            'agenda' => ['sometimes', 'string'],
            'join_url' => ['sometimes', 'string'],
            'meeting_password' => ['sometimes', 'string'],
            'session_image' => ['nullable', 'string'],
            'session_video' => ['nullable', 'string'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
