<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Requests\Front\RegisterRequest;
use App\Http\Requests\Front\VerifyRequest;
use App\Http\Requests\Front\LoginRequest;
use Monarobase\CountryList\CountryListFacade as CountryList;
use App\Helpers\Front;
use App\Models\TwillUsersAuth;
use App\Models\User;
use Carbon\Carbon;
use AfricasTalking\SDK\AfricasTalking;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Ixudra\Curl\Facades\Curl;
use Redirect;
use App\Models\Role;

use App\Edx\EdxAuthUser;

use Auth;
use Toastr;
use Session;
use Mail;
use App;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    // use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');

       
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required'],
            'password' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function register(RegisterRequest $request)
    {
      
      
     $code = array();
       
     $data=$request->validated();

    dd($data);
    
		// $user = $this->repository->createForRegister($data);
    
    // $edxResponse =  $this->edxRegister($user);
    // if ($edxResponse !== true) {
    //   $user->delete();
    //   return Redirect::back()->with('register_error', 'An error occured while submitting your registration. Please refresh the page and try again.');

    // }
    // $validation = $this->validationToken($user, $length = 6);
    //    $a = Front::getCountriesList();
    //      foreach ($a as $key => $value){
    //          if($key === $user->phone_locale){
    //              $code[] = $value['code'];
    //          }
    //      }

    //     $phone_number = $code[0].''.$user->phone;
         
    //     $username   = "sandbox";
    //     $apiKey     = "19fda8111de58daeb19c01107d3c0aeb8b22c47b5501004ad81f263bb3741535";

    //      $AT       = new AfricasTalking($username, $apiKey);
    //        // Get one of the services
    //       $sms      = $AT->sms();
    //       $text = 'Hello , Use this code to verify your account before resetting your pin '. $validation->token.' ';

    //       // Use the service
    //       $result   = $sms->send([
    //           'to'      => $phone_number,
    //           'message' => $text,
    //           'from' => 'Sandbox'
    //       ]);

    //       Mail::send('emails.token',['name' => $user->first_name.' '.$user->last_name,  'code' => $validation->token ],
    //       function ($message) use ($user) {
    //         $message->from('support@farwell-consultants.com', 'Welcome to The Challenging Patriarchy Program');
    //         $message->to($user->email, $user->first_name);
    //         $message->subject('The Challenging Patriarchy Program Account Verification');
    //       });


    //       $userData = [
    //         'otp_requested'    => true,
		// 	'otp_for_user' => $user->email,
		// 	'user_id' => $user->id,
		// ];
		// //dd($user, $user->profile, $token, $notification);
		// session($userData);

		// return redirect('/user/verify');
    }
   
    public function showRegistrationForm(){
      $roles = Role::published()->orderBy('position','ASC')->get();
        return view('site.auth.register',['roles'=>$roles]);
      }


      protected function getRepository()
      {
          return App::make("App\\Repositories\\UserRepository");
      }

      private function edxRegister($user)
      {

      

        //Validate user
        if ($user === null) {
          return;
        }
        //Package data to be sent
    if (filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
             $email = $user->email;
           }else{
            $email = $user->email.'@local.com';
      }
        $data = [
          'email' => $email,
          'name' => $user->first_name . ' ' . $user->last_name,
          'username' => $user->username,
          'honor_code' => 'true',
          'password' => request()->get('password'),
          'country'=> 'KE',
          'terms_of_service'=> 'true',
        ];
    
        $headers = array(
          'Content-Type'=>'application/x-www-form-urlencoded',
          'cache-control' => 'no-cache',
          'Referer'=>env('APP_URL').'/register',
        );
    
        $client = new \GuzzleHttp\Client();
    
        try {
    
          $response = $client->request('POST', env('LMS_REGISTRATION_URL'), [
            'form_params' => $data,
            'headers'=>$headers,
          ]);
    
          return true;
    
        }catch (\GuzzleHttp\Exception\ClientException $e) {
    
          $responseJson = $e->getResponse();
          $response = json_decode($responseJson->getBody()->getContents(),true);
          //Error, delete user
          $user->delete();
    
          //Delete password resets
          //PasswordReset::where('email', '=', $user->email)->delete();
          $errors = [];
          foreach ($response as $key => $error) {
            //Return error
            $errors[] = $error;
          }
          //echo "CATCH 1";
          //dd($errors);
          return $errors[0];
    
    
        }catch ( \Exception $e){
    
          //Error, delete user
          $user->delete();
          //Delete password resets
          //PasswordReset::where('email', '=', $user->email)->delete();
          //echo "CATCH 2";
          //echo $e->getMessage();
          //die;
          return $e->getMessage();
    
        }
      }

protected function generateRandomString($length)
{
  $characters = '0123456789';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[ rand(0, $charactersLength - 1) ];
  }

  return $randomString;
}
}
