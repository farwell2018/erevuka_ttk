<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\Front\LoginRequest;
use App;
use App\Models\Daycare;
use Illuminate\Support\Facades\Auth;
use App\Models\TwillUsersAuth;
use App\Models\User;
use App\Network;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Toastr;
use Config;
use Illuminate\Support\Facades\Session;
use Input;
use View;
use DB;
use Carbon;
use Log;
use Hash;
use Illuminate\Auth\AuthManager;

use Ixudra\Curl\Facades\Curl;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
      AuthManager $authManager
    )
    {
      $this->authManager = $authManager;

        $this->middleware('guest')->except('logout');
    }


    public function showLoginForm(){

        return view('site.auth.login');
    }

      public function username()
      {
        return 'username';
      }


      public function checkAuth(Request $request)
  {
    $username = $request->input('username');
    $password = $request->input('password');

    $user = User::where('username', $username)->firstOrFail();

    $hasher = app('hash');
    if ($hasher->check($password, $user->password)) {
      return response('Valid credentials', 200);
    } else {
      return response('Invalid username or password', 400);
    }
  }
    /**
	 * @param Login $request
	 */
    public function login(LoginRequest $request){
        
        $data = $request->all();

      
         // Validation Process
        if(strpos($request->input('username'), '@') !== false && strpos($request->input('username'), '.') !== false){
            $validator = Validator::make($data, [
            'username'    => 'required|email',
            'login_password' => 'required|string',
            ]);
            //verify login to microsite;
          $credentials = ['email' => $request->input('username'), 'password' => $request->input('login_password')];
      
            //verify email activation to microsite;
          $user = User::where('email', $request->input('username'))->first();
      
          }else{
            $validator = Validator::make($data, [
            'username'    => 'required',
            'login_password' => 'required|string',
            ]);
            
             $string = substr($request->input('username'), 0, 1);

             if($string === "0"){

              $phone = substr($request->input('username'),1);
             }
             else{

              $phone = $request->input('username');
             }

             $user = User::where('phone', $phone)->first();
            //verify login to microsite;
             $credentials = ['phone' => $phone, 'password' => $request->input('login_password')];
            //verify email activation to microsite;
        
          }
      
          if ($validator->fails()) {
            $errors = $validator->errors();
      
            return Redirect::back()->withErrors($errors);
          }

          
        
          if (Auth::attempt($credentials)) {
            $credentials['email'] = $user->email;
        
            if ($user->published != 1 && $user->is_activated != 1) {
              Auth::logout();
            return Redirect::back()->with('login_error', 'Kindly verify your email first before proceeding');
            }

            $userz = Auth::user();
            // dd($userz);

         

            // Check for user's phone number in network table
        $networkUser = Daycare::where('daycare_id', $userz->daycare_id)->first();


           
     

        if ($networkUser) {
          // Check if parent user is active
          $parentUser = User::where('phone', $networkUser->contact_number)->where('published', true)->first();
          
          if ($parentUser) {
            // Set user subscription to true
            $user->subscription = true;
          } else {
            // Set user subscription to false
            $user->subscription = false;
          }
        } else {
          // Set user subscription to false
          $user->subscription = false;
        }
        
        $user->save(); 

        if( $user->daycare_id  != null && $parentUser)
        {
          $user->subscription = true;
             
        }else if($user->daycare_id) {
          // Set user subscription to false
          $user->subscription = true;
          
      }else{
        $user->subscription = false;
      }
      $user->save();

          if($user->role === "SUPERADMIN" || $user->role === "ADMIN"){
              $admin = $this->authManager->guard('twill_users')->loginUsingId($user->id);
               //login to edx;
            $edxStatus = $this->edxLogin($request);
            
            
          if(!$edxStatus){
            Auth::logout();
            return Redirect::back()->with('login_error', 'The username or password you entered did not match our records. Please confirm you have registered');
          }
          
            }else{
             
          //    //login to edx;
             $edxStatus = $this->edxLogin($request);
        
             
           if(!$edxStatus){
             Auth::logout();
             return Redirect::back()->with('login_error', 'The username or password you entered did not match our records. Please confirm you have registered');
           }

            }
            
            $user->last_login_at = Carbon\Carbon::now();
            $user->save();
        
            return redirect('/');
        
            }else{
              $link = '<a href="/register">Here</a>';
              return Redirect::back()->with('login_error', 'The username or password you entered did not match our records. Please confirm you have registered or Register '.$link);
            }


    }

    //Function to login user into edx
    private function edxLogin($request)
    {
      //dd("got it");
  
      $configLms = config()->get("settings.lms.live");
      $configApp = config()->get("settings.app");
  
      $webcookies = $request->cookie();
  
    
      $string = substr($request->input('username'), 0, 1);

      if($string === "0"){

       $phone = substr($request->input('username'),1);
      }
      else{

       $phone = $request->input('username');
      }


   $user_name = User::where('phone', $phone)->first();

   if($user_name != null){
           $user = $user_name;
   }else{
           $user = User::where('email', $phone)->first();
   }
        $email = $user->email;
        $username =  $user->email;
        $password = $request->input('login_password');
        //Get CSRF Token
  
        $client = new \GuzzleHttp\Client(['verify' => $configApp['VERIFY_SSL']]);
        $response = $client->request('GET', $configLms['LMS_LOGIN_URL']);
        $csrfToken = null;
        foreach ($response->getHeader('Set-Cookie') as $key => $cookie) {
            if (strpos($cookie, 'csrftoken') === FALSE) {
                continue;
            }
            $csrfToken = explode('=', explode(';', $cookie)[0])[1];
            break;
        }
  
        if (!$csrfToken) {
            //Error, reactivate reset
         return Redirect::back()->with('login_message', 'The email or password you entered did not match our records. Please confirm you have registered');
  
        }
  
        $data = [
            'email' => $email,
            'password' => $password,
        ];
        $headers = [
            'Content-Type' => ' application/x-www-form-urlencoded ; charset=UTF-8',
            'Accept' => ' text/html,application/json',
            'X-CSRFToken' => $csrfToken,
            'Cookie' => ' csrftoken=' . $csrfToken,
            'Origin' => $configLms['LMS_BASE'],
            'Referer' => $configLms['LMS_BASE'] . '/login',
            'X-Requested-With' => ' XMLHttpRequest',
        ];
        $client = new \GuzzleHttp\Client(['verify' => $configApp['VERIFY_SSL']]);
        $cookieJar = \GuzzleHttp\Cookie\CookieJar::fromArray([
            'csrftoken' => $csrfToken
        ], env('LMS_DOMAIN'));
        $response = $client->request('POST', $configLms['LMS_LOGIN_URL'], [
            'form_params' => $data,
            'headers' => $headers,
            'cookies' => $cookieJar
        ]);
        //set cookies
  
        if (!$response->hasHeader('Set-Cookie')) {
         return Redirect::back()->with('login_message', 'The email or password you entered did not match our records. Please confirm you have registered');
        }
  
  
        $loggedInCookies = $response->getHeader('Set-Cookie');
  
        $setCookies = [];
        foreach ($loggedInCookies as $userCookie) {
            //format cookies
            $cookieDetails = (explode(';', $userCookie));
            $ourCookie = [];
            foreach ($cookieDetails as $cookieDetail) {
                $key = strtolower(trim(explode('=', $cookieDetail)[0]));
                $value = isset(explode('=', $cookieDetail)[1]) ? trim(explode('=', $cookieDetail)[1]) : 1;
                if (
                    in_array(
                        strtolower($key),
                        ['__cfduid', 'csrftoken', 'edxloggedin', 'sessionid', 'openedx-language-preference', 'edx-user-info']
                    )
                ) {
                    $ourCookie['name'] = $key;
                    $ourCookie['value'] = $value;
                } else {
                    $ourCookie[$key] = $value;
                }
            }
            if ($ourCookie['name'] != 'edx-user-info' && !isset($ourCookie['domain'])) {
                if ($ourCookie['name'] == 'csrftoken') {
                    $ourCookie['domain'] = '';
                } else {
                    $ourCookie['domain'] = '.' . $configApp['APP_DOMAIN'];
                }
                //Set the cookie
                $setCookies[] = ['name' => $ourCookie['name'], 'value' => $ourCookie['value'], 'domain' => $ourCookie['domain']];
            }
        }
  
  
        $data = [
            'grant_type' => 'password',
            'client_id' => $configLms['EDX_KEY'],
            'client_secret' => $configLms['EDX_SECRET'],
            'username' => auth()->user()->username,
            'password' => $password,
            //'token_type'=>'jwt',
        ];
        $tokenUrl = $configLms['LMS_BASE']. '/oauth2/access_token/';
        //Get authorization token
        $accessResponse = Curl::to($tokenUrl)
            ->withData($data)
            ->withResponseHeaders()
            ->returnResponseObject()
            ->post();
            //dd($accessResponse);
  
        if ($accessResponse->status !== 200) {
         return Redirect::back()->with('login_message', 'Username does not exist or invalid credentials given');
        }
        //Set access token
        $accessToken = json_decode($accessResponse->content, true);
  
        $setCookies[] = ['name' => 'edinstancexid', 'value' => $accessToken['access_token'], 'expiry' => $accessToken['expires_in']];
        foreach ($setCookies as $cookie) {
            $cookie['expiry'] = 0;
            // if (!isset($cookie['expiry'])) {
            // }
  
            if (!isset($cookie['domain'])) {
                $cookie['domain'] = '.' . $configApp['APP_DOMAIN'];
            }
            setrawcookie($cookie['name'], $cookie['value'], $cookie['expiry'], '/', $cookie['domain']);
        }
        //dd($setCookies);
        return true;
    }


  /*
  * Function to log users out and redirect to login page
  */
  public function logout(Request $request)
  {
    if($this->edxLogout()){
      auth()->logout();
      return redirect()->route('home');
    }
    }

  private function edxLogout(){
    //logout

    $cookies = ['csrftoken','edxloggedin','edx-user-info','openedx-language-preference','edinstancexid','sessionid'];
    foreach ($cookies as $key => $cookie) {
      if (isset($_COOKIE[$cookie])) {
        setrawcookie($cookie, '', time() - 3600, '/', env('APP_DOMAIN') );
        unset($_COOKIE[$cookie]);
      }
    }
    return true;
  }
}
