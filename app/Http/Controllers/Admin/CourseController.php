<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class CourseController extends ModuleController
{
    protected $moduleName = 'courses';
    
}
