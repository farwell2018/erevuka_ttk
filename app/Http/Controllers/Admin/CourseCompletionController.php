<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Models\LearningStatus;
use App\Models\Course;
use Illuminate\Http\Request;

class CourseCompletionController extends ModuleController
{ 
    /**
    * @var Config
    */
   protected $config;

   /**
    * @var AuthFactory
    */
   protected $authFactory;
  /**
    * Indicates if this module is edited through a parent module.
    *
    * @var bool
    */
   protected $submodule = false;

   /**
    * @var int|null
    */
   protected $submoduleParentId = null;


   protected $perPage = 10000;


   protected $moduleName = 'courseCompletions';

   protected $titleColumnKey = 'username';

   protected $indexOptions = [
       'create' =>false,
       'edit' =>false,
       'permalink' => false,
       'publish' =>false,
   ];



   protected $filters = [
    'course_id' => 'course_id',
   'status_type'=> 'status_type',
   'enrollment_date'=> 'enrollment_date',
   'completion_date'=> 'completion_date',
   ];
  
   protected $indexColumns = [

       'username' => [ // field column
           'title' => 'User Name',
           'field' => 'username',
       ],

       'email' => [ // field column
           'title' => 'Email',
           'field' => 'email',
       ],

        'course_id' => [ // relation column
           'title' => 'Course',
           'sort' => true,
           'relationship' => 'courses',
           'field' => 'name'
       ],

       'completion_status' => [ // relation column
           'title' => 'Status',
           'field' => 'completion_status'
       ],
       'enrollment_date' => [ // field column
           'title' => 'Enrollment Date',
           'field' => 'enrollment_date',
       ],
       'updated_at' => [ // field column
           'title' => 'Completion Date',
           'field' => 'updated_at',
       ],
   ];




   protected function indexData($request)
   {

       return [
           'course_idList' =>  Course::where('status',1)->get(),
           'status_typeList' =>  LearningStatus::all(),
           'completion_dateList' => '',
           'enrollment_dateList' => '',
           
       ];
   }


   public function dashboard(Request $request){
  
    $launchYear = "2022";
    $eyear = date('Y');
    $cyear = date('Y');


    $emonth = date('m');
    $cmonth = date('m');

  /** Active vs Inactive accounts graph */
   $completedRegistrationoption = $this->repository->getCompleteRegistrations();

   $pendingRegistrationoption = $this->repository->getPendingRegistrations();
 /** End Accounts graph */ 

   /** Top 5 courses Enrollments */
   $yearlyEnrollmentGraph = [
    'chart_title' => 'Top 5 courses enrolled into to date',
    'enrollment_data'=>$this->repository->getYearlyTopEnrollments($eyear,$launchYear ),
    ];

  /** End Top 5 courses */

    /** Top 5 courses Completions */
    $yearlyCompletionGraph = [
        'chart_title' => 'Top 5 courses completed to date',
        'completion_data' => $this->repository->getYearlyTopCompletions($cyear,$launchYear),
    ];

    /** End Top 5 courses */




    /** Top 5 Quick Tips Enrollments */
   $yearlyQuickTipEnrollmentGraph = [
    'chart_title' => 'Top 5 quick tip enrolled into to date',
    'enrollment_data'=>$this->repository->getYearlyQuickTipEnrollments($eyear,$launchYear ),
    ];

  /** End Top 5 courses */

    /** Top 5 Quick Tips Completions */
    $yearlyQuickTipCompletionGraph = [
        'chart_title' => 'Top 5 quick tip completed to date',
        'completion_data' => $this->repository->getYearlyQuickTipCompletions($cyear,$launchYear),
    ];

    /** End Top 5 courses */


    /** Top 10 users with Completions */
 
     $userCompletions = $this->repository->getTopUserCompletions();

    /**End top 10 users with Completions */


    return view('admin.courseCompletions.dashboard',compact('completedRegistrationoption', 'pendingRegistrationoption', 'yearlyEnrollmentGraph','yearlyCompletionGraph', 'userCompletions','yearlyQuickTipEnrollmentGraph','yearlyQuickTipCompletionGraph'));
}



     /**Yearly Filter Graph Enrollments Reports */

     public function yearlyEResults(Request $request){

        $nyear = $request->eyear;

        return $this->repository->getTopYearlyEnrollments($nyear);

    }

     /** End Yearly Filter Graph Enrollments Report */


     /**Yearly Filter Graph Completions Reports */

    public function yearlyCRresults(Request $request){
        $nyear = $request->cyear;

        return $this->repository->getTopYearlyCompletions($nyear);

    }

     /** End Yearly Filter Graph Report */





      /**Yearly Filter Graph Enrollments Reports */

      public function yearlyQuickTipEResults(Request $request){

        $nyear = $request->eyear;

        return $this->repository->getTopYearlyQuickTipEnrollments($nyear);

    }

     /** End Yearly Filter Graph Enrollments Report */


     /**Yearly Filter Graph Completions Reports */

    public function yearlyQuickTipCRresults(Request $request){
        $nyear = $request->cyear;

        return $this->repository->getTopYearlyQuickTipCompletions($nyear);

    }

     /** End Yearly Filter Graph Report */


    
}
