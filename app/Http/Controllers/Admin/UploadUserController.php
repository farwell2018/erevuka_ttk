<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use Illuminate\Http\Request;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;
use App;
use Session;
use Storage;
use Illuminate\Support\Facades\Cookie;


class UploadUserController extends ModuleController
{
    protected $moduleName = 'uploadUsers';


    public function usersUpload(){

        return view('admin.uploadUsers.upload-form');
    }



    public function uploadStore(Request $request){
      
         Excel::import(new UsersImport, request()->file('file'));

          $name= Session::get('excel_download');
           
          $file_path = public_path('uploads/exports/'.$name);
          
          Session::flash('status', 'Import Successful');
          return response()->download($file_path);
         
        //  Session::flash('status', 'Import Successful');

        // return redirect()->back();

    }


    
    public function downloadExcel() 
      {
        Storage::download('2022-01-04 09:09:43_UsersExport.xlsx');
    //    $content = Storage::get('2022-01-04 09:09:43_UsersExport.xlsx');
    //    dd($content);
      }
    
}
