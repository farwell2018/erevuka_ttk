<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;

class RateController extends BaseModuleController
{
    protected $moduleName = 'rates';

    protected $indexOptions = [
        'permalink' => false,
    ];
}
