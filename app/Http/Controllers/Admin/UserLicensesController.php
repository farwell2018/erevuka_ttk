<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserLicense\BulkDestroyUserLicense;
use App\Http\Requests\Admin\UserLicense\DestroyUserLicense;
use App\Http\Requests\Admin\UserLicense\IndexUserLicense;
use App\Http\Requests\Admin\UserLicense\StoreUserLicense;
use App\Http\Requests\Admin\UserLicense\UpdateUserLicense;
use App\Models\UserLicense;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class UserLicensesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexUserLicense $request
     * @return array|Factory|View
     */
    public function index(IndexUserLicense $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(UserLicense::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'course_id', 'user_id', 'company_license_id', 'enrolled_at', 'expired_at'],

            // set columns to searchIn
            ['id', 'course_id', 'user_id', 'company_license_id'],

            function ($query) use ($request) {
                $query->with(['user','course']);
    
               
            }
        
        );


        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.user-license.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        

        return view('admin.user-license.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUserLicense $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreUserLicense $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the UserLicense
        $userLicense = UserLicense::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/user-licenses'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/user-licenses');
    }

    /**
     * Display the specified resource.
     *
     * @param UserLicense $userLicense
     * @throws AuthorizationException
     * @return void
     */
    public function show(UserLicense $userLicense)
    {
        $this->authorize('admin.user-license.show', $userLicense);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param UserLicense $userLicense
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(UserLicense $userLicense)
    {
        


        return view('admin.user-license.edit', [
            'userLicense' => $userLicense,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserLicense $request
     * @param UserLicense $userLicense
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateUserLicense $request, UserLicense $userLicense)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values UserLicense
        $userLicense->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/user-licenses'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/user-licenses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyUserLicense $request
     * @param UserLicense $userLicense
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyUserLicense $request, UserLicense $userLicense)
    {
        $userLicense->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyUserLicense $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyUserLicense $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    UserLicense::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
