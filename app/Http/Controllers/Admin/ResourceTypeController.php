<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Models\Role;
use App\Repositories\ResourceTypeRepository;
use App\Repositories\RoleRepository;

class ResourceTypeController extends ModuleController
{
    protected $moduleName = 'resourceTypes';

    protected $indexColumns = [
        
        'title' => [ // field column
            'title' => 'Title',
            'field' => 'title',
        ],
        
        'icon_name' => [ // relation column
            'title' => 'Icon Name',
            'field' => 'icon_name'
        ],
        'user_role' => [
            'title'=>'User Role',
            'field'=> 'user_role'
        ],
        
    ];

    protected function formData($request)
{
    return [
        'user_roles' => app()->make(RoleRepository::class)->listAll('title')
    ];
}
    
}
