<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SessionMeeting\BulkDestroySessionMeeting;
use App\Http\Requests\Admin\SessionMeeting\DestroySessionMeeting;
use App\Http\Requests\Admin\SessionMeeting\IndexSessionMeeting;
use App\Http\Requests\Admin\SessionMeeting\StoreSessionMeeting;
use App\Http\Requests\Admin\SessionMeeting\UpdateSessionMeeting;
use App\Models\SessionMeeting;
use App\Traits\ZoomMeetingTrait;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class SessionMeetingsController extends Controller
{
    use ZoomMeetingTrait;

    const MEETING_TYPE_INSTANT = 1;
    const MEETING_TYPE_SCHEDULE = 2;
    const MEETING_TYPE_RECURRING = 3;
    const MEETING_TYPE_FIXED_RECURRING_FIXED = 8;

    /**
     * Display a listing of the resource.
     *
     * @param IndexSessionMeeting $request
     * @return array|Factory|View
     */
    public function index(IndexSessionMeeting $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(SessionMeeting::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'meeting_id', 'host_email', 'agenda', 'start_time', 'topic', 'session_image', 'session_video','join_url'],

            // set columns to searchIn
            ['id', 'topic',  'start_time', 'agenda', ]
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.session-meeting.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
      

        return view('admin.session-meeting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreSessionMeeting $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreSessionMeeting $request)
    {
        // Sanitize input
        // $sanitized = $request->getSanitized();
        dd($request->file('session_image'));

        $create = $this->create($request->all());
        $start = $this->toZoomTimeFormat($request->start_time);
        
    

        $session = new SessionMeeting();
        $session->meeting_id = $create['data']['id'];
        $session->host_email = $create['data']['host_email'];
        $session->topic = $create['data']['topic'];
        $session->status = $create['data']['status'];
        $session->start_time =  $start;
        $session->agenda = $create['data']['agenda'];
        $session->join_url = $create['data']['join_url'];
        $session->meeting_password = $create['data']['password'];

        if (!is_null($request->file('session_image')))
        {
          $session->session_image = $this->Fileupload($request,'session_image','Sessions');
        }

        $session->save();
        
        foreach($users as $user){

        Mail::send('emails.meeting',['url' => $session->join_url, 'topic' => $session->topic, 'agenda' => $session->agenda, 'start_time' => $session->start_time, 'host' => $session->host_email],
        function ($message) use ($user) {
          $message->from('support@farwell-consultants.com', 'Meeting Invite');
          $message->to($user, $user);
          $message->subject('The Erevuka eLearning Zoom Meeting');
        });

        }




        // // Store the SessionMeeting
        // $sessionMeeting = SessionMeeting::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/session-meetings'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/session-meetings');
    }

    /**
     * Display the specified resource.
     *
     * @param SessionMeeting $sessionMeeting
     * @throws AuthorizationException
     * @return void
     */
    public function show(SessionMeeting $sessionMeeting)
    {
        $this->authorize('admin.session-meeting.show', $sessionMeeting);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param SessionMeeting $sessionMeeting
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(SessionMeeting $sessionMeeting)
    {
        


        return view('admin.session-meeting.edit', [
            'sessionMeeting' => $sessionMeeting,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSessionMeeting $request
     * @param SessionMeeting $sessionMeeting
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateSessionMeeting $request, SessionMeeting $sessionMeeting)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values SessionMeeting
        $sessionMeeting->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/session-meetings'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/session-meetings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroySessionMeeting $request
     * @param SessionMeeting $sessionMeeting
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroySessionMeeting $request, SessionMeeting $sessionMeeting)
    {
        $sessionMeeting->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroySessionMeeting $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroySessionMeeting $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    SessionMeeting::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }


    public function Fileupload ($request,$file = 'file', $folder)
    {
            $upload = null;
            //create folder if none
            if (!File::exists($folder)) {
                    File::makeDirectory($folder);
            }
            //chec fo file
            if (!is_null($request->file($file))) {
                    $extension = $request->file($file)->getClientOriginalExtension();
                    $fileName = rand(11111, 99999).uniqid().'.' . $extension;
                    $upload = $request->file($file)->move($folder, $fileName);
                    if ($upload) {
                            $upload = $fileName;
                    } else {
                            $upload = null;
                    }
            }
            return $upload;
    }
}
