<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Course\BulkDestroyCourse;
use App\Http\Requests\Admin\Course\DestroyCourse;
use App\Http\Requests\Admin\Course\IndexCourse;
use App\Http\Requests\Admin\Course\StoreCourse;
use App\Http\Requests\Admin\Course\UpdateCourse;
use App\Models\Course;
use App\Edx\CourseOverviewsCourseOverview;
use App\Edx\CourseModesCourseMode;
use App\Models\SessionMeeting;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Carbon\Carbon;

class CoursesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexCourse $request
     * @return array|Factory|View
     */
    public function index(IndexCourse $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Course::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'name',  'start', 'end', 'enrol_start', 'enrol_end',  'status'],

            // set columns to searchIn
            ['id', 'name', 'effort']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.course.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
       

        $edx_courses = $this->edxgetCourses();
    
       
        foreach ($edx_courses as $key => $edx_course) {
            //Process data
            if (Course::where('course_id', $edx_course['id'])->count()) {
                //Update
                $slug = str_replace(' ', '-', $edx_course['name']);
                $slug2 = str_replace('-–-', '-', $slug);

                $video = str_replace('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/', $edx_course['course_video_uri']);

                //Update
              

                   DB::table('courses')
                 ->where('course_id', $edx_course['id'])
                ->update([
                  'course_id' => $edx_course['id'],
                  'name' =>  $edx_course['name'],
                  'short_description' => $edx_course['short_description'],
                  'overview' => $edx_course['overview'],
                   'effort' => $edx_course['effort'],
                   'start' => $edx_course['start'],
                   'end' => $edx_course['end'],
                   'enrol_start' => $edx_course['enrollment_start'],
                   'enrol_end' => $edx_course['enrollment_end'],
                   'course_image_uri'=> $edx_course['course_image_uri'],
                   'course_video_uri' => $edx_course['course_video_uri'],
                   'slug'=> $slug2,
                    'course_video' =>$video

                ]);

            } else {
                //Create
                
                $course = Course::create($edx_course);

                $course['course_id'] = $edx_course['id'];
                $course->save();
            }
            // $course->setOverview();
            // $course->setEnabled();
        }


      
        return redirect('admin/courses')->with(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);





        // return view('admin.course.create',['sessions' => SessionMeeting::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCourse $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreCourse $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();



        // Store the Course
        $course = Course::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/courses'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/courses');
    }

    /**
     * Display the specified resource.
     *
     * @param Course $course
     * @throws AuthorizationException
     * @return void
     */
    public function show(Course $course)
    {
        $this->authorize('admin.course.show', $course);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Course $course
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Course $course)
    {
    
  
        return view('admin.course.edit', [
            'course' => $course,
           
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCourse $request
     * @param Course $course
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateCourse $request, Course $course)
    {

        // Sanitize input
        $sanitized = $request->getSanitized();
     

        if($sanitized['mobile_available'] = true){

         $mobile = CourseOverviewsCourseOverview::where('id',$course->course_id)->first();
         $mobile->mobile_available = 1;
         $mobile->save();

        }else{
         
            $mobile = CourseOverviewsCourseOverview::where('id',$course->course_id)->first();
            $mobile->mobile_available = 0;
            $mobile->save();

        }

        $mode = CourseModesCourseMode::where('course_id',$course->course_id)->first();
        if(empty($mode)){
            $modes = new CourseModesCourseMode();
           $modes->course_id = $course->course_id;
           $modes->mode_slug = 'honor';
           $modes->mode_display_name = $course->name;
           $modes->min_price = 0;
           $modes->currency = 'usd';
           $modes->expiration_datetime_is_explicit = 0;
           $modes->suggested_prices = 0;
           $modes->save();
        }

        // Update changed values Course
        $course->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/courses'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/courses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyCourse $request
     * @param Course $course
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyCourse $request, Course $course)
    {
        $course->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyCourse $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyCourse $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Course::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }




      //Function to get courses from edx
  private function edxgetCourses()
  {

    $configLms = config()->get("settings.lms.live");

        $client = new \GuzzleHttp\Client();
        try {
            $response = $client->request('GET', $configLms['LMS_BASE'] . '/api/courses/v1/courses/?page=1&page_size=500');
   
            $courses =  json_decode($response->getBody()->getContents())->results;
            
            foreach ($courses as $key => $value) {
                $course = (array)$value;
                $courses[$key] = (array)$courses[$key];
                $course['overview'] = $this->getOverview($course);
                //Remove unwanted fields
                $course['course_video_uri'] = $course['media']->course_video->uri;
                $course['course_image_uri'] = $configLms['LMS_BASE'] . $course['media']->course_image->uri;
                unset($course['media']);
                unset($course['course_id']);
                //Format datetime
                $vowels = array("T", "Z");
                $course['start'] =  date('Y-m-d H:m:i', strtotime(str_replace($vowels, " ", $course['start'])));
                $course['end'] = date('Y-m-d H:m:i', strtotime(str_replace($vowels, " ",$course['end'])));
                $course['enrollment_start'] = date('Y-m-d H:m:i', strtotime($course['enrollment_start']));
                $course['enrollment_end'] = date('Y-m-d H:m:i', strtotime($course['enrollment_end']));
                //Format time
                $exploded_effort = explode(":", $course['effort']);
                switch (count($exploded_effort)) {
                    case '3':
                        $course['effort'] = Carbon::createFromTime($exploded_effort[0], $exploded_effort[1], $exploded_effort[2])->toTimeString();
                        break;
                    case '2':
                        $course['effort'] = Carbon::createFromTime(0, $exploded_effort[0], $exploded_effort[1])->toTimeString();
                        break;
                }
                $courses[$key] = $course;
                
            }
            return $courses;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = $responseJson->getBody()->getContents();
            //dd($response);
        }
  }


      private function getOverview($course)
    {
        
    $configLms = config()->get("settings.lms.live");
    
        $client = new \GuzzleHttp\Client();
        try {
            //Get course description
            $request = $client->request('GET', $configLms['LMS_BASE'] . '/api/courses/v1/courses/' . $course['id']);
            $response = json_decode($request->getBody()->getContents());
            return $response->overview;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = $responseJson->getBody()->getContents();
            return Toastr::error("Error enrolling into course");
            return false;
        }
    }
}
