<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;
use App\DaycarePromocode;
use App\Models\Coupon;
use App\Models\Daycare;
use App\Models\GroupPurchase;
use App\Models\Mpesa;
use App\Models\Translations\CouponTranslation;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\IOFactory;

class CouponController extends BaseModuleController
{
    protected $moduleName = 'coupons';

    protected $indexWith = ['users'];


    protected $indexColumns  = [
        'title' => [ // field column
            'title' => 'Title',
            'field' => 'title',
        ],

        'user_id' => [ // relation column
            'title' => 'Related User',
            'sort' => true,
            'relationship' => 'users',
            'field' => 'name'
        ],

        'code' => [
            'title' => 'Promotion Code',
            'field' => 'code'
        ],
        'created_at' => [
            'title' => 'Created on',
            'field' => 'created_at',
            'present' => true,
        ],

        'total_use' => [
            'title' => 'Promotion Code Total Use',
            'field' => 'total_use',
            'present' => true,
        ],
    ];



    public function daycares(Request $request)
    {
        $query = Daycare::query()->orderBy('joining_date','DESC');

        if ($request->has('search')) {
            $searchTerm = $request->get('search');
            $query->where(function ($q) use ($searchTerm) {
                $q->where('name', 'like', '%'.$searchTerm.'%')
                    ->orWhere('location', 'like', '%'.$searchTerm.'%')
                    ->orWhere('contact_name', 'like', '%'.$searchTerm.'%')
                    ->orWhere('daycare_id', 'like', '%'.$searchTerm.'%')
                    ->orWhere('cohort', 'like', '%'.$searchTerm.'%');
            });
        }
    
        $daycares = $query->paginate(20);
        return view('admin.daycares.index', compact('daycares'));
    }

    public function daycareCreate()
    {
        return view('admin.daycares.create');

    }


    public function daycareStore(Request $request)
    {
        $request->validate([
            "daycare_id" => 'required|unique:daycares',
            "name" => 'required',
            "user_id" => 'required',
            "phone" => 'required',
            "location" => 'required',
            "cohort" =>'required',
            "joining_date" => 'required',

         


        ]);

        $name = $request->input('user_id');
        $user = User::where('name',$name)->first();
        $daycare = new Daycare();
        $daycare->daycare_id = $request->daycare_id;
        $daycare->name = $request->name;
        $daycare->contact_name = $name;
        $daycare->contact_number = $request->contact_number;
        $daycare->location = $request->location;
        $daycare->cohort = $request->cohort;
        $daycare->joining_date = $request->joining_date;

       

       $daycare->save();

        return redirect()->route('admin.daycare.index')
        ->with('success', 'Daycare created successfully.');

    }

     public function daycareShow(Request $request, $id)
     {
        $daycare = Daycare::findOrFail($id);

        return view('admin.daycares.show', compact('daycare'));
                        

     }

     public function daycareUpdate(Request $request, $id)
     {
        $request->validate([
            "daycare_id" => 'required',
            "name" => 'required',
            "user_id" => 'required',
            "phone" => 'required',
            "location" => 'required',
            "cohort" =>'required',
            "joining_date" => 'required',
        ]);

        $name = $request->input('user_id');
        $user = User::where('name',$name)->first();

        $daycare = Daycare::find($id);
        $daycare->daycare_id = $request->daycare_id;
        $daycare->name = $request->name;
        $daycare->contact_name = $name;
        $daycare->contact_number = $request->contact_number;
        $daycare->location = $request->location;
        $daycare->cohort = $request->cohort;
        $daycare->joining_date = $request->joining_date;

        $daycare->save();
        return redirect()->route('admin.daycare.index')
        ->with('success', 'Daycare updated successfully.');

     }

    public function daycareDesroy(Request $request, $id)
    {
        $daycare = Daycare::findOrFail($id);
        
        $daycare->delete();
    
        return redirect()->route('admin.daycare.index')
                        ->with('success','Daycare deleted successfully');

    }


    public function getIndex()
    {
        return view('admin.coupons.form');
    }

    public function getDaycareIndex()
    {
        return view('admin.coupons.daycare_promocode');
    }

    public function storeDaycareCoupon(Request $request)
    {

        $request->validate([
            "title" => 'required',
            //"user_id"=>'required',
            "prefix"=>'required',
           
        ]);
        $name = $request->input('user_id');
        $title = $request->title;
        $prefix = $request->prefix;
        $expirationDate = now()->addDays(30);
    
        $daycare = Daycare::where('name', $name)->first();
    
        $user = null;
        if (!empty($daycare)) {
            $user = User::where('phone', $daycare->contact_number)->first();
        }
    
        $code = implode('-', [strtoupper($prefix), substr(md5(uniqid(mt_rand(), true)), 0, 6)]);
    
        try {
            DB::beginTransaction();
    
            $coupon = DaycarePromocode::where('code', $code)->lockForUpdate()->first();
    
            if ($coupon) {
                // If coupon code already exists, generate a new one
                $code = implode('-', [strtoupper($prefix), substr(md5(uniqid(mt_rand(), true)), 0, 6)]);
                $coupon->title = $title;
                $coupon->code = $code;
                $coupon->daycare_id = !empty($daycare) ? $daycare->id : null;
                $coupon->user_id = !empty($user) ? $user->id : null;
                $coupon->expiration_date = $expirationDate;
                $coupon->save();
            } else {
                $coupon = new DaycarePromocode;
                $coupon->title = $title;
                $coupon->code = $code;
                $coupon->daycare_id = !empty($daycare) ? $daycare->id : null;
                $coupon->user_id = !empty($user) ? $user->id : null;
                $coupon->expiration_date = $expirationDate;
                $coupon->save();
            }
    
            DB::commit();
    
            
    
            return redirect('/admin/promoCodes/daycareCodes')->with('success','Coupon created successfully');
        } catch (Exception $e) {
            DB::rollback();
    
          
    
            return redirect()->back()->with('error','Coupon was not created. Try again');;
        }
    }
    

       public function storeCoupon(Request $request)
    {

        $request->validate([
            "title" => 'required',
            "user_id"=>'required',
            "prefix"=>'required',
           
        ]);

        $name = $request->input('user_id');
        $title = $request->title;
        $prefix = $request->prefix;
        $expirationDate = now()->addDays(30);
         $user = User::where('name',$name)->first();

        
       

        $code = implode('-', [strtoupper($prefix), substr(md5(uniqid(mt_rand(), true)), 0, 6)]);
    
        try {
            DB::beginTransaction();
    
            $coupon = Coupon::where('code', $code)->lockForUpdate()->first();
    
            if ($coupon) {
                // If coupon code already exists, generate a new one
                $code = implode('-', [strtoupper($prefix),  substr(md5(uniqid(mt_rand(), true)), 0, 6)]);
                $coupon->code = $code;
                $coupon->user_id = $user->id;
                $coupon->expiration_date = $expirationDate;
                $coupon->save();
            } else {
                $coupon = new Coupon;
                $coupon->code = $code;
                $coupon->user_id = $user->id;
                $coupon->expiration_date = $expirationDate;
                $coupon->save();
            }
    
            $couponTranslation = new CouponTranslation();
            $couponTranslation->coupon_id = $coupon->id;
            $couponTranslation->locale = app()->getLocale();
            $couponTranslation->active = true;
            $couponTranslation->title = $title;
            $couponTranslation->save();
    
            DB::commit();

            $site_success = [
                'title' => 'Success!',
                'content' => 'Coupon created successfully.',
            ];

            \Session::flash('course_success', $site_success);
            

        return redirect('/admin/promoCodes/promoCodes');
    } catch (Exception $e) {
        DB::rollback();

        $site_errors = [
            'title' => 'Sorry, an error occurred while creating the coupon',
            'content' => $e->getMessage(),
        ];

        \Session::flash('course_errors', $site_errors);

        return redirect()->back()->withInput();
    }

        
    }

    




    public function promoCodes(Request $request)
    {
       
        $search = $request->input('search');
        $codes = DB::table('coupons')
            ->leftJoin('twill_users', 'twill_users.id', '=', 'coupons.user_id')
            ->leftJoin('coupon_translations', 'coupon_translations.coupon_id', '=', 'coupons.id')
            ->leftJoin('subscriptions', 'subscriptions.coupon', '=', 'coupons.code')
            ->where('coupons.user_id', '!=', 0)
            ->where(function($query) use ($search) {
                $query->where('coupon_translations.title', 'LIKE', '%'.$search.'%')
                      ->orWhere('twill_users.name', 'LIKE', '%'.$search.'%')
                      ->orWhere('coupons.code', 'LIKE', '%'.$search.'%');
            })
            ->select('coupons.*', 'coupon_translations.title','twill_users.name', DB::raw('count(subscriptions.coupon) as count'))
            ->groupBy('coupons.id')
            ->paginate(20);


    
        return view('admin.coupons.index', compact('codes', 'search'));
    }
    

    public function userPromoCodes(Request $request, $id)
    {
        $promoUser = Mpesa::where('coupon', $id)->pluck('user_id')->toArray();
          
        $users =$codes = DB::table('subscriptions')
        ->leftJoin('twill_users', 'twill_users.id', '=', 'subscriptions.user_id')
         ->whereIn('twill_users.id', $promoUser) ->where('twill_users.daycare_id', null)->get();
         $promocode = $id;
         $promoowner = DB::table('coupons')->where('code',$id)->first();
        $owner = User::where('id',$promoowner->user_id)->first();
       
     
        return view('admin.coupons.userCodes', compact('users','promocode','owner'));
    }

    public function daycareCodes(Request $request){
        $search = $request->input('search');
        $codes = DB::table('daycare_promocodes')
            ->leftJoin('twill_users', 'twill_users.id', '=', 'daycare_promocodes.user_id')
            
            ->leftJoin('subscriptions', 'subscriptions.coupon', '=', 'daycare_promocodes.code')
            ->leftJoin('daycares', 'daycares.id', '=', 'daycare_promocodes.daycare_id')
           // ->where('daycare_promocodes.user_id', '!=', 0)
      
            ->where(function($query) use ($search) {
                $query->where('daycare_promocodes.title', 'LIKE', '%'.$search.'%')
                      ->orWhere('twill_users.name', 'LIKE', '%'.$search.'%')
                      ->orWhere('daycare_promocodes.code', 'LIKE', '%'.$search.'%');
            })
            ->select('daycare_promocodes.*','twill_users.name','daycares.name as daycare_name', DB::raw('count(subscriptions.coupon) as count'))
            ->groupBy('daycare_promocodes.id')
            ->paginate(20);


    
        return view('admin.coupons.daycare_index', compact('codes', 'search'));

    }

    public function daycarePromoCodes(Request $request, $id)
    {
    
        $users = DB::table('subscriptions')
            ->leftJoin('twill_users', 'twill_users.id', '=', 'subscriptions.user_id')
            ->where('subscriptions.coupon', $id);
            
    
        if ($request->has('search')) {
            $searchTerm = $request->input('search');
            $users->where(function ($query) use ($searchTerm) {
                $query->where('twill_users.name', 'like', '%'.$searchTerm.'%')
                      ->orWhere('twill_users.email', 'like', '%'.$searchTerm.'%');
            });
        }

        
    
        $users = $users->paginate(10); 
        
        $promocode = $id;
    
        $promoowner = DB::table('daycare_promocodes')->where('code',$id)->first();
      
       $owner = Daycare::where('id',$promoowner->id)->first();
       
      
    
        return view('admin.coupons.daycareCodes', compact('users','promocode','owner'));
    }
    
    
    

    public function promocodeEdit(Request $request, $id)
    {
        $promocode = DaycarePromocode::findOrFail($id);

       
        return view('admin.coupons.daycareCodeShow', compact('promocode'));
    }

    public function promocodeUpdate(Request $request, $id)
    {
        
        $request->validate([
            "title" => 'required',
            // "user_id"=>'required',
            //"prefix"=>'required',
           
        ]);
        $name = $request->input('user_id');
        $title = $request->input('title');
        $prefix = $request->input('prefix');
        $expirationDate = now()->addDays(30);
    
        $daycare = Daycare::where('name', $name)->first();
        
    
        $user = null;
        if (!empty($daycare)) {
            $user = User::where('phone', $daycare->contact_number)->first();
        }

  

   
    
        //$code = implode('-', [strtoupper($prefix), substr(md5(uniqid(mt_rand(), true)), 0, 6)]);
    
        try {
            DB::beginTransaction();
    
            $coupon = DaycarePromocode::where('id', $id)->lockForUpdate()->first();

            //dd($coupon);
    
            if ($coupon) {

             
                // If coupon code already exists, generate a new one
                //$code = implode('-', [strtoupper($prefix),  substr(md5(uniqid(mt_rand(), true)), 0, 6)]);
                //$coupon->code = $coupon;
                $coupon->daycare_id = !empty($daycare->id) ? $daycare->id: null;

                $coupon->user_id = !empty($user->id)  ? $user->id: null;
                $coupon->title = $title;
                $coupon->expiration_date = $expirationDate;
                $coupon->save();
            }
    
            DB::commit();
    
            $site_success = [
                'title' => 'Success!',
                'content' => 'Coupon updated successfully.',
            ];
    
            \Session::flash('course_success', $site_success);
    
            return redirect('/admin/promoCodes/daycareCodes');
        } catch (Exception $e) {
            DB::rollback();
    
            $site_errors = [
                'title' => 'Sorry, an error occurred while updating the coupon',
                'content' => $e->getMessage(),
            ];
    
            \Session::flash('course_errors', $site_errors);
    
            return redirect()->back()->withInput();
        }
    }
    
    public  function daycareUsers(Request $request, $id)
    {

        $daycareUsers = User::where('daycare_id', $id)->paginate(20);

     

  return view('admin.daycares.users', compact('daycareUsers'));

    }

    public function daycareUpload(Request $request)
    {
        // Check if a file was uploaded
        if ($request->hasFile('file')) {
            $file = $request->file('file');
    
            $rows = IOFactory::load($file)->getActiveSheet()->toArray(null, true, true, true);
    
            // Remove header row
            unset($rows[1]);
    
            foreach ($rows as $row) {
                $joining_date = date('Y-m-d H:i:s', strtotime($row['G']));
                DB::table('daycares')->insert([
                    'daycare_id' => $row['A'],
                    'name' => $row['B'],
                    'contact_name' => $row['C'],
                    'contact_number' => $row['D'],
                    'location' => $row['E'],
                    'cohort' => $row['F'],
                    'joining_date' =>$joining_date,
                ]);
            }
    
            return redirect()->route('admin.daycare.index')->with('success', 'File uploaded and processed successfully.');
        } else {
            return redirect()->back()->with('error', 'No file was uploaded.');
        }
    }
    





    protected function formData($request)
    {

        return [
            'userList' =>  app(UserRepository::class)->where('published', 1)->where('id', '!=', 1)->pluck('name', 'id'),


        ];
    }
}
