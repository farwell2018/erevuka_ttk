<?php

namespace App\Http\Controllers\Admin;
use App\Repositories\EvaluationOptionRepository;
use App\Repositories\CourseRepository;

use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;

class EvaluationController extends BaseModuleController
{
    protected $moduleName = 'evaluations';

    protected $indexOptions = [
        'permalink' => false,
    ];

    protected function formData($request)
    {
        return [
			'optionList' => app(EvaluationOptionRepository::class)->where('published',1)->pluck('name','id'),
            'courseList' => app(CourseRepository::class)->where('status',1)->pluck('name','id'),
        ];

	}
}
