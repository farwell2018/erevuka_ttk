<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class RoleController extends ModuleController
{
    protected $moduleName = 'roles';

    protected $indexOptions = [
        'reorder' => true,
];
    protected function indexData($request)
    {
        return [
            'single_primary_nav' => [
               
                'roles' => [
                    'title' => 'Roles',
                    'module' => true,
                    
                ],

               
            ],
            'customPublishedLabel' => 'Enabled',
            'customDraftLabel' => 'Disabled',
        ];
    }

    protected function formData($request)
    {
        return [
            'single_primary_nav' => [
                'roles' => [
                    'title' => 'Roles',
                    'module' => true,
                    
                ],
                
            ],
            'customPublishedLabel' => 'Enabled',
            'customDraftLabel' => 'Disabled',
        ];
    }

    
}
