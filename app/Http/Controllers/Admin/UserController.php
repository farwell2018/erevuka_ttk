<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Models\Enums\UserRole;
use Illuminate\Config\Repository as Config;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use AfricasTalking\SDK\AfricasTalking;
use App\Helpers\Front;
use Mail;
use Carbon\Carbon;
use Auth;
use App\Models\GroupPurchase;
use DB;
use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;


class UserController extends BaseModuleController
{

    /**
     * @var string
     */
    protected $moduleName = 'users';

    /**
     * @var array
     */
    protected $defaultOrders = ['name' => 'asc'];

    /**
     * @var array
     */
    protected $filters = [
        'role' => 'role',
    ];

    /**
     * @var string
     */
    protected $titleColumnKey = 'name';

    /**
     * @var array
     */
    protected $indexColumns = [
        'name' => [
            'title' => 'Name',
            'field' => 'name',
        ],
        'email' => [
            'title' => 'Email',
            'field' => 'email',
            'sort' => true,
        ],
        'phone_locale' => [
            'title' => 'Phone Locale',
            'field' => 'phone_locale',
            'sort' => true,

        ],
        'phone' => [
            'title' => 'Phone Number',
            'field' => 'phone',
            'sort' => true,

        ],

        'role_value' => [
            'title' => 'Role',
            'field' => 'role_value',
            'sort' => true,
            'sortKey' => 'role',
        ],
        'created_at' => [
            'title' => 'Registered on',
            'field' => 'created_at',
            'sort' => true,

        ],


        'last_login_at' => [
            'title' => 'Last Login',
            'field' => 'last_login_at',
            'sort' => true,

        ],
    ];






    public function groupPurchase(Request $request)
    {
        $purchases = GroupPurchase::select(DB::raw('user_id,count(*) as count'))
            ->groupBy('user_id')
            ->with('users')
            ->paginate(20);

        return view('admin.users.groupPurchase', compact('purchases'));
    }

    public function userGroupPurchase(Request $request, $id)
    {

        $purchases = GroupPurchase::where('user_id', $id)->get();

        return view('admin.users.userGroup', compact('purchases'));
    }
}
