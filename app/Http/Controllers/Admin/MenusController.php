<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Repositories\MenusRepository;

class MenusController extends ModuleController
{
    protected $moduleName = 'menuses';


    protected function formData($request)
    {

        return [
			'menuList' => app(MenusRepository::class)->listAll('title'),
			
        ];

	}
    
}
