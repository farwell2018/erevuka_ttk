<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class TestimonialController extends ModuleController
{
    protected $moduleName = 'testimonials';
}
