<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class ForumTopicController extends ModuleController
{
    protected $moduleName = 'forumTopics';
    
}
