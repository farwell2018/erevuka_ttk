<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Repositories\RoleRepository;

class ContentBucketController extends ModuleController
{
    protected $moduleName = 'contentBuckets';


    protected function formData($request)
    {

    
        return [
			'roleTypeList' => app(RoleRepository::class)->listAll('title'),
			
        ];

	}

   
    
}
