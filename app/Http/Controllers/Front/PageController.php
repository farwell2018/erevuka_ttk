<?php

namespace App\Http\Controllers\Front;

// use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\PageRepository;
use App\Repositories\ResourceRepository;
use A17\Twill\Http\Controllers\Front\Controller;
use App\Models\Session as SessionMeeting;
use App\Models\Resource;
use App\Models\SessionResource;
use App\Models\User;
use App\Models\Event;
use App\Models\Blog;
use App\Models\ResourceType;
use App\Models\Translations\ResourceCategoryTranslation;
use App\Models\ResourceTheme;
use App\Models\Forum;
use App\Models\GeneralMessage;
use App\Models\ForumTopic;
use Carbon\Carbon;
use Auth;
use Validator;
use Mail;
use Redirect;
use Session;
use App;
use Notification;
use Artisan;
use App\Notifications\GeneralMessageSent;
use Spatie\GoogleCalendar\Event as Events; 
use Monarobase\CountryList\CountryListFacade as CountryList;

class PageController extends Controller
{
    //

    protected $pageKey;

    public function __construct()
    {
		$this->homeKey = PageRepository::PAGE_HOMEPAGE;
    parent::__construct();
	}

	public function index(Request $request)
    {
      if($request->input('category')){
       $type = $request->input('category');
      }else{
        $type= '';
      }
    $resourceTypes = ResourceType::orderBy('position','asc')->get();
		$itemPage = app(PageRepository::class)->getPage($this->homeKey);
		$this->seo->title = !empty($title=$itemPage->seo_title) ? $title : "{$itemPage->title}";
        $this->seo->canonical = $itemPage->seo_canonical;
        $this->seo->description = mb_substr($itemPage->present()->seo_description,0, 255);
        $this->seo->image = $itemPage->present()->imageSeo;

    return view('site.pages.home',[
			'pageItem' => $itemPage,
       'resourceTypes' => $resourceTypes,
			 'resourcetype'=> $type,
		]);
    }



    public function pages(Request $request, $key){

      if (env('APP_DEBUG') === 'false' || env('APP_ENV') === 'production') {

        Artisan::call('view:clear');
        Artisan::call('optimize:clear');

    }
       
      if($request->isMethod('post')){
        
        if($request->input('category')){
          $type = $request->input('category');
          $free='';
          $slug_type ='';

        }elseif($request->input('type')){
        $slug_type = $request->input('type');
        $free='';
        $type='';
        }
        elseif($request->input('free_search')){
          $free = $request->input('free_search');
          $type ='';
          $slug_type='';
        }elseif($request->input('free_search') && $request->input('category') && $request->input('type')){
          $free = $request->input('free_search');
          $type = $request->input('category');
          $slug_type = $request->input('type');
        }
      
         $forums = Forum::where('forum_topic',$request->input('theme'))->latest()->get();
        }else{
            $forums = Forum::latest()->get();
            $type= '';
            $free = '';
            $slug_type ='';
        }
      $types = Forum::getTypes();

      $resourceTypes = ResourceType::orderBy('position','asc')->get();
      $resourceCategorySlugs = ResourceCategoryTranslation::orderBy('id','asc')->get();
      $topics = ForumTopic::all();
      $itemPage = app(PageRepository::class)->getPage($key);
      $this->seo->title = !empty($title=$itemPage->seo_title) ? $title : "{$itemPage->title}";
      $this->seo->canonical = $itemPage->seo_canonical;
      $this->seo->description = mb_substr($itemPage->present()->seo_description,0, 255);
      $this->seo->image = $itemPage->present()->imageSeo;

       if($key === 'home'){
          return redirect()->route('home');

       }else{
          return view('site.pages.'.$key,[
            'pageItem' => $itemPage,
            'resourceTypes' => $resourceTypes,
            'resourceCategorySlugs'=> $resourceCategorySlugs,
            'topics'=>$topics,
            'types'=> $types ?? '',
            'slug_type' => $slug_type ?? '',
            'forums'=>$forums,
            'resourcetype'=> $type ?? '',
            'freeSearch' => $free ?? ''
          ]);

       }
         if (Auth::check()) {
        return redirect()->route('home');
    } else {
        return view('home')
               ->with('userLogged', Auth::check());
    }
  }

  public function resourceDetails(Request $request, $id){
    $itemPage = Resource::where('id', '=', $id)->first();
    $related = Resource::where('content_bucket_id', '=', $itemPage->content_bucket_id)->latest()->take(2)->get(); 

      return view('site.pages.resources_details',[
        'pageItem' => $itemPage,
        'related' => $related,

        
      ]);
  }


  public function save_likedislike(Request $request){
    $data=new LikeDislike;
    $data->comment_id=$request->post;
    $data->user_id = Auth::user()->id;
    if($request->type=='like'){
        $data->like=1;
    }
    $data->save();
    return response()->json([
        'bool'=>true
    ]);
}


public function contactStore(Request $request)
{
    //Validate
    $request->validate([
        'name' => ['required', 'string', 'max:255'],
        'contact_phone' => ['required', 'string', 'max:255'],
        'subject' => ['required', 'string', 'max:255'],
        'contact_message' => ['required', 'string'],
    ]);

    //Get message
    $message = new GeneralMessage;
    $message->name = $request->input('name');
    $message->contact_phone = $request->input('contact_phone');
    $message->subject = $request->input('subject');
    $message->contact_message = $request->input('contact_message');
    $message->save();

    //Send email
    $contact_emails = explode(',', env('CONTACT_EMAILS'));
    foreach ($contact_emails as $email) {
        Notification::route('mail', $email)
            ->notify(new GeneralMessageSent($message));
    }


    //Show session message
    $redirectMessage = [
        'title' => 'Message sent',
        'content' => 'Thank you for your enquiry. We will respond to you as soon as possible.',
        // 'action'=>'Google',
        // 'link'=>'google.com'
    ];
    Session::flash('course_success', $redirectMessage);
    return redirect()->back();
}

    protected function getRepository()
    {
        return App::make("App\\Repositories\\ResourceRepository");
	}

}
