<?php

namespace App\Http\Controllers\Front;

use App\DaycarePromocode;
use App\Models\GroupPurchase;
use App\Http\Controllers\Controller;
use App\Models\Mpesa;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\Model;



class IpayController extends Controller
{
  //

  private function ipay($fields)
  {

    $datastring =  $fields['live'] . $fields['oid'] . $fields['inv'] . $fields['amount'] . $fields['tel'] . $fields['eml'] . $fields['vid'] . $fields['curr'] . $fields['p1'] . $fields['p2'] . $fields['p3'] . $fields['p4'] . $fields['cst'] . $fields['cbk'];
    $hashkey = "6WhNRB7fz2fW6S%9TeGSzC!gmc8#pRrF";
    $generated_hash = hash_hmac('sha256', $datastring, $hashkey);
    $fields['hash'] = $generated_hash;

    //dd($fields);
    $headers = array(
      'Content-Type' => 'application/x-www-form-urlencoded',
      'cache-control' => 'no-cache',
      'Referer' => env('APP_URL'),
      'Access-Control-Allow-Origin' => '*',
    );

    $client = new \GuzzleHttp\Client();

    try {

      $response = $client->request('POST', env('IPAY_URL'), [
        'form_params' => $fields
      ]);
      return $response;
    } catch (\GuzzleHttp\Exception\ClientException $e) {
      $responseJson = $e->getResponse();
      $response = json_decode($responseJson->getBody()->getContents(), true);

      foreach ($response as $key => $error) {
        //Return error
        $errors[] = $error;
      }
      //echo "CATCH 1";
      //dd($errors);
      return $errors[0];
    }
  }

  public function pay(Request $request)
  {
    
    session_start();
    $_SESSION['range'] = $request->range;
    $_SESSION['email'] = $request->email;
    $_SESSION['coupon'] = $request->coupon;

    if(!empty($request->coupon))
    {
      $daycareCoupon = DaycarePromocode::where('code', $request->coupon)->first();

     
      if($daycareCoupon && $daycareCoupon->code == $request->coupon) {
        $user = auth()->user();
        
        $user->subscription = true;
        $user->daycare_id =$daycareCoupon->daycare_id;
        $user->save();
    
        $subscription = new Mpesa();
        $subscription->user_id = $user->id;
        $subscription->subscription_start_date = now();
        $subscription->coupon = $request->coupon;
        $subscription->save();

        $course_success = [
          'title' => 'Congratulations!',
          'content' => 'Your subscription purchase was successful.​ You may access our content library while the subscription is valid.',
  
        ];
        
  
  
        Session::flash('course_success', $course_success);
        return redirect('/profile/course/detail/' . $_SESSION['course']);
      }
    }
    

    $amount = $request->amount;
    $phone = $request->phone;

    return view('site.pages.subscriptions.confirm-purchase', ['totalAmount' => $amount,  'phone' => $phone,]);
  }



  public function status(Request $request)
  {

  
    session_start();
    $order = $request->p1;
    $fields = array(
      "oid" => $order,
      "vid" => "farwell",
    );
    $ipayStatus =  $this->ipayStatus($fields);
    $resp = json_decode($ipayStatus->getBody()->getContents(), true);
    $total = $resp['data']['transaction_amount'];
    $code = $resp['data']['transaction_code'];
    $exipry = now()->addDays(30);
    $range = $_SESSION['range'];

    $accounts = 0;
    if ($resp['status'] == 1) {
      if (!empty($_SESSION['range'])) {
        if ($range === '1-10 users') {
          $accounts = 10;
        } elseif ($range === '11-20 users') {
          $accounts = 20;
        } else if ($range === '21-50 users') {
          $accounts = 50;
        }
        $accounts = $accounts - 1;
        for ($x = 0; $x <= $accounts; $x++) {
          $group = new GroupPurchase();
          $group->unique_code = $this->generateLicenseString(10);
          $group->user_id = Auth::user()->id;
          $group->save();
        }

        $codes = GroupPurchase::where('user_id', Auth::user()->id)->get();
        $user = $_SESSION['email'];
        Mail::send(
          'emails.user_code',
          ['name' => Auth::user()->first_name . ' ' . Auth::user()->last_name,  'codes' => $codes],
          function ($message) use ($user) {
            $message->from('support@farwell-consultants.com', 'Group Purchase User Codes');
            $message->to($user, Auth::user()->first_name);
            $message->subject('The mtoto Learn User Codes');
          }
        );
      }



      $paidUser = new Mpesa();
      $paidUser->user_id = Auth::user()->id;
      $paidUser->transaction_id = $resp['data']['transaction_code'];
      $paidUser->subscription_start_date = $resp['data']['paid_at'];
      $paidUser->subscription_end_date  = $exipry;
      $paidUser->coupon  =  $_SESSION['coupon'];
      $paidUser->save();

      $user = Auth::user();
      // Get the latest subscription record for the user
      $latestSubscription = Mpesa::where('user_id', $user->id)
      ->orderBy('subscription_end_date', 'desc')
      ->first();

      // Check if the latest subscription has expired
      if ($latestSubscription && now()->gt($latestSubscription->subscription_end_date)) {
      // If the latest subscription has expired, set the `subscription` column on the `twill_users` table to false
      $user->subscription = false;
      $user->save();
      }
      $user->subscription = true;
      $user->save();


      $course_success = [
        'title' => 'Congratulations!',
        'content' => 'Your subscription purchase was successful.  If this was a Single User ​subscription,​ you may access our content library while the subscription is valid. The subscription will expire on ' . $exipry . '
                     .​Incase you purchased a Group License, please check your email for enrolment user codes to be used by your team for a period of 1 month.',

      ];
      


      Session::flash('course_success', $course_success);
      return redirect('/profile/course/detail/' . $_SESSION['course']);
    }
  }

  private function ipayStatus($fields)
  {
    $datastring =  $fields['oid'] . $fields['vid'];
    $hashkey = "6WhNRB7fz2fW6S%9TeGSzC!gmc8#pRrF";
    $generated_hash = hash_hmac('sha256', $datastring, $hashkey);
    $fields['hash'] = $generated_hash;

    //dd($fields);
    $headers = array(
      'Content-Type' => 'application/x-www-form-urlencoded',
      'cache-control' => 'no-cache',
      'Referer' => env('APP_URL'),
      'Access-Control-Allow-Origin' => '*',
    );

    $client = new \GuzzleHttp\Client();

    try {

      $response = $client->request('POST', 'https://apis.ipayafrica.com/payments/v2/transaction/search', [
        'form_params' => $fields
      ]);
      return $response;
    } catch (\GuzzleHttp\Exception\ClientException $e) {
      $responseJson = $e->getResponse();
      $response = json_decode($responseJson->getBody()->getContents(), true);

      foreach ($response as $key => $error) {
        //Return error
        $errors[] = $error;
      }
      //echo "CATCH 1";
      //dd($errors);

      return $errors[0];
    }
  }

  protected function generateLicenseString($length = 6)
  {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

    return $randomString;
  }
}
