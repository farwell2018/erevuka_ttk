<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Mpesa;
use Illuminate\Http\Request;
use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class MpesaResponsesController extends Controller
{
    public function validation(Request $request)
    {
        Log::info('validation endpoint hit');
        Log::info($request->all());
        return [
            'ResultCode' => 0,
            'ResultDesc'=> 'Accept Service',
            'ThirdPartyTransID' => rand(3000,10000)
        ];



    }
    public function confirmation(Request $request)
    {
        Log::info('confirmation endpoint hit');
        Log::info($request->all());


    }

    public function stkPush(Request $request)
    {
        Log::info($request->all());
        Log::info('stk endpoint hit');
    $content = $request->getContent();
    $data = json_decode($content);
 

    // Check if the transaction was successful
    if ($request->Body->stkCallback->ResultCode == 0) {
        // Update your database with the transaction details
        $transactionId = $request->Body->stkCallback->CheckoutRequestID;
        $amount = $request->Body->stkCallback->CallbackMetadata->Item[0]->Value;
        $phone = $request->Body->stkCallback->CallbackMetadata->Item[4]->Value;
        // ...
        
        // Return a success response to M-Pesa
        Storage::put('mpesa_response.json', json_encode($request->all()));
        return response()->json(['ResultCode' => 0, 'ResultDesc' => 'The service was accepted successfully']);
    } else {
        // Handle the error
        // ...
       
        // Return an error response to M-Pesa
        Storage::put('mpesa_response.json', json_encode($request->all()));
        return response()->json(['ResultCode' => 1, 'ResultDesc' => 'The service was not accepted']);
    }




    }
}
