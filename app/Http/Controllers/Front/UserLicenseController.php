<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserLicense;
use App\Models\Course;
use App\Models\MyCommunity;
use App\Models\User;
use App\Models\TrainingComment;
use App\Models\TrainingLike;
use App\Models\UserEvaluation;
use App\Models\Evaluation;
use App\Edx\StudentCourseEnrollment;
use Auth;
use Redirect;
use Session;
use Carbon\Carbon;

use Symfony\Component\HttpFoundation\Response;
use Barryvdh\DomPDF\Facade\Pdf;
use A17\Twill\Models\Enums\UserRole;
use DB;
use Monarobase\CountryList\CountryListFacade as CountryList;
use PragmaRX\Countries\Package\Countries;
use PragmaRX\Countries\Package\Services\Config;
use App\Models\Projecttopic;
use App\Models\SerialNumber;
use App\Models\LearningStatus;
use App\Edx\EdxAuthUser;
use Illuminate\Support\Facades\Log;
use Artisan;


class UserLicenseController extends Controller
{
    //
    public $data ;
    public $country;
    public function __construct()
    {
        $this->data = [
            'ug'=>  '',
            'ng'=>  '',
            'st'=>  '',
            'tz'=>  '',
            'sl'=>  '',
            'gw'=>  '',
            'cv'=>  '',
            'sc'=>  '',
            'tn'=>  '',
            'mg'=>  '',
            'ke'=>  '',
            'cd'=>  '',
            // 'fr'=>  '',
            'mr'=>  '',
            'dz'=>  '',
            'er'=>  '',
            'gq'=>  '',
            'mu'=>  '',
            'sn'=>  '',
            'km'=>  '',
            'et'=>  '',
            'ci'=>  '',
            'gh'=>  '',
            'zm'=>  '',
            'na'=>  '',
            'rw'=>  '',
            'sx'=>  '',
            'so'=>  '',
            'cm'=>  '',
            'cg'=>  '',
            'eh'=>  '',
            'bj'=>  '',
            'bf'=>  '',
            'tg'=>  '',
            'ne'=>  '',
            'ly'=>  '',
            'lr'=>  '',
            'mw'=>  '',
            'gm'=>  '',
            'td'=>  '',
            'ga'=>  '',
            'dj'=>  '',
            'bi'=>  '',
            'ao'=>  '',
            'gn'=>  '',
            'zw'=>  '',
            'za'=>  '',
            'mz'=>  '',
            'sz'=>  '',
            'ml'=>  '',
            'bw'=>  '',
            'sd'=>  '',
            'ma'=>  '',
            'eg'=>  '',
            'ls'=>  '',
            'ss'=>  '',
            'cf'=>  '',
            'is'=>  '',
        ];
    }




    public function profile(Request $request)
    {
      // if (!isset($_COOKIE['edinstancexid'])) {

        //     Auth::logout();
        //     return false;
        // }

        Artisan::call('view:clear');
        Artisan::call('optimize:clear');

        if (env('APP_DEBUG') === 'false' || env('APP_ENV') === 'production') {

            Artisan::call('view:clear');
            Artisan::call('optimize:clear');
         
      
        }

        $passMark = env('PassMark');
        $courses = Course::orderBy('name')->get();
        $statuses = LearningStatus::all();
        $month = [];
         
        $years= range(now()->year, now()->year);
        // $years= range(now()->year, now()->year + 5);

        for ($m=1; $m<=12; $m++) {
             $month[] = date('F', mktime(0,0,0,$m, 1, date('Y')));
        }


        $future_courses = Course::where('start', '>', Carbon::today()->toDateString())->get();

        $coursesid = [];
        foreach ($future_courses as $id => $course) {
            $coursesid[] = $course->id;
        }

          //$licensed = UserLicense::where('user_id', Auth::user()->id)->latest()->get();
        $user = Auth::user()->id;

        $licensed = UserLicense::leftJoin('course_completions', function($join) use ($user) {
                            $join->on('user_licenses.completion_id', '=', 'course_completions.course_id')
                                ->where('course_completions.user_id', $user);
                                    
                        })
                        ->select('user_licenses.*','course_completions.completion_date')
                        ->where(function($query) use ($user) {
                            $query->where('user_licenses.user_id', $user); 
                            
                        })->orderBy('course_completions.completion_date', 'desc')->get();


  

           
        $licenseid = [];
        foreach ($licensed as $key => $license) {
            if (isset($license->course->id)) {
                $licenseid[] = $license->course->id;
            } else {
                unset($licensed[$key]);
            }
        }


        $result = array_diff($licenseid, $coursesid);
          
    
        $licenses = [];
        foreach ($licensed as $key => $license) {
            if (in_array($license->course->id, $result)) {
                $licenses[] = $license;
            }
            
            if (!empty($request->input('year'))) {
              
                if (Carbon::parse($license->created_at)->format('Y') != $request->input('year')) {
                    unset($licenses[$key]);
                }
            }

            if (!empty($request->input('month'))) {
              
                if (Carbon::parse($license->created_at)->format('m') != $request->input('month')) {
                    unset($licenses[$key]);
                }
            }

            if (!empty($request->input('status'))) {

                if ($license->status != $request->input('status')) {
                    unset($licenses[$key]);
                }
            }

            if (!empty($request->input('search'))) {
                if (strpos($license->course->name, $request->input('search')) === FALSE) {
                    
                    unset($licenses[$key]);
                 }
              
            }
        }

 

        return view('site.pages.user_licenses.courses', ['licenses' => $licenses, 'courses' => $courses, 'passMark' => $passMark, 'statuses'=>$statuses, 'month'=> $month, 'years'=>$years]);
    }
    /**
     * Display a listing of achievements by the profile
     *
     * @return \Illuminate\Http\Response
     */
    public function achievements()
    {
        $future_courses = Course::where('start', '>', Carbon::today()->toDateString())->get();
        $coursesid = [];
        foreach ($future_courses as $id => $course) {
            $coursesid[] = $course->course_id;
        }
        $licensed = UserLicense::where('user_id', Auth::user()->id)->get();
        $licenseid = [];
        foreach ($licensed as $key => $license) {
            if (isset($license->course->course_id)) {
                $licenseid[] = $license->course->course_id;
            } else {
                unset($licensed[$key]);
            }
        }

        $result = array_diff($licenseid, $coursesid);

        $licenses = [];

        foreach ($licensed as $key => $license) {
            if (in_array($license->course->course_id, $result)) {
                $licenses[] = $license;
            } else {
                $upcoming[] = $license;
            }

            if (!empty($_GET['year'])) {
                if (\Carbon\Carbon::parse($license->created_at)->format('Y') != $_GET['year']) {
                    unset($licenses[$key]);
                }
            }

            if (!empty($_GET['month'])) {
                if (\Carbon\Carbon::parse($license->created_at)->format('m')!= $_GET['month']) {
                    unset($licenses[$key]);
                }
            }

            if (!empty($_GET['status'])) {
                if ($license->status != $_GET['status']) {
                    unset($licenses[$key]);
                }
            }
        }

        return view('site.pages.user_licenses.achievements', ['licenses' => $licenses]);
    }



    public function evaluate($id){
        if (env('APP_DEBUG') === 'false' || env('APP_ENV') === 'production') {

            Artisan::call('view:clear');
            Artisan::call('optimize:clear');
        
        }
        $course = Course::where('course_id', $id)->first();
       
        $sections = Evaluation::where('course_id',$course->id)->first();
        $evaluation = UserEvaluation::where('user_id','=',Auth::user()->id)->first();
  
        return view('site.pages.user_licenses.evaluate',['sections'=>$sections, 'evaluation'=>$evaluation, 'id'=>$id]);
  
      }
  
  
      public function Evaluationstore(Request $request){
  
   
        foreach ($_POST as $key => $value) {
            $keys[]=$key;
            $values [] = $value;
           }

           $userEvaluation = new UserEvaluation;
           $userEvaluation->user_id = Auth::user()->id;
           $userEvaluation->username = Auth::user()->name;
           $userEvaluation->course_id = $request->input('course_id');
           $userEvaluation->question1 = $keys[2];
           $userEvaluation->answer1 = $values[2];
           $userEvaluation->question2 = $keys[3];
           $userEvaluation->answer2 = $values[3];
           $userEvaluation->question3 = $keys[4];
           $userEvaluation->answer3 = $values[4];
           $userEvaluation->question4 = $keys[5];
           $userEvaluation->answer4 = $values[5];
           if(!empty($keys[6])){
           $userEvaluation->question5 = $keys[6];
           $userEvaluation->answer5 = $values[6];
           }
           if(!empty($keys[7])){
           $userEvaluation->question6 = $keys[7];
           $userEvaluation->answer6 = $values[7];
           }
           if(!empty($keys[8])){
             $userEvaluation->question7 = $keys[8];
             $userEvaluation->answer7 = $values[8];
           }
           if(!empty($keys[9])){
             $userEvaluation->question8 = $keys[9];
             $userEvaluation->answer8 = $values[9];
           }
           if(!empty($keys[10])){
             $userEvaluation->question9 = $keys[10];
             $userEvaluation->answer9 = $values[10];
           }
           if(!empty($keys[11])){
             $userEvaluation->question10 = $keys[11];
             $userEvaluation->answer10 = $values[11];
           }
           $userEvaluation->save();
       
  
        $redirectMessage = [
            'title' => 'Thank you for Evaluating the course',
            'content' => 'You can now download your certificate.',
        ];
        Session::flash('course_success', $redirectMessage);
        return redirect()->route('profile.courses');
  
      }
  
    public function certificate($courseid)
    {
        // $user = EdxAuthUser::where('email', Auth::user()->email)->firstOrFail();
        $user = User::where('username', Auth::user()->username)->firstOrFail();
        $enrollment = StudentCourseEnrollment::where(['user_id' => $user->id, 'course_id' => $courseid])->first();
        $course = Course::where('id', $courseid)->first();
        $name = ucfirst($user->first_name).' '.ucfirst($user->last_name);
        $date = Carbon::today()->isoFormat('Do MMMM YYYY');

        $number = SerialNumber::where('user_id','=', Auth::user()->id)->first();
       
        if($number){
           $cert_number = $number->serial_number ;
        }else{

           $serial = new SerialNumber();
           $serial->username = Auth::user()->username;
           $serial->user_id = Auth::user()->id;
           $serial->serial_number = self::generateRandomString(16);

           $serial->save();
           
           $cert_number = $serial->serial_number ;
        }


        $data = [
        'name' => $name,
        'date' => $date,
        'course_name' => $course->name,
        'cert_number' => $cert_number,
         ];

        //  return view('site.pages.user_licenses.certificate', compact('data'))


        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('site.pages.user_licenses.certificate', $data)->setPaper('a4', 'landscape');





        //return $pdf->download('Certificate_'.$name.'.pdf');
        return $pdf->stream();

        //return  $pdf->stream();
    }

    
    protected function generateRandomString($length = 10)
    {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[ rand(0, $charactersLength - 1) ];
      }
    
      return $randomString;
    }
}
