<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpSpreadsheet\Calculation\Financial\Securities\Rates;
use App\Models\Rate;
class UserWishlistController extends Controller
{
    public function purchaseGroup(Request $request)
    {

      
      $validator = Validator::make($request->except('_token'), [
        'bundles' => ['required'],

      ]);

      if ($validator->fails()) {
        $errors = $validator->errors();
        return Redirect::back()->withErrors($errors);
      }
       $qty = $request->input('bundles');
     
       $bundle = Rate::where('id',$qty)->first();
    

      // $courseId = $request->input('course');
        //Add course to wishlist
        //$this->store($courseId,$qty);
        //Mark all courses as not for immediate purchase
        // UserWishlist::where('user_id', Auth::user()->id)->update(['purchase' => 0]);
        //Mark new course as for purchase
        //UserWishlist::where(['user_id' => Auth::user()->id, 'course_id' => $courseId])->update(['purchase' => 1, 'quantity'=> $bundle->range, 'price'=>$bundle->price]);
        //Redirect to purchase page
        return view('site.pages.subscriptions.group', compact('bundle'));
        // return redirect()->route('uwishlist.view-purchase');
    }
}
