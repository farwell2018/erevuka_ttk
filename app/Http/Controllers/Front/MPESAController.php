<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class MPESAController extends Controller
{
    public function index()
    {
        //Display subscription page
        return view('site.pages.subscriptions.index');
    }

    public function single()
    {
        //Display subscription page
        return view('site.pages.subscriptions.single');
    }

    public function group()
    {
        //Display subscription page
        return view('site.pages.subscriptions.group');
    }

    public function generateMtoken()
    {
        // First, check if the access token is already cached
        $cacheKey = 'mpesa_access_token';
        $accessToken = Cache::get($cacheKey);
    
        if (!$accessToken) {
            // If the access token is not cached, generate a new one
            $consumer_key = env('MPESA_CONSUMER_KEY');
            $consumer_secret = env('MPESA_CONSUMER_SECRET');
          
        
            $url =  env('MPESA_ENV') == 0 
            ? 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials'
            : 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';
        
            
            $credentials = base64_encode($consumer_key.':'.$consumer_secret);
        
            $client = new \GuzzleHttp\Client();
        
            $response = $client->request('GET', $url, [
                'headers' => [
                    'Authorization' => 'Basic '.$credentials,
                    'Content-Type' => 'application/json'
                ]
            ]);
        
            $accessToken = json_decode($response->getBody())->access_token;
            
            // Cache the access token for 1 hour
            Cache::put($cacheKey, $accessToken, 60 * 60);
        }
    
        return $accessToken;
    }


    /**
 * Register URL
 */


 public function registerURLS()
 {
     // First, generate a new access token
     $accessToken = $this->generateMtoken();
     
     $body = array(
         'ShortCode' => env('MPESA_SHORTCODE'),
         'ResponseType'=> 'Completed',
         'ConfirmationURL'=> env('MPESA_TEST_URL','https://https://dev.mtotolearn.tinytotos.com') . '/api/confirmation',
         'ValidationURL' => env('MPESA_TEST_URL','https://https://dev.mtotolearn.tinytotos.com') . '/api/validation',
     ); 
 
     $url = env('MPESA_ENV') == 0 
     ?  'https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl'
     :  'https://api.safaricom.co.ke/mpesa/c2b/v1/registerurl';
 
     $headers = [
         'Authorization' => 'Bearer '.$accessToken,
         'Content-Type' => 'application/json'
     ];
 
     $response = $this->makeHttp($url,$body, $headers);
     return $response;
 }

 /**
  * STK Push
  */
  public function stkPush(Request $request){
    $this->registerURLS();
    $timestamp = date('YmdHis');
    $password = $this->lipaNaMpesaPassword();
   
  $curl_post_data = array(
    'BusinessShortCode'=>env('MPESA_STK_SHORTCODE','174379'),
    'Password' => $password,
    'Timestamp'=>$timestamp,
    'TransactionType'=>'CustomerPayBillOnline',
    'Amount' => $request->amount,
    'PartyA' => $request->phone,
    'PartyB' => env('MPESA_STK_SHORTCODE','174379'),
    'PhoneNumber' => $request->phone,
    'CallBackURL' => 'https://dev.mtotolearn.tinytotos.com/api/stkpush',
    'AccountReference'=> $request->account,
    'TransactionDesc' => $request->account,
  );

  $url   = env('MPESA_ENV') == 0 
  ? 'https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest'
  : 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
   

 
  $response = $this->makeHttp($url, $curl_post_data);
  return $response;

  }


  public function lipaNaMpesaPassword()
  {
      //timestamp
      $timestamp = date('YmdHis');
      //passkey
      $passKey ="bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919";
      $businessShortCOde =174379;
      //generate password
      $mpesaPassword = base64_encode($businessShortCOde.$passKey.$timestamp);

      return $mpesaPassword;
      
  }








/**
 * General method for curl requests
 */

 public function makeHttp($url, $body)
    {
       
        $curl = curl_init();
        curl_setopt_array(
            $curl,
            array(
                CURLOPT_URL=>$url,
                CURLOPT_HTTPHEADER => array('Content-Type:application/json','Authorization:Bearer ' . $this->generateMtoken()),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => json_encode($body)
            )
            );
        $curl_response = curl_exec($curl);
        curl_close($curl);
        return $curl_response;

    }


}
