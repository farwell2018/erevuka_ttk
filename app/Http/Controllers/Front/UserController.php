<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Front\RegisterRequest;
use App\Http\Requests\Front\VerifyRequest;
use App\Http\Requests\Front\LoginRequest;
use Monarobase\CountryList\CountryListFacade as CountryList;
use App\Helpers\Front;
use App\Models\TwillUsersAuth;
use App\Models\User;
use App\Models\Projecttopic;
use App\Models\ProjectUser;
use Carbon\Carbon;
use AfricasTalking\SDK\AfricasTalking;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Ixudra\Curl\Facades\Curl;
use Redirect;

use App\Edx\EdxAuthUser;
use App\Models\Role;

use Auth;
use Toastr;
use Session;
use Mail;
use App;

class UserController extends Controller
{
    //

    public function __construct()
    {
        $this->repository = $this->getRepository();
    }
    
    public function resendCode(Request $request)
    {
        return view('site.auth.resend');
    }

  
    /**
     * @param RegisterRequest $request
     */

    public function showRegistrationForm()
    {
        return view('site.auth.register');
    }


    public function create(RegisterRequest $request)
    {
        $code = array();
        $configSms = config()->get("settings.AfricasTalking.live");
        $data=$request->validated();

        $user = $this->repository->createForRegister($data);
    
        $edxResponse =  $this->edxRegister($user);
   
        if ($edxResponse !== true) {
            $user->delete();
            return Redirect::back()->with('register_error', 'An error occured while submitting your registration. Please refresh the page and try again.');
        }
        $validation = $this->validationToken($user, $length = 6);
        //  $a = Front::getCountriesList();
        //    foreach ($a as $key => $value){
        //        if($key === $user->phone_locale){
        //            $code[] = $value['code'];
        //        }
        //    }

        $phone_number = $user->phone_locale.''.$user->phone;
         
        $username   = $configSms['AFRICASTALKING_USERNAME'];
        $apiKey     = $configSms['AFRICASTALKING_APIKEY'];
        $from = $configSms['AFRICASTALKING_FROM'];

        $AT       = new AfricasTalking($username, $apiKey);
        // Get one of the services
        $sms      = $AT->sms();
        $text = 'Hello , Use this code to verify your account on Tiny Totos platform '. $validation->token.' ';

        // Use the service
        $result   = $sms->send(
            [
              'to'      => $phone_number,
              'message' => $text,
              'from' => $from,
            ]
        );
         
        // Mail::send('emails.token',['name' => $user->first_name.' '.$user->last_name,  'code' => $validation->token ],
        // function ($message) use ($user) {
        //   $message->from('support@farwell-consultants.com', 'Welcome to The Tiny Totos Program');
        //   $message->to($user->email, $user->first_name);
        //   $message->subject('The Tiny Totos Program Account Verification');
        // });


        $userData = [
        'otp_requested'    => true,
            'otp_for_user' => $user->email,
            'user_id' => $user->id,
        ];
        //dd($user, $user->profile, $token, $notification);
        session($userData);

        return redirect('/user/verify');
    }

    /**
     * Verify Form
     */

    public function verifyForm()
    {
        if (request()->session()->get('otp_requested')==null) {
            abort(403, 'You are not allowed on this page');
        }
        $user_id = request()->session()->get('user_id');
        
        return view(
            'site.auth.verify',
            [
            'user_id' => $user_id
            ]
        );
    }


    /**
      * @param VerifyRequest $request
      */
    public function verify(VerifyRequest $request)
    {
        $data = $request->validated();
        // $user = $this->repository->createForValidation($data);
        $verify = TwillUsersAuth::where('user_id', $data['user'])->where('token', $data['token'])->first();

        if (!empty($verify)) {
            $user = User::where('id', $verify->user_id)->first();
        
            if (!$user) {
                return redirect('/');
            }

            $edxuser = EdxAuthUser::where('username', $user->username)->first();


            if ($edxuser === null || $edxuser->authRegistration->activation_key === null) {
                return redirect()->route('back')->with('login_error', 'There was a problem updating your account. Please try again later or report to support');
            }
            //Update edxuser names and active
            $edxuser->first_name = $user->first_name;
            $edxuser->last_name = $user->last_name;
            $edxuser->email = $user->email;
            $edxuser->is_active =  1;
            $edxuser->save();

            $user->published = 1;
            $user->is_activated = true;
            $user->registered_at = Carbon::now();
            $user->save();

            session()->forget('otp_requested');
            session()->forget('otp_for_user');

            $course_success = [
            'title' => 'Congratulations!',
            'content' => 'You have successfully activated your account. Please proceed and login.',
             ];
        
            Session::flash('course_success', $course_success);

            return redirect()->route('login');
        }
    }
  
    public function resendStore(Request $request)
    {
        $configSms = config()->get("settings.AfricasTalking.live");

        if (substr($request->input('number'), 0, 1) === '0') {
            $phone = str_replace('0', '', $request->input('number'));
            $user = User::where('phone', $phone)->first();

        }
     
        $user = User::where('phone', $request->input('number'))->first();
        // $user = User::all();

        // dd($user);
        if ($user) {
            if ($user->published != 1) {
                $verify = TwillUsersAuth::where('user_id', $user->id)->first();
                $validation = $this->validationToken($user, $length = 6);

              
                $phone_number = $user->phone_locale.''.$user->phone;

                 $username   = $configSms['AFRICASTALKING_USERNAME'];
                 $apiKey     = $configSms['AFRICASTALKING_APIKEY'];
                 $from = $configSms['AFRICASTALKING_FROM'];

                $AT       = new AfricasTalking($username, $apiKey);
                // Get one of the services
                $sms      = $AT->sms();
                $text = 'Hello , Use this code to verify your account on Tiny Totos platform '. $validation->token.' ';


                $result   = $sms->send(
                    [
                      'to'      => $phone_number,
                      'message' => $text,
                      'from' => $from,
                    ]
                );
        
               
                $userData = [
                'otp_requested'    => true,
                'otp_for_user' => $user->email,
                'user_id' => $user->id,
              ];
                //dd($user, $user->profile, $token, $notification);
                session($userData);

                return redirect('/user/verify');
            } else {
                return Redirect::route('login')->with('login_success', 'The account is activated please proceed and login');
            }
        } else {
            return Redirect::back()->with('login_error', 'The phone number you entered did not match our records. Please confirm you have registered');
        }
       
       
    }

    

    public function profile(Request $request)
    {
        //Get user
        $user = Auth::user();
  
        //dd($user);
        if ($request->isMethod('post')) {
  
        //Validate
            $request->validate(
                [
                'first_name' => ['required', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'phone' => ['required', 'unique:twill_users,phone,' . $user->id]
                ]
            );

     
  
            //Get updates
            $updates = $request->all();

  
            //Save changes
            $user = User::find($user->id);
            if ($user->update($updates)) {
                return Redirect::back()->with('register_success', 'Your profile has been successfully updated.');
            } else {
                //Show session message
  
                return Redirect::back()->with('register_error', 'There was a problem updating your profile.Please refresh the page and try again.');
            }
        }

        $roles = Role::all();
  
        if (strpos(Auth::user()->email, 'noemail')) {
            $user->email = '';
        }
  
        return view('site.pages.user.profile', ['user' => $user,'roles'=>$roles]);
    }


    public function picture(Request $request)
    {
        if (!Auth::check()) {
            return response('Unauthorized', 403);
        }

        $user = Auth::user();

        // //Save to storage
        // $ppic = $request->file('ppic');
        // $extension = $ppic->getClientOriginalExtension();
        // $filename = $ppic->getFilename() . '.' . $extension;

    
        // Storage::disk('public')->put($filename,  File::get($ppic));

        // //Delete old file
        // Storage::disk('public')->delete($user->profile_pic);

        //Save to user
        $user->profile_pic = $this->Fileupload($request, 'ppic', 'uploads');
        if ($user->save()) {
            return Redirect::back()->with('register_success', 'Your profile photo has been successfully updated.');
        } else {
            return Redirect::back()->with('register_error', 'Profile photo updating failed. Please refresh the page and try again.');
        }
    }

    

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * @return \A17\Twill\Repositories\ModuleRepository
     */
    protected function getRepository()
    {
        return App::make("App\\Repositories\\UserRepository");
    }

    protected function validationToken($user, $length)
    {
        $validation = new TwillUsersAuth();
        $validation->user_id  = $user->id;
        $validation->token = $this->generateRandomString($length);
        $validation->save();


        return $validation;
    }


    public function Fileupload($request, $file = 'file', $folder = null)
    {
        $upload = null;
        //create folder if none
        if (!File::exists($folder)) {
            File::makeDirectory($folder);
        }
        //chec fo file
        if (!is_null($request->file($file))) {
            $extension = $request->file($file)->getClientOriginalExtension();
            $fileName = rand(11111, 99999).uniqid().'.' . $extension;
            $upload = $request->file($file)->move($folder, $fileName);
            if ($upload) {
                $upload = $fileName;
            } else {
                $upload = null;
            }
        }
        return $upload;
    }
    private function edxRegister($user)
    {

        $configLms = config()->get("settings.lms.live");

        //Validate user
        if ($user === null) {
            return;
        }
        //Package data to be sent
        if (filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
            $email = $user->email;
        } else {
            $email = $user->email.'@local.com';
        }
        $data = [
          'email' => $email,
          'name' => $user->first_name . ' ' . $user->last_name,
          'username' => $user->username,
          'honor_code' => 'true',
          'password' => request()->get('password'),
          'country'=> 'KE',
          'terms_of_service'=> 'true',
        ];
    
        $headers = array(
          'Content-Type'=>'application/x-www-form-urlencoded',
          'cache-control' => 'no-cache',
          'Referer'=>env('APP_URL').'/register',
        );
    
        $client = new \GuzzleHttp\Client();
    
        try {
            $response = $client->request(
                'POST',
                $configLms['LMS_REGISTRATION_URL'],
                [
                'form_params' => $data,
                'headers'=>$headers,
                ]
            );
    
            return true;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user
            $user->delete();
    
            //Delete password resets
            //PasswordReset::where('email', '=', $user->email)->delete();
            $errors = [];
            foreach ($response as $key => $error) {
                //Return error
                $errors[] = $error;
            }
            //echo "CATCH 1";
            //dd($errors);
            return $errors[0];
        } catch (\Exception $e) {
    
          //Error, delete user
            $user->delete();
            //Delete password resets
            //PasswordReset::where('email', '=', $user->email)->delete();
            //echo "CATCH 2";
            //echo $e->getMessage();
            //die;
            return $e->getMessage();
        }
    }

    protected function generateRandomString($length)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[ rand(0, $charactersLength - 1) ];
        }

        return $randomString;
    }
}
