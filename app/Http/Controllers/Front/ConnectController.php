<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\Front\CreateActivityRequest;
use App\Models\MyCommunity;
use App\Models\UserActivity;
use App\Models\UserActivityReply;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\GroupUser;
use DB;
use Monarobase\CountryList\CountryListFacade as CountryList;
use PragmaRX\Countries\Package\Countries;
use PragmaRX\Countries\Package\Services\Config;

class ConnectController extends Controller
{

    // protected $pageKey;

    // public function __construct()
    // {
	// $this->homeKey = PageRepository::PAGE_HOMEPAGE;
    // $this->aboutKey = PageRepository::PAGE_ABOUT;
    // $this->sessionKey = PageRepository::PAGE_SESSION;
    // $this->resourceKey = PageRepository::PAGE_RESOURCE;
    // $this->eventKey = PageRepository::PAGE_EVENT;
    // $this->blogKey = PageRepository::PAGE_BLOG;

    // $this->repository = $this->getRepository();

    // parent::__construct();
	// }

    public function activity()
    {
        $user = Auth::user();

        $following = MyCommunity::following();

        $userIds = $following->pluck('follow_id')->merge(Auth::id())->toArray();

        $activities = UserActivity::with(['user', 'replies.replyReplies', 'replies.replyReactions', 'reactions'])
                    ->whereIn('user_id', $userIds)->latest()->get();

        return view('site.pages.connect.activity', ['activities' => $activities, 'user' => $user]);
    }

    public function createAcivity(CreateActivityRequest $request)
    {
        $images = $request->file('images');
        $gifs = $request->file('gifs');
        $files = $request->file('files');

        $imagesToSave = UserActivity::saveFiles($images);
        $gifsToSave = UserActivity::saveFiles($gifs);
        $filesToSave = UserActivity::saveFiles($files);

        $activity = new UserActivity([
            'user_id' => Auth::id(),
            'body' => $request->input('body'),
            'images' => json_encode($imagesToSave),
            'gifs' => json_encode($gifsToSave),
            'attachments' => json_encode($filesToSave)
        ]);

        $activity->save();

        return redirect()->back();
    }



    public function profile($id = null){

        if($id){
            $user = User::where('id', $id)->first();
        }else{
            $user = User::where('id', Auth::id())->first();
        }

        $country = Countries::where('postal', $user->phone_locale)->first();
        $flag = substr($country->flag['svg_path'], strpos($country->flag['svg_path'], "data/") + 5);

       
        $activities = UserActivity::with(['user', 'replies.replyReplies', 'replies.replyReactions', 'reactions'])
                    ->where('user_id', $user->id)->latest()->get();

        $groups = GroupUser::where('twill_users_id', $user->id)->where('joined','=',1)->get();

        return view('site.pages.connect.profile', ['user' => $user, 'country'=>$country,'flag'=>$flag, 'activities'=>$activities, 'groups'=>$groups]);
    }

    public function deleteActivity(UserActivity $userActivity)
    {
        $userActivity->delete();

        return redirect()->back();
    }

    /**
     * Create Activity Reply
     *
     * @param model $userActivity
     * @param request $request
     */
    public function createAcivityReply(Request $request, UserActivity $userActivity)
    {
        $images = $request->file('images');
        $gifs = $request->file('gifs');

        $imagesToSave = UserActivity::saveFiles($images);
        $gifsToSave = UserActivity::saveFiles($gifs);

        $reply = new UserActivityReply([
            'user_activity_id' => $userActivity->id,
            'user_id' => Auth::id(),
            'comment' => $request->input('comment'),
            'images' => json_encode($imagesToSave),
            'gifs' => json_encode($gifsToSave)
        ]);

        $reply->save();

        return redirect()->back();
    }

    /**
     * Delete Reply
     *
     * @param model $userActivityReply
     */
    public function deleteReply(UserActivityReply $userActivityReply)
    {
        $userActivityReply->deleted_by = Auth::id();

        $userActivityReply->save();

        $userActivityReply->delete();

        return redirect()->back()->withSuccess('Reply deleted successfully');
    }
}
