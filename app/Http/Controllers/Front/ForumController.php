<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Forum;
use App\Models\ForumTopic;
use App\Models\ForumUser;
use App\Models\ForumReply;
use App\Models\ForumReaction;
use App\Models\ForumReplyReaction;
use App\Models\ForumReplyReplies;
use App\Http\Requests\Front\CreateActivityRequest;

use Session;
use Auth;

class ForumController extends Controller
{
    //

    public function index(Request $request){
        if($request->isMethod('post')){
            $forums = Forum::where('forum_topic',$request->input('theme'))->latest()->get();
        }else{
            $forums = Forum::latest()->get();
        }
        $types = Forum::getTypes();
        $topics = ForumTopic::all();



        return view('site.pages.forum',['topics'=>$topics, 'types'=> $types,'forums'=>$forums]);

    }

    public function storeTopic(Request $request){

        $topic = new ForumTopic();
        $topic->title = $request->input('title');
        $topic->save();

        $course_success = [
            'title' => 'Congratulations!',
            'content' => ' Topic successfully created.',
         ];


         Session::flash('course_success', $course_success);

         return redirect()->back();


    }

    public function store(Request $request){

        $forum = new Forum();
        $forum->title = $request->input('title');
        $forum->description = $request->input('description');
        $forum->forum_type = $request->input('type');
        $forum->forum_topic = $request->input('topic');
        $forum->created_by = Auth::user()->id;

        $forum->save();

        $user = new ForumUser();
        $user->forum_id = $forum->id;
        $user->user_id = $forum->created_by;
        $user->joined= 1;

        $user->save();


        $course_success = [
            'title' => 'Congratulations!',
            'content' => ' Forum successfully created.',
         ];

         Session::flash('course_success', $course_success);

         return redirect()->back();

    }

    public function follow($id){

         $forum = new ForumUser();
         $forum->forum_id = $id;
         $forum->user_id = Auth::user()->id;
         $forum->joined= 1;

         $forum->save();


         return redirect()->route('forum.details',['id'=>$id]);

    }


    public function getForums($id){

        $forums = Forum::with('topic','user')->where('forum_topic',$id)->latest()->get();

        return['forums'=>$forums];


    }

    public function getStatus($group, $user){

        $status = ForumUser::where('forum_id',$group)->where('user_id',$user)->first();

        if(empty($status)){
            return '0';
        }else{
            return $status->joined;
        }


    }


    public function details($id){

        $activities = Forum::with(['user', 'replies.replyReplies', 'replies.replyReactions', 'reactions'])
        ->where('id', $id)->latest()->get();

        $forum = Forum::where('id', $id)->first();

        $topics = ForumTopic::all();

         return view('site.pages.forum_detail', ['activities' => $activities, 'topics'=>$topics, 'forum'=>$forum]);

    }

        /**
     * Create Activity Reply
     *
     * @param model $userActivity
     * @param request $request
     */
    public function createAcivityReply(Request $request, Forum $userActivity)
    {


        $reply = new ForumReply([
            'forum_id' => $userActivity->id,
            'user_id' => Auth::id(),
            'comment' => $request->input('description'),

        ]);

        $reply->save();

        return redirect()->back();
    }

    /**
     * Delete Reply
     *
     * @param model $userActivityReply
     */
    public function deleteReply(ForumReply $userActivityReply)
    {
        $userActivityReply->deleted_by = Auth::id();

        $userActivityReply->save();

        $userActivityReply->delete();

        return redirect()->back()->withSuccess('Reply deleted successfully');
    }


    public function reaction(Request $request, $userActivityId)
    {
        if (!empty($request->data == 'like')) {
            $reactions = ForumReaction::firstOrCreate([
                'user_id' => Auth::id(),
                'forum_id' => $userActivityId
            ]);
            if ($reactions->save()) {
                $reactions->update([
                    'likes' => ForumReaction::REACTION,
                    'dislikes' => 0
                ]);
            }

        } elseif (!empty($request->data == 'dislike')) {
            $reactions = ForumReaction::firstOrCreate([
                'user_id' => Auth::id(),
                'forum_id' => $userActivityId
            ]);
            if ($reactions->save()) {
                $reactions->update([
                    'likes' => 0,
                    'dislikes' => ForumReaction::REACTION
                ]);
            }
        }

        return [
            'likes' => ForumReaction::where('forum_id', $userActivityId)->get()->sum('likes'),
            'dislikes' => ForumReaction::where('forum_id', $userActivityId)->get()->sum('dislikes')
        ];
    }

    public function replyOnActionReply(Request $request, Forum $userActivity, ForumReply $userActivityReply)
    {
        $images = $request->file('images');
        $gifs = $request->file('gifs');

        $imagesToSave = $this->saveFiles($images);
        $gifsToSave = $this->saveFiles($gifs);

        $reply = new ForumReplyReplies([
            'forum_id' => $userActivity->id,
            'forum_reply_id' => $userActivityReply->id,
            'user_id' => Auth::id(),
            'comment' => $request->input('description-reply'),
            'images' => json_encode($imagesToSave),
            'gifs' => json_encode($gifsToSave)
        ]);

        $reply->save();

        return redirect()->back();
    }

    public function reactOnReply(Request $request, $userActivityId, $userActivityReplyId)
    {
        if (!empty($request->data == 'like')) {
            $reactions = ForumReplyReaction::firstOrCreate([
                'user_id' => Auth::id(),
                'forum_id' => $userActivityId,
                'forum_reply_id' => $userActivityReplyId
            ]);
            if ($reactions->save()) {
                $reactions->update([
                    'likes' => ForumReaction::REACTION,
                    'dislikes' => 0
                ]);
            }
        } elseif (!empty($request->data == 'dislike')) {
            $reactions = ForumReplyReaction::firstOrCreate([
                'user_id' => Auth::id(),
                'forum_id' => $userActivityId,
                'forum_reply_id' => $userActivityReplyId
            ]);
            if ($reactions->save()) {
                $reactions->update([
                    'likes' => 0,
                    'dislikes' => ForumReaction::REACTION
                ]);
            }
        }

        return [
            'likes' => ForumReplyReaction::where('forum_id', $userActivityId)
                    ->where('forum_reply_id', $userActivityReplyId)->get()->sum('likes'),
            'dislikes' => ForumReplyReaction::where('forum_id', $userActivityId)
                    ->where('forum_reply_id', $userActivityReplyId)->get()->sum('dislikes')
        ];
    }

    public function saveFiles($files)
    {
        return collect($files)->map(function ($file) {
            $fileName = uniqid().$file->getClientOriginalName();
            $fileName = strtolower(str_replace(' ', '_', $fileName));
            $file->move('Groups/activity', $fileName);
            return $fileName;
        });
    }
}
