<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\UserLicense;
use App\Models\CompanyLicense;
use App\Edx\EdxAuthUser;
use App\Edx\StudentCourseEnrollment;
use App\Models\User;
use Illuminate\Http\Request;
use Toastr;
use Auth;
//use ngunyimacharia\openedx\Facades\openedx;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Ixudra\Curl\Facades\Curl;
use Cookie;
use PDF;
use Redirect;
use Session;
use Artisan;
use App\Models\GroupPurchase;
use App\Models\Mpesa;

class CourseController extends Controller
{
    //


    public function detail(Request $request, $slug)
    {

        if (env('APP_DEBUG') === 'false' || env('APP_ENV') === 'production') {

            Artisan::call('view:clear');
            Artisan::call('optimize:clear');
        }


        //
        $course = Course::where('id', '=', $slug)->first();

        if ($course) {
            if ($course->start > Carbon::today()->toDateString()) {
                $start = 1;
                $date = date('l jS \of F Y', strtotime($course->start));
                $poptxt = 'This course will be available from ' . $date . '';
            } else {
                $start = 0;
                $poptxt = '';
            }
            //Process effort
            $time = explode(":", $course->effort);
            if (sizeof($time) == 3) {
                $duration = CarbonInterval::hours($time[1]);
                $duration = $duration->minutes($time[2]);
                $course['effort'] = $duration;
            }


             $enrolled = Auth::check() ? $this->checkEnrollmentStatus($course->course_id) : false;

            //$enrolled = false;

            // //  $courses = Course::where('status', 1)->inRandomOrder()->take(3)->get();

            if (Auth::check() && UserLicense::where(['course_id' => $course->course_id, 'user_id' => Auth::user()->id])->first()) {
                $license = true;
            } else {
                $license = false;
            }
            $licensed = Auth::check() ?  $license : false;
            //['course' => $course, 'courses' => $courses, 'enrolled' => $enrolled, 'start' => $start, 'text' => $poptxt]
            return view('site.pages.courses.detail', ['course' => $course,  'start' => $start, 'poptxt' => $poptxt, 'enrolled' => $enrolled, 'licensed' => $licensed]);
        } else {

            return redirect()->back();
        }
    }


    public function enroll($id)
    {
        $configLms = config()->get("settings.lms.live");

        $course = Course::where(['course_id' => $id])->first();

        $url = $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware';
        $license = UserLicense::where(['course_id' => $course->course_id, 'user_id' => Auth::user()->id])->first();

      
        Logger($license);
        if (!empty($license)) {
            if (isset($license->enrolled_at)) {
                return redirect($url);
            } else {
                $enroll = $this->edxEnroll($id);
                if ($enroll) {
                    $license->update(['enrolled_at' => Carbon::today()->toDateTimeString()]);

                    return Redirect::back();
                }
            }
        } else {
            if ($this->checkEnrollmentStatus($id) == true) {
                $licenses = new UserLicense();
                $licenses->course_id = $course->course_id;
                $licenses->user_id = Auth::user()->id;
                $licenses->enrolled_at = Carbon::today()->toDateTimeString();
                $licenses->expired_at = Carbon::now()->endOfYear()->toDateTimeString();
                $licenses->company_license_id = 1;
                $licenses->completion_id = $course->id;
                $licenses->save();

                $course_success = [
                    'title' => 'Congratulations!',
                    'content' => 'You have successfully enrolled for the course. Click on Take Course to start.',
                ];

                Session::flash('course_success', $course_success);
                return Redirect::back();
            }
            $enroll = $this->edxEnroll($id);

            if ($enroll) {

                $licenses = new UserLicense();
                $licenses->course_id = $course->course_id;
                $licenses->user_id = Auth::user()->id;
                $licenses->enrolled_at = Carbon::today()->toDateTimeString();
                $licenses->expired_at = Carbon::now()->endOfYear()->toDateTimeString();
                $licenses->company_license_id = 1;
                $licenses->completion_id = $course->id;
                $licenses->save();

                $course_success = [
                    'title' => 'Congratulations!',
                    'content' => 'You have successfully enrolled for the course. Click on Take Course to start.',
                ];

                Session::flash('course_success', $course_success);
                return Redirect::back();
            } else {

                $course_errors = [
                    'title' => 'Sorry, course enrollment issues',
                    'content' => 'looks like something went wrong with your course enrollment confirmation.',
                ];

                Session::flash('course_errors', $course_errors);
                return Redirect::back();
            }
        }
    }


    public function create()
    {
        // Sync
        $edx_courses = $this->edxgetCourses();
        foreach ($edx_courses as $key => $edx_course) {


            //Process data
            if (Course::where('id', $edx_course['id'])->count()) {
                //Update
                $slug = str_replace(' ', '-', $edx_course['name']);
                $slug2 = str_replace('-–-', '-', $slug);

                $video = str_replace('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/', $edx_course['course_video_uri']);

                //Update
                $course = Course::where('id', $edx_course['id'])->first();
                $course->slug = $slug2;
                $course->course_video_uri = $video;
                $course->update($edx_course);
            } else {
                //Create
                $course = Course::create($edx_course);
            }
            // $course->setOverview();
            // $course->setEnabled();
        }


        Toastr::success('Courses successfully synced.');
        return redirect()->route('dashboard');
    }



    private function checkEnrollmentStatus($course_id)
    {
        $configLms = config()->get("settings.lms.live");
        //dd(isset($_COOKIE['edinstancexid']));
        if (!isset($_COOKIE['edinstancexid'])) {
            Auth::logout();
            return false;
        }
        $client = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Authorization' => 'Bearer ' . $_COOKIE['edinstancexid']
                ]
            ]
        );

        $request = $client->request('GET', $configLms['LMS_BASE'] . '/api/enrollment/v1/enrollment/' . Auth::user()->username . ',' . $course_id);

        $response = json_decode($request->getBody()->getContents());

        if ($response && $response->is_active == true) {
            $enrollmentStatus = true;
        } else {
            $enrollmentStatus = false;
        }
        //dd($enrollmentStatus);
        return $enrollmentStatus;
    }



    private function edxEnroll($course_id)
    {

    
        $configLms = config()->get("settings.lms.live");
        
        
        if ($this->checkEnrollmentStatus($course_id)) {

            $course_success = [
                'title' => 'Congratulations!',
                'content' => "You're already enrolled to this course.",
            ];

            Session::flash('course_success', $course_success);
            return back();
        }

        

        $courseInfoObject = new \stdClass();
        $courseInfoObject->course_id = $course_id;
        $enollAttributesObject = new \stdClass();
        $enollAttributesObject->namespace = 'honor';
        $enollAttributesObject->name = env('APP_NAME');
        $enollAttributesObject->value = env('APP_NAME');
        $enrollmentInfoObject = new \stdClass();
        $enrollmentInfoObject->user = Auth::user()->username;
        $enrollmentInfoObject->mode = 'honor';
        $enrollmentInfoObject->is_active = true;
        $enrollmentInfoObject->course_details = $courseInfoObject;
        $enrollmentInfoObject->enrollment_attributes = [$enollAttributesObject];
        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Authorization' => 'Bearer ' . $_COOKIE['edinstancexid']
                ]
            ]
        );
        try {
         
            $response = $enrollClient->request('POST', $configLms['LMS_BASE'] . '/api/enrollment/v1/enrollment', [
                \GuzzleHttp\RequestOptions::JSON => $enrollmentInfoObject
            ]);
             
            return true; //Toastr::success("You have successfully enrolled into this course");
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();

            return false;
        }
    }


    private function edxgetCourses()
    {
        $configLms = config()->get("settings.lms.live");
        $client = new \GuzzleHttp\Client();
        try {
            $response = $client->request('GET', $configLms['LMS_BASE'] .  '/api/courses/v1/courses/?page_size=1000');
            $courses =  json_decode($response->getBody()->getContents())->results;
            foreach ($courses as $key => $value) {
                $course = (array)$value;
                $courses[$key] = (array)$courses[$key];
                $course['overview'] = $this->getOverview($course);
                //Remove unwanted fields
                $course['course_video_uri'] = $course['media']->course_video->uri;
                $course['course_image_uri'] = $configLms['LMS_BASE'] . $course['media']->course_image->uri;
                unset($course['media']);
                unset($course['course_id']);
                //Format datetime
                $vowels = array("T", "Z");
                $course['start'] =  date('Y-m-d H:m:i', strtotime(str_replace($vowels, " ", $course['start'])));
                $course['end'] = date('Y-m-d H:m:i', strtotime(str_replace($vowels, " ", $course['end'])));
                $course['enrollment_start'] = date('Y-m-d H:m:i', strtotime($course['enrollment_start']));
                $course['enrollment_end'] = date('Y-m-d H:m:i', strtotime($course['enrollment_end']));
                //Format time
                $exploded_effort = explode(":", $course['effort']);
                switch (count($exploded_effort)) {
                    case '3':
                        $course['effort'] = Carbon::createFromTime($exploded_effort[0], $exploded_effort[1], $exploded_effort[2])->toTimeString();
                        break;
                    case '2':
                        $course['effort'] = Carbon::createFromTime(0, $exploded_effort[0], $exploded_effort[1])->toTimeString();
                        break;
                }
                $courses[$key] = $course;
            }
            return $courses;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = $responseJson->getBody()->getContents();
            //dd($response);
        }
    }


    private function getOverview($course)
    {
        $configLms = config()->get("settings.lms.live");
        $client = new \GuzzleHttp\Client();
        try {
            //Get course description
            $request = $client->request('GET', $configLms['LMS_BASE'] .  '/api/courses/v1/courses/' . $course['id']);
            $response = json_decode($request->getBody()->getContents());
            return $response->overview;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = $responseJson->getBody()->getContents();
            return Toastr::error("Error enrolling into course");
            return false;
        }
    }



    public function purchasedCode(Request $request)
    {
        session_start();

        if (empty($request->input('confirmation_code'))) {

            $site_errors = [
                'title' => 'Sorry, course enrollment issues',
                'content' => ' Please enter the code before submiting.',
            ];

            Session::flash('course_errors', $site_errors);
            return back();
        } else {


            $group = GroupPurchase::where('unique_code', $request->input('confirmation_code'))->first();

            if (empty($group)) {
                $site_errors = [
                    'title' => 'Invalid code',
                    'content' => 'The code submitted doesn\'t exsist.',
                ];
                Session::flash('course_errors', $site_errors);
                return back();
            } else {


                $configLms = config()->get("settings.lms.live");
                $id = $group->course_id;
                $course_id = Course::where('id', $_SESSION['course'])->pluck('course_id');


                $license = UserLicense::where(['course_id' => $course_id, 'user_id' => Auth::user()->id])->first();
                $url = $configLms['LMS_BASE'] . '/courses/' . $course_id[0] . '/courseware';

                if (!empty($license)) {
                    if (isset($license->enrolled_at)) {


                        return redirect($url);
                    } else {


                        $enroll = $this->edxEnroll($id);

                        if ($enroll) {
                            $license->update(['enrolled_at' => Carbon::today()->toDateTimeString()]);

                            return Redirect::back();
                        }
                    }
                } else {
                    if ($this->checkEnrollmentStatus($id) == true) {

                        $licenses = new UserLicense();
                        $licenses->course_id = $id;
                        $licenses->user_id = Auth::user()->id;
                        $licenses->enrolled_at = Carbon::today()->toDateTimeString();
                        $licenses->expired_at = Carbon::now()->endOfYear()->toDateTimeString();
                        $licenses->company_license_id = Auth::user()->company_id;
                        $licenses->save();

                        $site_success = [
                            'title' => 'Congratulations!',
                            'content' => 'You have successfully enrolled for the course. Click on Take Module course to start the training.',
                        ];

                        Session::flash('course_success', $site_success);

                        return back();
                    }
                    $enroll = $this->edxEnroll($id);
                    //dd($enroll);

                    if ($enroll) {

                        $licenses = new UserLicense();
                        $licenses->course_id = $id;
                        $licenses->user_id = Auth::user()->id;
                        $licenses->enrolled_at = Carbon::today()->toDateTimeString();
                        $licenses->expired_at = Carbon::now()->endOfYear()->toDateTimeString();
                        $licenses->company_license_id = Auth::user()->company_id;
                        $licenses->save();



                        $group->status = 1;
                        $group->date_used = Carbon::now();
                        $group->save();

                        return back();


                        $site_success = [
                            'title' => 'Congratulations!',
                            'content' => 'You have successfully enrolled for the course. Click on Take Module course to start the training.',
                        ];

                        Session::flash('course_success', $site_success);
                        return back();
                    } else {

                        $site_errors = [
                            'title' => 'Sorry, course enrollment issues',
                            'content' => ' looks like something went wrong with your course enrollment confirmation.',
                        ];

                        Session::flash('course_errors', $site_errors);
                        return back();
                    }
                }
            }
        }


        // dd($request);

    }
}
