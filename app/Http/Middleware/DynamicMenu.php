<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Config;

class DynamicMenu
{
    public function handle(Request $request, Closure $next)
    {
		$adminRoles = ['SUPERADMIN'];
		$countryAdminRoles = ['Facilitator'];
		

		if(auth()->guard('twill_users')->user() && (auth()->guard('twill_users')->user()->is_admin || in_array(auth()->guard('twill_users')->user()->roleValue,$adminRoles ))){
			config(['twill-navigation' => adminMenu()]);
		}

		if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$countryAdminRoles ))){
			config(['twill-navigation' => facilitatorAdminMenu()]);
		}
        return $next($request);
    }
}
