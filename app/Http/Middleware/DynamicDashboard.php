<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Config;

class DynamicDashboard
{
    public function handle(Request $request, Closure $next)
    {
		$adminRoles = ['Administrator'];
		$countryAdminRoles = ['Country-Administrator'];
		$providerRoles = ['Provider', 'Registered-User'];

		if(auth()->guard('twill_users')->user() && (auth()->guard('twill_users')->user()->is_superadmin || in_array(auth()->guard('twill_users')->user()->roleValue,$adminRoles ))){
			config(['twill.dashboard' => adminDashboard()]);
		}

		if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$countryAdminRoles ))){
			config(['twill.dashboard' => countryAdminDashboard()]);
		}

		if(auth()->guard('twill_users')->user() && in_array(auth()->guard('twill_users')->user()->roleValue,$providerRoles)){
			$user = auth()->guard('twill_users')->user();
			if (hasBdsProvidersPermissions()) {
				config(['twill.dashboard' => bdsProviderDashboard()]);
			}
			if (hasFinancingProvidersPermissions()) {
				config(['twill.dashboard' => fpProviderDashboard()]);
			}
			if (hasBdsProvidersPermissions() && hasFinancingProvidersPermissions()) {
				config(['twill.dashboard' => providerDashboard()]);
			}
		}

        return $next($request);
    }
}
