<?php

namespace App\Presenters\Front;

class SessionResourcePresenter extends BasePresenter
{
	public function filename()
    {
        $output = '<a target="_blank" href="' . asset('sessionResources/'. $this->entity->filename) . '">'.$this->entity->filename. '</a>';

        return $output;

	}

    public function created_at()
    {
        return $this->entity->created_at != null ? $this->entity->created_at->formatLocalized("%B %e, %Y %I:%M:%S %p") : "";
	}


}
