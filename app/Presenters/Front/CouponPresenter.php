<?php

namespace App\Presenters\Front;

use App\Models\Mpesa;


class CouponPresenter extends BasePresenter
{
    public function image()
    {

        return $this->imageMedia('resource_image');
    }


    public function imageResource()
    {
        return $this->imageMedia('cover', 'resource');
    }

    public function description()
    {
        return $this->entity->description;
    }

    public function url()
    {
        return "#";
    }


    public function created_at()
    {
        return $this->entity->created_at != null ? $this->entity->created_at->formatLocalized("%B %e, %Y %I:%M:%S %p") : "";
    }


    public function total_use()
    {
        $subscriptions = Mpesa::where('coupon', '=', $this->entity->code)->get();

        return (!empty($subscriptions)) ? count($subscriptions) : 0;
    }
}
