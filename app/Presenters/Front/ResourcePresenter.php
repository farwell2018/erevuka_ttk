<?php

namespace App\Presenters\Front;


class ResourcePresenter extends BasePresenter
{
	public function image()
    {
        
        return $this->imageMedia('resource_image');
	}


    public function imageResource()
    {
        return $this->imageMedia('cover', 'resource');
	}

	public function description()
    {
        return $this->entity->description;
	}

	public function url()
    {
        return "#";
    }


    public function created_at()
    {
        return $this->entity->created_at != null ? $this->entity->created_at->formatLocalized("%B %e, %Y %I:%M:%S %p") : "";
	}


    public function created_at()
    {
        return $this->entity->created_at != null ? $this->entity->created_at->formatLocalized("%B %e, %Y %I:%M:%S %p") : "";
	}
}
